\chapter{Energiemanagement}\label{energiemanagement}

Im Allgemeinen liegt der Fokus eines \gls{EnMS} auf folgenden Zielen \cite[S.5]{tuev1}:

\begin{itemize}
\item Energiekosten reduzieren
\item Produktion energietechnisch optimieren
\item Emissionen mindern.
\end{itemize}

Das Konzept des Energiemanagements baut unter anderem auf dem Ansatz des Energiecontrolling auf. Bei diesem Ansatz werden Energiedaten von bestimmten Energieverbrauchern aufgenommen. Anhand dieser Daten wird anschließend versucht den Produktionsprozess zu optimieren. Ein umfassendes \gls{EnMS} hat hierauf aufbauend noch eine organisatorische Wirkung. Verantwortliche Mitarbeiter können bei sich verändernden Bedingungen (z.B. einem signifikant steigenden Verbrauch), einschreiten und entsprechend der Zielsetzung, welche in Dokumenten wie Energiepolitik, -zielen und -programmen definiert sind, handeln. Eine systematische Analyse und Handlungsgrundlage wird hierdurch gewährleistet.

Eine konkrete Ausgestaltung eines \gls{EnMS} bietet die \gls{DIN} EN \gls{ISO} 50001 ``Energiemanagementsysteme - Anforderungen mit Anleitung zur Anwendung'', welche im April 2012 die bisher bestehende \gls{DIN} EN 16001 abgelöst hat. Ziel der neuen Norm ist es, 

\begin{quotation}
“Organisationen in die Lage zu versetzen, Systeme und Prozesse aufzubauen, welche zur Verbesserung der energiebezogenen Leistung, einschließlich Energieeffizienz, Energieeinsatz und Energieverbrauch erforderlich sind.“ 

\cite[S. 5]{iso50001}
\end{quotation}

Umgesetzt werden soll dieses Ziel durch die Implementierung eines strukturierten und systematischen Energiemanagements. Dabei ist die Norm unabhängig von Größe und Art der Organisation anwendbar, wodurch einige Anforderungen als optional anzusehen sind. Im Folgenden wird der Aufbau der \gls{DIN} EN ISO 50001 erläutert und anschließend das Vorgehen und die Ergebnisse der Analyse der Norm vorgestellt.

\section{Aufbau der ISO 50001}\label{energiemanagement:aufbauIso50001}

Der Aufbau eines \gls{EnMS} nach \gls{DIN} EN \gls{ISO} 50001 orientiert sich an bereits bestehenden Managementsystemen wie \gls{DIN} EN \gls{ISO} 9001 (Qualitätsmanagement) und \gls{DIN} EN \gls{ISO} 14001 (Umweltmanagement). Somit stützt es sich ebenfalls auf den \gls{KVP}. Der \gls{KVP} oder auch \gls{PDCA}- Zyklus genannte Prozess soll dazu dienen  das \gls{EnMS} in das Tagesgeschäft der Organisationen einzubetten und eine fortwährende Verbesserung zum festen Bestandteil der Arbeitsabläufe zu machen \cite[S. 5]{iso50001}. 

\begin{figure}
\includegraphics[width=8cm]{grafiken/02pdcaZyklus.png}
\caption[Phasen im \protect\gls{PDCA}-Zyklus]{Phasen im \gls{PDCA}-Zyklus, Quelle: Eigene Darstellung} 
\label{02pdcaZyklus}
\end{figure}

Der Zyklus beginnt in der Plan-Phase (zu sehen oben rechts in Abbildung \ref{02pdcaZyklus}). Hier wird die Problemstellung definiert und im Rahmen einer IST-Analyse die notwendigen Daten ermittelt. Anhand dieser Informationen wird die Zielsetzung formuliert und Maßnahmen zur Lösung, Verbesserung und Optimierung festgestellt. In der anschließenden Do-Phase werden diese dann schrittweise umgesetzt. Die dabei entstehenden Ergebnisse und Erfahrungen werden aufgezeichnet. Mithilfe dieser Dokumentation kann in der folgenden Check-Phase eine Darstellung der Ergebnisse erfolgen und überprüft werden, ob die erprobten Maßnahmen zu einer Verbesserung führen. In der Act-Phase werden diese Erkenntnisse genutzt, um die Maßnahmen zu bewerten und Rückschlüsse für das weitere Vorgehen zu ziehen. Erfolgreiche Maßnahmen werden beispielsweise standardisiert und/oder weitere Folgeaktivitäten angestoßen. Daraus soll sich eine Verbesserung des Vorgehens ergeben. Da dies kontinuierlich erfolgt, schließt sich der Kreis nach der Act-Phase wieder, da deren Erkenntnisse dazu genutzt werden, die Problemstellung zu verändern oder eine neue zu definieren. Wird dieser Zyklus aus der Perspektive der \gls{ISO} 50001 betrachtet, finden sich die Abschnitte der Norm in den einzelnen Phasen des \gls{KVP} wieder. Diese werden im Folgenden n\"aher erl\"autert.

\subsection{Plan}\label{energiemanagement:aufbauIso50001:plan}

Durch das \gls{topmanagement} wird in der Plan-Phase zunächst die  Energiepolitik festgelegt. Sie zeigt die Einstellung des Top-Managements gegenüber dem Energiemanagement und macht deutlich, welche Relevanz das Energiemanagement innerhalb der Organisation hat. Damit dient die Energiepolitik als Leitlinie für die Mitarbeiter einer Organisation, um über das weitere Vorgehen des \gls{EnMS} informiert zu sein. Durch die Norm werden Pflichtbestandteile für die Energiepolitik vorgegeben, welche sicherstellen, dass sich das Top-Management zu allen notwendigen Bestandteilen eines \gls{EnMS} bekennt. 

Anschließend wird die Energieplanung durchgeführt. Hier wird über eine so genannte energetische Bewertung zunächst eine IST-Analyse der energetischen Situation innerhalb der Organisation vorgenommen. Dabei werden nicht nur aktuelle Energieverbräuche aufgenommen, sondern auch die Teile der Organisation identifiziert, welche wesentlich die energetische Leistung beeinflussen. Darauf aufbauend können Energieleistungskennzahlen (\gls{EnPI}) entwickelt werden, die zukünftig einen schnellen Überblick über die energetische Situation ermöglichen. Durch die Festlegung von strategischen und operativen Energiezielen werden die global formulierten Ansprüche der Energiepolitik weiter konkretisiert. Um diese Ziele umzusetzen, werden konkrete Maßnahmen und Methoden in Form von Aktionsplänen definiert. 

\subsection{Do}\label{energiemanagement:aufbauIso50001:do}

In der folgenden Do-Phase werden die beschlossenen Aktionspläne realisiert. Dabei hat die Umsetzung Auswirkungen auf allen Ebenen einer Organisation. So müssen die Mitarbeiter zur Umsetzung von Energieffizienzmaßnahmen geschult und sensibilisiert werden. Damit einher geht eine umfassende Kommunikation der Energieziele. Auch Prozesse und Abläufe müssen den Aktionsplänen entsprechend angepasst werden. Um die Auswirkungen der Aktionspläne bewerten zu können, ist es erforderlich, dass entsprechende Aufzeichnungen angefertigt und systematisch dokumentiert werden.

\subsection{Check}\label{energiemanagement:aufbauIso50001:check}

Die folgende Check-Phase des \gls{PDCA}-Zyklus beinhaltet die Kontrolle der relevanten Abläufe, Prozesse und Anlagen der Organisation. So werden die Bereiche mit wesentlichem Energieeinsatz überwacht und der aktuelle mit dem geplanten Energieverbrauch verglichen. Dies dient unter Anderem zur Kontrolle der Wirksamkeit der Aktionspläne. Weiterhin sollen bei dieser Überprüfung Abweichungen (Nichtkonformitäten\footnote{Der Begriff Nichtkonformität ist in der \gls{ISO} 50001 als die “Nichterfüllung einer Anforderung” definiert \cite[S. 10]{iso50001}. Diese kann zum Beispiel die Abweichung von einem gesetzten Verbrauchsziel, aber auch die Nichterfüllung einer gesetzlichen Anforderung bedeuten (legal compliance).}) deutlich werden. Für diese müssen Korrektur und oder Vorbeugungsmaßnahmen entwickelt werden. Des Weiteren werden im Rahmen der Kontrollmaßnahmen interne Audits durchgeführt, in denen festgestellt wird, ob sich das \gls{EnMS} weiterhin in Einklang mit der Norm und den strategischen und operativen Energiezielen befindet. In diesem Zusammenhang wird die wirksame und effiziente Verwirklichung der Energiemanagementprogramme, Prozesse und Systeme geprüft. Möglichkeiten für weitere Verbesserungen werden gesammelt. Gleichzeitig wird hier die Evaluierung des Verbesserungspotentials durch den Einsatz von Informationstechnologie genannt \cite[S. 30]{iso50001}.

\subsection{Act}\label{energiemanagement:aufbauIso50001:act}

In der Act-Phase wird ein Management-Review durchgeführt im Zuge dessen auf Grundlage der dokumentierten Ergebnisse alle durchgeführten Maßnahmen bewerten werden. Insbesondere wird überprüft, ob die definierten Ziele (Energiepolitik, Energieziele, Energieprogramme) mit dem \gls{EnMS} erreicht worden sind oder dies noch möglich ist. Darüber hinaus ist neben der reinen Ergebnisanalyse auch eine Prognose für die energiebezogene Leistung und Empfehlungen für Verbesserungen in der Zukunft abzugeben \cite[S. 31f]{tuev1}.

\begin{figure}
\includegraphics[width=15cm]{grafiken/03pdcaZyklusErlaeuterungen.png}
\caption[Phasen im \protect\gls{PDCA}-Zyklus mit Erl\"auterungen]{Phasen im \protect\gls{PDCA}-Zyklus mit Erl\"auterungen, Quelle: Eigene Darstellung in Anlehnung an \protect\gls{DIN} EN \gls{ISO} 50001, S.6}
\label{03pdcaZyklusErlaeuterungen}
\end{figure}

Abbildung \ref{03pdcaZyklusErlaeuterungen} f\"ugt dem in Abbildung \ref{02pdcaZyklus} gezeigten \gls{PDCA}-Zyklus die entsprechenden Abschnitte der \gls{ISO}-Norm hinzu. Durch das Hinuzf\"ugen kurzer Erl\"auterungen zu den einzelnen Abschnitten erm\"oglicht Abbildung \ref{03pdcaZyklusErlaeuterungen} einen schnellen \"Uberblick \"uber die Hauptbestandteile eines \gls{EnMS} nach \gls{ISO} 50001.

\section{Analyse der Anforderungen}\label{energiemanagement:anforderungen}
In diesem Kapitel wird sowohl die Vorgehensweise zur Analyse der Anforderungen, als auch die resultierenden Ergebnisse dargestellt. Auf Grund der Gr\"o{\ss}e befinden sich die Tabellen hierzu im Anhang Abschnitt \ref{isoNormTabellen}.

\subsection{Vorgehensweise}\label{energiemanagement:anforderungen:vorgehensweise}
Durch eine Analyse der entsprechenden Abschnitte der Norm sollen konkrete Anforderungen herausgearbeitet werden. Betrachtet man die \gls{ISO}-Norm eingehender, wird deutlich, dass diese bereits sehr stark strukturiert ist und sich auf einem hohe Grad der Aggregration befindet. Somit ergeben sich kaum bis keine Ansätze um die Norm weiter zusammenzufassen. Jeder Satz beinhaltet Anforderungen, die sich nur schwer umformulieren lassen. Folglich konzentriert sich die Analyse darauf, diese Anforderungen leichter zugänglich zu machen. Hierfür werden die Abschnitte der Norm in Tabellenform übertragen und die Anforderungen, wenn möglich, ansprechender umformuliert.\\

\subsection{Ergebnis}\label{energiemanagement:anforderungen:ergebnis}

Das Ergebnis der Analyse zeigen die Tabellen \ref{isoNorm} - \ref{isoNormEnde}. Jede Tabelle beinhaltet Anforderungen aus einem Abschnitt der Norm. Zusätzlich wurde die Tabelle “Initialisierung” hinzugefügt. In dieser werden allgemeine Anforderungen beschrieben, welche beim Einführen eines \gls{EnMS} zu beachten sind.

Die erste Spalte jeder Tabelle beinhaltet eine Indexnummer. In der zweiten Spalte wird das Kapitel der Norm benannt, aus dem die Anforderung abgeleitet wurde. Dies ermöglicht ein Nachverfolgen der Informationen zu ihrem Ursprung. Die Spalte “Anforderung” beinhaltet eine konkrete Anforderung der Norm an ein \gls{EnMS}. In der Spalte “Anmerkungen zur Anforderung” wird die Anforderung weiter konkretisiert. Die Spalte “Dokumente” zeigt die Namen der Dokumente, deren Erstellung sich aus der vorangestellten Anforderung ableiten oder von ihr betroffen sind. Somit wird ein schneller Überblick über die dokumentarischen Anforderungen einzelner Bereiche der Norm gegeben. 


\subsection{Abgrenzung zur DIN EN 16001}\label{energiemanagement:anforderungen:abgrenzung}

Die Unterschiede zur \gls{DIN} EN 16001 bestehen hauptsächlich in geringfügigen Veränderungen der Bereiche: Energiepolitik, Energieplanung,  Energetische Bewertung (Energieaspekte), Energetische Ausgangsbasis (Baseline), Energieleistungskennzahlen, Energieziele und Aktionspläne, Auslegung und Beschaffung \cite[]{tuev1}. Größter Unterschied ist die vorgegebene Überwachung der energetischen Leistung mittels \gls{EnPI} in der \gls{ISO} 50001. Weiterhin wurden die Punkte \glqq Verantwortung des TOP-Managements \grqq, \glqq Entwicklungs- und Einkaufprozesse\grqq sowie \glqq Verbesserungsprozess (Energieeffizienz) \grqq  höher priorisiert \cite[Folie 6]{regen}.

\section{Energiemanagement in der Praxis}\label{energiemanagement:praxis}

Um Erfahrungen aus der Praxis mit in die Konzeption einzubeziehen, werden in diesem Kapitel bereits praktizierte Methoden zur Einführung und Betrieb eines \gls{EnMS} vorgestellt. Dabei wird zunächst der netzwerkorientierte Ansatz des Modell Hohenlohe (Kapitel \ref{energiemanagement:praxis:energieEffizienzNetzwerke}) dargelegt. Anschließend werden die Inhalte des Praxisleitfadens der Zertifizierungsstelle GUTcert skizziert (Kapitel \ref{energiemanagement:praxis:gutCert}). Zum Abschluss des Kapitels wird die Fachbroschüre des \gls{TUV}-Rheinlands kurz vorgestellt (Kapitel \ref{energiemanagement:praxis:tuev}).

\subsection{Erfahrungen aus Energieeffizienz-Netzwerken}\label{energiemanagement:praxis:energieEffizienzNetzwerke}

Als Beispiel dafür, wie in der Praxis eine Beratung zum Energiemanagement durchgeführt werden kann, wird in diesem Abschnitt die Vorgehensweise “Lernendes-Energieeffizienz Netzwerk (\gls{LEEN})“\footnote{\href{http://leen-system.de/}{http://leen-system.de/} letzter Abruf 11.08.2012} skizziert. Unter Anwendung der vorgestellten Methodik sind 30 Pilotprojekte gestartet. Parallel zum Projektbetrieb wird gleichzeitig auch die Effektivität der Methodik untersucht. Die \gls{LEEN} Idee besteht darin, dass durch den Austausch von adaptierbarem Wissen und der Erfahrungen anderer Teilnehmer im Netzwerk eine erhebliche Reduzierung der Such- und Entscheidungskosten erreicht werden kann. Darüber hinaus können auch externe Dienstleitungen, wie beispielsweise Expertenvorträge, gemeinsam genutzt werden. Ein Netzwerk besteht aus Vertretern von 10 bis 15 Unternehmen, die einem hohen Anteil von Energiekosten aufweisen. 

Als Ergebnis dieses Ansatzes soll ein kontinuierlicher Verbesserungs- und Lernprozess sowohl für das Netzwerk, als auch für die einzelnen Unternehmen, resultieren. Innerhalb des Betriebes entsteht somit ein einfaches \gls{EnMS}, welches auch wesentliche Bausteine eines \gls{EnMS} nach \gls{DIN} \gls{ISO} 50001 enthält. Somit bietet sich auch eine spätere Zertifizierung nach \gls{DIN} EN \gls{ISO} 50001 an.


\begin{figure}
\includegraphics[width=15cm]{grafiken/12leenAblauf.png}
\caption[Ablauf eines Energieeffizienz-Netzwerks nach \protect\gls{LEEN}]{Ablauf eines Energieeffizienz-Netzwerks nach \protect\gls{LEEN}, Quelle: \citealt[S. 4]{leen}}
\label{12leenAblauf}
\end{figure}

Abbildung \ref{12leenAblauf} zeigt die verschiedenen Phasen des Vorgehens nach \gls{LEEN}. Phase 0 ist dabei dem Netzwerkbetrieb vorgelagert. Eine genauere Betrachtung ist in diesem Kontext daher nicht zielführend. Das Augenmerk richtet sich vielmehr auf die Phase 1, in der eine mehrtägige und somit kostenintensive Betriebsbegehung durch einen externen Energieberater erfolgt. Dieser Analyse geht eine initiale Analyse der betrieblichen Gegebenheiten voraus. Die dabei auszufüllenden Auditunterlagen betrachten alle energierelevanten Bereiche des Unternehmens. 

\begin{figure}
\includegraphics[width=15cm]{grafiken/leenScreenshot.png}
\caption[Initiale Datenaufnahme im \protect\gls{LEEN}-Netzwerk]{Initiale Datenaufnahme im \protect\gls{LEEN}-Netzwerk (Screenshot), Quelle: Eigene Darstellung}
\label{leenScreenshot}
\end{figure}

Die Datenaufnahme in Phase 1 wird bisher mit Hilfe einer Tabellenkalkulation durchgeführt (siehe Abbildung \ref{leenScreenshot}). Es hat sich gezeigt, dass dies für die ausführenden Unternehmen, insbesondere auf Grund der Komplexität und Datenfülle, eine Hürde darstellt. Eine bedienerfreundlichere Software würde hier den Ablauf der Initialberatung beschleunigen. Analog zur \gls{ISO} 50001 Norm werden in Phase 1 zusätzlich Effizienz- und Reduktionsziele definiert. 

Die sich anschlie{\ss}ende zwei bis vier Jahre dauernde Phase 2 umfasst ein Monitoring der Ergebnisse aus dem Initialberatungsbericht, welcher in Phase 1 erstellt worden ist. Hierzu finden zwei unterschiedliche Verfahren Anwendung. Einerseits das Bottom-Up-Monitoring, welches ausgehend von den wirksamen Maßnahmen einen Überblick über die aktiven Bemühungen zur Effizienzverbesserung zeigt. Andererseits ein Top-Down-Monitoring, welches durch Kennwertzahlenbildung die energiebezogenen Veränderungen im Betrieb aufzeigt. Für beide Verfahren oder eine Mischform dieser, wird die Nutzung eines Softwaretools empfohlen. Zudem wird in Phase 2 die Netzwerkphase (drei bis vier Treffen im Jahr) gestartet. Im Anschluss wird durch das Management entschieden, inwieweit das Energiemanagement weitergeführt wird \cite[S.3ff]{leen}.

Abschließend lässt sich festhalten, dass die Ausgestaltung der einzelnen \gls{EnMS} innerhalb eines Netzwerks, aber auch im Gegensatz zu anderen Netzwerken, durchaus unterschiedlich sein kann. Der \gls{LEEN}-Ansatz bietet vielmehr eine Struktur, wie der Austausch zu anderen Unternehmen und damit dem im Netzwerk befindlichen Wissen gestaltet sein kann. Wichtige Schnittstellen wie der Initialberatungsbogen und -bericht sind aber standardisiert. Dies erm\"oglicht eine einfachere Vergleichbarkeit zwischen den Unternehmen im Netzwerk. Die Erfahrungen des \gls{LEEN}-Ansatzes in der Praxis zeigen hohe überdurchschnittliche Einsparungen von Energiekosten und CO\textsubscript{2}-Emissionen \cite[S. 74ff]{hohenlohe1}. Zusätzlich findet eine klare Trennung zwischen Akquisition, Initialisierung und Durchführung/Monitoring des \gls{EnMS} statt. Diese lässt sich auch in den in der Folge beschriebenen Leitfäden wiederfinden.

\subsection{Leitfaden zur Umsetzung der Norm der GUTcert}\label{energiemanagement:praxis:gutCert}

Der Leitfaden “In 18 Schritten über 3 Stufen zum effizienten Energiemanagement nach 50001” der Zertifizierungsorganisation “GUTcert” bietet Unternehmen einen praktischen Ansatz ein \gls{EnMS} Schritt für Schritt ein- und durchzuführen. GUTcert\footnote{\href{http://www.gut-cert.de/}{http://www.gut-cert.de/} letzter Abruf 01.08.2012} ist eine auf Zertifizierungen im Umwelt-, als auch im Arbeits- und Gesundheitsschutzbereich spezialisierte Organisation mit Sitz in Berlin. Sie ist ein Tochterunternehmen des \gls{DIN}-ähnlichen, französischen Instituts für Normung: \gls{afnor}. In der vorliegenden Publikation sind die Aggregation der zugrundeliegenden Norm, als auch die praktischen Tipps zur Umsetzung dieser interessant. Der Leitfaden ist insbesondere durch die Erfahrung der Autoren im Bereich der Zertifizierung realitätsnäher als die Norm an sich. Er richtet sich auch an Unternehmen, welche ein \gls{EnMS} ohne anschließende Zertifizierung nach \gls{ISO} 50001 einführen wollen. Da die Aufteilung der Arbeitspakete im Leitfaden sich von der \gls{ISO}-Norm unterscheidet, werden diese in der Folge kurz dargestellt, anschließend findet eine Bewertung der Vorgehensweise statt.

\subsubsection{Stufen des Leitfadens}\label{energiemanagement:praxis:gutCert:Stufen}

Im Leitfaden sind die Arbeitsschritte zur Umsetzung des \gls{EnMS} in Phasen eingeteilt:

\begin{quotation}
``Stufe I: Analysieren Sie Ihre Energiesituation und erkennen Sie schon dabei nebenher und automatisch viele Einsparpotentiale.'' 

``Die Erhebung des Ist-Zustandes mit Ableitung erster Einsparmaßnahmen (Schritte 1-6)''

\cite[S. 3, S. 9]{gutcert}
\end{quotation}



Stufe 1 des Leitfadens ist als Initialphase zu verstehen. Sie beginnt mit einer Willenserklärung des Managements. Die Hauptschritte umfassen einerseits ein Erfassen der Eckdaten des Unternehmens und eine grobe Erstanalyse (Erfassung der energetischen, rechtlichen und organisatorischen Situation, Bewertung von Energieeinflussfaktoren). Schritt 4 stellt hier mit der Energieanalyse arbeitsintensivste Aufgabe dar. Andererseits wird auch über das weitere Vorgehen beraten: Soll ein \gls{EnMS} eingeführt werden? Soll nach \gls{ISO} 50001 zertifiziert werden? Es kann auch entschieden werden, dass ein \gls{EnMS} nicht weiter fortgeführt wird.

\begin{quotation}
``Stufe II: Passen Sie das Vorgehen dabei an Ihre eigenen Planungsprozesse an und – wenn möglich – integrieren Sie es in vorhandene Prozesse und Systeme.'' 

``Die Einführung ergänzender oder neuer Regelungen zur Steuerung eines Systems (Schritte 7-14)''

\cite[S. 3]{gutcert}
\end{quotation}


Die Stufe 2 des GUTcert Leifadens ist das Kernstück des \gls{EnMS}. Die Schritte aus Stufe 1 werden verfestigt und strukturelle Änderungen werden vorgenommen. Durch die formellere Systematik werden hier die ersten Ansätze zu \gls{ISO}-konformen Dokumenten (Energiepolitik, Kommunikationsregeln, Dokumentation des \gls{EnMS}) ausgearbeitet oder Aktivitäten in diese Richtung ermittelt und teilweise auch durchgeführt (Aufbau einer Energieorganisation, Schulungen der Mitarbeiter, systematische Planung und Durchführung von Maßnahmen). Nach GUTcert wird am Ende dieser Stufe wieder das weitere Vorgehen hinterfragt. Folglich wird die Entscheidung getroffen, ob die Durchführung von Stufe 1 und 2 einmalig war oder ob die Aktivitäten in ein kontinuierliches (und somit möglicherweise auch nach \gls{ISO} zertifizierbares) \gls{EnMS} überführt werden sollen.

\begin{quotation}
``Stufe III: Steigen Sie in einen systematischen und kontinuierlichen Verbesserungsprozess ein!'' 

``Der umfassende Betrieb eines am \gls{PDCA}-Zyklus ausgerichteten Managementsystems zur kontinuierlichen Verbesserung (Schritte 15-18)''

\cite[S. 3, S. 9]{gutcert}
\end{quotation}


Durch Stufe 3 wird der GUTcert-Ansatz zu einem \gls{ISO}-konformen \gls{EnMS}. Hier beginnt die Implementierung des \gls{PDCA}-Zyklus. Noch nicht durchgeführte strukturelle Änderungen etwa zur Organisation oder Regeln der Kommunikation werden zu diesem Zeitpunkt durchgeführt. Des Weiteren werden die Energiebilanz aktualisiert und interne Audits durchgeführt. Letztere untersucht das \gls{EnMS} an sich, die Ermittlung von Einsparmöglichkeiten sowie die Regelkonformität (Compliance). 

\subsubsection{Zusammenfassung}\label{energiemanagement:praxis:gutCert:zusammenfassung}

Abbildung \ref{13gutCert} zeigt den Aufbau der \gls{ISO}-Norm und skizziert, welche Bereiche der Norm mit welcher Phase der GUTcert Struktur deckungsgleich sind. Insbesondere durch die schrittweise Abarbeitung der \gls{ISO}-Anforderungen ist in einigen Bereichen eine scharfe Abgrenzung nicht immer durchzuführen. Da in Phase 1 eher vorbereitende Maßnahmen durchgeführt werden, ist diese Stufe als eine Art Initialisierung anzusehen. Eine genaue Zuordnung in der \gls{ISO}-Norm gibt es nicht. Folglich ist Phase 1 leicht neben den Stufen des \gls{PDCA}-Zyklus angeordnet. 

\begin{figure}
\includegraphics[width=15cm]{grafiken/13gutCert.png}
\caption[Aufbau der Phasen im GUTcert Leitfaden]{Aufbau der Phasen im GUTcert Leitfaden, Quelle: Eigene Darstellung aufbauend auf Abbildung \ref{03pdcaZyklusErlaeuterungen} }
\label{13gutCert}
\end{figure}

Der Praxisleitfaden von GUTcert ist darauf ausgelegt, Organisationen einen leichten Einstieg in das Energiemanagement zu ermöglichen. Allerdings wird nur die Struktur vereinfacht. Expertenwissen etwa zur Ermittlung von Einsparmöglichkeiten und Organisationsumstrukturierungen ist weiterhin unerlässlich. Einem Energieteam / Energiemanager werden aber Werkzeuge an die Hand gegeben, die sich so nicht in der \gls{ISO}-Norm wiederfinden. Beispiele hierzu sind unter anderem die Vorgehensweise bei Amortisationsrechnungen, Vorgehen bei der Dokumentation zu einem Rechtskataster, eines Korrektur- und Verbesserungsmaßnahmenplans. 

Den Leitfaden charakterisiert zudem  ein schrittweises Vorgehen, welches erst mit Abschluss der letzten Phase eine \gls{ISO} 50001-Zertifizierung anstrebt. Dies unterscheidet ihn naturgemäß von dem in der Norm skizzierten Verfahren, welches von Anfang an auf eine Zertifizierung abzielt. Dieses sukzessive Einführen spiegelt gleichzeitig das dem praktischen Alltag geschuldete zeitintensive Vorgehen wider. Dort ist die Umsetzung eines \gls{EnMS} nicht von heute auf morgen möglich. Die Praxis zeigt, dass bei der ersten Zertifizierung nicht immer die Norm zu 100\% erfüllt sein muss.

\subsection{Fachbroschüre des T\"UV Rheinland}\label{energiemanagement:praxis:tuev}

Die Fachbrosch\"ure des \gls{TUV}-Rheinland bietet nicht nur Erl\"auterungen und Hinweise zur \gls{ISO}-Norm, vielmehr erweitert es die Norm um praxisnahe Informationen. Da ein Energiemanagement flexibel in jeder Branche eingesetzt werden kann, bietet die Literatur hier spezielle, branchenbezogene Optimierungsm\"oglichkeiten (z.B. f\"ur Lackierereien, Rechenzentren) an. Besteht bereits ein Managementsystem etwa nach \gls{DIN} EN \gls{ISO} 14001 oder \gls{DIN} EN \gls{ISO} 9001, so werden Entsprechungen und Abweichungen zu diesem dargestellt. In der Praxis k\"onnten insbesondere diese Teile f\"ur die konkrete Umsetzung eine Hilfestellung bieten. Der Hauptteil der \gls{TUV}-Literatur konzentriert sich hingegen auf die Vereinfachung der Norm. Es werden Anmerkungen und Erkl\"arungen zu Teilen der Norm dargelegt: Welche Vorgehensweisen bieten sich bei der Erstellung eines energetischen Planungsprozesses an? Wie sieht eine beispielhafte Energiepolitik aus? 
Anders als im Leitfaden von GUTcert werden diese Hinweise den Formulierungen der Ursprungskapiteln der Norm gegen\"ubergestellt. Ein Verfahren zur Einf\"uhrung eines \gls{EnMS} wird hingegen nicht formuliert.
