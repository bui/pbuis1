Vorlage fuer Diplom- und Studienarbeiten mit LaTeX

In der Datei "arbeit.tex" müssen oben der Titel der Arbeit, Name des Autors,
usw. angepasst werden. Die Kapitel liegen im Ordner "kapitel", weitere
Kapitel koennen unten in der Datei "arbeit.tex" eingebunden werden.
Bilder werden im Ordner "bilder" angelegt. Es empfiehlt sich, Bilder im
PDF (Portable Document Format) Format zu verwenden. Soll eine PostScript
Datei erzeugt werden, so sollten die Bilder zudem im EPS (Encapsuled
PostScript) vorliegen.
Literatur wird in der Datei "quellen.bib" eingetragen und kann dann in der
Arbeit entsprechend referenziert werden.

Es liegt ein Makefile vor, welches die Arbeit mit LaTeX auf der Kommandozeile
erleichtern sollte. Wird der Name der Dateien arbeit.tex oder quellen.bib
geaendert, so muss dies oben im Makefile entsprechend angepasst werden.

Weitere Informationen zu Diplom- und Studienarbeiten finden sich auf:
  http://wwwhni.uni-paderborn.de/iug/lehre/studien-und-diplomarbeiten/

