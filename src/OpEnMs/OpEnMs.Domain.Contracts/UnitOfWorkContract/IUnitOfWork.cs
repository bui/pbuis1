#region Licence

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0.html
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// 
// Copyright (c) 2013, HTW Berlin

#endregion

using System;
using OpEnMs.Domain.RepositoryContracts;

namespace OpEnMs.Domain.UnitOfWorkContract
{
	public interface IUnitOfWork: IDisposable
	{
		void Save();

		IUserProfileRepository UserProfileRepository { get; }
		ICompanyRepository CompanyRepository { get; }
		INetworkRepository NetworkRepository { get; }
		IMeasuringPointRepository MeasuringPointRepository { get; }
		IMeasureRepository MeasureRepository { get; }
		IIsoMeasureRepository IsoMeasureRepository { get; }
		IIdeaRepository IdeaRepository { get; }
		IEnergyConsumptionRepository EnergyConsumptionRepository { get; }
		IUnitRepository UnitRepository { get; }
		ISourceRepository SourceRepository { get; }
		IReadingRepository ReadingRepository { get; }
		IValuationRepository ValuationRepository { get; }
		IIndicatorValueRepository IndicatorValueRepository { get; }
		IIndicatorRepository IndicatorRepository { get; }
		ITopicRepository TopicRepository { get; }
	}
}