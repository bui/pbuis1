﻿//------------------------------------------------------------------------------
// <auto-generated>
//     Dieser Code wurde von einem Tool generiert.
//     Laufzeitversion:4.0.30319.18213
//
//     Änderungen an dieser Datei können falsches Verhalten verursachen und gehen verloren, wenn
//     der Code erneut generiert wird.
// </auto-generated>
//------------------------------------------------------------------------------

namespace OpEnMs.Resources {
    using System;
    
    
    /// <summary>
    ///   Eine stark typisierte Ressourcenklasse zum Suchen von lokalisierten Zeichenfolgen usw.
    /// </summary>
    // Diese Klasse wurde von der StronglyTypedResourceBuilder automatisch generiert
    // -Klasse über ein Tool wie ResGen oder Visual Studio automatisch generiert.
    // Um einen Member hinzuzufügen oder zu entfernen, bearbeiten Sie die .ResX-Datei und führen dann ResGen
    // mit der /str-Option erneut aus, oder Sie erstellen Ihr VS-Projekt neu.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class DisplayNameResources {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal DisplayNameResources() {
        }
        
        /// <summary>
        ///   Gibt die zwischengespeicherte ResourceManager-Instanz zurück, die von dieser Klasse verwendet wird.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("OpEnMs.Resources.DisplayNameResources", typeof(DisplayNameResources).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Überschreibt die CurrentUICulture-Eigenschaft des aktuellen Threads für alle
        ///   Ressourcenzuordnungen, die diese stark typisierte Ressourcenklasse verwenden.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die Maßnahmen
        /// ähnelt.
        /// </summary>
        public static string Activities {
            get {
                return ResourceManager.GetString("Activities", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die Bewertung ähnelt.
        /// </summary>
        public static string ActivityValuation {
            get {
                return ResourceManager.GetString("ActivityValuation", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die Adresse ähnelt.
        /// </summary>
        public static string Address {
            get {
                return ResourceManager.GetString("Address", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die Anmerkung ähnelt.
        /// </summary>
        public static string Annotation {
            get {
                return ResourceManager.GetString("Annotation", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die CO2-Reduktion ähnelt.
        /// </summary>
        public static string CarbonReduction {
            get {
                return ResourceManager.GetString("CarbonReduction", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die Stadt ähnelt.
        /// </summary>
        public static string City {
            get {
                return ResourceManager.GetString("City", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die Unternehmen ähnelt.
        /// </summary>
        public static string Companies {
            get {
                return ResourceManager.GetString("Companies", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die Unternehmen ähnelt.
        /// </summary>
        public static string Company {
            get {
                return ResourceManager.GetString("Company", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die Bestätigt ähnelt.
        /// </summary>
        public static string Confirmed {
            get {
                return ResourceManager.GetString("Confirmed", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die Neues Kennwort bestätigen ähnelt.
        /// </summary>
        public static string ConfirmNewPassword {
            get {
                return ResourceManager.GetString("ConfirmNewPassword", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die Kennwort bestätigen ähnelt.
        /// </summary>
        public static string ConfirmPassword {
            get {
                return ResourceManager.GetString("ConfirmPassword", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die Erstellt ähnelt.
        /// </summary>
        public static string Created {
            get {
                return ResourceManager.GetString("Created", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die Ersteller ähnelt.
        /// </summary>
        public static string Creator {
            get {
                return ResourceManager.GetString("Creator", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die Aktuelles Kennwort ähnelt.
        /// </summary>
        public static string CurrentPassword {
            get {
                return ResourceManager.GetString("CurrentPassword", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die Beschreibung ähnelt.
        /// </summary>
        public static string Description {
            get {
                return ResourceManager.GetString("Description", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die Fälligkeitsdatum ähnelt.
        /// </summary>
        public static string DueDate {
            get {
                return ResourceManager.GetString("DueDate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die Laufzeit ähnelt.
        /// </summary>
        public static string Duration {
            get {
                return ResourceManager.GetString("Duration", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die Kapitalwert ähnelt.
        /// </summary>
        public static string DynamicAmortization {
            get {
                return ResourceManager.GetString("DynamicAmortization", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die Email ähnelt.
        /// </summary>
        public static string Email {
            get {
                return ResourceManager.GetString("Email", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die Mitarbeiter ähnelt.
        /// </summary>
        public static string Employee {
            get {
                return ResourceManager.GetString("Employee", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die Verantwortlicher Mitarbeiter ähnelt.
        /// </summary>
        public static string EmployeeInCharge {
            get {
                return ResourceManager.GetString("EmployeeInCharge", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die Verantwortlicher Mitarbeiter ähnelt.
        /// </summary>
        public static string EmployeeInChargeString {
            get {
                return ResourceManager.GetString("EmployeeInChargeString", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die Mitarbeiter ähnelt.
        /// </summary>
        public static string Employees {
            get {
                return ResourceManager.GetString("Employees", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die Enddatum ähnelt.
        /// </summary>
        public static string EndDate {
            get {
                return ResourceManager.GetString("EndDate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die Energetische Einsparung ähnelt.
        /// </summary>
        public static string EnergeticSavings {
            get {
                return ResourceManager.GetString("EnergeticSavings", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die Energieträger ähnelt.
        /// </summary>
        public static string EnergyConsumptions {
            get {
                return ResourceManager.GetString("EnergyConsumptions", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die Energiepreis ähnelt.
        /// </summary>
        public static string EnergyPrice {
            get {
                return ResourceManager.GetString("EnergyPrice", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die Energiesteuerrückvergütung ähnelt.
        /// </summary>
        public static string EnergyTaxRefunds {
            get {
                return ResourceManager.GetString("EnergyTaxRefunds", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die Ingenieur ähnelt.
        /// </summary>
        public static string Engineer {
            get {
                return ResourceManager.GetString("Engineer", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die Durchgeführt am ähnelt.
        /// </summary>
        public static string EntryDate {
            get {
                return ResourceManager.GetString("EntryDate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die € ähnelt.
        /// </summary>
        public static string Euro {
            get {
                return ResourceManager.GetString("Euro", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die Erklärung ähnelt.
        /// </summary>
        public static string Explanation {
            get {
                return ResourceManager.GetString("Explanation", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die Anlagen ähnelt.
        /// </summary>
        public static string Facilities {
            get {
                return ResourceManager.GetString("Facilities", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die Tatsächliche Energetische Einsparung ähnelt.
        /// </summary>
        public static string FinalEnergeticSavings {
            get {
                return ResourceManager.GetString("FinalEnergeticSavings", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die Tatsächliche Interne Verzinsung ähnelt.
        /// </summary>
        public static string FinalInterestRate {
            get {
                return ResourceManager.GetString("FinalInterestRate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die Tatsächliche Investition ähnelt.
        /// </summary>
        public static string FinalInvestment {
            get {
                return ResourceManager.GetString("FinalInvestment", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die Tatsächliche Lebenszeit ähnelt.
        /// </summary>
        public static string FinalLifetime {
            get {
                return ResourceManager.GetString("FinalLifetime", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die Tatsächliche Statistische Einsparung ähnelt.
        /// </summary>
        public static string FinalMonetarySavings {
            get {
                return ResourceManager.GetString("FinalMonetarySavings", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die Abschließende Bemerkungen ähnelt.
        /// </summary>
        public static string FinalValuation {
            get {
                return ResourceManager.GetString("FinalValuation", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die Geschäftsjahr entspricht Kalenderjahr ähnelt.
        /// </summary>
        public static string FiscalYearIsCalendarYear {
            get {
                return ResourceManager.GetString("FiscalYearIsCalendarYear", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die Berechnungsformel ähnelt.
        /// </summary>
        public static string Formula {
            get {
                return ResourceManager.GetString("Formula", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die Vorname ähnelt.
        /// </summary>
        public static string GivenName {
            get {
                return ResourceManager.GetString("GivenName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die Ideen ähnelt.
        /// </summary>
        public static string Ideas {
            get {
                return ResourceManager.GetString("Ideas", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die Status der Idee ähnelt.
        /// </summary>
        public static string IdeaState {
            get {
                return ResourceManager.GetString("IdeaState", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die Kalkulatorischer Zinssatz ähnelt.
        /// </summary>
        public static string InterestRate {
            get {
                return ResourceManager.GetString("InterestRate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die Interne Verzinsung ähnelt.
        /// </summary>
        public static string InternalInterestRate {
            get {
                return ResourceManager.GetString("InternalInterestRate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die Investment ähnelt.
        /// </summary>
        public static string Investment {
            get {
                return ResourceManager.GetString("Investment", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die KMU ja/nein ähnelt.
        /// </summary>
        public static string IsASME {
            get {
                return ResourceManager.GetString("IsASME", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die Auswahl ähnelt.
        /// </summary>
        public static string IsChecked {
            get {
                return ResourceManager.GetString("IsChecked", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die Iso-Maßnahmen ähnelt.
        /// </summary>
        public static string IsoMeasures {
            get {
                return ResourceManager.GetString("IsoMeasures", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die Zuletzt bearbeitet ähnelt.
        /// </summary>
        public static string LastEdit {
            get {
                return ResourceManager.GetString("LastEdit", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die Lebenszeit ähnelt.
        /// </summary>
        public static string Lifetime {
            get {
                return ResourceManager.GetString("Lifetime", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die Koordinator ähnelt.
        /// </summary>
        public static string Manager {
            get {
                return ResourceManager.GetString("Manager", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die Maßnahmen ähnelt.
        /// </summary>
        public static string Measures {
            get {
                return ResourceManager.GetString("Measures", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die Maßnahmenart ähnelt.
        /// </summary>
        public static string MeasureType {
            get {
                return ResourceManager.GetString("MeasureType", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die Messstellen ähnelt.
        /// </summary>
        public static string MeasuringPoints {
            get {
                return ResourceManager.GetString("MeasuringPoints", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die Messstellentyp ähnelt.
        /// </summary>
        public static string MeasuringPointType {
            get {
                return ResourceManager.GetString("MeasuringPointType", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die Moderator ähnelt.
        /// </summary>
        public static string Moderator {
            get {
                return ResourceManager.GetString("Moderator", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die Einsparung ähnelt.
        /// </summary>
        public static string MonetarySavings {
            get {
                return ResourceManager.GetString("MonetarySavings", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die Name ähnelt.
        /// </summary>
        public static string Name {
            get {
                return ResourceManager.GetString("Name", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die Neues Kennwort ähnelt.
        /// </summary>
        public static string NewPassword {
            get {
                return ResourceManager.GetString("NewPassword", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die Ausstehend ähnelt.
        /// </summary>
        public static string NotConfirmed {
            get {
                return ResourceManager.GetString("NotConfirmed", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die Nummer ähnelt.
        /// </summary>
        public static string Number {
            get {
                return ResourceManager.GetString("Number", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die Anzahl der Unternehmen ähnelt.
        /// </summary>
        public static string NumberOfCompanies {
            get {
                return ResourceManager.GetString("NumberOfCompanies", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die Anzahl der Mitarbeiter ähnelt.
        /// </summary>
        public static string NumberOfEmployees {
            get {
                return ResourceManager.GetString("NumberOfEmployees", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die Kennwort ähnelt.
        /// </summary>
        public static string Password {
            get {
                return ResourceManager.GetString("Password", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die Telefon ähnelt.
        /// </summary>
        public static string Phone {
            get {
                return ResourceManager.GetString("Phone", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die Postleitzahl ähnelt.
        /// </summary>
        public static string PostalCode {
            get {
                return ResourceManager.GetString("PostalCode", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die Priorität ähnelt.
        /// </summary>
        public static string Priority {
            get {
                return ResourceManager.GetString("Priority", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die Anbieter ähnelt.
        /// </summary>
        public static string Provider {
            get {
                return ResourceManager.GetString("Provider", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die Ablesedatum ähnelt.
        /// </summary>
        public static string ReadingDate {
            get {
                return ResourceManager.GetString("ReadingDate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die Ablesungen ähnelt.
        /// </summary>
        public static string Readings {
            get {
                return ResourceManager.GetString("Readings", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die Registrierung ähnelt.
        /// </summary>
        public static string Registration {
            get {
                return ResourceManager.GetString("Registration", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die Speichern? ähnelt.
        /// </summary>
        public static string RememberMe {
            get {
                return ResourceManager.GetString("RememberMe", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die Rollen ähnelt.
        /// </summary>
        public static string Roles {
            get {
                return ResourceManager.GetString("Roles", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die Lösung ähnelt.
        /// </summary>
        public static string Solution {
            get {
                return ResourceManager.GetString("Solution", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die Energieträger ähnelt.
        /// </summary>
        public static string Source {
            get {
                return ResourceManager.GetString("Source", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die Startdatum ähnelt.
        /// </summary>
        public static string StartDate {
            get {
                return ResourceManager.GetString("StartDate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die Status ähnelt.
        /// </summary>
        public static string State {
            get {
                return ResourceManager.GetString("State", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die Status gesetzt durch ähnelt.
        /// </summary>
        public static string StateEnabler {
            get {
                return ResourceManager.GetString("StateEnabler", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die Status ähnelt.
        /// </summary>
        public static string StateName {
            get {
                return ResourceManager.GetString("StateName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die Status ähnelt.
        /// </summary>
        public static string States {
            get {
                return ResourceManager.GetString("States", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die Amortisationszeit ähnelt.
        /// </summary>
        public static string StaticAmortization {
            get {
                return ResourceManager.GetString("StaticAmortization", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die Statistische Amortisation ähnelt.
        /// </summary>
        public static string StatisticAmortization {
            get {
                return ResourceManager.GetString("StatisticAmortization", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die Straße ähnelt.
        /// </summary>
        public static string Street {
            get {
                return ResourceManager.GetString("Street", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die Nachname ähnelt.
        /// </summary>
        public static string SurName {
            get {
                return ResourceManager.GetString("SurName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die Symbol ähnelt.
        /// </summary>
        public static string Symbol {
            get {
                return ResourceManager.GetString("Symbol", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die Laufzeit ähnelt.
        /// </summary>
        public static string Term {
            get {
                return ResourceManager.GetString("Term", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die Zeitpunkt der Ablesung ähnelt.
        /// </summary>
        public static string TimeStamp {
            get {
                return ResourceManager.GetString("TimeStamp", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die Thema ähnelt.
        /// </summary>
        public static string Topic {
            get {
                return ResourceManager.GetString("Topic", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die Gesamte Energiekosten ähnelt.
        /// </summary>
        public static string TotalEnergyCosts {
            get {
                return ResourceManager.GetString("TotalEnergyCosts", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die Gesamtumsatz ähnelt.
        /// </summary>
        public static string TotalRevenue {
            get {
                return ResourceManager.GetString("TotalRevenue", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die Einheit ähnelt.
        /// </summary>
        public static string Unit {
            get {
                return ResourceManager.GetString("Unit", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die Benutzername ähnelt.
        /// </summary>
        public static string UserName {
            get {
                return ResourceManager.GetString("UserName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die Benutzerprofile ähnelt.
        /// </summary>
        public static string UserProfile {
            get {
                return ResourceManager.GetString("UserProfile", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die Energetische Bewertung ähnelt.
        /// </summary>
        public static string Valuation {
            get {
                return ResourceManager.GetString("Valuation", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die Wert ähnelt.
        /// </summary>
        public static string Value {
            get {
                return ResourceManager.GetString("Value", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die Homepage ähnelt.
        /// </summary>
        public static string Website {
            get {
                return ResourceManager.GetString("Website", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Sucht eine lokalisierte Zeichenfolge, die Jahre ähnelt.
        /// </summary>
        public static string Year {
            get {
                return ResourceManager.GetString("Year", resourceCulture);
            }
        }
    }
}
