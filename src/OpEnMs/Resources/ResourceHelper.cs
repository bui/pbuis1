﻿#region Licence

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0.html
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// 
// Copyright (c) 2013, HTW Berlin

#endregion

using System;
using System.Reflection;

namespace OpEnMs.Resources
{
	public class ResourceHelper
	{
		public static string GetResourceLookup(Type resourceType, string resourceName)
		{
			if ((resourceType != null) && (resourceName != null))
			{
				PropertyInfo property = resourceType.GetProperty(resourceName,
				                                                 BindingFlags.Public | BindingFlags.Static);
				if (property == null)
				{
					throw new InvalidOperationException(string.Format("Resource Type Does Not Have Property"));
				}
				if (property.PropertyType != typeof (string))
				{
					throw new InvalidOperationException(string.Format("Resource Property is Not String Type"));
				}
				return (string) property.GetValue(null,
				                                  null);
			}
			return null;
		}
	}
}