﻿#region Licence

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0.html
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// 
// Copyright (c) 2013, HTW Berlin

#endregion

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Emporer.Unit;
using OpEnMs.Domain.Entities.Attributes;
using OpEnMs.Resources;

namespace OpEnMs.Domain.Entities
{
    public class Indicator
    {
        public int Id { get; set; }

        [Required(ErrorMessageResourceName = "NumberIsRequired", ErrorMessageResourceType = typeof (ErrorMessageResources))]
        [Display(ResourceType = typeof (DisplayNameResources), Name = "Number")]
        /*[PlaceHolder(typeof (DisplayNameResources), "Number")]*/
        [ToolTip(typeof (ToolTipResources), "Indicator_Number")]
        [UniqueAttribute]
        public String Number { get; set; }

        [Required(ErrorMessageResourceName = "NameIsRequired", ErrorMessageResourceType = typeof (ErrorMessageResources))]
        [Display(ResourceType = typeof (DisplayNameResources), Name = "Name")]
        /*[PlaceHolder(typeof (DisplayNameResources), "Name")]*/
        [ToolTip(typeof (ToolTipResources), "Indicator_Name")]
        public String Name { get; set; }

        [DataType(DataType.MultilineText)]
        [Required(ErrorMessageResourceName = "ExplanationIsRequired", ErrorMessageResourceType = typeof (ErrorMessageResources))]
        [Display(ResourceType = typeof (DisplayNameResources), Name = "Explanation")]
        [ToolTip(typeof (ToolTipResources), "Indicator_Explanation")]
        public String Explanation { get; set; }

        [Required(ErrorMessageResourceName = "UnitIsRequired", ErrorMessageResourceType = typeof (ErrorMessageResources))]
        public int UnitId { get; set; }

        [Display(ResourceType = typeof (DisplayNameResources), Name = "Unit")]
        public virtual Unit Unit { get; set; }

        [Required(ErrorMessageResourceName = "TopicIsRequired", ErrorMessageResourceType = typeof (ErrorMessageResources))]
        public int TopicId { get; set; }

        [Display(ResourceType = typeof (DisplayNameResources), Name = "Topic")]
        public virtual Topic Topic { get; set; }

        [Display(ResourceType = typeof (DisplayNameResources), Name = "Created")]
        public DateTime Created { get; set; }

        public virtual UserProfile CreatedBy { get; set; }

        [Display(ResourceType = typeof (DisplayNameResources), Name = "LastEdit")]
        public DateTime LastEdit { get; set; }

        public int UserProfileId { get; set; }
        public virtual UserProfile LastEditBy { get; set; }

        [PlaceHolder("Keine Formel hinterlegt.")]
        [ToolTip("Hier klicken um den Formeleditor zu öffnen.")]
        [Display(ResourceType = typeof (DisplayNameResources), Name = "Formula")]
        public string IndicatorExpression { get; set; }

        public virtual ICollection<IndicatorValue> IndicatorValues { get; set; }
    }
}