﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using OpEnMs.Resources;

namespace OpEnMs.Domain.Entities
{
	public class Reading
	{
		[HiddenInput(DisplayValue = false)]
		public int Id { get; set; }

		[DataType(DataType.DateTime)]
		[Display(ResourceType = typeof(DisplayNameResources), Name = "TimeStamp")]
		public DateTime Timestamp { get; set; }

		[Display(ResourceType = typeof(DisplayNameResources), Name = "Value")]
		[Required(ErrorMessageResourceName = "ValueIsRequired", ErrorMessageResourceType = typeof(ErrorMessageResources))]
		public double Value { get; set; }
	}
}