using System.ComponentModel.DataAnnotations;

namespace OpEnMs.Domain.Entities
{
	public class LocalPassword
	{
		[Required]
		[DataType(DataType.Password)]
		[Display(Name = "Aktuelles Kennwort")]
		public string OldPassword { get; set; }

		[Required]
		[StringLength(100, ErrorMessage = "&quote{0}&quote muss mindestens {2} Zeichen lang sein.", MinimumLength = 6)]
		[DataType(DataType.Password)]
		[Display(Name = "Neues Kennwort")]
		public string NewPassword { get; set; }

		[DataType(DataType.Password)]
		[Display(Name = "Neues Kennwort bestätigen")]
		[Compare("NewPassword", ErrorMessage = "Das neue Kennwort entspricht nicht dem Bestätigungskennwort.")]
		public string ConfirmPassword { get; set; }
	}
}