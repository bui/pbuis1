﻿#region Licence

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0.html
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// 
// Copyright (c) 2013, HTW Berlin

#endregion

using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using OpEnMs.Domain.Entities.Attributes;
using OpEnMs.Resources;

namespace OpEnMs.Domain.Entities
{
	public class Company
	{
		public int Id { get; set; }

		[Display(ResourceType = typeof (DisplayNameResources), Name = "IsASME")]
		public bool IsASME { get; set; }

		[Display(ResourceType = typeof (DisplayNameResources), Name = "FiscalYearIsCalendarYear")]
		public bool FiscalYearIsCalendarYear { get; set; }

		[Display(ResourceType = typeof (DisplayNameResources), Name = "Name")]
        [PlaceHolder(typeof(DisplayNameResources), "Name")]
		public string Name { get; set; }

		[Display(ResourceType = typeof (DisplayNameResources), Name = "Street")]
        [PlaceHolder(typeof(DisplayNameResources), "Street")]
		public string Street { get; set; }

		[Display(ResourceType = typeof (DisplayNameResources), Name = "City")]
        [PlaceHolder(typeof(DisplayNameResources), "City")]
		public string City { get; set; }

		[DataType(DataType.PostalCode)]
		[Display(ResourceType = typeof (DisplayNameResources), Name = "PostalCode")]
		public string PostalCode { get; set; }

		[DataType(DataType.Url)]
		[Display(ResourceType = typeof (DisplayNameResources), Name = "Website")]
		public string Website { get; set; }

		[Display(ResourceType = typeof (DisplayNameResources), Name = "NumberOfEmployees")]
		public int NumberOfEmployees { get; set; }

		[DataType(DataType.Currency)]
		[Display(ResourceType = typeof (DisplayNameResources), Name = "TotalRevenue")]
		public double TotalRevenue { get; set; }

		[DataType(DataType.Currency)]
		[Display(ResourceType = typeof (DisplayNameResources), Name = "TotalEnergyCosts")]
		public double TotalEnergyCosts { get; set; }

		[DataType(DataType.Currency)]
		[Display(ResourceType = typeof (DisplayNameResources), Name = "EnergyTaxRefunds")]
		public double EnergyTaxRefunds { get; set; }

		public virtual ICollection<UserProfile> Employees { get; set; }

		[Display(ResourceType = typeof (DisplayNameResources), Name = "MeasuringPoints")]
		public virtual ICollection<MeasuringPoint> MeasuringPoints { get; set; }

		[Display(ResourceType = typeof (DisplayNameResources), Name = "EnergyConsumptions")]
		public virtual ICollection<EnergyConsumption> EnergyConsumptions { get; set; }

		[Display(ResourceType = typeof (DisplayNameResources), Name = "Measures")]
		public virtual ICollection<Measure> Measures { get; set; }

		[Display(ResourceType = typeof (DisplayNameResources), Name = "IsoMeasures")]
		public virtual ICollection<IsoMeasure> IsoMeasures { get; set; }

		public virtual ICollection<Network> Networks { get; set; }

		//public virtual ICollection<ProductionDay> ProductionDays
		//{
		//	get;
		//	set;
		//}

		//public virtual ICollection<Space> Spaces
		//{
		//	get;
		//	set;
		//}

		//public virtual ICollection<Product> Products
		//{
		//	get;
		//	set;
		//}

		public virtual ICollection<Idea> Ideas { get; set; }
	}
}