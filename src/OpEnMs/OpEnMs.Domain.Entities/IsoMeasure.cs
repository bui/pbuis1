﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using OpEnMs.Resources;

namespace OpEnMs.Domain.Entities
{
	public class IsoMeasure: Measure
	{
		[Display(ResourceType = typeof (DisplayNameResources), Name = "EmployeeInCharge")]
		public virtual UserProfile EmployeeInCharge { get; set; }

		[HiddenInput(DisplayValue = false)]
		[Display(ResourceType = typeof (DisplayNameResources), Name = "Creator")]
		public virtual UserProfile Creator { get; set; }
	}
}