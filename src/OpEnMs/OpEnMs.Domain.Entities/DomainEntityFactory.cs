﻿#region Licence

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0.html
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// 
// Copyright (c) 2013, HTW Berlin

#endregion

using System;
using System.Collections.ObjectModel;

namespace OpEnMs.Domain.Entities
{
	public class DomainEntityFactory: IDomainEntityFactory
	{
		public Company CreateCompany()
		{
			var company = new Company{
				                         Measures = new Collection<Measure>(),
				                         MeasuringPoints = new Collection<MeasuringPoint>(),
				                         Employees = new Collection<UserProfile>(),
				                         EnergyConsumptions = new Collection<EnergyConsumption>()
			                         };
			return company;
		}

		public Network CreateNetwork()
		{
			var network = new Network{
				                         Companies = new Collection<Company>(),
				                         StartDate = DateTime.Now,
				                         EndDate = DateTime.Now.AddYears(3),
			                         };
			return network;
		}

		public Idea CreateIdea()
		{
			throw new NotImplementedException();
		}

		public Reading CreateReading()
		{
			throw new NotImplementedException();
		}

		public EnergyConsumption CreateEnergyConsumption()
		{
		  var energyConsumption = new EnergyConsumption{
		                                                 Readings = new Collection<Reading>()
		                                               };
		  return energyConsumption;
		}

		public IsoMeasure CreateIsoMeasure()
		{
			var isoMeasure = new IsoMeasure
			{
				DueDate = DateTime.Now,
				EntryDate = DateTime.Now,
				Ideas = new Collection<Idea>()
			};
			return isoMeasure;
		}

		public LocalPassword CreateLocalPassword()
		{
			throw new NotImplementedException();
		}

		public Login CreateLogin()
		{
			throw new NotImplementedException();
		}

		public Measure CreateMeasure()
		{
			var measure = new Measure{
				                         DueDate = DateTime.Now,
				                         EntryDate = DateTime.Now
			                         };
			return measure;
		}

		public MeasuringPoint CreateMeasuringPoint()
		{
			var measuringPoint = new MeasuringPoint{
				                                       EnergyConsumptions = new Collection<EnergyConsumption>()
			                                       };
			return measuringPoint;
		}

		public ResetPassword CreateResetPassword()
		{
			throw new NotImplementedException();
		}

		public ResetPasswordConfirm CreateResetPasswordConfirm()
		{
			throw new NotImplementedException();
		}

		public Source CreateSource()
		{
			throw new NotImplementedException();
		}

		public Valuation CreateValuation()
		{
			throw new NotImplementedException();
		}

		public UserProfile CreateUserProfile()
		{
			throw new NotImplementedException();
		}
	}

	public interface IDomainEntityFactory
	{
		Company CreateCompany();
		Network CreateNetwork();
		Idea CreateIdea();
		Reading CreateReading();
		EnergyConsumption CreateEnergyConsumption();
		IsoMeasure CreateIsoMeasure();
		LocalPassword CreateLocalPassword();
		Login CreateLogin();
		Measure CreateMeasure();
		MeasuringPoint CreateMeasuringPoint();
		ResetPassword CreateResetPassword();
		ResetPasswordConfirm CreateResetPasswordConfirm();
		Source CreateSource();
		Valuation CreateValuation();
		UserProfile CreateUserProfile();
	}
}