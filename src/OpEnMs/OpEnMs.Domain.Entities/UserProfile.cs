using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using OpEnMs.Resources;

namespace OpEnMs.Domain.Entities
{
  [Table("UserProfile")]
  public class UserProfile
  {
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public int UserId { get; set; }

    [Display(ResourceType = typeof (DisplayNameResources), Name = "UserName")]
    public string UserName { get; set; }

    [Display(ResourceType = typeof (DisplayNameResources), Name = "GivenName")]
    public string GivenName { get; set; }

    [Display(ResourceType = typeof (DisplayNameResources), Name = "SurName")]
    public string SurName { get; set; }

    [Display(ResourceType = typeof (DisplayNameResources), Name = "Phone")]
    public string Phone { get; set; }

    [Display(ResourceType = typeof (DisplayNameResources), Name = "Email")]
    public string Email { get; set; }
  }
}