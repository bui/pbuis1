﻿using System;

namespace OpEnMs.Domain.Entities.Attributes
{
    [AttributeUsage(AttributeTargets.Property, Inherited = true, AllowMultiple = true)]
    public class UniqueAttribute: Attribute
    {
    }
}