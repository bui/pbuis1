﻿using System;
using System.Web.Mvc;
using OpEnMs.Resources;

namespace OpEnMs.Domain.Entities.Attributes
{
	public class HelpPopoverAttribute:Attribute, IMetadataAware
	{
		private readonly string m_HelpPopover;

		public HelpPopoverAttribute(string helpPopover)
		{
			m_HelpPopover = helpPopover;
		}

		public HelpPopoverAttribute(Type resourceType, string resourceName)
		{
			m_HelpPopover = ResourceHelper.GetResourceLookup(resourceType,resourceName);
		}

		public void OnMetadataCreated(ModelMetadata metadata)
		{
			metadata.AdditionalValues["popover"] = m_HelpPopover;
		}
	}
}
