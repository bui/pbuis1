using System.ComponentModel.DataAnnotations;

namespace OpEnMs.Domain.Entities
{
	public class Register
	{
		[Required]
		[Display(Name = "Benutzername")]
		public string UserName { get; set; }

		[Required]
		[StringLength(100, ErrorMessage = "&quote{0}&quote muss mindestens {2} Zeichen lang sein.", MinimumLength = 6)]
		[DataType(DataType.Password)]
		[Display(Name = "Kennwort")]
		public string Password { get; set; }

		[DataType(DataType.Password)]
		[Display(Name = "Kennwort bestätigen")]
		[Compare("Password", ErrorMessage = "Das Kennwort entspricht nicht dem Bestätigungskennwort.")]
		public string ConfirmPassword { get; set; }

		public string SurName { get; set; }

		public string Phone { get; set; }

		public string Email { get; set; }
	}
}