﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Emporer.Unit;
using OpEnMs.Resources;

namespace OpEnMs.Domain.Entities
{
	public class EnergyConsumption
	{
		[HiddenInput(DisplayValue = false)]
		public int Id { get; set; }

    [DataType(DataType.Currency)]
    [Display(ResourceType = typeof(DisplayNameResources), Name = "EnergyPrice")]
    public double EnergyPrice { get; set; }

		public virtual Source Source { get; set; }

		public virtual ICollection<Reading> Readings
		{
			get;
			set;
		}

		public virtual Unit Unit
		{
			get;
			set;
		}
	}
}