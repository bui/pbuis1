﻿using System.ComponentModel.DataAnnotations;

namespace OpEnMs.Domain.Entities
{
	public enum NetworkState
	{
		[Display(Description = "Phase0", ResourceType = typeof(Resources.Resources))]
		Phase0 = 0,
		[Display(Description = "Phase1", ResourceType = typeof(Resources.Resources))]
		Phase1 = 1,
		[Display(Description = "Phase2", ResourceType = typeof(Resources.Resources))]
		Phase2 = 2,
		[Display(Description = "Closed", ResourceType = typeof(Resources.Resources))]
		Closed = 3
	}

	public enum Priority
	{
		[Display(Description = "Enum_Priority_High", ResourceType = typeof(Resources.Resources))]
		High = 0,
		[Display(Description = "Enum_Priority_Medium", ResourceType = typeof(Resources.Resources))]
		Medium = 1,
		[Display(Description = "Enum_Priority_Low", ResourceType = typeof(Resources.Resources))]
		Low = 2
	}

	public enum MeasureState
	{
		[Display(Description = "Open", ResourceType = typeof(Resources.Resources))]
		Open = 0,
		[Display(Description = "Reject", ResourceType = typeof(Resources.Resources))]
		Reject = 1,
		[Display(Description = "DuringImplementation", ResourceType = typeof(Resources.Resources))]
		DuringImplementation = 2,
		[Display(Description = "Accomplished", ResourceType = typeof(Resources.Resources))]
		Accomplished = 3
	}

	public enum MeasureType
	{
		OrganizationalMeasure = 0,
		TechnicalMeasure = 1
	}

	public enum MeasuringPointType
	{
		[Display(Description = "Consumer", ResourceType = typeof(Resources.Resources))]
		Consumer = 0,
		[Display(Description = "Meter", ResourceType = typeof(Resources.Resources))]
		Meter = 1,
		[Display(Description = "Producer", ResourceType = typeof(Resources.Resources))]
		Producer = 2,
	}

    public enum IdeaState
    {
        [Display(Description = "Enum_IdeaStateName_Invalid", ResourceType = typeof(Resources.Resources))]
        Invalid = -1,
        [Display(Description = "Enum_IdeaStateName_New", ResourceType = typeof(Resources.Resources))]
        New = 0,
        [Display(Description = "Enum_IdeaStateName_Revision", ResourceType = typeof(Resources.Resources))]
        Revision = 1,
        [Display(Description = "Enum_IdeaStateName_Rejected", ResourceType = typeof(Resources.Resources))]
        Rejected = 2,
        [Display(Description = "Enum_IdeaStateName_DuringImplementation", ResourceType = typeof(Resources.Resources))]
        DuringImplementation = 3,
        [Display(Description = "Enum_IdeaStateName_Successful", ResourceType = typeof(Resources.Resources))]
        Successful = 4
    }
}