﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using OpEnMs.Domain.Entities.Attributes;
using OpEnMs.Resources;

namespace OpEnMs.Domain.Entities
{
  public class Measure
  {
    [HiddenInput(DisplayValue = false)]
    public int Id { get; set; }

    [Required(ErrorMessageResourceName = "NameIsRequired", ErrorMessageResourceType = typeof (ErrorMessageResources))]
    [Display(ResourceType = typeof (DisplayNameResources), Name = "Name")]
    [ToolTip(typeof(ToolTipResources), "Activity_Name")]
    [PlaceHolder(typeof(DisplayNameResources), "Name")]
		[HelpPopover("Help")]
    public string Name { get; set; }

    [DataType(DataType.MultilineText)]
    [Display(ResourceType = typeof (DisplayNameResources), Name = "Description")]
    [ToolTip(typeof(ToolTipResources), "Activity_Description")]
    [PlaceHolder(typeof(DisplayNameResources), "Description")]
    public string Description { get; set; }

    [DataType(DataType.MultilineText)]
    [Display(ResourceType = typeof (DisplayNameResources), Name = "Solution", Description = "Solution")]
    [ToolTip(typeof(ToolTipResources), "Activity_Solution")]
    [PlaceHolder(typeof(DisplayNameResources), "Solution")]
    public string Solution { get; set; }

    [Display(ResourceType = typeof (DisplayNameResources), Name = "Valuation")]
    public virtual Valuation Valuation { get; set; }

    [DataType(DataType.Date)]
    [Display(ResourceType = typeof (DisplayNameResources), Name = "DueDate")]
    [ToolTip(typeof(ToolTipResources), "Activity_DueDate")]
    public DateTime? DueDate { get; set; }

    [DataType(DataType.Date)]
    [Display(ResourceType = typeof (DisplayNameResources), Name = "EntryDate")]
    public DateTime? EntryDate { get; set; }

    [Display(ResourceType = typeof (DisplayNameResources), Name = "State")]
    public MeasureState State { get; set; }

    [Display(ResourceType = typeof (DisplayNameResources), Name = "Priority")]
		[ToolTip(typeof(ToolTipResources), "Measure_Priority")]
    public Priority Priority { get; set; }

    [Display(ResourceType = typeof(DisplayNameResources), Name = "MeasureType")]
    public MeasureType MeasureType { get; set; }
    
    [DataType(DataType.MultilineText)]
    [Display(ResourceType = typeof (DisplayNameResources), Name = "Annotation")]
		[ToolTip(typeof(ToolTipResources), "Measure_Annotation")]
    public string Annotation { get; set; }

    [HiddenInput(DisplayValue = false)]
    public virtual ICollection<Idea> Ideas { get; set; }
  }
}