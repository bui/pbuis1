using System.ComponentModel.DataAnnotations;

namespace OpEnMs.Domain.Entities
{
	public class Login
	{
		[Required]
		[Display(Name = "Benutzername")]
		public string UserName { get; set; }

		[Required]
		[DataType(DataType.Password)]
		[Display(Name = "Kennwort")]
		public string Password { get; set; }

		[Display(Name = "Speichern?")]
		public bool RememberMe { get; set; }
	}
}