﻿#region Licence

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0.html
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// 
// Copyright (c) 2013, HTW Berlin

#endregion

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using OpEnMs.Resources;

namespace OpEnMs.Domain.Entities
{
	public class Network
	{
		[HiddenInput(DisplayValue = false)]
		public int Id { get; set; }

		[Required(ErrorMessageResourceName = "NameIsRequired", ErrorMessageResourceType = typeof(ErrorMessageResources))]
		[Display(ResourceType = typeof(DisplayNameResources), Name = "Name")]
		public string Name { get; set; }

		[DataType(DataType.MultilineText)]
		[Display(ResourceType = typeof(DisplayNameResources), Name = "Description")]
		public string Description { get; set; }

		[DataType(DataType.Date)]
		[Display(ResourceType = typeof(DisplayNameResources), Name = "StartDate")]
		public virtual DateTime StartDate { get; set; }

		[DataType(DataType.Date)]
		[Display(ResourceType = typeof(DisplayNameResources), Name = "EndDate")]
		public virtual DateTime EndDate { get; set; }

        [Display(ResourceType = typeof(DisplayNameResources), Name = "State")]
        public NetworkState State { get; set; }

        //[HiddenInput(DisplayValue = false)]
        //public virtual int StateValue { get; set; }

		public virtual ICollection<Company> Companies { get; set; }
	}
}