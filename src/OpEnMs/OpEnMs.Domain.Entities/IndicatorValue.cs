﻿#region Licence

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0.html
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// 
// Copyright (c) 2013, HTW Berlin

#endregion

using System;

namespace OpEnMs.Domain.Entities
{
    public class IndicatorValue: Reading
    {
        public DateTime Created { get; set; }

        public UserProfile CreatedBy { get; set; }

        public DateTime LastEdited { get; set; }

        public UserProfile LastEditedBy { get; set; }

        public virtual Indicator Indicator { get; set; }
    }
}