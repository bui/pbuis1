﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using OpEnMs.Domain.Entities.Attributes;
using OpEnMs.Resources;

namespace OpEnMs.Domain.Entities
{
	public class Idea
	{
		[HiddenInput(DisplayValue = false)]
		public int Id { get; set; }

		[Required(ErrorMessageResourceName = "NameIsRequired", ErrorMessageResourceType = typeof (ErrorMessageResources))]
		[StringLength(40, ErrorMessageResourceName = "NameNotLongerThan40Characters", ErrorMessageResourceType = typeof (ErrorMessageResources))]
		[Display(ResourceType = typeof (DisplayNameResources), Name = "Name")]
		[PlaceHolder(typeof(DisplayNameResources), "Name")]
		[ToolTip(typeof(ToolTipResources), "Idea_Name")]
		[HelpPopover(typeof(HelpPopoverResources), "Idea_Name")]
		public string Name { get; set; }

		[DataType(DataType.MultilineText)]
		[Display(ResourceType = typeof (DisplayNameResources), Name = "Description")]
		[Required(ErrorMessageResourceName = "DescriptionIsRequired", ErrorMessageResourceType = typeof (ErrorMessageResources))]
		[PlaceHolder(typeof(DisplayNameResources), "Description")]
		[ToolTip(typeof(ToolTipResources), "Idea_Description")]
		public string Description { get; set; }

		[DataType(DataType.MultilineText)]
		[Display(ResourceType = typeof (DisplayNameResources), Name = "Solution")]
		[Required(ErrorMessageResourceName = "SolutionIsRequired", ErrorMessageResourceType = typeof (ErrorMessageResources))]
		[PlaceHolder(typeof(DisplayNameResources), "Solution")]
		[ToolTip(typeof(ToolTipResources), "Idea_Solution")]
		public string Solution { get; set; }

		[Display(ResourceType = typeof (DisplayNameResources), Name = "Valuation")]
		public virtual Valuation Valuation { get; set; }

		[HiddenInput(DisplayValue = false)]
		public virtual ICollection<Measure> Measures { get; set; }

		[HiddenInput(DisplayValue = false)]
		public virtual ICollection<IsoMeasure> IsoMeasures { get; set; }

		[HiddenInput(DisplayValue = false)]
        [Display(ResourceType = typeof(DisplayNameResources), Name = "IdeaState")]
		public virtual IdeaState IdeaState { get; set; }

		[HiddenInput(DisplayValue = false)]
		public virtual UserProfile Creator { get; set; }

		[DataType(DataType.MultilineText)]
		[Display(ResourceType = typeof (DisplayNameResources), Name = "Annotation")]
		public string Annotation { get; set; }
	}
}