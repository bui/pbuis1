﻿#region Licence

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0.html
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// 
// Copyright (c) 2013, HTW Berlin

#endregion

using System;
using System.ComponentModel.DataAnnotations;
using OpEnMs.Domain.Entities.Attributes;
using OpEnMs.Resources;

namespace OpEnMs.Domain.Entities
{
    public class Topic
    {
        public int Id { get; set; }

        [Required(ErrorMessageResourceName = "TopicIsRequired", ErrorMessageResourceType = typeof (ErrorMessageResources))]
        [Display(ResourceType = typeof (DisplayNameResources), Name = "Topic")]
        [PlaceHolder(typeof (DisplayNameResources), "Topic")]
        [Unique]
        public String Name { get; set; }
    }
}