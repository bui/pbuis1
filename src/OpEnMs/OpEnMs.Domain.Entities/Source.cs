﻿using System.Web.Mvc;

namespace OpEnMs.Domain.Entities
{
	public class Source
	{
		[HiddenInput(DisplayValue = false)]
		public int Id { get; set; }

		public string Name { get; set; }

		public double CarbonCoefficient { get; set; }
	}
}