﻿using System.Collections;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;
using System.Dynamic;
using System.Web.Mvc;
using Emporer.Unit;
using OpEnMs.Domain.Entities.Attributes;
using OpEnMs.Resources;

namespace OpEnMs.Domain.Entities
{
	public class Valuation
	{
		[HiddenInput(DisplayValue = false)]
		public int Id { get; set; }

		[DataType(DataType.Currency)]
		[Display(ResourceType = typeof (DisplayNameResources), Name = "Investment")]
		public decimal? Investment { get; set; }

		[Display(ResourceType = typeof (DisplayNameResources), Name = "EnergeticSavings")]
		public double? EnergeticSavings { get; set; }

		[DataType(DataType.Currency)]
		[Display(ResourceType = typeof (DisplayNameResources), Name = "MonetarySavings")]
		public decimal? MonetarySavings { get; set; }

		[Display(ResourceType = typeof(DisplayNameResources), Name = "CarbonReduction")]
		public double? CarbonReduction { get; set; }

		[Display(ResourceType = typeof (DisplayNameResources), Name = "Lifetime")]
		public double? Lifetime { get; set; }

		[Display(ResourceType = typeof (DisplayNameResources), Name = "InterestRate")]
		public double? InterestRate { get; set; }

		[Display(ResourceType = typeof(DisplayNameResources), Name = "DynamicAmortization")]
		public double? DynamicAmortization { get; set; }

		[Display(ResourceType = typeof(DisplayNameResources), Name = "StaticAmortization")]
		public double? StatisticAmortization { get; set; }

		//[Display(ResourceType = typeof(DisplayNameResources), Name = "InternalInterestRate")]
		//public double? InternalInterestRate { get; set; }

		[Display(ResourceType = typeof(DisplayNameResources), Name = "Source")]
		public virtual Source Source { get; set; }

		[Display(ResourceType = typeof (DisplayNameResources), Name = "Unit")]
		public virtual Unit Unit { get; set; }
	}
}