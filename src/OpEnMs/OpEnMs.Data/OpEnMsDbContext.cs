#region Licence

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0.html
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// 
// Copyright (c) 2013, HTW Berlin

#endregion

using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Data.Objects;
using Emporer.Unit;
using OpEnMs.Data.Validators;
using OpEnMs.Domain.Entities;

namespace OpEnMs.Data
{
    public class OpEnMsDbContext: DbContext,
                                  IOpEnMsDbContext
    {
        public OpEnMsDbContext()
            : base("OpEnMsDbContext")
        {
            //Configuration.LazyLoadingEnabled = false;
        }


        protected override DbEntityValidationResult ValidateEntity(DbEntityEntry entityEntry, IDictionary<object, object> items)
        {
            var errors = new List<DbValidationError>();
            Type type = ObjectContext.GetObjectType(entityEntry.Entity.GetType());

            errors.AddRange(UniqueValidator.ValidateEntity(this, entityEntry, type));

            var myItems = new Dictionary<object, object>{
                                                            {
                                                                "Context", this
                                                            }
                                                        };
            errors.AddRange(base.ValidateEntity(entityEntry,
                                                myItems).
                                 ValidationErrors);

            return new DbEntityValidationResult(entityEntry,
                                                errors);
        }

        public DbSet<UserProfile> UserProfiles { get; set; }
        public DbSet<Company> Companies { get; set; }
        public DbSet<Network> Networks { get; set; }
        public DbSet<MeasuringPoint> MeasuringPoints { get; set; }
        public DbSet<Measure> Measures { get; set; }
        public DbSet<IsoMeasure> IsoMeasures { get; set; }
        public DbSet<EnergyConsumption> EnergyConsumptions { get; set; }
        public DbSet<Valuation> Valuations { get; set; }
        public DbSet<Source> Sources { get; set; }
        public DbSet<Reading> Readings { get; set; }
        public DbSet<Unit> Units { get; set; }
        public DbSet<Idea> Ideas { get; set; }
        public DbSet<Indicator> Indicators { get; set; }
        public DbSet<Topic> Topics { get; set; }
    }
}