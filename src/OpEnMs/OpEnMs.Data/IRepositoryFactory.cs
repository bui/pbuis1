﻿#region Licence

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0.html
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// 
// Copyright (c) 2013, HTW Berlin

#endregion

using OpEnMs.Domain.RepositoryContracts;

namespace OpEnMs.Data
{
	public interface IRepositoryFactory
	{
		IUserProfileRepository CreateUserProfileRepository(IOpEnMsDbContext opEnMsDbContext);
		INetworkRepository CreateNetworkRepository(IOpEnMsDbContext opEnMsDbContext);
		ICompanyRepository CreateCompanyRepository(IOpEnMsDbContext opEnMsDbContext);
		IMeasuringPointRepository CreateMeasuringPointRepository(IOpEnMsDbContext opEnMsDbContext);
		IReadingRepository CreateReadingRepository(IOpEnMsDbContext opEnMsDbContext);
		ISourceRepository CreateSourceRepository(IOpEnMsDbContext opEnMsDbContext);
		IUnitRepository CreateUnitRepository(IOpEnMsDbContext opEnMsDbContext);
		IEnergyConsumptionRepository CreateEnergyConsumptionRepository(IOpEnMsDbContext opEnMsDbContext);
		IMeasureRepository CreateMeasureRepository(IOpEnMsDbContext opEnMsDbContext);
		IIsoMeasureRepository CreateIsoMeasureRepository(IOpEnMsDbContext opEnMsDbContext);
		IIdeaRepository CreateIdeaRepository(IOpEnMsDbContext opEnMsDbContext);
		IValuationRepository CreateValuationRepository(IOpEnMsDbContext opEnMsDbContext);
		IIndicatorRepository CreateIndicatorRepository(IOpEnMsDbContext opEnMsDbContext);
		IIndicatorValueRepository CreateIndicatorValueRepository(IOpEnMsDbContext opEnMsDbContext);
		ITopicRepository CreateTopicRepository(IOpEnMsDbContext opEnMsDbContext);
	}
}