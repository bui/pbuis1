﻿#region Licence

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0.html
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// 
// Copyright (c) 2013, HTW Berlin

#endregion

using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using Emporer.Unit;
using OpEnMs.Domain.Entities;

namespace OpEnMs.Data
{
    public interface IOpEnMsDbContext
    {
        DbSet<T> Set<T>() where T : class;
        DbSet<UserProfile> UserProfiles { get; set; }
        DbSet<Company> Companies { get; set; }
        DbSet<Network> Networks { get; set; }
        DbSet<MeasuringPoint> MeasuringPoints { get; set; }
        DbSet<Measure> Measures { get; set; }
        DbSet<IsoMeasure> IsoMeasures { get; set; }
        DbSet<EnergyConsumption> EnergyConsumptions { get; set; }
        DbSet<Valuation> Valuations { get; set; }
        DbSet<Source> Sources { get; set; }
        DbSet<Reading> Readings { get; set; }
        DbSet<Unit> Units { get; set; }
        DbSet<Idea> Ideas { get; set; }
        DbSet<Indicator> Indicators { get; set; }
        DbSet<Topic> Topics { get; set; }
        DbEntityEntry Entry(object o);
        void Dispose();
        int SaveChanges();
    }
}