﻿#region Licence

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0.html
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// 
// Copyright (c) 2013, HTW Berlin

#endregion

using OpEnMs.Data.Repositories;
using OpEnMs.Domain.RepositoryContracts;

namespace OpEnMs.Data
{
	public class RepositoryFactory: IRepositoryFactory
	{
		public IUserProfileRepository CreateUserProfileRepository(IOpEnMsDbContext opEnMsDbContext)
		{
			return new UserProfileRepository(opEnMsDbContext);
		}

		public INetworkRepository CreateNetworkRepository(IOpEnMsDbContext opEnMsDbContext)
		{
			return new NetworkRepository(opEnMsDbContext);
		}

		public ICompanyRepository CreateCompanyRepository(IOpEnMsDbContext opEnMsDbContext)
		{
			return new CompanyRepository(opEnMsDbContext);
		}

		public IMeasuringPointRepository CreateMeasuringPointRepository(IOpEnMsDbContext opEnMsDbContext)
		{
			return new MeasuringPointRepository(opEnMsDbContext);
		}

		public IReadingRepository CreateReadingRepository(IOpEnMsDbContext opEnMsDbContext)
		{
			return new ReadingRepository(opEnMsDbContext);
		}

		public ISourceRepository CreateSourceRepository(IOpEnMsDbContext opEnMsDbContext)
		{
			return new SourceRepository(opEnMsDbContext);
		}

		public IUnitRepository CreateUnitRepository(IOpEnMsDbContext opEnMsDbContext)
		{
			return new UnitRepository(opEnMsDbContext);
		}

		public IEnergyConsumptionRepository CreateEnergyConsumptionRepository(IOpEnMsDbContext opEnMsDbContext)
		{
			return new EnergyConsumptionRepository(opEnMsDbContext);
		}

		public IMeasureRepository CreateMeasureRepository(IOpEnMsDbContext opEnMsDbContext)
		{
			return new MeasureRepository(opEnMsDbContext);
		}

		public IIsoMeasureRepository CreateIsoMeasureRepository(IOpEnMsDbContext opEnMsDbContext)
		{
			return new IsoMeasureRepository(opEnMsDbContext);
		}

		public IIdeaRepository CreateIdeaRepository(IOpEnMsDbContext opEnMsDbContext)
		{
			return new IdeaRepository(opEnMsDbContext);
		}

		public IValuationRepository CreateValuationRepository(IOpEnMsDbContext opEnMsDbContext)
		{
			return new ValuationRepository(opEnMsDbContext);
		}

		public IIndicatorRepository CreateIndicatorRepository(IOpEnMsDbContext opEnMsDbContext)
		{
			return new IndicatorRepository(opEnMsDbContext);
		}

		public IIndicatorValueRepository CreateIndicatorValueRepository(IOpEnMsDbContext opEnMsDbContext)
		{
			return new IndicatorValueRepository(opEnMsDbContext);
		}

		public ITopicRepository CreateTopicRepository(IOpEnMsDbContext opEnMsDbContext)
		{
			return new TopicRepository(opEnMsDbContext);
		}
	}
}