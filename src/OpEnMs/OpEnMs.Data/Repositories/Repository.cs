﻿#region Licence

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0.html
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// 
// Copyright (c) 2013, HTW Berlin

#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using OpEnMs.Domain.RepositoryContracts;

namespace OpEnMs.Data.Repositories
{
	public class Repository<TEntity>: IRepository<TEntity> where TEntity : class
	{
		protected readonly IOpEnMsDbContext m_Context;
		private readonly IDbSet<TEntity> m_DbSet;

		public Repository(IOpEnMsDbContext context)
		{
			m_Context = context;
			m_DbSet = context.Set<TEntity>();
		}

		public virtual IEnumerable<TEntity> Get(Expression<Func<TEntity, bool>> filter = null, Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null, string includeProperties = "")
		{
			IQueryable<TEntity> query = m_DbSet;

			if (filter != null)
			{
				query = query.Where(filter);
			}

			foreach (var includeProperty in includeProperties.Split(new[]{
				                                                             ','
			                                                             },
			                                                        StringSplitOptions.RemoveEmptyEntries))
			{
				query = query.Include(includeProperty);
			}

			if (orderBy != null)
			{
				return orderBy(query).
					ToList();
			}
			else
			{
				return query.ToList();
			}
		}


		public TEntity GetById(object id)
		{
			return m_DbSet.Find(id);
		}

		public virtual void Insert(TEntity entity)
		{
			m_DbSet.Add(entity);
		}

		public virtual void Delete(object id)
		{
			TEntity entityToDelete = m_DbSet.Find(id);
			Delete(entityToDelete);
		}

		public virtual void Delete(TEntity entityToDelete)
		{
			if (m_Context.Entry(entityToDelete).
			              State == EntityState.Detached)
			{
				m_DbSet.Attach(entityToDelete);
			}
			m_DbSet.Remove(entityToDelete);
		}

		public virtual void Update(TEntity entityToUpdate)
		{
			m_DbSet.Attach(entityToUpdate);
			m_Context.Entry(entityToUpdate).
			          State = EntityState.Modified;
		}
	}
}