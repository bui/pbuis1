﻿#region Licence

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0.html
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// 
// Copyright (c) 2013, HTW Berlin

#endregion

using System;
using OpEnMs.Domain.RepositoryContracts;
using OpEnMs.Domain.UnitOfWorkContract;

namespace OpEnMs.Data.UnitOfWork
{
	public class UnitOfWork: IUnitOfWork
	{
		private bool m_Disposed;

		private readonly IOpEnMsDbContext m_Context;

		private readonly IRepositoryFactory m_RepositoryFactory;

		private IUserProfileRepository m_UserProfileRepository;
		private ICompanyRepository m_CompanyRepository;
		private INetworkRepository m_NetworkRepository;
		private IMeasuringPointRepository m_MeasuringPointRepository;
		private IReadingRepository m_ReadingRepository;
		private ISourceRepository m_SourceRepository;
		private IUnitRepository m_UnitRepository;
		private IIsoMeasureRepository m_IsoMeasureRepository;
		private IMeasureRepository m_MeasureRepository;
		private IEnergyConsumptionRepository m_EnergyConsumptionRepository;
		private IIdeaRepository m_IdeaRepository;
		private IValuationRepository m_ValuationRepository;
		private IIndicatorValueRepository m_IndicatorValueRepository;
		private IIndicatorRepository m_IndicatorRepository;
		private ITopicRepository m_TopicRepository;


		public UnitOfWork(IOpEnMsDbContext context, IRepositoryFactory repositoryFactory)
		{
			m_Context = context;
			m_RepositoryFactory = repositoryFactory;
		}

		public IUserProfileRepository UserProfileRepository
		{
			get
			{
				if (m_UserProfileRepository == null)
				{
					m_UserProfileRepository = m_RepositoryFactory.CreateUserProfileRepository(m_Context);
				}
				return m_UserProfileRepository;
			}
		}

		public ICompanyRepository CompanyRepository
		{
			get
			{
				if (m_CompanyRepository == null)
				{
					m_CompanyRepository = m_RepositoryFactory.CreateCompanyRepository(m_Context);
				}
				return m_CompanyRepository;
			}
		}

		public INetworkRepository NetworkRepository
		{
			get
			{
				if (m_NetworkRepository == null)
				{
					m_NetworkRepository = m_RepositoryFactory.CreateNetworkRepository(m_Context);
				}
				return m_NetworkRepository;
			}
		}

		public IMeasuringPointRepository MeasuringPointRepository
		{
			get
			{
				if (m_MeasuringPointRepository == null)
				{
					m_MeasuringPointRepository = m_RepositoryFactory.CreateMeasuringPointRepository(m_Context);
				}
				return m_MeasuringPointRepository;
			}
		}

		public IMeasureRepository MeasureRepository
		{
			get
			{
				if (m_MeasureRepository == null)
				{
					m_MeasureRepository = m_RepositoryFactory.CreateMeasureRepository(m_Context);
				}
				return m_MeasureRepository;
			}
		}

		public IIsoMeasureRepository IsoMeasureRepository
		{
			get
			{
				if (m_IsoMeasureRepository == null)
				{
					m_IsoMeasureRepository = m_RepositoryFactory.CreateIsoMeasureRepository(m_Context);
				}
				return m_IsoMeasureRepository;
			}
		}

		public IIdeaRepository IdeaRepository
		{
			get
			{
				if (m_IdeaRepository == null)
				{
					m_IdeaRepository = m_RepositoryFactory.CreateIdeaRepository(m_Context);
				}
				return m_IdeaRepository;
			}
		}

		public IEnergyConsumptionRepository EnergyConsumptionRepository
		{
			get
			{
				if (m_EnergyConsumptionRepository == null)
				{
					m_EnergyConsumptionRepository = m_RepositoryFactory.CreateEnergyConsumptionRepository(m_Context);
				}
				return m_EnergyConsumptionRepository;
			}
		}


		public IUnitRepository UnitRepository
		{
			get
			{
				if (m_UnitRepository == null)
				{
					m_UnitRepository = m_RepositoryFactory.CreateUnitRepository(m_Context);
				}
				return m_UnitRepository;
			}
		}

		public ISourceRepository SourceRepository
		{
			get
			{
				if (m_SourceRepository == null)
				{
					m_SourceRepository = m_RepositoryFactory.CreateSourceRepository(m_Context);
				}
				return m_SourceRepository;
			}
		}

		public IReadingRepository ReadingRepository
		{
			get
			{
				if (m_ReadingRepository == null)
				{
					m_ReadingRepository = m_RepositoryFactory.CreateReadingRepository(m_Context);
				}
				return m_ReadingRepository;
			}
		}

		public IValuationRepository ValuationRepository
		{
			get
			{
				if (m_ValuationRepository == null)
				{
					m_ValuationRepository = m_RepositoryFactory.CreateValuationRepository(m_Context);
				}
				return m_ValuationRepository;
			}
		}

		public IIndicatorValueRepository IndicatorValueRepository
		{
			get
			{
				if (m_IndicatorValueRepository == null)
				{
					m_IndicatorValueRepository = m_RepositoryFactory.CreateIndicatorValueRepository(m_Context);
				}
				return m_IndicatorValueRepository;
			}
		}

		public IIndicatorRepository IndicatorRepository
		{
			get
			{
				if (m_IndicatorRepository == null)
				{
					m_IndicatorRepository = m_RepositoryFactory.CreateIndicatorRepository(m_Context);
				}
				return m_IndicatorRepository;
			}
		}

		public ITopicRepository TopicRepository
		{
			get
			{
				if (m_TopicRepository == null)
				{
					m_TopicRepository = m_RepositoryFactory.CreateTopicRepository(m_Context);
				}
				return m_TopicRepository;
			}
		}


		public void Save()
		{
			m_Context.SaveChanges();
		}


		protected virtual void Dispose(bool disposing)
		{
			if (!m_Disposed)
			{
				if (disposing)
				{
					m_Context.Dispose();
				}
			}
			m_Disposed = true;
		}

		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}
	}
}