#region Licence

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0.html
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// 
// Copyright (c) 2013, HTW Berlin

#endregion

using System.Linq;
using System.Net;
using System.Web.Http;
using System.Web.Http.OData;
using OpEnMs.Application.Services.CompanyManagement;
using OpEnMs.Domain.Entities;

namespace OpEnMs.OData.Controllers
{
	[Authorize]
	public class CompaniesController: EntitySetController<Company, int>

	{
		private readonly ICompanyManagementService m_CompanyManagementService;

		public CompaniesController(ICompanyManagementService companyManagementService)
		{
			m_CompanyManagementService = companyManagementService;
		}

		public override IQueryable<Company> Get()
		{
			return m_CompanyManagementService.Get().
			                                  AsQueryable();
		}

		protected override Company GetEntityByKey(int key)
		{
			return m_CompanyManagementService.FindById(key);
		}

		protected override Company CreateEntity(Company entity)
		{
			m_CompanyManagementService.AddCompany(entity);
			return entity;
		}

		protected override int GetKey(Company entity)
		{
			return entity.Id;
		}

		protected override Company UpdateEntity(int key, Company update)
		{
			// Verify that a product with this ID exists.
			if (m_CompanyManagementService.FindById(key) == null)
			{
				throw new HttpResponseException(HttpStatusCode.NotFound);
			}

			m_CompanyManagementService.UpdateCompany(update); // Replace the existing entity in the DbSet.
			return update;
		}

		protected override Company PatchEntity(int key, Delta<Company> patch)
		{
			Company company = m_CompanyManagementService.FindById(key);
			if (company == null)
			{
				throw new HttpResponseException(HttpStatusCode.NotFound);
			}
			patch.Patch(company);
			m_CompanyManagementService.UpdateCompany(company);
			return company;
		}

		public override void Delete([FromODataUri] int key)
		{
			Company company = m_CompanyManagementService.FindById(key);
			if (company == null)
			{
				throw new HttpResponseException(HttpStatusCode.NotFound);
			}
			m_CompanyManagementService.DeleteCompany(key);
		}
	}
}