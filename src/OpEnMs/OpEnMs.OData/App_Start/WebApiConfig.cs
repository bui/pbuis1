#region Licence

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0.html
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// 
// Copyright (c) 2013, HTW Berlin

#endregion

using System.Web.Http;
using System.Web.Http.OData.Builder;
using Microsoft.Data.Edm;
using OpEnMs.Domain.Entities;

namespace OpEnMs.OData.App_Start
{
	public static class WebApiConfig
	{
		public static void Register(HttpConfiguration config)
		{
			ODataModelBuilder modelBuilder = new ODataConventionModelBuilder();
			modelBuilder.EntitySet<Company>("Companies");

			IEdmModel model = modelBuilder.GetEdmModel();
			config.Routes.MapODataRoute("ODataRoute",
			                            "odata",
			                            model);

			config.Routes.MapHttpRoute(name : "DefaultApi",
			                           routeTemplate : "api/{controller}/{id}",
			                           defaults : new{
				                                         id = RouteParameter.Optional
			                                         });


			// Heben Sie die Kommentierung der folgenden Codezeile auf, um Abfrageunterst�tzung f�r Aktionen mit dem R�ckgabetyp "IQueryable" oder "IQueryable<T>" zu aktivieren.
			// Damit die Verarbeitung unerwarteter oder b�swilliger Abfragen vermieden wird, verwenden Sie die �berpr�fungseinstellungen f�r "QueryableAttribute" zum �berpr�fen eingehender Abfragen.
			// Weitere Informationen finden Sie unter "http://go.microsoft.com/fwlink/?LinkId=279712".
			//config.EnableQuerySupport();
		}
	}
}