﻿#region Licence

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0.html
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// 
// Copyright (c) 2013, HTW Berlin

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using OpEnMs.Domain.Entities;
using OpEnMs.Domain.RepositoryContracts;
using OpEnMs.Domain.UnitOfWorkContract;

namespace OpEnMs.Application.Services.ReadingManagement
{
	public class ReadingManagementService: IReadingManagementService
	{
		private readonly IUnitOfWork m_UnitOfWork;
		private readonly IReadingRepository m_ReadingRepository;
		private readonly DomainEntityFactory m_DomainEntityFactory;
		private readonly IUserProfileRepository m_UserProfileRepository;

		public ReadingManagementService(IUnitOfWork unitOfWork, DomainEntityFactory domainEntityFactory)
		{
			m_UnitOfWork = unitOfWork;
			m_ReadingRepository = m_UnitOfWork.ReadingRepository;
			m_UserProfileRepository = m_UnitOfWork.UserProfileRepository;
			m_DomainEntityFactory = domainEntityFactory;
		}

		public IEnumerable<Reading> Get(Expression<Func<Reading, bool>> filter = null, Func<IQueryable<Reading>, IOrderedQueryable<Reading>> orderBy = null, string includeProperties = "")
		{
			return m_ReadingRepository.Get(filter,
			                               orderBy,
			                               includeProperties);
		}

		public IEnumerable<UserProfile> GetUserProfiles(Expression<Func<UserProfile, bool>> filter = null, Func<IQueryable<UserProfile>, IOrderedQueryable<UserProfile>> orderBy = null, string includeProperties = "")
		{
			return m_UserProfileRepository.Get(filter,
			                                   orderBy,
			                                   includeProperties);
		}

		public void AddReading(Reading reading)
		{
			m_ReadingRepository.Insert(reading);
			m_UnitOfWork.Save();
		}

		public Reading FindById(int id)
		{
			return m_ReadingRepository.GetById(id);
		}

		public Reading CreateReading()
		{
			return m_DomainEntityFactory.CreateReading();
		}

		public void UpdateReading(Reading reading)
		{
			m_ReadingRepository.Update(reading);
			m_UnitOfWork.Save();
		}

		public void DeleteReading(int id)
		{
			var readings = m_ReadingRepository.Get(); //Properties müssen noch eingebunden werden oder Lazyloading deaktivieren
			var reading = readings.FirstOrDefault(c => c.Id == id);

			m_ReadingRepository.Delete(reading);
			m_UnitOfWork.Save();
		}

		public void Dispose()
		{
			m_UnitOfWork.Dispose();
		}
	}
}