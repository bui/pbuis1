﻿#region Licence

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0.html
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// 
// Copyright (c) 2013, HTW Berlin

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using OpEnMs.Domain.Entities;
using OpEnMs.Domain.RepositoryContracts;
using OpEnMs.Domain.UnitOfWorkContract;

namespace OpEnMs.Application.Services.IsoMeasureManagement
{
	public class IsoMeasureManagementService: IIsoMeasureManagementService
	{
		private readonly IUnitOfWork m_UnitOfWork;
		private readonly IIsoMeasureRepository m_IsoMeasureRepository;
		private readonly DomainEntityFactory m_DomainEntityFactory;
		private readonly IUserProfileRepository m_UserProfileRepository;
		private readonly ICompanyRepository m_CompanyRepository;
		private readonly IIdeaRepository m_IdeaRepository;

		public IsoMeasureManagementService(IUnitOfWork unitOfWork, DomainEntityFactory domainEntityFactory)
		{
			m_UnitOfWork = unitOfWork;
			m_IsoMeasureRepository = m_UnitOfWork.IsoMeasureRepository;
			m_UserProfileRepository = m_UnitOfWork.UserProfileRepository;
			m_DomainEntityFactory = domainEntityFactory;
			m_CompanyRepository = m_UnitOfWork.CompanyRepository;
			m_IdeaRepository = m_UnitOfWork.IdeaRepository;
		}

		public IEnumerable<IsoMeasure> Get(Expression<Func<IsoMeasure, bool>> filter = null, Func<IQueryable<IsoMeasure>, IOrderedQueryable<IsoMeasure>> orderBy = null, string includeProperties = "")
		{
			return m_IsoMeasureRepository.Get(filter,
				orderBy,
				includeProperties);
		}

		public IEnumerable<UserProfile> GetUserProfiles(Expression<Func<UserProfile, bool>> filter = null, Func<IQueryable<UserProfile>, IOrderedQueryable<UserProfile>> orderBy = null, string includeProperties = "")
		{
			return m_UserProfileRepository.Get(filter,
				orderBy,
				includeProperties);
		}

		public Company GetCompanyByUserIdentity(string userIdentity)
		{
			var userProfile = m_UserProfileRepository.Get().
				FirstOrDefault(u => u.UserName.Equals(userIdentity));
			var company = m_CompanyRepository.Get().
				FirstOrDefault(c => c.Employees.Contains(userProfile));
			return company;
		}

		public void AddIsoMeasure(IsoMeasure isoMeasure, int companyId)
		{
			var company = m_CompanyRepository.GetById(companyId);
			m_IsoMeasureRepository.Insert(isoMeasure);
			company.IsoMeasures.Add(isoMeasure);
			m_CompanyRepository.Update(company);
			
			m_UnitOfWork.Save();
		}

		public IsoMeasure FindById(int id)
		{
			return m_IsoMeasureRepository.GetById(id);
		}

		public IsoMeasure CreateIsoMeasure()
		{
			return m_DomainEntityFactory.CreateIsoMeasure();
		}

		public void UpdateIsoMeasure(IsoMeasure isoMeasure)
		{
			m_IsoMeasureRepository.Update(isoMeasure);
			m_UnitOfWork.Save();
		}

		public void DeleteIsoMeasure(int id)
		{
			var isoMeasures = m_IsoMeasureRepository.Get(); //TODO: Properties müssen noch eingebunden werden oder Lazyloading deaktivieren
			var isoMeasure = isoMeasures.FirstOrDefault(c => c.Id == id);

			m_IsoMeasureRepository.Delete(isoMeasure);
			m_UnitOfWork.Save();
		}

		public void AddIdea(IsoMeasure isoMeasure, Idea idea)
		{
			isoMeasure.Ideas.Add(idea);
			m_UnitOfWork.Save();
		}


		public Idea FindIdeaById(int id)
		{
			return m_IdeaRepository.GetById(id);
		}

		public bool IdeaIsInMeasure(Idea idea)
		{
			foreach (IsoMeasure isoMeasure in m_IsoMeasureRepository.Get())
			{
				if (isoMeasure.Ideas != null)
				{
					if (isoMeasure.Ideas.Contains(idea))
					{
						return true;
					}
				}
			}

			return false;
		}

	    public IEnumerable<IsoMeasure> GetIsoMeasuresByUserIdentity(string name)
	    {
            var userProfile = m_UserProfileRepository.Get().FirstOrDefault(u => u.UserName.Equals(name));

            if (userProfile == null)
            {
                throw new NullReferenceException("User Profile does not exist");
            }

            var isoMeasures = m_IsoMeasureRepository.Get().Where(u => u.Creator.UserId.Equals(userProfile.UserId));

            return isoMeasures;
	    }

	    public void Dispose()

		{
			m_UnitOfWork.Dispose();
		}
	}
}