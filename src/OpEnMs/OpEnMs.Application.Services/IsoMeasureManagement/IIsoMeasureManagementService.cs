﻿#region Licence

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0.html
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// 
// Copyright (c) 2013, HTW Berlin

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using OpEnMs.Domain.Entities;

namespace OpEnMs.Application.Services.IsoMeasureManagement
{
	public interface IIsoMeasureManagementService: IDisposable
	{
		IEnumerable<IsoMeasure> Get(Expression<Func<IsoMeasure, bool>> filter = null, Func<IQueryable<IsoMeasure>, IOrderedQueryable<IsoMeasure>> orderBy = null, string includeProperties = "");
		IEnumerable<UserProfile> GetUserProfiles(Expression<Func<UserProfile, bool>> filter = null, Func<IQueryable<UserProfile>, IOrderedQueryable<UserProfile>> orderBy = null, string includeProperties = "");
		void AddIsoMeasure(IsoMeasure isoMeasure, int companyId);
		Company GetCompanyByUserIdentity(string userIdentity);
		IsoMeasure FindById(int id);
		IsoMeasure CreateIsoMeasure();
		void UpdateIsoMeasure(IsoMeasure isoMeasure);
		void DeleteIsoMeasure(int id);
		Idea FindIdeaById(int id);
		bool IdeaIsInMeasure(Idea idea);
		IEnumerable<IsoMeasure> GetIsoMeasuresByUserIdentity(string name);
		void AddIdea(IsoMeasure isoMeasure, Idea idea);
	}
}