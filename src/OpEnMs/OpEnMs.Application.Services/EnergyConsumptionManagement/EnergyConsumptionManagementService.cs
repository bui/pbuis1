﻿#region Licence

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0.html
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// 
// Copyright (c) 2013, HTW Berlin

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Emporer.Unit;
using OpEnMs.Domain.Entities;
using OpEnMs.Domain.RepositoryContracts;
using OpEnMs.Domain.UnitOfWorkContract;

namespace OpEnMs.Application.Services.EnergyConsumptionManagement
{
  public class EnergyConsumptionManagementService: IEnergyConsumptionManagementService
  {
    private readonly IUnitOfWork m_UnitOfWork;
    private readonly IEnergyConsumptionRepository m_EnergyConsumptionRepository;
    private readonly DomainEntityFactory m_DomainEntityFactory;
    private readonly IUserProfileRepository m_UserProfileRepository;
    private readonly ICompanyRepository m_CompanyRepository;
    private readonly ISourceRepository m_SourceRepository;
    private readonly IUnitRepository m_UnitRepository;

    public EnergyConsumptionManagementService(IUnitOfWork unitOfWork, DomainEntityFactory domainEntityFactory)
    {
      m_UnitOfWork = unitOfWork;
      m_EnergyConsumptionRepository = m_UnitOfWork.EnergyConsumptionRepository;
      m_UserProfileRepository = m_UnitOfWork.UserProfileRepository;
      m_CompanyRepository = m_UnitOfWork.CompanyRepository;
      m_UnitRepository = m_UnitOfWork.UnitRepository;
      m_SourceRepository = m_UnitOfWork.SourceRepository;
      m_DomainEntityFactory = domainEntityFactory;
    }

    public IEnumerable<EnergyConsumption> Get(Expression<Func<EnergyConsumption, bool>> filter = null, Func<IQueryable<EnergyConsumption>, IOrderedQueryable<EnergyConsumption>> orderBy = null, string includeProperties = "")
    {
      return m_EnergyConsumptionRepository.Get(filter,
        orderBy,
        includeProperties);
    }

    public IEnumerable<UserProfile> GetUserProfiles(Expression<Func<UserProfile, bool>> filter = null, Func<IQueryable<UserProfile>, IOrderedQueryable<UserProfile>> orderBy = null, string includeProperties = "")
    {
      return m_UserProfileRepository.Get(filter,
        orderBy,
        includeProperties);
    }

    public IEnumerable<Source> GetSources(Expression<Func<Source, bool>> filter = null, Func<IQueryable<Source>, IOrderedQueryable<Source>> orderBy = null, string includeProperties = "")
    {
      return m_SourceRepository.Get(filter,
        orderBy,
        includeProperties);
    }

    public IEnumerable<Unit> GetUnits(Expression<Func<Unit, bool>> filter = null, Func<IQueryable<Unit>, IOrderedQueryable<Unit>> orderBy = null, string includeProperties = "")
    {
      return m_UnitRepository.Get(filter,
        orderBy,
        includeProperties);
    }

    public Company GetCompanyByUserIdentity(string userIdentity)
    {
      var userProfile = m_UserProfileRepository.Get().
        FirstOrDefault(u => u.UserName.Equals(userIdentity));
      var company = m_CompanyRepository.Get().
        FirstOrDefault(c => c.Employees.Contains(userProfile));
      return company;
    }

    public void AddEnergyConsumption(EnergyConsumption energyConsumption, int companyId, int unitId, int sourceId)
    {
      var unit = m_UnitRepository.GetById(unitId);
      var source = m_SourceRepository.GetById(sourceId);
      var company = m_CompanyRepository.GetById(companyId);

      energyConsumption.Source = source;
      energyConsumption.Unit = unit;

      company.EnergyConsumptions.Add(energyConsumption);
      m_CompanyRepository.Update(company);

      m_EnergyConsumptionRepository.Insert(energyConsumption);
      m_UnitOfWork.Save();
    }

    public EnergyConsumption FindById(int id)
    {
      return m_EnergyConsumptionRepository.GetById(id);
    }

    public EnergyConsumption CreateEnergyConsumption()
    {
      return m_DomainEntityFactory.CreateEnergyConsumption();
    }

    public void UpdateEnergyConsumption(EnergyConsumption energyConsumption)
    {
      m_EnergyConsumptionRepository.Update(energyConsumption);
      m_UnitOfWork.Save();
    }

    public void DeleteEnergyConsumption(int id)
    {
      var energyConsumptions = m_EnergyConsumptionRepository.Get(includeProperties : "Readings"); //Properties müssen noch eingebunden werden oder Lazyloading deaktivieren
      var energyConsumption = energyConsumptions.FirstOrDefault(c => c.Id == id);

      m_EnergyConsumptionRepository.Delete(energyConsumption);
      m_UnitOfWork.Save();
    }

    public void Dispose()
    {
      m_UnitOfWork.Dispose();
    }
  }
}