﻿#region Licence

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0.html
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// 
// Copyright (c) 2013, HTW Berlin

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Emporer.Unit;
using OpEnMs.Domain.Entities;

namespace OpEnMs.Application.Services.EnergyConsumptionManagement
{
	public interface IEnergyConsumptionManagementService: IDisposable
	{
		IEnumerable<EnergyConsumption> Get(Expression<Func<EnergyConsumption, bool>> filter = null, Func<IQueryable<EnergyConsumption>, IOrderedQueryable<EnergyConsumption>> orderBy = null, string includeProperties = "");
		IEnumerable<UserProfile> GetUserProfiles(Expression<Func<UserProfile, bool>> filter = null, Func<IQueryable<UserProfile>, IOrderedQueryable<UserProfile>> orderBy = null, string includeProperties = "");
    IEnumerable<Source> GetSources(Expression<Func<Source, bool>> filter = null, Func<IQueryable<Source>, IOrderedQueryable<Source>> orderBy = null, string includeProperties = "");
    IEnumerable<Unit> GetUnits(Expression<Func<Unit, bool>> filter = null, Func<IQueryable<Unit>, IOrderedQueryable<Unit>> orderBy = null, string includeProperties = "");
    Company GetCompanyByUserIdentity(string userIdentity);
		void AddEnergyConsumption(EnergyConsumption energyConsumption, int companyId, int unitId, int sourceId);
		EnergyConsumption FindById(int id);
		EnergyConsumption CreateEnergyConsumption();
		void UpdateEnergyConsumption(EnergyConsumption energyConsumption);
		void DeleteEnergyConsumption(int id);
	}
}