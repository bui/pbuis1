﻿#region Licence

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0.html
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// 
// Copyright (c) 2013, HTW Berlin

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using OpEnMs.Domain.Entities;

namespace OpEnMs.Application.Services.UserProfileManagement
{
	public interface IUserProfileManagementService: IDisposable
	{
		IEnumerable<UserProfile> Get(Expression<Func<UserProfile, bool>> filter = null, Func<IQueryable<UserProfile>, IOrderedQueryable<UserProfile>> orderBy = null, string includeProperties = "");
    IEnumerable<Company> GetCompanies(Expression<Func<Company, bool>> filter = null, Func<IQueryable<Company>, IOrderedQueryable<Company>> orderBy = null, string includeProperties = "");
		void AddUserProfile(UserProfile userProfile);
		UserProfile FindById(int id);
		Company FindCompanyById(int id);
		UserProfile CreateUserProfile();
    Company FindCompanyByUser(UserProfile userProfile);
    IEnumerable<Object> FindByString(string term);
		void UpdateUserProfile(UserProfile userProfile);
		void UpdateCompany(Company company);
		void DeleteUserProfile(int id);
	}
}