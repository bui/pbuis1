﻿#region Licence

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0.html
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// 
// Copyright (c) 2013, HTW Berlin

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection.Emit;
using System.Web.Mvc;
using OpEnMs.Domain.Entities;
using OpEnMs.Domain.RepositoryContracts;
using OpEnMs.Domain.UnitOfWorkContract;

namespace OpEnMs.Application.Services.UserProfileManagement
{
	public class UserProfileManagementService: IUserProfileManagementService
	{
		private readonly IUnitOfWork m_UnitOfWork;
		private readonly IUserProfileRepository m_UserProfileRepository;
		private readonly ICompanyRepository m_CompanyRepository;
		private readonly DomainEntityFactory m_DomainEntityFactory;

		public UserProfileManagementService(IUnitOfWork unitOfWork, DomainEntityFactory domainEntityFactory)
		{
			m_UnitOfWork = unitOfWork;
			m_UserProfileRepository = m_UnitOfWork.UserProfileRepository;
		  m_CompanyRepository = m_UnitOfWork.CompanyRepository;
			m_DomainEntityFactory = domainEntityFactory;
		}

		public IEnumerable<UserProfile> Get(Expression<Func<UserProfile, bool>> filter = null, Func<IQueryable<UserProfile>, IOrderedQueryable<UserProfile>> orderBy = null, string includeProperties = "")
		{
			return m_UserProfileRepository.Get(filter,
			                                   orderBy,
			                                   includeProperties);
		}

    public IEnumerable<Company> GetCompanies(Expression<Func<Company, bool>> filter = null, Func<IQueryable<Company>, IOrderedQueryable<Company>> orderBy = null, string includeProperties = "")
	  {
      return m_CompanyRepository.Get(filter,
                                       orderBy,
                                       includeProperties);
	  }


	  public void AddUserProfile(UserProfile userProfile)
		{
			m_UserProfileRepository.Insert(userProfile);
			m_UnitOfWork.Save();
		}

		public UserProfile FindById(int id)
		{
			return m_UserProfileRepository.GetById(id);
		}

	  public Company FindCompanyById(int id)
	  {
	    return m_CompanyRepository.GetById(id);
	  }

	  public Company FindCompanyByUser(UserProfile userProfile)
	  {
      var company = m_CompanyRepository.Get().FirstOrDefault(c =>
      {
        var firstOrDefault = c.Employees.FirstOrDefault(e => e.UserId == userProfile.UserId);
        return firstOrDefault != null && firstOrDefault.UserId == userProfile.UserId;
      });

	    return company;
	  }

	  public IEnumerable<object> FindByString(string term)
		{
			var employees = m_UserProfileRepository.Get();
			// it would be better if we used here Query from Repository, if possible.
			var filteredEmployees = employees.Where(employee => employee.SurName.Contains(term) || employee.GivenName.Contains(term)).
																				Take(10).
																				Select(u => new
																				{
																					label = u.SurName + ", " + u.GivenName
																				});
			return filteredEmployees.ToList();
		}

		public UserProfile CreateUserProfile()
		{
			return m_DomainEntityFactory.CreateUserProfile();
		}

		public void UpdateUserProfile(UserProfile userProfile)
		{
			m_UserProfileRepository.Update(userProfile);
			m_UnitOfWork.Save();
		}

	  public void UpdateCompany(Company company)
	  {
      m_CompanyRepository.Update(company);
      m_UnitOfWork.Save();
	  }

	  public void DeleteUserProfile(int id)
		{
			var userProfiles = m_UserProfileRepository.Get(); //Properties müssen noch eingebunden werden oder Lazyloading deaktivieren
			var userProfile = userProfiles.FirstOrDefault(c => c.UserId == id);

			m_UserProfileRepository.Delete(userProfile);
			m_UnitOfWork.Save();
		}

		public void Dispose()
		{
			m_UnitOfWork.Dispose();
		}

	}
}