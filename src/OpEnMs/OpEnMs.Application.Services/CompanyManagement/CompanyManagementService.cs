﻿#region Licence

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0.html
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// 
// Copyright (c) 2013, HTW Berlin

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using OpEnMs.Domain.Entities;
using OpEnMs.Domain.RepositoryContracts;
using OpEnMs.Domain.UnitOfWorkContract;

namespace OpEnMs.Application.Services.CompanyManagement
{
	public class CompanyManagementService: ICompanyManagementService
	{
		private readonly IUnitOfWork m_UnitOfWork;
		private readonly ICompanyRepository m_CompanyRepository;
		private readonly DomainEntityFactory m_DomainEntityFactory;
		private readonly IUserProfileRepository m_UserProfileRepository;

		public CompanyManagementService(IUnitOfWork unitOfWork, DomainEntityFactory domainEntityFactory)
		{
			m_UnitOfWork = unitOfWork;
			m_CompanyRepository = m_UnitOfWork.CompanyRepository;
			m_UserProfileRepository = m_UnitOfWork.UserProfileRepository;
			m_DomainEntityFactory = domainEntityFactory;
		}

		public IEnumerable<Company> Get(Expression<Func<Company, bool>> filter = null, Func<IQueryable<Company>, IOrderedQueryable<Company>> orderBy = null, string includeProperties = "")
		{
			return m_CompanyRepository.Get(filter,
			                               orderBy,
			                               includeProperties);
		}

		public IEnumerable<UserProfile> GetUserProfiles(Expression<Func<UserProfile, bool>> filter = null, Func<IQueryable<UserProfile>, IOrderedQueryable<UserProfile>> orderBy = null, string includeProperties = "")
		{
			return m_UserProfileRepository.Get(filter,
			                                   orderBy,
			                                   includeProperties);
		}

		public Company GetCompanyByUserIdentity(string userIdentity)
		{
			var userProfile = m_UserProfileRepository.Get().
			                                          FirstOrDefault(u => u.UserName.Equals(userIdentity));
			var company = m_CompanyRepository.Get().
			                                  FirstOrDefault(c => c.Employees.Contains(userProfile));
			return company;
		}

		public void AddCompany(Company company)
		{
			m_CompanyRepository.Insert(company);
			m_UnitOfWork.Save();
		}

		public Company FindById(int id)
		{
			return m_CompanyRepository.GetById(id);
		}

		public Company CreateCompany()
		{
			return m_DomainEntityFactory.CreateCompany();
		}

		public void UpdateCompany(Company company)
		{
			m_CompanyRepository.Update(company);
			m_UnitOfWork.Save();
		}

		public void DeleteCompany(int id)
		{
			var companies = m_CompanyRepository.Get(includeProperties : "Employees,MeasuringPoints,Networks,EnergyConsumptions,Measures,IsoMeasures, Ideas");
			var company = companies.FirstOrDefault(c => c.Id == id);

			m_CompanyRepository.Delete(company);
			m_UnitOfWork.Save();
		}

		public void Dispose()
		{
			m_UnitOfWork.Dispose();
		}
	}
}