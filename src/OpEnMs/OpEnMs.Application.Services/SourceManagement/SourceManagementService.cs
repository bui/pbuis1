﻿#region Licence

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0.html
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// 
// Copyright (c) 2013, HTW Berlin

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using OpEnMs.Domain.Entities;
using OpEnMs.Domain.RepositoryContracts;
using OpEnMs.Domain.UnitOfWorkContract;

namespace OpEnMs.Application.Services.SourceManagement
{
	public class SourceManagementService: ISourceManagementService
	{
		private readonly IUnitOfWork m_UnitOfWork;
		private readonly ISourceRepository m_SourceRepository;
		private readonly DomainEntityFactory m_DomainEntityFactory;
		private readonly IUserProfileRepository m_UserProfileRepository;

		public SourceManagementService(IUnitOfWork unitOfWork, DomainEntityFactory domainEntityFactory)
		{
			m_UnitOfWork = unitOfWork;
			m_SourceRepository = m_UnitOfWork.SourceRepository;
			m_UserProfileRepository = m_UnitOfWork.UserProfileRepository;
			m_DomainEntityFactory = domainEntityFactory;
		}

		public IEnumerable<Source> Get(Expression<Func<Source, bool>> filter = null, Func<IQueryable<Source>, IOrderedQueryable<Source>> orderBy = null, string includeProperties = "")
		{
			return m_SourceRepository.Get(filter,
			                              orderBy,
			                              includeProperties);
		}

		public IEnumerable<UserProfile> GetUserProfiles(Expression<Func<UserProfile, bool>> filter = null, Func<IQueryable<UserProfile>, IOrderedQueryable<UserProfile>> orderBy = null, string includeProperties = "")
		{
			return m_UserProfileRepository.Get(filter,
			                                   orderBy,
			                                   includeProperties);
		}

		public void AddSource(Source source)
		{
			m_SourceRepository.Insert(source);
			m_UnitOfWork.Save();
		}

		public Source FindById(int id)
		{
			return m_SourceRepository.GetById(id);
		}

		public Source CreateSource()
		{
			return m_DomainEntityFactory.CreateSource();
		}

		public void UpdateSource(Source source)
		{
			m_SourceRepository.Update(source);
			m_UnitOfWork.Save();
		}

		public void DeleteSource(int id)
		{
			var sources = m_SourceRepository.Get(); //Properties müssen noch eingebunden werden oder Lazyloading deaktivieren
			var source = sources.FirstOrDefault(c => c.Id == id);

			m_SourceRepository.Delete(source);
			m_UnitOfWork.Save();
		}

		public void Dispose()
		{
			m_UnitOfWork.Dispose();
		}
	}
}