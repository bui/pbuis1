﻿#region Licence

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0.html
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// 
// Copyright (c) 2013, HTW Berlin

#endregion

using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Linq.Expressions;
using System.Text.RegularExpressions;
using Emporer.Unit;
using OpEnMs.Domain.Entities;
using OpEnMs.Domain.RepositoryContracts;
using OpEnMs.Domain.UnitOfWorkContract;
using Expression = NCalc.Expression;

namespace OpEnMs.Application.Services.IndicatorManagement
{
    public class IndicatorManagementService: IIndicatorManagementService
    {
        private readonly IUnitOfWork m_UnitOfWork;
        private readonly IIndicatorRepository m_IndicatorRepository;
        private readonly DomainEntityFactory m_DomainEntityFactory;
        private readonly IUserProfileRepository m_UserProfileRepository;
        private readonly IUnitRepository m_UnitRepository;
        private readonly ITopicRepository m_TopicRepository;
        private readonly IIndicatorValueRepository m_IndicatorValueRepository;

        public IndicatorManagementService(IUnitOfWork unitOfWork, DomainEntityFactory domainEntityFactory)
        {
            m_UnitOfWork = unitOfWork;
            m_IndicatorRepository = m_UnitOfWork.IndicatorRepository;
            m_IndicatorValueRepository = m_UnitOfWork.IndicatorValueRepository;
            m_TopicRepository = m_UnitOfWork.TopicRepository;
            m_UnitRepository = m_UnitOfWork.UnitRepository;
            m_UserProfileRepository = m_UnitOfWork.UserProfileRepository;
            m_DomainEntityFactory = domainEntityFactory;
        }

        public IEnumerable<Indicator> Get(Expression<Func<Indicator, bool>> filter = null, Func<IQueryable<Indicator>, IOrderedQueryable<Indicator>> orderBy = null, string includeProperties = "")
        {
            return m_IndicatorRepository.Get(filter,
                                             orderBy,
                                             includeProperties);
        }

        public IEnumerable<Unit> GetUnits(Expression<Func<Unit, bool>> filter = null, Func<IQueryable<Unit>, IOrderedQueryable<Unit>> orderBy = null, string includeProperties = "")
        {
            return m_UnitRepository.Get(filter,
                                        orderBy,
                                        includeProperties);
        }

        public IEnumerable<Topic> GetTopics(Expression<Func<Topic, bool>> filter = null, Func<IQueryable<Topic>, IOrderedQueryable<Topic>> orderBy = null, string includeProperties = "")
        {
            return m_TopicRepository.Get(filter,
                                         orderBy,
                                         includeProperties);
        }

        public IEnumerable<IndicatorValue> GetIndicatorValues(Expression<Func<IndicatorValue, bool>> filter = null, Func<IQueryable<IndicatorValue>, IOrderedQueryable<IndicatorValue>> orderBy = null, string includeProperties = "")
        {
            return m_IndicatorValueRepository.Get(filter,
                                                  orderBy,
                                                  includeProperties);
        }

        public IEnumerable<UserProfile> GetUserProfiles(Expression<Func<UserProfile, bool>> filter = null, Func<IQueryable<UserProfile>, IOrderedQueryable<UserProfile>> orderBy = null, string includeProperties = "")
        {
            return m_UserProfileRepository.Get(filter,
                                                        orderBy,
                                                        includeProperties);
        }


        public DbEntityValidationResult GetValidationResult(Indicator indicator)
        {
            return m_IndicatorRepository.GetValidationResult(indicator);
        }

        public DbEntityValidationResult GetValidationResult(Topic topic)
        {
            return m_TopicRepository.GetValidationResult(topic);
        }

        public void AddIndicator(Indicator indicator)
        {
            m_IndicatorRepository.Insert(indicator);
            m_UnitOfWork.Save();
        }

        public Indicator FindIndicatorById(int id)
        {
            return m_IndicatorRepository.GetById(id);
        }

        public void UpdateIndicator(Indicator indicator)
        {
            m_IndicatorRepository.Update(indicator);
            m_UnitOfWork.Save();
        }

        public void DeleteIndicator(int id)
        {
            Indicator indicator = m_IndicatorRepository.Get(i => i.Id == id,
                                                        includeProperties: "IndicatorValues").
                                                    First();

            List<IndicatorValue> toDelete = indicator.IndicatorValues.ToList();

            //TODO: Hier sollten auch Indicators gelöscht werden die IndicatorValues als Parameter in Formel haben
            foreach (var indicatorValue in toDelete)
            {
                m_IndicatorValueRepository.Delete(indicatorValue);
            }

            m_IndicatorRepository.Delete(indicator);
            m_UnitOfWork.Save();
        }

        public void AddIndicatorValue(IndicatorValue indicatorValue)
        {
            m_IndicatorValueRepository.Insert(indicatorValue);
            m_UnitOfWork.Save();
        }

        public void DeleteIndicatorValue(int id)
        {
            IndicatorValue indicatorValue = m_IndicatorValueRepository.Get(i => i.Id == id,
                                                                                            includeProperties: "Indicator").
                                                                         First();

            List<Indicator> dependingIndicators = GetIndicatorstorsHavingIndicatorValueIndicatorAsParameter(indicatorValue).
                                                                               ToList();

            foreach (var dependingIndicator in dependingIndicators)
            {
                var toDelete = dependingIndicator.IndicatorValues.FirstOrDefault(iv => iv.Timestamp.Year == indicatorValue.Timestamp.Year);
                if (toDelete != null)
                {
                    dependingIndicator.IndicatorValues.Remove(indicatorValue);
                    m_IndicatorValueRepository.Delete(toDelete);
                }
            }
            m_IndicatorValueRepository.Delete(indicatorValue);
            m_UnitOfWork.Save();
        }

        public Topic AddTopic(Topic topic)
        {
            m_TopicRepository.Insert(topic);
            m_UnitOfWork.Save();
            return topic;
        }

        public Unit AddUnit(Unit unit)
        {
            m_UnitRepository.Insert(unit);
            m_UnitOfWork.Save();
            return unit;
        }

        public List<string> GetFormulaErrors(string formula)
        {
            var errorMessages = new List<string>();
            IEnumerable<KeyValuePair<string, bool>> indicatorExistence = CheckIfIndicatorsExist(formula);

            foreach (KeyValuePair<string, bool> indicatorExists in indicatorExistence)
            {
                if (!indicatorExists.Value)
                {
                    errorMessages.Add("Indicator " + indicatorExists.Key + " nicht vorhanden");
                }
            }
            try
            {
                EvaluateExpressionToVerify(formula);
            }
            catch (Exception e)
            {
                errorMessages.Add(e.Message);
            }
            return errorMessages;
        }

        public IEnumerable<KeyValuePair<string, bool>> CheckIfIndicatorsExist(string expression)
        {
            var indicatornumbersInExpression = getDistinctIndicatorNumbersOfExpression(expression);
            var result = new List<KeyValuePair<string, bool>>();
            foreach (string number in indicatornumbersInExpression)
            {
                result.Add(new KeyValuePair<string, bool>(number,
                                                          (m_IndicatorRepository.Get(i => i.Number.Equals(number)).FirstOrDefault() != null)));
            }
            return result;
        }

        public IEnumerable<string> getDistinctIndicatorNumbersOfExpression(string expression)
        {
            var exp = new Regex(@"\[(.*?)\]",
                                RegexOptions.IgnoreCase);
            MatchCollection matchlist = exp.Matches(expression);

            var result = new List<string>();
            foreach (Match match in matchlist)
            {
                if (!result.Contains(match.Groups[1].Value))
                {
                    result.Add(match.Groups[1].Value);
                }
            }
            return result;
        }

        public void EvaluateExpressionToVerify(string formula)
        {
            var parameters = new List<string>(getDistinctIndicatorNumbersOfExpression(formula));
            var e = new Expression(formula);

            foreach (var parameter in parameters)
            {
                e.Parameters.Add(parameter,
                                 1);
            }
            try
            {
                e.Evaluate();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public object EvaluateExpression(Indicator indicator, int year)
        {
            var parameters = new List<string>(getDistinctIndicatorNumbersOfExpression(indicator.IndicatorExpression));
            var e = new Expression(indicator.IndicatorExpression);

            foreach (var parameter in parameters)
            {
                e.Parameters.Add(parameter,
                                 m_IndicatorValueRepository.Get(iv => iv.Indicator.Number == parameter && iv.Timestamp.Year == year).
                                                            FirstOrDefault().
                                                            Value);
            }
            try
            {
                return e.Evaluate();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IEnumerable<Indicator> GetIndicatorstorsHavingIndicatorValueIndicatorAsParameter(IndicatorValue indicatorValue)
        {
            List<Indicator> havingIndicatorInExpression = m_IndicatorRepository.Get(i => i.IndicatorExpression.Contains(indicatorValue.Indicator.Number),
                                                                                    includeProperties : "IndicatorValues").
                                                                                ToList();
            var evaluate = new List<Indicator>();
            evaluate = havingIndicatorInExpression.ToList();

            foreach (var i in havingIndicatorInExpression)
            {
                List<String> indicatorNumbers = getDistinctIndicatorNumbersOfExpression(i.IndicatorExpression).
                    ToList();

                foreach (var indicatorNumber in indicatorNumbers)
                {
                    if (!m_IndicatorValueRepository.Get(iv => iv.Indicator.Number.Equals(indicatorNumber) && iv.Timestamp.Year == indicatorValue.Timestamp.Year).
                                                    Any())
                    {
                        evaluate.Remove(i);
                        break;
                    }
                }
            }
            return evaluate;
        }

        public void Dispose()
        {
            m_UnitOfWork.Dispose();
        }
    }
}