﻿#region Licence

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0.html
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// 
// Copyright (c) 2013, HTW Berlin

#endregion

using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Linq.Expressions;
using Emporer.Unit;
using OpEnMs.Domain.Entities;

namespace OpEnMs.Application.Services.IndicatorManagement
{
    public interface IIndicatorManagementService: IDisposable
    {
        IEnumerable<Indicator> Get(Expression<Func<Indicator, bool>> filter = null, Func<IQueryable<Indicator>, IOrderedQueryable<Indicator>> orderBy = null, string includeProperties = "");
        IEnumerable<Unit> GetUnits(Expression<Func<Unit, bool>> filter = null, Func<IQueryable<Unit>, IOrderedQueryable<Unit>> orderBy = null, string includeProperties = "");
        IEnumerable<Topic> GetTopics(Expression<Func<Topic, bool>> filter = null, Func<IQueryable<Topic>, IOrderedQueryable<Topic>> orderBy = null, string includeProperties = "");
        IEnumerable<IndicatorValue> GetIndicatorValues(Expression<Func<IndicatorValue, bool>> filter = null, Func<IQueryable<IndicatorValue>, IOrderedQueryable<IndicatorValue>> orderBy = null, string includeProperties = "");
        IEnumerable<UserProfile> GetUserProfiles(Expression<Func<UserProfile, bool>> filter = null, Func<IQueryable<UserProfile>, IOrderedQueryable<UserProfile>> orderBy = null, string includeProperties = "");
        IEnumerable<Indicator> GetIndicatorstorsHavingIndicatorValueIndicatorAsParameter(IndicatorValue indicatorValue);
        DbEntityValidationResult GetValidationResult(Indicator indicator);
        DbEntityValidationResult GetValidationResult(Topic topic);
        void AddIndicator(Indicator indicator);
        Indicator FindIndicatorById(int id);
        void UpdateIndicator(Indicator indicator);
        void DeleteIndicator(int id);
        void AddIndicatorValue(IndicatorValue indicatorValue);
        void DeleteIndicatorValue(int id);
        Topic AddTopic(Topic topic);
        Unit AddUnit(Unit Unit);
        List<string> GetFormulaErrors(string formula);
        object EvaluateExpression(Indicator indicator, int year);
    }
}