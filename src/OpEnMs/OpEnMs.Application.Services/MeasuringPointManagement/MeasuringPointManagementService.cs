﻿#region Licence

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0.html
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// 
// Copyright (c) 2013, HTW Berlin

#endregion

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Linq.Expressions;
using Emporer.Unit;
using OpEnMs.Domain.Entities;
using OpEnMs.Domain.RepositoryContracts;
using OpEnMs.Domain.UnitOfWorkContract;

namespace OpEnMs.Application.Services.MeasuringPointManagement
{
	public class MeasuringPointManagementService: IMeasuringPointManagementService
	{
		private readonly IUnitOfWork m_UnitOfWork;
		private readonly IMeasuringPointRepository m_MeasuringPointRepository;
		private readonly DomainEntityFactory m_DomainEntityFactory;
		private readonly ICompanyRepository m_CompanyRepository;
		private readonly IUserProfileRepository m_UserProfileRepository;
		private readonly ISourceRepository m_SourceRepository;
		private readonly IUnitRepository m_UnitRepository;

		public MeasuringPointManagementService(IUnitOfWork unitOfWork, DomainEntityFactory domainEntityFactory)
		{
			m_UnitOfWork = unitOfWork;
			m_MeasuringPointRepository = m_UnitOfWork.MeasuringPointRepository;
			m_CompanyRepository = m_UnitOfWork.CompanyRepository;
			m_UserProfileRepository = m_UnitOfWork.UserProfileRepository;
			m_UnitRepository = m_UnitOfWork.UnitRepository;
			m_SourceRepository = m_UnitOfWork.SourceRepository;
			m_DomainEntityFactory = domainEntityFactory;
		}

		public IEnumerable<MeasuringPoint> Get(Expression<Func<MeasuringPoint, bool>> filter = null, Func<IQueryable<MeasuringPoint>, IOrderedQueryable<MeasuringPoint>> orderBy = null, string includeProperties = "")
		{
			return m_MeasuringPointRepository.Get(filter,
												  orderBy,
												  includeProperties);
		}

		public IEnumerable<Source> GetSources(Expression<Func<Source, bool>> filter = null, Func<IQueryable<Source>, IOrderedQueryable<Source>> orderBy = null, string includeProperties = "")
		{
			return m_SourceRepository.Get(filter,
																						orderBy,
																						includeProperties);
		}

		public IEnumerable<Unit> GetUnits(Expression<Func<Unit, bool>> filter = null, Func<IQueryable<Unit>, IOrderedQueryable<Unit>> orderBy = null, string includeProperties = "")
		{
			return m_UnitRepository.Get(filter,
																						orderBy,
																						includeProperties);
		}

		public IEnumerable<MeasuringPoint> GetMeasuringPointsByUserIdentity(string userIdentity)
		{
			var userProfile = m_UserProfileRepository.Get().
																								FirstOrDefault(u => u.UserName.Equals(userIdentity));
			var company = m_CompanyRepository.Get().
																				FirstOrDefault(c => c.Employees.Contains(userProfile));
			if (company == null)
			{
				return null;
			}
			return company.MeasuringPoints;
		}

		public void AddMeasuringPoint(MeasuringPoint measuringPoint, int companyId)
		{
			var company = m_CompanyRepository.GetById(companyId);
			company.MeasuringPoints.Add(measuringPoint);
			m_CompanyRepository.Update(company);
			m_MeasuringPointRepository.Insert(measuringPoint);
			m_UnitOfWork.Save();
		}

		public MeasuringPoint FindById(int id)
		{
			return m_MeasuringPointRepository.GetById(id);
		}

		public MeasuringPoint CreateMeasuringPoint()
		{
			return m_DomainEntityFactory.CreateMeasuringPoint();
		}

	  public ICollection<EnergyConsumption> CreateEnergyConsumption(int unitId, int sourceId)
	  {
	    var energyConsumptions = new Collection<EnergyConsumption>();
      
      var energyConsumption = m_DomainEntityFactory.CreateEnergyConsumption();
	    var unit = m_UnitRepository.GetById(unitId);
	    var source = m_SourceRepository.GetById(sourceId);

	    energyConsumption.Source = source;
	    energyConsumption.Unit = unit;

      energyConsumptions.Add(energyConsumption);

      return energyConsumptions;
	  }

	  public void UpdateMeasuringPoint(MeasuringPoint measuringPoint)
		{
			m_MeasuringPointRepository.Update(measuringPoint);
			m_UnitOfWork.Save();
		}

		public void DeleteMeasuringPoint(int id)
		{
      var measuringPoints = m_MeasuringPointRepository.Get(includeProperties : "EnergyConsumptions");
      var measuringPoint = measuringPoints.FirstOrDefault(m => m.Id == id);

			m_MeasuringPointRepository.Delete(measuringPoint);
			m_UnitOfWork.Save();
		}

		public void Dispose()
		{
			m_UnitOfWork.Dispose();
		}
	}
}