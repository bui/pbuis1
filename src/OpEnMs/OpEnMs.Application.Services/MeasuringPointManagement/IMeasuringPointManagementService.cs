﻿#region Licence

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0.html
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// 
// Copyright (c) 2013, HTW Berlin

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Emporer.Unit;
using OpEnMs.Domain.Entities;

namespace OpEnMs.Application.Services.MeasuringPointManagement
{
	public interface IMeasuringPointManagementService: IDisposable
	{
		IEnumerable<MeasuringPoint> Get(Expression<Func<MeasuringPoint, bool>> filter = null, Func<IQueryable<MeasuringPoint>, IOrderedQueryable<MeasuringPoint>> orderBy = null, string includeProperties = "");
		IEnumerable<Source> GetSources(Expression<Func<Source, bool>> filter = null, Func<IQueryable<Source>, IOrderedQueryable<Source>> orderBy = null, string includeProperties = "");
		IEnumerable<Unit> GetUnits(Expression<Func<Unit, bool>> filter = null, Func<IQueryable<Unit>, IOrderedQueryable<Unit>> orderBy = null, string includeProperties = "");
		IEnumerable<MeasuringPoint> GetMeasuringPointsByUserIdentity(string userIdentity);
		void AddMeasuringPoint(MeasuringPoint measuringPoint, int companyId);
		MeasuringPoint FindById(int id);
		MeasuringPoint CreateMeasuringPoint();
    ICollection<EnergyConsumption> CreateEnergyConsumption(int unitId, int sourceId);
		void UpdateMeasuringPoint(MeasuringPoint measuringPoint);
		void DeleteMeasuringPoint(int id);
	}
}