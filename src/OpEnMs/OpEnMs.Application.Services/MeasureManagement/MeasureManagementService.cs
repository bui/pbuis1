﻿#region Licence

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0.html
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// 
// Copyright (c) 2013, HTW Berlin

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using OpEnMs.Domain.Entities;
using OpEnMs.Domain.RepositoryContracts;
using OpEnMs.Domain.UnitOfWorkContract;

namespace OpEnMs.Application.Services.MeasureManagement
{
	public class MeasureManagementService: IMeasureManagementService
	{
		private readonly IUnitOfWork m_UnitOfWork;
		private readonly IMeasureRepository m_MeasureRepository;
		private readonly DomainEntityFactory m_DomainEntityFactory;
		private readonly IUserProfileRepository m_UserProfileRepository;
		private readonly ICompanyRepository m_CompanyRepository;

		public MeasureManagementService(IUnitOfWork unitOfWork, DomainEntityFactory domainEntityFactory)
		{
			m_UnitOfWork = unitOfWork;
			m_MeasureRepository = m_UnitOfWork.MeasureRepository;
			m_UserProfileRepository = m_UnitOfWork.UserProfileRepository;
			m_CompanyRepository = m_UnitOfWork.CompanyRepository;
			m_DomainEntityFactory = domainEntityFactory;
		}

		public IEnumerable<Measure> Get(Expression<Func<Measure, bool>> filter = null, Func<IQueryable<Measure>, IOrderedQueryable<Measure>> orderBy = null, string includeProperties = "")
		{
			return m_MeasureRepository.Get(filter,
				orderBy,
				includeProperties);
		}

		public IEnumerable<UserProfile> GetUserProfiles(Expression<Func<UserProfile, bool>> filter = null, Func<IQueryable<UserProfile>, IOrderedQueryable<UserProfile>> orderBy = null, string includeProperties = "")
		{
			return m_UserProfileRepository.Get(filter,
				orderBy,
				includeProperties);
		}

		public Company GetCompanyByUserIdentity(string userIdentity)
		{
			var userProfile = m_UserProfileRepository.Get().
				FirstOrDefault(u => u.UserName.Equals(userIdentity));
			var company = m_CompanyRepository.Get().
				FirstOrDefault(c => c.Employees.Contains(userProfile));
			return company;
		}

		public void AddMeasure(Measure measure, int companyId)
		{
			var company = m_CompanyRepository.GetById(companyId);
			m_MeasureRepository.Insert(measure);
			company.Measures.Add(measure);
			m_CompanyRepository.Update(company);

			m_UnitOfWork.Save();
		}

		public Measure FindById(int id)
		{
			return m_MeasureRepository.GetById(id);
		}

		public Measure CreateMeasure()
		{
			return m_DomainEntityFactory.CreateMeasure();
		}

		public void UpdateMeasure(Measure measure)
		{
			m_MeasureRepository.Update(measure);
			m_UnitOfWork.Save();
		}

		public void DeleteMeasure(int id)
		{
			var measures = m_MeasureRepository.Get(); //Properties müssen noch eingebunden werden oder Lazyloading deaktivieren
			var measure = measures.FirstOrDefault(c => c.Id == id);

			m_MeasureRepository.Delete(measure);
			m_UnitOfWork.Save();
		}

		public bool IdeaIsInMeasure(Idea idea)
		{
			foreach (Measure measure in m_MeasureRepository.Get())
			{
				if (measure.Ideas != null)
				{
					if (measure.Ideas.Contains(idea))
					{
						return true;
					}
				}
			}

			return false;
		}

		public void Dispose()
		{
			m_UnitOfWork.Dispose();
		}
	}
}