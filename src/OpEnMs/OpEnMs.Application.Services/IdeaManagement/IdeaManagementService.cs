﻿#region Licence

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0.html
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// 
// Copyright (c) 2013, HTW Berlin

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web.Mvc;
using OpEnMs.Domain.Entities;
using OpEnMs.Domain.RepositoryContracts;
using OpEnMs.Domain.UnitOfWorkContract;

namespace OpEnMs.Application.Services.IdeaManagement
{
	public class IdeaManagementService: IIdeaManagementService
	{
		private readonly IUnitOfWork m_UnitOfWork;
		private readonly IIdeaRepository m_IdeaRepository;
		private readonly DomainEntityFactory m_DomainEntityFactory;
		private readonly IUserProfileRepository m_UserProfileRepository;
        private readonly ICompanyRepository m_CompanyRepository;

		public IdeaManagementService(IUnitOfWork unitOfWork, DomainEntityFactory domainEntityFactory)
		{
			m_UnitOfWork = unitOfWork;
			m_IdeaRepository = m_UnitOfWork.IdeaRepository;
			m_UserProfileRepository = m_UnitOfWork.UserProfileRepository;
			m_DomainEntityFactory = domainEntityFactory;
            m_CompanyRepository = m_UnitOfWork.CompanyRepository;
		}

		public IEnumerable<Idea> Get(Expression<Func<Idea, bool>> filter = null, Func<IQueryable<Idea>, IOrderedQueryable<Idea>> orderBy = null, string includeProperties = "")
		{
			return m_IdeaRepository.Get(filter,
			                            orderBy,
			                            includeProperties);
		}

		public IEnumerable<UserProfile> GetUserProfiles(Expression<Func<UserProfile, bool>> filter = null, Func<IQueryable<UserProfile>, IOrderedQueryable<UserProfile>> orderBy = null, string includeProperties = "")
		{
			return m_UserProfileRepository.Get(filter,
			                                   orderBy,
			                                   includeProperties);
		}

		public void AddIdea(Idea idea, int companyId)
		{
            var company = m_CompanyRepository.GetById(companyId);
            company.Ideas.Add(idea);
            m_CompanyRepository.Update(company);
			m_IdeaRepository.Insert(idea);
			m_UnitOfWork.Save();
		}

		public Idea FindById(int id)
		{
			return m_IdeaRepository.GetById(id);
		}

        public Company GetCompanyByUserIdentity(string userIdentity)
        {
            var userProfile = m_UserProfileRepository.Get().FirstOrDefault(u => u.UserName.Equals(userIdentity));

            var company = m_CompanyRepository.Get().FirstOrDefault(c => c.Employees.Contains(userProfile));

            return company;
        }

	    public IEnumerable<Idea> GetIdeasByUserIdentity(string userIdentity)
	    {
	        var userProfile = m_UserProfileRepository.Get().FirstOrDefault(u => u.UserName.Equals(userIdentity));

	        if (userProfile == null)
	        {
	            throw new NullReferenceException("User Profile does not exist");
	        }

	        var ideas = m_IdeaRepository.Get().Where(u => u.Creator.UserId.Equals(userProfile.UserId));

	        return ideas;
	    }

	    public IEnumerable<Idea> GetSearchQuery(string query)
	    {
            var ideas = m_IdeaRepository.Get(
                            idea => idea.Name.Contains(query) || idea.Description.Contains(query) || idea.Solution.Contains(query));

	        return ideas;
	    }

		public Idea CreateIdea()
		{
			return m_DomainEntityFactory.CreateIdea();
		}

		public void UpdateIdea(Idea idea)
		{
			m_IdeaRepository.Update(idea);
			m_UnitOfWork.Save();
		}

		public void DeleteIdea(int id)
		{
			var ideas = m_IdeaRepository.Get(includeProperties : "Measures, IsoMeasures");
			var idea = ideas.FirstOrDefault(c => c.Id == id);

			m_IdeaRepository.Delete(idea);
			m_UnitOfWork.Save();
		}

		public void Dispose()
		{
			m_UnitOfWork.Dispose();
		}
	}
}