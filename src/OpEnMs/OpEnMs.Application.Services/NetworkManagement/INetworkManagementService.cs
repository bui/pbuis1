﻿#region Licence

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0.html
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// 
// Copyright (c) 2013, HTW Berlin

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using OpEnMs.Domain.Entities;

namespace OpEnMs.Application.Services.NetworkManagement
{
	public interface INetworkManagementService: IDisposable
	{
		IEnumerable<Network> Get(Expression<Func<Network, bool>> filter = null, Func<IQueryable<Network>, IOrderedQueryable<Network>> orderBy = null, string includeProperties = "");
		IEnumerable<Company> GetCompanies(Expression<Func<Company, bool>> filter = null, Func<IQueryable<Company>, IOrderedQueryable<Company>> orderBy = null, string includeProperties = "");
		void AddNetwork(Network network);
		Network FindById(int id);
		Network CreateNetwork();
		void UpdateNetwork(Network network);
		void DeleteNetwork(int id);
	}
}