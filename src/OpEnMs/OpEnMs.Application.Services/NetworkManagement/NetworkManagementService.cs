﻿#region Licence

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0.html
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// 
// Copyright (c) 2013, HTW Berlin

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using OpEnMs.Domain.Entities;
using OpEnMs.Domain.RepositoryContracts;
using OpEnMs.Domain.UnitOfWorkContract;

namespace OpEnMs.Application.Services.NetworkManagement
{
	public class NetworkManagementService: INetworkManagementService
	{
		private readonly IUnitOfWork m_UnitOfWork;
		private readonly INetworkRepository m_NetworkRepository;
		private readonly IDomainEntityFactory m_DomainEntityFactory;
		private readonly ICompanyRepository m_CompanyRepository;

		public NetworkManagementService(IUnitOfWork unitOfWork, IDomainEntityFactory domainEntityFactory)
		{
			m_UnitOfWork = unitOfWork;
			m_NetworkRepository = m_UnitOfWork.NetworkRepository;
			m_CompanyRepository = m_UnitOfWork.CompanyRepository;
			m_DomainEntityFactory = domainEntityFactory;
		}

		public IEnumerable<Network> Get(Expression<Func<Network, bool>> filter = null, Func<IQueryable<Network>, IOrderedQueryable<Network>> orderBy = null, string includeProperties = "")
		{
			return m_NetworkRepository.Get(filter,
			                               orderBy,
			                               includeProperties);
		}

		public IEnumerable<Company> GetCompanies(Expression<Func<Company, bool>> filter = null, Func<IQueryable<Company>, IOrderedQueryable<Company>> orderBy = null, string includeProperties = "")
		{
			return m_CompanyRepository.Get(filter,
			                               orderBy,
			                               includeProperties);
		}

		public void AddNetwork(Network network)
		{
			m_NetworkRepository.Insert(network);
			m_UnitOfWork.Save();
		}

		public Network FindById(int id)
		{
			return m_NetworkRepository.GetById(id);
		}

		public Network CreateNetwork()
		{
			return m_DomainEntityFactory.CreateNetwork();
		}

		public void UpdateNetwork(Network network)
		{
			m_NetworkRepository.Update(network);
			m_UnitOfWork.Save();
		}

		public void DeleteNetwork(int id)
		{
			var networks = m_NetworkRepository.Get(includeProperties : "Employees,MeasuringPoints,Networks,EnergyConsumptions");
			var network = networks.FirstOrDefault(c => c.Id == id);

			m_NetworkRepository.Delete(network);
			m_UnitOfWork.Save();
		}

		public void Dispose()
		{
			m_UnitOfWork.Dispose();
		}
	}
}