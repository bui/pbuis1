﻿#region Licence

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0.html
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// 
// Copyright (c) 2013, HTW Berlin

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using OpEnMs.Domain.Entities;
using OpEnMs.Domain.RepositoryContracts;
using OpEnMs.Domain.UnitOfWorkContract;

namespace OpEnMs.Application.Services.ValuationManagement
{
	public class ValuationManagementService: IValuationManagementService
	{
		private readonly IUnitOfWork m_UnitOfWork;
		private readonly IValuationRepository m_ValuationRepository;
		private readonly DomainEntityFactory m_DomainEntityFactory;
		private readonly IUserProfileRepository m_UserProfileRepository;

		public ValuationManagementService(IUnitOfWork unitOfWork, DomainEntityFactory domainEntityFactory)
		{
			m_UnitOfWork = unitOfWork;
			m_ValuationRepository = m_UnitOfWork.ValuationRepository;
			m_UserProfileRepository = m_UnitOfWork.UserProfileRepository;
			m_DomainEntityFactory = domainEntityFactory;
		}

		public IEnumerable<Valuation> Get(Expression<Func<Valuation, bool>> filter = null, Func<IQueryable<Valuation>, IOrderedQueryable<Valuation>> orderBy = null, string includeProperties = "")
		{
			return m_ValuationRepository.Get(filter,
			                                 orderBy,
			                                 includeProperties);
		}

		public IEnumerable<UserProfile> GetUserProfiles(Expression<Func<UserProfile, bool>> filter = null, Func<IQueryable<UserProfile>, IOrderedQueryable<UserProfile>> orderBy = null, string includeProperties = "")
		{
			return m_UserProfileRepository.Get(filter,
			                                   orderBy,
			                                   includeProperties);
		}

		public void AddValuation(Valuation valuation)
		{
			m_ValuationRepository.Insert(valuation);
			m_UnitOfWork.Save();
		}

		public Valuation FindById(int id)
		{
			return m_ValuationRepository.GetById(id);
		}

		public Valuation CreateValuation()
		{
			return m_DomainEntityFactory.CreateValuation();
		}

		public void UpdateValuation(Valuation valuation)
		{
			m_ValuationRepository.Update(valuation);
			m_UnitOfWork.Save();
		}

		public void DeleteValuation(int id)
		{
			var valuations = m_ValuationRepository.Get(); //Properties müssen noch eingebunden werden oder Lazyloading deaktivieren
			var valuation = valuations.FirstOrDefault(c => c.Id == id);

			m_ValuationRepository.Delete(valuation);
			m_UnitOfWork.Save();
		}

		public void Dispose()
		{
			m_UnitOfWork.Dispose();
		}
	}
}