﻿#region Licence

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0.html
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// 
// Copyright (c) 2013, HTW Berlin

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using OpEnMs.Domain.Entities;

namespace OpEnMs.Application.Services.ValuationManagement
{
	public interface IValuationManagementService: IDisposable
	{
		IEnumerable<Valuation> Get(Expression<Func<Valuation, bool>> filter = null, Func<IQueryable<Valuation>, IOrderedQueryable<Valuation>> orderBy = null, string includeProperties = "");
		IEnumerable<UserProfile> GetUserProfiles(Expression<Func<UserProfile, bool>> filter = null, Func<IQueryable<UserProfile>, IOrderedQueryable<UserProfile>> orderBy = null, string includeProperties = "");
		void AddValuation(Valuation valuation);
		Valuation FindById(int id);
		Valuation CreateValuation();
		void UpdateValuation(Valuation valuation);
		void DeleteValuation(int id);
	}
}