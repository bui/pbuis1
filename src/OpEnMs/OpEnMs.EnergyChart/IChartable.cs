﻿using System.Collections.Generic;
using OpEnMs.Domain.Entities;

namespace OpEnMs.EnergyChart
{
	public interface IChartable
	{
		string Name { get; }

		ICollection<EnergyConsumption> EnergyConsumptions { get; }
	}
}