﻿#region Licence

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0.html
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// 
// Copyright (c) 2013, HTW Berlin

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using DotNet.Highcharts;
using DotNet.Highcharts.Enums;
using DotNet.Highcharts.Helpers;
using DotNet.Highcharts.Options;
using OpEnMs.Domain.Entities;

namespace OpEnMs.EnergyChart.Factories
{
    public class HighchartFactory: IHighchartFactory
    {
        public Highcharts CreateLineChart(IChartable chartable)
        {
            var chart = new Highcharts(chartable.Name).SetTitle(CreateTitle(chartable.Name)).
                                                       SetXAxis(CreateXAxis(CreateCategoriesForColumn(chartable.EnergyConsumptions))).
                                                       SetYAxis(CreateYAxis()).
                                                       SetSeries(CreateSeriesForLine(chartable.EnergyConsumptions).
                                                                     ToArray());
            return chart;
        }

        public Highcharts CreateLineChart(IEnumerable<Reading> readings, string name, string unit)
        {
            var chart = new Highcharts(name).SetTitle(CreateTitle(name)).
                                             SetXAxis(CreateXAxis(CreateCategoriesForColumn(unit))).
                                             SetYAxis(CreateYAxis()).
                                             SetSeries(CreateSeriesForLine(readings,
                                                                           name).
                                                           ToArray());
            return chart;
        }

        public Highcharts CreateLineChartForDateTime(IEnumerable<Reading> readings, string name, string unit)
        {
            var chart = new Highcharts("chart" + new Random().Next(0,
                                                                   int.MaxValue).
                                                              ToString()).SetTitle(CreateTitle(name)).
                                                                          SetXAxis(CreateXAxisDateTime()).
                                                                          SetYAxis(CreateYAxis(unit)).
                                                                          SetSeries(CreateSeriesForLineDateTime(readings,
                                                                                                                name).
                                                                                        ToArray());
            return chart;
        }

        public Highcharts CreateColumnChart(IChartable chartable)
        {
            var chart = new Highcharts(chartable.Name).SetTitle(CreateTitle(chartable.Name)).
                                                       SetTooltip(new Tooltip{
                                                                                 Formatter = "function() { return '<b>'+ this.point.name +'</b>: '+ this.percentage +' %'; }"
                                                                             }).
                                                       SetXAxis(CreateXAxis(CreateCategoriesForColumn(chartable.EnergyConsumptions))).
                                                       SetYAxis(CreateYAxis()).
                                                       SetTooltip(new Tooltip{
                                                                                 Formatter = "TooltipFormatter"
                                                                             }).
                                                       SetLabels(new Labels{
                                                                               Items = new[]{
                                                                                                new LabelsItems{
                                                                                                                   Style = "left: '40px', top: '8px', color: 'black'"
                                                                                                               }
                                                                                            }
                                                                           }).
                                                       SetPlotOptions(new PlotOptions()).
                                                       SetSeries(CreateSeriesForColumn(chartable.EnergyConsumptions)).
                                                       AddJavascripVariable("colors",
                                                                            "Highcharts.getOptions().colors").
                                                       AddJavascripFunction("TooltipFormatter",
                                                                            @"var s;
																																		if (this.point.name) { // the pie chart
																																			 s = ''+
																																					this.point.name +': '+ this.y +' kWh';
																																		} else {
																																			 s = ''+
																																					this.x  +': '+ this.y;
																																		}
																																		return s;");
            return chart;
        }

        public Highcharts CreatePieChart(IChartable chartable)
        {
            var chart = new Highcharts(chartable.Name).SetTitle(CreateTitle(chartable.Name)).
                                                       SetTooltip(new Tooltip{
                                                                                 Formatter = "function() { return '<b>'+ this.point.name +'</b>: '+ this.percentage +' %'; }"
                                                                             }).
                                                       SetXAxis(CreateXAxis(CreateCategoriesForPie(chartable.EnergyConsumptions.FirstOrDefault()))).
                                                       SetTooltip(new Tooltip{
                                                                                 Formatter = "TooltipFormatter"
                                                                             }).
                                                       SetLabels(new Labels{
                                                                               Items = new[]{
                                                                                                new LabelsItems{
                                                                                                                   Style = "left: '40px', top: '8px', color: 'black'"
                                                                                                               }
                                                                                            }
                                                                           }).
                                                       SetPlotOptions(new PlotOptions{
                                                                                         Pie = new PlotOptionsPie{
                                                                                                                     ShowInLegend = true,
                                                                                                                     AllowPointSelect = true,
                                                                                                                     Cursor = Cursors.Pointer,
                                                                                                                     DataLabels = new PlotOptionsPieDataLabels{
                                                                                                                                                                  Enabled = true
                                                                                                                                                              }
                                                                                                                 }
                                                                                     }).
                                                       SetSeries(CreateSeriesForPie(chartable.EnergyConsumptions)).
                                                       AddJavascripVariable("colors",
                                                                            "Highcharts.getOptions().colors").
                                                       AddJavascripFunction("TooltipFormatter",
                                                                            @"var s;
																																		if (this.point.name) { // the pie chart
																																			 s = ''+
																																					this.point.name +': '+ this.y +' kWh';
																																		} else {
																																			 s = ''+
																																					this.x  +': '+ this.y;
																																		}
																																		return s;");
            return chart;
        }

        private string[] CreateCategoriesForColumn(IEnumerable<EnergyConsumption> energyData)
        {
            if (energyData != null)
            {
                var categories = new List<string>();

                foreach (var energyConsumption in energyData)
                {
                    categories.Add(energyConsumption.Source.Name);
                }
                return categories.ToArray();
            }
            return null;
        }

        private string[] CreateCategoriesForColumn(string unit)
        {
            if (unit != null)
            {
                var categories = new List<string>();

                categories.Add(unit);

                return categories.ToArray();
            }
            return null;
        }

        private string[] CreateCategoriesForPie(EnergyConsumption energyData)
        {
            var categories = new List<string>();

            foreach (var item in energyData.Readings)
            {
                categories.Add(item.Timestamp.ToShortDateString());
            }
            return categories.ToArray();
        }

        private Title CreateTitle(string title)
        {
            return new Title{
                                Text = title
                            };
        }

        private XAxis CreateXAxis(string[] categories)
        {
            return new XAxis{
                                Categories = categories
                            };
        }

        private XAxis CreateXAxisDateTime()
        {
            return new XAxis{
                                Type = AxisTypes.Datetime
                            };
        }


        private YAxis CreateYAxis(string unit)
        {
            return new YAxis{
                                Min = 0,
                                Title = new YAxisTitle{
                                                          Text = unit
                                                      }
                            };
        }

        private YAxis CreateYAxis()
        {
            return new YAxis{
                                Min = 0,
                                Title = new YAxisTitle{
                                                          Text = Resources.Resources.EnergyConsumptionInMwh
                                                      }
                            };
        }

        private IEnumerable<Series> CreateSeriesForLine(IEnumerable<EnergyConsumption> energyData)
        {
            foreach (var energyConsumption in energyData)
            {
                yield return new Series{
                                           Name = energyConsumption.Source.Name,
                                           Type = ChartTypes.Line,
                                           Data = CreateEnergyDataForLine(energyConsumption.Readings,
                                                                          energyConsumption.Source.Name)
                                       };
            }
        }

        private IEnumerable<Series> CreateSeriesForLine(IEnumerable<Reading> readings, string name)
        {
            yield return new Series{
                                       Name = name,
                                       Type = ChartTypes.Line,
                                       Data = CreateEnergyDataForLine(readings,
                                                                      name)
                                   };
        }

        private IEnumerable<Series> CreateSeriesForLineDateTime(IEnumerable<Reading> readings, string name)
        {
            yield return new Series{
                                       Name = name,
                                       Type = ChartTypes.Line,
                                       Data = CreateEnergyDataForLineDateTime(readings,
                                                                              name)
                                   };
        }


        private Series CreateSeriesForColumn(IEnumerable<EnergyConsumption> energyData)
        {
            return new Series{
                                 Name = Resources.Resources.EnergyConsumptions,
                                 Type = ChartTypes.Column,
                                 Data = CreateEnergyDataForPie(energyData)
                             };
        }

        private Series CreateSeriesForPie(IEnumerable<EnergyConsumption> energyData)
        {
            return new Series{
                                 Name = Resources.Resources.EnergyConsumptions,
                                 Type = ChartTypes.Pie,
                                 Data = CreateEnergyDataForPie(energyData)
                             };
        }

        private Data CreateEnergyDataForLine(IEnumerable<Reading> readings, string name)
        {
            var data = new List<Point>();

            foreach (var reading in readings)
            {
                data.Add(CreatePoint(reading,
                                     name));
            }

            return new Data(data.ToArray());
        }

        private Data CreateEnergyDataForLineDateTime(IEnumerable<Reading> readings, string name)
        {
            var data = new List<Point>();

            foreach (var reading in readings)
            {
                data.Add(CreatePointDateTime(reading,
                                             name));
            }

            return new Data(data.ToArray());
        }

        private Data CreateEnergyDataForPie(IEnumerable<EnergyConsumption> energyData)
        {
            var data = new List<Point>();

            foreach (var energyConsumption in energyData)
            {
                data.Add(CreatePoint(energyConsumption));
            }

            return new Data(data.ToArray());
        }

        private Point CreatePoint(Reading reading, string name)
        {
            if (reading != null)
            {
                return new Point{
                                    Name = name,
                                    //ToDo: We'll need DATETIME!
                                    X = Convert.ToInt32(reading.Timestamp.Year),
                                    Y = reading.Value
                                };
            }
            return null;
        }

        private Point CreatePointDateTime(Reading reading, string name)
        {
            if (reading != null)
            {
                return new Point{
                                    Name = name,
                                    //ToDo: We'll need DATETIME!
                                    X = getMilliSeconds(reading.Timestamp),
                                    Y = reading.Value
                                };
            }
            return null;
        }

        private Point CreatePoint(EnergyConsumption energyData)
        {
            var reading = energyData.Readings.FirstOrDefault(r => r.Timestamp == energyData.Readings.Max(rt => rt.Timestamp));

            if (reading != null)
            {
                return new Point{
                                    Name = energyData.Source.Name,
                                    Y = reading.Value
                                };
            }
            return null;
        }

        private Data CreateEnergyData(IEnumerable<Reading> readings)
        {
            var data = new List<object>();
            foreach (var item in readings)
            {
                data.Add(item.Value);
            }

            object[] objectArray = data.ToArray();
            return new Data(objectArray);
        }

        private double getMilliSeconds(DateTime timestamp)
        {
            var dt1 = new DateTime(1970,
                                   1,
                                   1);
            DateTime dt2 = timestamp;
            TimeSpan s = dt2 - dt1;
            return s.TotalMilliseconds;
        }
    }
}