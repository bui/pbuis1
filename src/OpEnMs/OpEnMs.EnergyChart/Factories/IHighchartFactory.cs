﻿using System.Collections.Generic;
using DotNet.Highcharts;
using OpEnMs.Domain.Entities;

namespace OpEnMs.EnergyChart.Factories
{
	public interface IHighchartFactory
	{
		Highcharts CreateLineChart(IChartable chartable);
		Highcharts CreateLineChart(IEnumerable<Reading> readings, string name, string unit);
        Highcharts CreateLineChartForDateTime(IEnumerable<Reading> readings, string name, string unit);
        Highcharts CreateColumnChart(IChartable chartable);
		Highcharts CreatePieChart(IChartable chartable);
	}
}