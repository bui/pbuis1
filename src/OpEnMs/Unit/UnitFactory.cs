﻿#region Licence

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0.html
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// 
// Copyright (c) 2013, HTW Berlin

#endregion

using System.Collections.Generic;

namespace Emporer.Unit
{
	public class UnitFactory
	{
		public IEnumerable<Unit> CreateMassSiUnits()
		{
			var gram = new Unit{
				                   Dimension = "Mass",
				                   Name = "Gramm",
				                   Symbol = "g",
				                   Coefficient = 0,
			                   };
			yield return gram;

			var hectogram = new Unit{
				                        Dimension = "Mass",
				                        Name = "Hectogramm",
				                        Symbol = "hg",
				                        Coefficient = 10,
				                        ReferencedUnit = gram,
			                        };
			yield return hectogram;

			var kilogram = new Unit{
				                       Dimension = "Mass",
				                       Name = "Kilogramm",
				                       Symbol = "kg",
				                       Coefficient = 100,
				                       ReferencedUnit = hectogram
			                       };
			yield return kilogram;

			var centigram = new Unit{
				                        Dimension = "Mass",
				                        Name = "Zentigramm",
				                        Symbol = "cg",
				                        ReferencedUnit = gram,
				                        Coefficient = 0.01,
			                        };
			yield return centigram;

			var milligram = new Unit{
				                        Dimension = "Mass",
				                        Name = "Milligramm",
				                        Symbol = "mg",
				                        ReferencedUnit = centigram,
				                        Coefficient = 0.1,
			                        };
			yield return milligram;

			var microgram = new Unit{
				                        Dimension = "Mass",
				                        Name = "Microgramm",
				                        Symbol = "µg",
				                        ReferencedUnit = milligram,
				                        Coefficient = 0.001,
			                        };
			yield return microgram;

			var nanogram = new Unit{
				                       Dimension = "Mass",
				                       Name = "Nanogramm",
				                       Symbol = "ng",
				                       ReferencedUnit = microgram,
				                       Coefficient = 0.001,
			                       };
			yield return nanogram;

			var tone = new Unit{
				                   Dimension = "Mass",
				                   Name = "Tone",
				                   Symbol = "t",
				                   Coefficient = 1000,
				                   ReferencedUnit = kilogram
			                   };
			yield return tone;
		}


		public IEnumerable<Unit> CreateEnergySiUnits()
		{
			var joule = new Unit{
				                    Dimension = "Energy",
				                    Name = "Joule",
				                    Symbol = "J",
				                    Coefficient = 0,
			                    };
			yield return joule;

			var kilojoule = new Unit{
				                        Dimension = "Energy",
				                        Name = "Kilojoule",
				                        Symbol = "kJ",
				                        ReferencedUnit = joule,
				                        Coefficient = 1000,
			                        };
			yield return kilojoule;

			var megajoule = new Unit{
				                        Dimension = "Energy",
				                        Name = "Megajoule",
				                        Symbol = "MJ",
				                        ReferencedUnit = joule,
				                        Coefficient = 1000000,
			                        };
			yield return megajoule;

			var kilowatthour = new Unit{
				                           Dimension = "Energy",
				                           Name = "Kilowatt per hour",
				                           Symbol = "kWh",
				                           ReferencedUnit = joule,
				                           Coefficient = 3600,
			                           };
			yield return kilowatthour;

			var megawatthour = new Unit{
				                           Dimension = "Energy",
				                           Name = "Megawatt per hour",
				                           Symbol = "MWh",
				                           ReferencedUnit = joule,
				                           Coefficient = 3600000,
			                           };
			yield return megawatthour;

			var kilocalorien = new Unit{
				                           Dimension = "Energy",
				                           Name = "Kilocalorien",
				                           Symbol = "kcal",
				                           ReferencedUnit = joule,
				                           Coefficient = 4.1868,
			                           };
			yield return kilocalorien;
		}

		public IEnumerable<Unit> CreateTimeSiUnits()
		{
			var millisecond = new Unit{
				                          Dimension = "Time",
				                          Name = "millisecond",
				                          Symbol = "ms",
			                          };
			yield return millisecond;

			var second = new Unit{
				                     Dimension = "Time",
				                     Name = "second",
				                     Symbol = "s",
				                     ReferencedUnit = millisecond,
				                     Coefficient = 1000,
			                     };
			yield return second;

			var minute = new Unit{
				                     Dimension = "Time",
				                     Name = "minute",
				                     Symbol = "min",
				                     ReferencedUnit = second,
				                     Coefficient = 60,
			                     };
			yield return minute;

			var hour = new Unit{
				                   Dimension = "Time",
				                   Name = "hour",
				                   Symbol = "h",
				                   ReferencedUnit = minute,
				                   Coefficient = 60,
			                   };
			yield return hour;

			var day = new Unit{
				                  Dimension = "Time",
				                  Name = "day",
				                  Symbol = "d",
				                  ReferencedUnit = hour,
				                  Coefficient = 24,
			                  };
			yield return day;

			var week = new Unit{
				                   Dimension = "Time",
				                   Name = "week",
				                   Symbol = "w",
				                   ReferencedUnit = day,
				                   Coefficient = 7,
			                   };
			yield return week;
		}

		public IEnumerable<Unit> CreateVolumeSiUnits()
		{
			var cubicmetre = new Unit{
				                         Dimension = "Volume",
				                         Name = "cubic metre",
				                         Symbol = "m³",
			                         };
			yield return cubicmetre;

			var cubicdecimetre = new Unit{
				                             Dimension = "Volume",
				                             Name = "cubic decimetre",
				                             Symbol = "dm³",
				                             ReferencedUnit = cubicmetre,
				                             Coefficient = 0.001,
			                             };
			yield return cubicdecimetre;

			var cubiccentimetre = new Unit{
				                              Dimension = "Volume",
				                              Name = "cubic centimetre",
				                              Symbol = "cm³",
				                              ReferencedUnit = cubicmetre,
				                              Coefficient = 0.000001,
			                              };
			yield return cubicdecimetre;

			var cubicmillimetre = new Unit{
				                              Dimension = "Volume",
				                              Name = "cubic millimetre",
				                              Symbol = "mm³",
				                              ReferencedUnit = cubicmetre,
				                              Coefficient = 0.000000001,
			                              };
			yield return cubicmillimetre;

			var cubicdecametre = new Unit{
				                             Dimension = "Volume",
				                             Name = "cubic decametre",
				                             Symbol = "dam³",
				                             ReferencedUnit = cubicmetre,
				                             Coefficient = 1000,
			                             };
			yield return cubicmillimetre;

			var cubichectometre = new Unit{
				                              Dimension = "Volume",
				                              Name = "cubic hectometre",
				                              Symbol = "hm³",
				                              ReferencedUnit = cubicmetre,
				                              Coefficient = 1000000,
			                              };
			yield return cubichectometre;

			var cubickilometre = new Unit{
				                             Dimension = "Volume",
				                             Name = "cubic kilometre",
				                             Symbol = "km³",
				                             ReferencedUnit = cubicmetre,
				                             Coefficient = 1000000000,
			                             };
			yield return cubickilometre;

			var litre = new Unit{
				                    Dimension = "Volume",
				                    Name = "litre",
				                    Symbol = "l",
				                    ReferencedUnit = cubicdecimetre,
				                    Coefficient = 1,
			                    };
			yield return litre;

			var millilitre = new Unit{
				                         Dimension = "Volume",
				                         Name = "millilitre",
				                         Symbol = "ml",
				                         ReferencedUnit = litre,
				                         Coefficient = 0.001,
			                         };
			yield return millilitre;
		}

		public IEnumerable<Unit> CreateLengthSiUnits()
		{
			var metre = new Unit{
				                    Dimension = "Length",
				                    Name = "metre",
				                    Symbol = "m",
			                    };
			yield return metre;

			var decimetre = new Unit{
				                        Dimension = "Length",
				                        Name = "decimetre",
				                        Symbol = "dm",
				                        ReferencedUnit = metre,
				                        Coefficient = 0.1,
			                        };
			yield return decimetre;

			var centimetre = new Unit{
				                         Dimension = "Length",
				                         Name = "centimetre",
				                         Symbol = "cm",
				                         ReferencedUnit = metre,
				                         Coefficient = 0.01,
			                         };
			yield return centimetre;

			var millimetre = new Unit{
				                         Dimension = "Length",
				                         Name = "millimetre",
				                         Symbol = "mm",
				                         ReferencedUnit = metre,
				                         Coefficient = 0.001,
			                         };
			yield return millimetre;

			var kilometre = new Unit{
				                        Dimension = "Length",
				                        Name = "kilometre",
				                        Symbol = "km",
				                        ReferencedUnit = metre,
				                        Coefficient = 1000,
			                        };
			yield return kilometre;
		}

		public IEnumerable<Unit> CreateAreaSiUnits()
		{
			var squaremetre = new Unit{
				                          Dimension = "Area",
				                          Name = "square metre",
				                          Symbol = "m²",
			                          };
			yield return squaremetre;

			var squaredecimetre = new Unit{
				                              Dimension = "Area",
				                              Name = "square decimetre",
				                              Symbol = "dm²",
				                              ReferencedUnit = squaremetre,
				                              Coefficient = 0.01,
			                              };
			yield return squaredecimetre;

			var squarecentimetre = new Unit{
				                               Dimension = "Area",
				                               Name = "square centimetre",
				                               Symbol = "cm²",
				                               ReferencedUnit = squaremetre,
				                               Coefficient = 0.0001,
			                               };
			yield return squarecentimetre;

			var squaremillimetre = new Unit{
				                               Dimension = "Area",
				                               Name = "square millimetre",
				                               Symbol = "mm²",
				                               ReferencedUnit = squaremetre,
				                               Coefficient = 0.000001,
			                               };
			yield return squaremillimetre;

			var ar = new Unit{
				                 Dimension = "Area",
				                 Name = "ar",
				                 Symbol = "a",
				                 ReferencedUnit = squaremetre,
				                 Coefficient = 100,
			                 };
			yield return ar;

			var hectare = new Unit{
				                      Dimension = "Area",
				                      Name = "hectare",
				                      Symbol = "ha",
				                      ReferencedUnit = squaremetre,
				                      Coefficient = 10000,
			                      };
			yield return hectare;

			var squarekilometre = new Unit{
				                              Dimension = "Area",
				                              Name = "square kilometre",
				                              Symbol = "km²",
				                              ReferencedUnit = squaremetre,
				                              Coefficient = 1000000,
			                              };
			yield return squarekilometre;
		}

		public IEnumerable<Unit> CreateElectricCurrentSiUnits()
		{
			var ampere = new Unit{
				                     Dimension = "Electric current",
				                     Name = "ampere",
				                     Symbol = "A",
			                     };
			yield return ampere;

			var kiloampere = new Unit{
				                         Dimension = "Electric current",
				                         Name = "kiloampere",
				                         Symbol = "kA",
				                         ReferencedUnit = ampere,
				                         Coefficient = 1000,
			                         };
			yield return kiloampere;

			var milliampere = new Unit{
				                          Dimension = "Electric current",
				                          Name = "milliampere",
				                          Symbol = "mA",
				                          ReferencedUnit = ampere,
				                          Coefficient = 0.001,
			                          };
			yield return milliampere;

			var volt = new Unit{
				                   Dimension = "Electric potential",
				                   Name = "volt",
				                   Symbol = "V",
			                   };
			yield return volt;

			var kilovolt = new Unit{
				                       Dimension = "Electric potential",
				                       Name = "kilovolt",
				                       Symbol = "kV",
				                       ReferencedUnit = volt,
				                       Coefficient = 1000,
			                       };
			yield return kilovolt;

			var millivolt = new Unit{
				                        Dimension = "Electric potential",
				                        Name = "millivolt",
				                        Symbol = "mV",
				                        ReferencedUnit = volt,
				                        Coefficient = 0.001,
			                        };
			yield return millivolt;

			var centigrade = new Unit{
				                         Dimension = "Temperature",
				                         Name = "celsius",
				                         Symbol = "°C",
			                         };
			yield return centigrade;
		}

		public IEnumerable<Unit> CreateSiUnits()
		{
			var units = new List<Unit>();

			units.AddRange(CreateAreaSiUnits());
			units.AddRange(CreateElectricCurrentSiUnits());
			units.AddRange(CreateEnergySiUnits());
			units.AddRange(CreateLengthSiUnits());
			units.AddRange(CreateMassSiUnits());
			units.AddRange(CreateTimeSiUnits());
			units.AddRange(CreateVolumeSiUnits());


			return units;
		}
	}
}