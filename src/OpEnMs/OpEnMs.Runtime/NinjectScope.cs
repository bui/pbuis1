#region Licence

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0.html
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// 
// Copyright (c) 2013, HTW Berlin

#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http.Dependencies;
using Ninject.Activation;
using Ninject.Parameters;
using Ninject.Syntax;

namespace OpEnMs.Runtime
{
	public class NinjectScope: IDependencyScope
	{
		private IResolutionRoot m_ResolutionRoot;

		public NinjectScope(IResolutionRoot kernel)
		{
			m_ResolutionRoot = kernel;
		}

		public object GetService(Type serviceType)
		{
			IRequest request = m_ResolutionRoot.CreateRequest(serviceType,
			                                                  null,
			                                                  new Parameter[0],
			                                                  true,
			                                                  true);
			return m_ResolutionRoot.Resolve(request).
			                        SingleOrDefault();
		}

		public IEnumerable<object> GetServices(Type serviceType)
		{
			IRequest request = m_ResolutionRoot.CreateRequest(serviceType,
			                                                  null,
			                                                  new Parameter[0],
			                                                  true,
			                                                  true);
			return m_ResolutionRoot.Resolve(request).
			                        ToList();
		}

		public void Dispose()
		{
			var disposable = (IDisposable) m_ResolutionRoot;
			if (disposable != null)
			{
				disposable.Dispose();
			}
			m_ResolutionRoot = null;
		}
	}
}