﻿#region Licence

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0.html
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// 
// Copyright (c) 2013, HTW Berlin

#endregion

using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace OpEnMs.Runtime
{
	public class ServiceResolverAdapter: IDependencyResolver
	{
		private readonly System.Web.Http.Dependencies.IDependencyResolver m_DependencyResolver;

		public ServiceResolverAdapter(System.Web.Http.Dependencies.IDependencyResolver dependencyResolver)
		{
			if (dependencyResolver == null)
			{
				throw new ArgumentNullException("dependencyResolver");
			}
			m_DependencyResolver = dependencyResolver;
		}

		public object GetService(Type serviceType)
		{
			return m_DependencyResolver.GetService(serviceType);
		}

		public IEnumerable<object> GetServices(Type serviceType)
		{
			return m_DependencyResolver.GetServices(serviceType);
		}
	}
}