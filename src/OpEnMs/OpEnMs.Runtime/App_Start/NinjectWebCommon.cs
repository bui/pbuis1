#region Licence

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0.html
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// 
// Copyright (c) 2013, HTW Berlin

#endregion

using System;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using Microsoft.Web.Infrastructure.DynamicModuleHelper;
using Ninject;
using Ninject.Web.Common;
using OpEnMs.Application.Services.CompanyManagement;
using OpEnMs.Application.Services.EnergyConsumptionManagement;
using OpEnMs.Application.Services.IdeaManagement;
using OpEnMs.Application.Services.IndicatorManagement;
using OpEnMs.Application.Services.IsoMeasureManagement;
using OpEnMs.Application.Services.MeasureManagement;
using OpEnMs.Application.Services.MeasuringPointManagement;
using OpEnMs.Application.Services.NetworkManagement;
using OpEnMs.Application.Services.ReadingManagement;
using OpEnMs.Application.Services.SourceManagement;
using OpEnMs.Application.Services.UserProfileManagement;
using OpEnMs.Application.Services.ValuationManagement;
using OpEnMs.Data;
using OpEnMs.Data.Repositories;
using OpEnMs.Data.UnitOfWork;
using OpEnMs.Domain.Entities;
using OpEnMs.Domain.RepositoryContracts;
using OpEnMs.Domain.UnitOfWorkContract;
using OpEnMs.EnergyChart.Factories;
using OpEnMs.Runtime.App_Start;
using WebActivator;

[assembly : WebActivator.PreApplicationStartMethod(typeof (NinjectWebCommon), "Start")]
[assembly : ApplicationShutdownMethod(typeof (NinjectWebCommon), "Stop")]

namespace OpEnMs.Runtime.App_Start
{
    public static class NinjectWebCommon
    {
        private static readonly Bootstrapper bootstrapper = new Bootstrapper();

        /// <summary>
        /// Starts the application
        /// </summary>
        public static void Start()
        {
            DynamicModuleUtility.RegisterModule(typeof (OnePerRequestHttpModule));
            DynamicModuleUtility.RegisterModule(typeof (NinjectHttpModule));
            bootstrapper.Initialize(CreateKernel);
        }

        /// <summary>
        /// Stops the application.
        /// </summary>
        public static void Stop()
        {
            bootstrapper.ShutDown();
        }

        /// <summary>
        /// Creates the kernel that will manage your application.
        /// </summary>
        /// <returns>The created kernel.</returns>
        private static IKernel CreateKernel()
        {
            var kernel = new StandardKernel();
            kernel.Bind<Func<IKernel>>().
                   ToMethod(ctx => () => new Bootstrapper().Kernel);
            kernel.Bind<IHttpModule>().
                   To<HttpApplicationInitializationHttpModule>();

            RegisterServices(kernel);

            var dependencyResolverWebApi = new NinjectResolver(kernel);

            GlobalConfiguration.Configuration.DependencyResolver = dependencyResolverWebApi;

            var dependencyResolverMvc = dependencyResolverWebApi.ToServiceResolver();

            DependencyResolver.SetResolver(dependencyResolverMvc);

            return kernel;
        }


        /// <summary>
        /// Load your modules or register your services here!
        /// </summary>
        /// <param name="kernel">The kernel.</param>
        private static void RegisterServices(IKernel kernel)
        {
            kernel.Bind<IOpEnMsDbContext>().
                   To<OpEnMsDbContext>();

            kernel.Bind<IRepositoryFactory>().
                   To<RepositoryFactory>();
            kernel.Bind<IDomainEntityFactory>().
                   To<DomainEntityFactory>();
            kernel.Bind<IHighchartFactory>().
                   To<HighchartFactory>();

            kernel.Bind<IUnitOfWork>().
                   To<UnitOfWork>();

            kernel.Bind<ICompanyRepository>().
                   To<CompanyRepository>();
            kernel.Bind<INetworkRepository>().
                   To<NetworkRepository>();
            kernel.Bind<IMeasuringPointRepository>().
                   To<MeasuringPointRepository>();
            kernel.Bind<IMeasureRepository>().
                   To<MeasureRepository>();
            kernel.Bind<IIsoMeasureRepository>().
                   To<IsoMeasureRepository>();
            kernel.Bind<IIdeaRepository>().
                   To<IdeaRepository>();
            kernel.Bind<IUnitRepository>().
                   To<UnitRepository>();
            kernel.Bind<ISourceRepository>().
                   To<SourceRepository>();
            kernel.Bind<IReadingRepository>().
                   To<ReadingRepository>();
            kernel.Bind<IEnergyConsumptionRepository>().
                   To<EnergyConsumptionRepository>();
            kernel.Bind<IValuationRepository>().
                   To<ValuationRepository>();
            kernel.Bind<IIndicatorRepository>().
                   To<IndicatorRepository>();
            kernel.Bind<IIndicatorValueRepository>().
                   To<IndicatorValueRepository>();
            kernel.Bind<ITopicRepository>().
                   To<TopicRepository>();


            kernel.Bind<ICompanyManagementService>().
                   To<CompanyManagementService>();
            kernel.Bind<INetworkManagementService>().
                   To<NetworkManagementService>();
            kernel.Bind<IMeasuringPointManagementService>().
                   To<MeasuringPointManagementService>();
            kernel.Bind<IMeasureManagementService>().
                   To<MeasureManagementService>();
            kernel.Bind<IIsoMeasureManagementService>().
                   To<IsoMeasureManagementService>();
            kernel.Bind<IIdeaManagementService>().
                   To<IdeaManagementService>();
            kernel.Bind<IEnergyConsumptionManagementService>().
                   To<EnergyConsumptionManagementService>();
            kernel.Bind<IReadingManagementService>().
                   To<ReadingManagementService>();
            kernel.Bind<ISourceManagementService>().
                   To<SourceManagementService>();
            kernel.Bind<IUserProfileManagementService>().
                   To<UserProfileManagementService>();
            kernel.Bind<IValuationManagementService>().
                   To<ValuationManagementService>();
            kernel.Bind<IIndicatorManagementService>().
                   To<IndicatorManagementService>();
        }
    }
}