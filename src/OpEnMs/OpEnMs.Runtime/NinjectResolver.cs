#region Licence

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0.html
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// 
// Copyright (c) 2013, HTW Berlin

#endregion

using System.Web.Http.Dependencies;
using Ninject;

namespace OpEnMs.Runtime
{
	public class NinjectResolver: NinjectScope,
	                              IDependencyResolver
	{
		private readonly IKernel m_Kernel;

		public NinjectResolver(IKernel kernel)
			: base(kernel)
		{
			m_Kernel = kernel;
		}

		public IDependencyScope BeginScope()
		{
			return new NinjectScope(m_Kernel.BeginBlock());
		}
	}
}