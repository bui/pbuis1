﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web.Security;
using Emporer.Unit;
using OpEnMs.Data;
using OpEnMs.Domain.Entities;
using WebMatrix.WebData;

namespace OpEnMs.EETS.Migrations
{
    public static class DbHelper
    {
        private static OpEnMsDbContext m_Context;

        public static void CreateTestData(OpEnMsDbContext context)
        {
            m_Context = context;
            CreateUserData();
            CreateSources(context);
            CreateUnits(context);
            CreateNetwork(context);
            CreateIndicators(context);
        }

        private static void CreateNetwork(OpEnMsDbContext context)
        {
            var companies = CreateCompanies(context).
                ToList();

            var network = new Network{
                                         Name = "Anonymisierter EET",
                                         StartDate = new DateTime(2010,
                                                                  5,
                                                                  1),
                                         EndDate = new DateTime(2013,
                                                                7,
                                                                1),
                                         Description = "Das ist ein anoynmisierter EET",
                                         State = NetworkState.Closed,
                                         Companies = companies
                                     };


            var network2 = new Network{
                                          Name = "Ein weiterer EET",
                                          StartDate = new DateTime(2012,
                                                                   12,
                                                                   22),
                                          EndDate = new DateTime(2015,
                                                                 12,
                                                                 22),
                                          Description = "Das ist ein anoynmisierter EET",
                                          State = NetworkState.Phase0,
                                          Companies = new Collection<Company>()
                                      };
            var network3 = new Network{
                                          Name = "Noch ein EET",
                                          StartDate = new DateTime(2013,
                                                                   1,
                                                                   5),
                                          EndDate = new DateTime(2016,
                                                                 1,
                                                                 5),
                                          Description = "Das ist ein anoynmisierter EET",
                                          State = NetworkState.Phase1,
                                          Companies = new Collection<Company>()
                                      };
            context.Networks.Add(network);
            context.Networks.Add(network2);
            context.Networks.Add(network3);

            context.SaveChanges();
        }

        private static IEnumerable<Company> CreateCompanies(OpEnMsDbContext context)
        {
            var companies = new Collection<Company>();

            var unternehmenA = new Company{
                                              Name = "UnternehmenA",
                                              Street = "FiktivestraßeA 1",
                                              PostalCode = "21837",
                                              City = "Fiktive Stadt A",
                                              Website = "www.unternehmen-a.de"
                                          };
            unternehmenA.EnergyConsumptions = CreateEnergyConsumptions(context,
                                                                       unternehmenA.Name);
            unternehmenA.Measures = CreateMeasures(unternehmenA.Name);
            unternehmenA.MeasuringPoints = CreateMeasuringPoints(unternehmenA.Name);
            unternehmenA.Employees = new Collection<UserProfile>();
            unternehmenA.Employees.Add(m_Context.UserProfiles.Find(17));

            var unternehmenB = new Company{
                                              Name = "Unternehmen B",
                                              Street = "FiktivestraßeB 1",
                                              PostalCode = "21837",
                                              City = "Fiktive Stadt B",
                                              Website = "www.unternehmen-b.de"
                                          };
            unternehmenB.EnergyConsumptions = CreateEnergyConsumptions(context,
                                                                       unternehmenB.Name);

            unternehmenB.Measures = CreateMeasures(unternehmenB.Name);
            unternehmenB.MeasuringPoints = CreateMeasuringPoints(unternehmenB.Name);

            var unternehmenD = new Company{
                                              Name = "Unternehmen D",
                                              Street = "FiktivestraßeD 1",
                                              PostalCode = "21837",
                                              City = "Fiktive Stadt D",
                                              Website = "www.unternehmen-d.de"
                                          };
            unternehmenD.EnergyConsumptions = CreateEnergyConsumptions(context,
                                                                       unternehmenD.Name);
            unternehmenD.Measures = CreateMeasures(unternehmenD.Name);
            unternehmenD.MeasuringPoints = CreateMeasuringPoints(unternehmenD.Name);

            var unternehmenE = new Company{
                                              Name = "Unternehmen E",
                                              Street = "FiktivestraßeE 1",
                                              PostalCode = "21837",
                                              City = "Fiktive Stadt E",
                                              Website = "www.unternehmen-e.de"
                                          };
            unternehmenE.EnergyConsumptions = CreateEnergyConsumptions(context,
                                                                       unternehmenE.Name);
            unternehmenE.Measures = CreateMeasures(unternehmenE.Name);
            unternehmenE.MeasuringPoints = CreateMeasuringPoints(unternehmenE.Name);

            var unternehmenF = new Company{
                                              Name = "Unternehmen F",
                                              Street = "FiktivestraßeF 1",
                                              PostalCode = "21837",
                                              City = "Fiktive Stadt F",
                                              Website = "www.unternehmen-f.de"
                                          };
            unternehmenF.EnergyConsumptions = CreateEnergyConsumptions(context,
                                                                       unternehmenF.Name);
            unternehmenF.Measures = CreateMeasures(unternehmenF.Name);
            unternehmenF.MeasuringPoints = CreateMeasuringPoints(unternehmenF.Name);

            var unternehmenG = new Company{
                                              Name = "Unternehmen G",
                                              Street = "FiktivestraßeG 1",
                                              PostalCode = "21837",
                                              City = "Fiktive Stadt G",
                                              Website = "www.unternehmen-g.de"
                                          };
            unternehmenG.EnergyConsumptions = CreateEnergyConsumptions(context,
                                                                       unternehmenG.Name);
            unternehmenG.Measures = CreateMeasures(unternehmenG.Name);
            unternehmenG.MeasuringPoints = CreateMeasuringPoints(unternehmenG.Name);

            var unternehmenH = new Company{
                                              Name = "Unternehmen H",
                                              Street = "FiktivestraßeH 1",
                                              PostalCode = "21837",
                                              City = "Fiktive Stadt H",
                                              Website = "www.unternehmen-h.de"
                                          };
            unternehmenH.EnergyConsumptions = CreateEnergyConsumptions(context,
                                                                       unternehmenH.Name);
            unternehmenH.Measures = CreateMeasures(unternehmenH.Name);
            unternehmenH.MeasuringPoints = CreateMeasuringPoints(unternehmenH.Name);

            var unternehmenI = new Company{
                                              Name = "Unternehmen I",
                                              Street = "FiktivestraßeI 1",
                                              PostalCode = "21837",
                                              City = "Fiktive Stadt I",
                                              Website = "www.unternehmen-i.de"
                                          };
            unternehmenI.EnergyConsumptions = CreateEnergyConsumptions(context,
                                                                       unternehmenI.Name);
            unternehmenI.Measures = CreateMeasures(unternehmenI.Name);
            unternehmenI.MeasuringPoints = CreateMeasuringPoints(unternehmenI.Name);

            var unternehmenJ = new Company{
                                              Name = "Unternehmen J",
                                              Street = "FiktivestraßeJ 1",
                                              PostalCode = "21837",
                                              City = "Fiktive Stadt J",
                                              Website = "www.unternehmen-j.de"
                                          };
            unternehmenJ.EnergyConsumptions = CreateEnergyConsumptions(context,
                                                                       unternehmenJ.Name);
            unternehmenJ.Measures = CreateMeasures(unternehmenJ.Name);
            unternehmenJ.MeasuringPoints = CreateMeasuringPoints(unternehmenJ.Name);

            var unternehmenK = new Company{
                                              Name = "Unternehmen K",
                                              Street = "FiktivestraßeK 1",
                                              PostalCode = "21837",
                                              City = "Fiktive Stadt K",
                                              Website = "www.unternehmen-k.de"
                                          };
            unternehmenK.EnergyConsumptions = CreateEnergyConsumptions(context,
                                                                       unternehmenK.Name);
            unternehmenK.Measures = CreateMeasures(unternehmenK.Name);
            unternehmenK.MeasuringPoints = CreateMeasuringPoints(unternehmenK.Name);

            var unternehmenL = new Company{
                                              Name = "Unternehmen L",
                                              Street = "FiktivestraßeL 1",
                                              PostalCode = "21837",
                                              City = "Fiktive Stadt L",
                                              Website = "www.unternehmen-l.de"
                                          };
            unternehmenL.EnergyConsumptions = CreateEnergyConsumptions(context,
                                                                       unternehmenL.Name);
            unternehmenL.Measures = CreateMeasures(unternehmenL.Name);
            unternehmenL.MeasuringPoints = CreateMeasuringPoints(unternehmenL.Name);

            var unternehmenC = new Company{
                                              Name = "Unternehmen C",
                                              Street = "FiktivestraßeC 1",
                                              PostalCode = "21837",
                                              City = "Fiktive Stadt C",
                                              Website = "www.unternehmen-c.de"
                                          };
            unternehmenC.EnergyConsumptions = CreateEnergyConsumptions(context,
                                                                       unternehmenC.Name);
            unternehmenC.Measures = CreateMeasures(unternehmenC.Name);
            unternehmenC.MeasuringPoints = CreateMeasuringPoints(unternehmenC.Name);

            var isoUnternehmen = new Company{
                                                Name = "Iso Unternehmen",
                                                Street = "Isostrasse",
                                                PostalCode = "21837",
                                                City = "Iso Stadt",
                                                Website = "www.unternehmen-iso.de"
                                            };
            isoUnternehmen.EnergyConsumptions = CreateEnergyConsumptions(context,
                                                                         isoUnternehmen.Name);
            isoUnternehmen.Measures = CreateMeasures(isoUnternehmen.Name);
            isoUnternehmen.MeasuringPoints = CreateMeasuringPoints(isoUnternehmen.Name);
            isoUnternehmen.Ideas = CreateIdeas(isoUnternehmen.Name);
            isoUnternehmen.Employees = new Collection<UserProfile>();
            isoUnternehmen.Employees.Add(m_Context.UserProfiles.Find(5));
            isoUnternehmen.Employees.Add(m_Context.UserProfiles.Find(19));

            companies.Add(unternehmenA);
            companies.Add(unternehmenB);
            companies.Add(unternehmenD);
            companies.Add(unternehmenE);
            companies.Add(unternehmenF);
            companies.Add(unternehmenG);
            companies.Add(unternehmenH);
            companies.Add(unternehmenI);
            companies.Add(unternehmenJ);
            companies.Add(unternehmenK);
            companies.Add(unternehmenL);
            companies.Add(unternehmenC);
            companies.Add(isoUnternehmen);

            foreach (var company in companies)
            {
                context.Companies.Add(company);
            }

            context.SaveChanges();

            return companies;
        }

        private static ICollection<MeasuringPoint> CreateMeasuringPoints(string companyName)
        {
            var measuringPoints = new Collection<MeasuringPoint>();
            var electicity = m_Context.Sources.FirstOrDefault(s => s.Name == Resources.Resources.Electicity);
            var naturalGas = m_Context.Sources.FirstOrDefault(s => s.Name == Resources.Resources.NatrualGas);
            var heatingOil = m_Context.Sources.FirstOrDefault(s => s.Name == Resources.Resources.HeatingOil);

            var kWh = m_Context.Units.FirstOrDefault(u => u.Symbol == "kWh");

            #region LTI-Metalltechnik GmbH

            if (companyName.Equals("UnternehmenA"))
            {
                var activity1 = CreateMeasuringPoint("HZ01");
                var activity2 = CreateMeasuringPoint("HZ02");
                var activity3 = CreateMeasuringPoint("HZ03");
                var activity4 = CreateMeasuringPoint("HZ04");
                var activity5 = CreateMeasuringPoint("HZ05");


                measuringPoints.Add(activity1);
                measuringPoints.Add(activity2);
                measuringPoints.Add(activity3);
                measuringPoints.Add(activity4);
                measuringPoints.Add(activity5);
            }

            #endregion

            #region Solvay Fluor GmbH

            if (companyName.Equals("Unternehmen B"))
            {
                var activity1 = CreateMeasuringPoint("HZ01");
                var activity2 = CreateMeasuringPoint("HZ02");
                var activity3 = CreateMeasuringPoint("HZ03");
                var activity4 = CreateMeasuringPoint("HZ04");
                var activity5 = CreateMeasuringPoint("HZ05");
                var activity6 = CreateMeasuringPoint("HZ05");
                var activity7 = CreateMeasuringPoint("HZ05");


                measuringPoints.Add(activity1);
                measuringPoints.Add(activity2);
                measuringPoints.Add(activity3);
                measuringPoints.Add(activity4);
                measuringPoints.Add(activity5);
                measuringPoints.Add(activity6);
                measuringPoints.Add(activity7);
            }

            #endregion

            #region Unternehmen C

            if (companyName.Equals("Unternehmen C"))
            {
                var activity1 = CreateMeasuringPoint("HZ01");
                var activity2 = CreateMeasuringPoint("HZ02");
                var activity3 = CreateMeasuringPoint("HZ03");
                var activity4 = CreateMeasuringPoint("HZ04");
                var activity5 = CreateMeasuringPoint("HZ05");
                var activity6 = CreateMeasuringPoint("HZ05");


                measuringPoints.Add(activity1);
                measuringPoints.Add(activity2);
                measuringPoints.Add(activity3);
                measuringPoints.Add(activity4);
                measuringPoints.Add(activity5);
                measuringPoints.Add(activity6);
            }

            #endregion

            #region Unternehmen D

            if (companyName.Equals("Unternehmen D"))
            {
                var activity1 = CreateMeasuringPoint("HZ01");
                var activity2 = CreateMeasuringPoint("HZ02");
                var activity3 = CreateMeasuringPoint("HZ03");
                var activity4 = CreateMeasuringPoint("HZ04");
                var activity5 = CreateMeasuringPoint("HZ05");
                var activity6 = CreateMeasuringPoint("HZ05");
                var activity7 = CreateMeasuringPoint("HZ05");
                var activity8 = CreateMeasuringPoint("HZ05");
                var activity9 = CreateMeasuringPoint("HZ05");
                var activity10 = CreateMeasuringPoint("HZ05");


                measuringPoints.Add(activity1);
                measuringPoints.Add(activity2);
                measuringPoints.Add(activity3);
                measuringPoints.Add(activity4);
                measuringPoints.Add(activity5);
                measuringPoints.Add(activity6);
                measuringPoints.Add(activity7);
                measuringPoints.Add(activity8);
                measuringPoints.Add(activity9);
                measuringPoints.Add(activity10);
            }

            #endregion

            #region Unternehmen E

            if (companyName.Equals("Unternehmen E"))
            {
                var activity1 = CreateMeasuringPoint("HZ01");
                var activity2 = CreateMeasuringPoint("HZ02");
                var activity3 = CreateMeasuringPoint("HZ03");
                var activity4 = CreateMeasuringPoint("HZ04");
                var activity5 = CreateMeasuringPoint("HZ05");


                measuringPoints.Add(activity1);
                measuringPoints.Add(activity2);
                measuringPoints.Add(activity3);
                measuringPoints.Add(activity4);
                measuringPoints.Add(activity5);
            }

            #endregion

            #region Unternehmen F

            if (companyName.Equals("Unternehmen F"))
            {
                var activity1 = CreateMeasuringPoint("HZ01");
                var activity2 = CreateMeasuringPoint("HZ02");
                var activity3 = CreateMeasuringPoint("HZ03");
                var activity4 = CreateMeasuringPoint("HZ04");
                var activity5 = CreateMeasuringPoint("HZ05");
                var activity6 = CreateMeasuringPoint("HZ05");
                var activity7 = CreateMeasuringPoint("HZ05");
                var activity8 = CreateMeasuringPoint("HZ05");


                measuringPoints.Add(activity1);
                measuringPoints.Add(activity2);
                measuringPoints.Add(activity3);
                measuringPoints.Add(activity4);
                measuringPoints.Add(activity5);
                measuringPoints.Add(activity6);
                measuringPoints.Add(activity7);
                measuringPoints.Add(activity8);
            }

            #endregion

            #region Unternehmen G

            if (companyName.Equals("Unternehmen G"))
            {
                var activity1 = CreateMeasuringPoint("HZ01");
                var activity2 = CreateMeasuringPoint("HZ02");
                var activity3 = CreateMeasuringPoint("HZ03");
                var activity4 = CreateMeasuringPoint("HZ04");
                var activity5 = CreateMeasuringPoint("HZ05");
                var activity6 = CreateMeasuringPoint("HZ05");


                measuringPoints.Add(activity1);
                measuringPoints.Add(activity2);
                measuringPoints.Add(activity3);
                measuringPoints.Add(activity4);
                measuringPoints.Add(activity5);
                measuringPoints.Add(activity6);
            }

            #endregion

            #region Unternehmen H

            if (companyName.Equals("Unternehmen H"))
            {
                var activity1 = CreateMeasuringPoint("HZ01");
                var activity2 = CreateMeasuringPoint("HZ02");
                var activity3 = CreateMeasuringPoint("HZ03");


                measuringPoints.Add(activity1);
                measuringPoints.Add(activity2);
                measuringPoints.Add(activity3);
            }

            #endregion

            #region Unternehmen J

            if (companyName.Equals("Unternehmen J"))
            {
                var activity1 = CreateMeasuringPoint("HZ01");
                var activity2 = CreateMeasuringPoint("HZ02");
                var activity3 = CreateMeasuringPoint("HZ03");
                var activity4 = CreateMeasuringPoint("HZ04");
                var activity5 = CreateMeasuringPoint("HZ05");
                var activity6 = CreateMeasuringPoint("HZ06");
                var activity7 = CreateMeasuringPoint("HZ07");
                var activity8 = CreateMeasuringPoint("HZ05");
                var activity9 = CreateMeasuringPoint("HZ05");


                measuringPoints.Add(activity1);
                measuringPoints.Add(activity2);
                measuringPoints.Add(activity3);
                measuringPoints.Add(activity4);
                measuringPoints.Add(activity5);
                measuringPoints.Add(activity6);
                measuringPoints.Add(activity7);
                measuringPoints.Add(activity8);
                measuringPoints.Add(activity9);
            }

            #endregion

            #region Unternehmen L

            if (companyName.Equals("Unternehmen L"))
            {
                var activity1 = CreateMeasuringPoint("HZ01");
                var activity2 = CreateMeasuringPoint("HZ02");
                var activity3 = CreateMeasuringPoint("HZ03");


                measuringPoints.Add(activity1);
                measuringPoints.Add(activity2);
                measuringPoints.Add(activity3);
            }

            #endregion

            #region Unternehmen K

            if (companyName.Equals("Unternehmen K"))
            {
                var activity1 = CreateMeasuringPoint("HZ01");
                var activity2 = CreateMeasuringPoint("HZ02");
                var activity3 = CreateMeasuringPoint("HZ03");
                var activity4 = CreateMeasuringPoint("HZ04");
                var activity5 = CreateMeasuringPoint("HZ05");
                var activity6 = CreateMeasuringPoint("HZ05");
                var activity7 = CreateMeasuringPoint("HZ05");
                var activity8 = CreateMeasuringPoint("HZ05");
                var activity9 = CreateMeasuringPoint("HZ05");
                var activity10 = CreateMeasuringPoint("HZ05");
                var activity11 = CreateMeasuringPoint("HZ05");
                var activity12 = CreateMeasuringPoint("HZ05");


                measuringPoints.Add(activity1);
                measuringPoints.Add(activity2);
                measuringPoints.Add(activity3);
                measuringPoints.Add(activity4);
                measuringPoints.Add(activity5);
                measuringPoints.Add(activity6);
                measuringPoints.Add(activity7);
                measuringPoints.Add(activity8);
                measuringPoints.Add(activity9);
                measuringPoints.Add(activity10);
                measuringPoints.Add(activity11);
                measuringPoints.Add(activity12);
            }

            #endregion

            #region Iso Unternehmen

            if (companyName.Equals("Iso Unternehmen"))
            {
                var activity1 = CreateMeasuringPoint("HZ01");
                var activity2 = CreateMeasuringPoint("HZ02");
                var activity3 = CreateMeasuringPoint("HZ03");
                var activity4 = CreateMeasuringPoint("HZ04");
                var activity5 = CreateMeasuringPoint("HZ05");
                var activity6 = CreateMeasuringPoint("HZ05");
                var activity7 = CreateMeasuringPoint("HZ05");
                var activity8 = CreateMeasuringPoint("HZ05");
                var activity9 = CreateMeasuringPoint("HZ05");
                var activity10 = CreateMeasuringPoint("HZ05");
                var activity11 = CreateMeasuringPoint("HZ05");
                var activity12 = CreateMeasuringPoint("HZ05");


                measuringPoints.Add(activity1);
                measuringPoints.Add(activity2);
                measuringPoints.Add(activity3);
                measuringPoints.Add(activity4);
                measuringPoints.Add(activity5);
                measuringPoints.Add(activity6);
                measuringPoints.Add(activity7);
                measuringPoints.Add(activity8);
                measuringPoints.Add(activity9);
                measuringPoints.Add(activity10);
                measuringPoints.Add(activity11);
                measuringPoints.Add(activity12);
            }

            #endregion

            foreach (var measuringPoint in measuringPoints)
            {
                m_Context.MeasuringPoints.Add(measuringPoint);
            }

            m_Context.SaveChanges();

            return measuringPoints;
        }

        private static ICollection<Measure> CreateMeasures(string companyName)
        {
            var measures = new Collection<Measure>();
            var electicity = m_Context.Sources.FirstOrDefault(s => s.Name == Resources.Resources.Electicity);
            var naturalGas = m_Context.Sources.FirstOrDefault(s => s.Name == Resources.Resources.NatrualGas);
            var heatingOil = m_Context.Sources.FirstOrDefault(s => s.Name == Resources.Resources.HeatingOil);

            var kWh = m_Context.Units.FirstOrDefault(u => u.Symbol == "kWh");

            #region LTI-Metalltechnik GmbH

            if (companyName.Equals("UnternehmenA"))
            {
                var activity1 = CreateActivity("HZ01",
                                               "Optimierung der Heizungsanlage",
                                               Priority.Medium,
                                               1450,
                                               15,
                                               3985,
                                               electicity,
                                               kWh,
                                               MeasureState.Accomplished);

                var activity2 = CreateActivity("HZ02",
                                               "Anbringung Thermostatventile an Umluftheizregister Lackiererei",
                                               Priority.Medium,
                                               12300,
                                               15,
                                               45000,
                                               heatingOil,
                                               kWh,
                                               MeasureState.Open);

                var activity3 = CreateActivity("HZ03",
                                               "Abschaltung des Kessels in den Sommermonaten",
                                               Priority.Medium,
                                               1500,
                                               15,
                                               25267,
                                               heatingOil,
                                               kWh,
                                               MeasureState.Accomplished);

                var activity4 = CreateActivity("HZ04",
                                               "Heizungsanlage Produktionsgebäude. Vervollständigung der Dämmung an Heizungsrohrleitungen",
                                               Priority.Medium,
                                               3253,
                                               10,
                                               42458,
                                               heatingOil,
                                               kWh,
                                               MeasureState.Accomplished);

                var activity5 = CreateActivity("HZ05",
                                               "Anbringung Thermostatventile an Umluftheizregister Lackiererei",
                                               Priority.Medium,
                                               3500,
                                               10,
                                               5535,
                                               heatingOil,
                                               kWh,
                                               MeasureState.Accomplished);

                var activity6 = CreateActivity("HZ06",
                                               "Optimierung der Heizungspumpe (Einsatz geregelte Pumpen)",
                                               Priority.Medium,
                                               570,
                                               10,
                                               1240,
                                               electicity,
                                               kWh,
                                               MeasureState.Open);

                var activity7 = CreateActivity("HZ08",
                                               "Abschaltung des Kessels (380 kW)  in den Sommermonaten",
                                               Priority.Medium,
                                               500,
                                               10,
                                               21841,
                                               electicity,
                                               kWh,
                                               MeasureState.Open);

                var activity8 = CreateActivity("AW01",
                                               "Nutzung der Abwärme aus dem Lackierprozess und Druckluft zur Gebäu-deheizung oder Prozesserwärmung",
                                               Priority.Medium,
                                               25000,
                                               10,
                                               122384,
                                               naturalGas,
                                               kWh,
                                               MeasureState.Open);


                var activity9 = CreateActivity("BL01",
                                               "Austausch Quecksilberdampflampen in Halle L20 (Lagerbereich)",
                                               Priority.Medium,
                                               5400,
                                               15,
                                               6438,
                                               electicity,
                                               kWh,
                                               MeasureState.Reject);


                var activity10 = CreateActivity("BL02",
                                                "Überarbeitung der Beleuchtung in den Produktionsbereichen in Halle L20 (Bereich Anlieferung)",
                                                Priority.Medium,
                                                500,
                                                10,
                                                3825,
                                                electicity,
                                                kWh,
                                                MeasureState.Reject);


                var activity11 = CreateActivity("AN01",
                                                "Einsatz elektrischer Antriebe mit verbesserter Energieeffizienzklasse",
                                                Priority.Medium,
                                                3000,
                                                10,
                                                60711,
                                                electicity,
                                                kWh,
                                                MeasureState.Reject);


                var activity12 = CreateActivity("DL01",
                                                "Einsatz von Pneumatischen-Ventilen an Steuerluftverbrauchern",
                                                Priority.Medium,
                                                4500,
                                                10,
                                                17388,
                                                electicity,
                                                kWh,
                                                MeasureState.Open);


                var activity13 = CreateActivity("DL03",
                                                "Reduzierung der Leckageverluste im Druckluftnetz (Arbeitsluft)",
                                                Priority.Medium,
                                                2000,
                                                10,
                                                9506,
                                                electicity,
                                                kWh,
                                                MeasureState.Reject);


                var activity14 = CreateActivity("KÄ01",
                                                "Brunnenwasser zur Rückkühlung nutzen",
                                                Priority.Medium,
                                                20000,
                                                15,
                                                8000,
                                                electicity,
                                                kWh,
                                                MeasureState.Reject);


                var activity15 = CreateActivity("REG01",
                                                "PV-Anlage",
                                                Priority.Medium,
                                                200000,
                                                15,
                                                26734,
                                                electicity,
                                                kWh,
                                                MeasureState.Reject);
                measures.Add(activity1);
                measures.Add(activity2);
                measures.Add(activity3);
                measures.Add(activity4);
                measures.Add(activity5);
                measures.Add(activity6);
                measures.Add(activity7);
                measures.Add(activity8);
                measures.Add(activity9);
                measures.Add(activity10);
                measures.Add(activity11);
                measures.Add(activity12);
                measures.Add(activity13);
                measures.Add(activity14);
                measures.Add(activity15);
            }

            #endregion

            #region Solvay Fluor GmbH

            if (companyName.Equals("Unternehmen B"))
            {
                var activity1 = CreateActivity("HZ01",
                                               "Einbau Zeitgesteuerte Zirkulationseinrichtung",
                                               Priority.Medium,
                                               200,
                                               10,
                                               3500,
                                               electicity,
                                               kWh,
                                               MeasureState.Accomplished);


                var activity2 = CreateActivity("HZ02",
                                               "Zeitsteuerung für Dunkel-Gasstrahler im Gebäude 23: ",
                                               Priority.Medium,
                                               300,
                                               10,
                                               90000,
                                               naturalGas,
                                               kWh,
                                               MeasureState.Accomplished);


                var activity3 = CreateActivity("DA01",
                                               "Optimierung WRG Economizer des Dampfkessels für Speisewassererwärmung ",
                                               Priority.Medium,
                                               60000,
                                               10,
                                               370000,
                                               naturalGas,
                                               kWh,
                                               MeasureState.Accomplished);


                var activity4 = CreateActivity("DA02",
                                               "Prüfung Einsatz Dampfmotor",
                                               Priority.Medium,
                                               360000,
                                               15,
                                               1000000,
                                               electicity,
                                               kWh,
                                               MeasureState.Reject);


                var activity5 = CreateActivity("BL01",
                                               "Überarbeitung der Beleuchtung im Gebäude 21, Elektro-Werkstatt und Einbau neue EVG",
                                               Priority.Medium,
                                               630,
                                               15,
                                               3158,
                                               electicity,
                                               kWh,
                                               MeasureState.Accomplished);


                var activity6 = CreateActivity("BL02",
                                               "Bewegungsmelder im Bereich Herren WC im Bau 21 (Labor und Werkstatt)",
                                               Priority.Medium,
                                               210,
                                               10,
                                               1677,
                                               electicity,
                                               kWh,
                                               MeasureState.Accomplished);


                var activity7 = CreateActivity("BL03",
                                               "Zonenregelung im Gebäude 026 (Kfz-Werkstatt) + Einbau Reflektoren und EVG",
                                               Priority.Medium,
                                               720,
                                               10,
                                               1848,
                                               electicity,
                                               kWh,
                                               MeasureState.Accomplished);


                var activity8 = CreateActivity("BL04",
                                               "Überprüfung HQL Beleuchtung in der Lagerhalle Kryolith",
                                               Priority.Medium,
                                               4000,
                                               10,
                                               2456,
                                               electicity,
                                               kWh,
                                               MeasureState.Reject);

                var activity9 = CreateActivity("E01",
                                               "Einbau von Frequenzregelung an Brunnenwasserförderpumpe",
                                               Priority.Medium,
                                               45000,
                                               10,
                                               110000,
                                               electicity,
                                               kWh,
                                               MeasureState.Accomplished);

                var activity10 = CreateActivity("E02",
                                                "Austausch Rohrgehäusepumpe im Rückkühlwerk",
                                                Priority.Medium,
                                                60000,
                                                10,
                                                230000,
                                                electicity,
                                                kWh,
                                                MeasureState.Accomplished);

                var activity11 = CreateActivity("E02a",
                                                "Einbau von drehzahlgeregelten Kühlturmpumpen",
                                                Priority.Medium,
                                                115000,
                                                10,
                                                245000,
                                                electicity,
                                                kWh,
                                                MeasureState.Reject);

                var activity12 = CreateActivity("E03",
                                                "Gebäudetechnische Anlagen",
                                                Priority.Medium,
                                                5000,
                                                10,
                                                27000,
                                                electicity,
                                                kWh,
                                                MeasureState.Accomplished);

                var activity13 = CreateActivity("DL01",
                                                "Reduzierung der Leckageverluste",
                                                Priority.Medium,
                                                20000,
                                                10,
                                                100241,
                                                electicity,
                                                kWh,
                                                MeasureState.Accomplished);

                var activity14 = CreateActivity("DL01",
                                                "Absenkung des Druckluftniveaus",
                                                Priority.Medium,
                                                0,
                                                10,
                                                221197,
                                                electicity,
                                                kWh,
                                                MeasureState.Accomplished);

                var activity15 = CreateActivity("DL04",
                                                "Übergeordnete Steuerung Drucklufterzeugung",
                                                Priority.Medium,
                                                90000,
                                                10,
                                                60000,
                                                electicity,
                                                kWh,
                                                MeasureState.Accomplished);

                var activity16 = CreateActivity("DL04",
                                                "Übergeordnete Steuerung Drucklufterzeugung",
                                                Priority.Medium,
                                                140000,
                                                10,
                                                400000,
                                                electicity,
                                                kWh,
                                                MeasureState.Accomplished);

                var activity17 = CreateActivity("DL05",
                                                "Abwärmenutzung aus den Druckkluftanlagen im anorganischen Bereich zur Gebäudeheizung und BWW-Erwärmung",
                                                Priority.Medium,
                                                140000,
                                                10,
                                                400000,
                                                electicity,
                                                kWh,
                                                MeasureState.Reject);

                var activity18 = CreateActivity("DL03",
                                                "Aufbau getrennte Druckluftversorgung für HCL-Ausblasung",
                                                Priority.Medium,
                                                140000,
                                                10,
                                                400000,
                                                electicity,
                                                kWh,
                                                MeasureState.Reject);


                measures.Add(activity1);
                measures.Add(activity2);
                measures.Add(activity3);
                measures.Add(activity4);
                measures.Add(activity5);
                measures.Add(activity6);
                measures.Add(activity7);
                measures.Add(activity8);
                measures.Add(activity9);
                measures.Add(activity10);
                measures.Add(activity11);
                measures.Add(activity12);
                measures.Add(activity13);
                measures.Add(activity14);
                measures.Add(activity15);
                measures.Add(activity16);
                measures.Add(activity17);
                measures.Add(activity18);
            }

            #endregion

            #region Unternehmen C

            if (companyName.Equals("Unternehmen C"))
            {
                var activity2 = CreateActivity("HZ02",
                                               "Zeitsteuerung für Dunkel-Gasstrahler im Gebäude 23: ",
                                               Priority.Medium,
                                               300,
                                               10,
                                               90000,
                                               naturalGas,
                                               kWh,
                                               MeasureState.Accomplished);


                var activity3 = CreateActivity("DA01",
                                               "Optimierung WRG Economizer des Dampfkessels für Speisewassererwärmung ",
                                               Priority.Medium,
                                               60000,
                                               10,
                                               370000,
                                               naturalGas,
                                               kWh,
                                               MeasureState.Accomplished);


                var activity4 = CreateActivity("DA02",
                                               "Prüfung Einsatz Dampfmotor",
                                               Priority.Medium,
                                               360000,
                                               15,
                                               1000000,
                                               electicity,
                                               kWh,
                                               MeasureState.Reject);


                var activity5 = CreateActivity("BL01",
                                               "Überarbeitung der Beleuchtung im Gebäude 21, Elektro-Werkstatt und Einbau neue EVG",
                                               Priority.Medium,
                                               630,
                                               15,
                                               3158,
                                               electicity,
                                               kWh,
                                               MeasureState.Accomplished);


                var activity6 = CreateActivity("BL02",
                                               "Bewegungsmelder im Bereich Herren WC im Bau 21 (Labor und Werkstatt)",
                                               Priority.Medium,
                                               210,
                                               10,
                                               1677,
                                               electicity,
                                               kWh,
                                               MeasureState.Accomplished);


                var activity7 = CreateActivity("BL03",
                                               "Zonenregelung im Gebäude 026 (Kfz-Werkstatt) + Einbau Reflektoren und EVG",
                                               Priority.Medium,
                                               720,
                                               10,
                                               1848,
                                               electicity,
                                               kWh,
                                               MeasureState.Accomplished);


                var activity8 = CreateActivity("BL04",
                                               "Überprüfung HQL Beleuchtung in der Lagerhalle Kryolith",
                                               Priority.Medium,
                                               4000,
                                               10,
                                               2456,
                                               electicity,
                                               kWh,
                                               MeasureState.Reject);

                var activity9 = CreateActivity("E01",
                                               "Einbau von Frequenzregelung an Brunnenwasserförderpumpe",
                                               Priority.Medium,
                                               45000,
                                               10,
                                               110000,
                                               electicity,
                                               kWh,
                                               MeasureState.Accomplished);

                var activity10 = CreateActivity("E02",
                                                "Austausch Rohrgehäusepumpe im Rückkühlwerk",
                                                Priority.Medium,
                                                60000,
                                                10,
                                                230000,
                                                electicity,
                                                kWh,
                                                MeasureState.Accomplished);

                var activity11 = CreateActivity("E02a",
                                                "Einbau von drehzahlgeregelten Kühlturmpumpen",
                                                Priority.Medium,
                                                115000,
                                                10,
                                                245000,
                                                electicity,
                                                kWh,
                                                MeasureState.Reject);

                var activity12 = CreateActivity("E03",
                                                "Gebäudetechnische Anlagen",
                                                Priority.Medium,
                                                5000,
                                                10,
                                                27000,
                                                electicity,
                                                kWh,
                                                MeasureState.Accomplished);

                var activity13 = CreateActivity("DL01",
                                                "Reduzierung der Leckageverluste",
                                                Priority.Medium,
                                                20000,
                                                10,
                                                100241,
                                                electicity,
                                                kWh,
                                                MeasureState.Accomplished);

                var activity14 = CreateActivity("DL01",
                                                "Absenkung des Druckluftniveaus",
                                                Priority.Medium,
                                                0,
                                                10,
                                                221197,
                                                electicity,
                                                kWh,
                                                MeasureState.Accomplished);

                var activity15 = CreateActivity("DL04",
                                                "Übergeordnete Steuerung Drucklufterzeugung",
                                                Priority.Medium,
                                                90000,
                                                10,
                                                60000,
                                                electicity,
                                                kWh,
                                                MeasureState.Accomplished);


                measures.Add(activity2);
                measures.Add(activity3);
                measures.Add(activity4);
                measures.Add(activity5);
                measures.Add(activity6);
                measures.Add(activity7);
                measures.Add(activity8);
                measures.Add(activity9);
                measures.Add(activity10);
                measures.Add(activity11);
                measures.Add(activity12);
                measures.Add(activity13);
                measures.Add(activity14);
                measures.Add(activity15);
            }

            #endregion

            #region Unternehmen D

            if (companyName.Equals("Unternehmen D"))
            {
                var activity1 = CreateActivity("HZ01",
                                               "Einbau Zeitgesteuerte Zirkulationseinrichtung",
                                               Priority.Medium,
                                               200,
                                               10,
                                               3500,
                                               electicity,
                                               kWh,
                                               MeasureState.Accomplished);


                var activity2 = CreateActivity("HZ02",
                                               "Zeitsteuerung für Dunkel-Gasstrahler im Gebäude 23: ",
                                               Priority.Medium,
                                               300,
                                               10,
                                               90000,
                                               naturalGas,
                                               kWh,
                                               MeasureState.Accomplished);


                var activity3 = CreateActivity("DA01",
                                               "Optimierung WRG Economizer des Dampfkessels für Speisewassererwärmung ",
                                               Priority.Medium,
                                               60000,
                                               10,
                                               370000,
                                               naturalGas,
                                               kWh,
                                               MeasureState.Accomplished);


                var activity4 = CreateActivity("DA02",
                                               "Prüfung Einsatz Dampfmotor",
                                               Priority.Medium,
                                               360000,
                                               15,
                                               1000000,
                                               electicity,
                                               kWh,
                                               MeasureState.Reject);


                var activity5 = CreateActivity("BL01",
                                               "Überarbeitung der Beleuchtung im Gebäude 21, Elektro-Werkstatt und Einbau neue EVG",
                                               Priority.Medium,
                                               630,
                                               15,
                                               3158,
                                               electicity,
                                               kWh,
                                               MeasureState.Accomplished);


                var activity6 = CreateActivity("BL02",
                                               "Bewegungsmelder im Bereich Herren WC im Bau 21 (Labor und Werkstatt)",
                                               Priority.Medium,
                                               210,
                                               10,
                                               1677,
                                               electicity,
                                               kWh,
                                               MeasureState.Accomplished);


                var activity7 = CreateActivity("BL03",
                                               "Zonenregelung im Gebäude 026 (Kfz-Werkstatt) + Einbau Reflektoren und EVG",
                                               Priority.Medium,
                                               720,
                                               10,
                                               1848,
                                               electicity,
                                               kWh,
                                               MeasureState.Accomplished);


                var activity8 = CreateActivity("BL04",
                                               "Überprüfung HQL Beleuchtung in der Lagerhalle Kryolith",
                                               Priority.Medium,
                                               4000,
                                               10,
                                               2456,
                                               electicity,
                                               kWh,
                                               MeasureState.Reject);

                var activity9 = CreateActivity("E01",
                                               "Einbau von Frequenzregelung an Brunnenwasserförderpumpe",
                                               Priority.Medium,
                                               45000,
                                               10,
                                               110000,
                                               electicity,
                                               kWh,
                                               MeasureState.Accomplished);

                var activity10 = CreateActivity("E02",
                                                "Austausch Rohrgehäusepumpe im Rückkühlwerk",
                                                Priority.Medium,
                                                60000,
                                                10,
                                                230000,
                                                electicity,
                                                kWh,
                                                MeasureState.Accomplished);

                var activity11 = CreateActivity("E02a",
                                                "Einbau von drehzahlgeregelten Kühlturmpumpen",
                                                Priority.Medium,
                                                115000,
                                                10,
                                                245000,
                                                electicity,
                                                kWh,
                                                MeasureState.Reject);

                var activity12 = CreateActivity("E03",
                                                "Gebäudetechnische Anlagen",
                                                Priority.Medium,
                                                5000,
                                                10,
                                                27000,
                                                electicity,
                                                kWh,
                                                MeasureState.Accomplished);

                var activity13 = CreateActivity("DL01",
                                                "Reduzierung der Leckageverluste",
                                                Priority.Medium,
                                                20000,
                                                10,
                                                100241,
                                                electicity,
                                                kWh,
                                                MeasureState.Accomplished);

                var activity14 = CreateActivity("DL01",
                                                "Absenkung des Druckluftniveaus",
                                                Priority.Medium,
                                                0,
                                                10,
                                                221197,
                                                electicity,
                                                kWh,
                                                MeasureState.Accomplished);


                measures.Add(activity1);
                measures.Add(activity2);
                measures.Add(activity3);
                measures.Add(activity4);
                measures.Add(activity5);
                measures.Add(activity6);
                measures.Add(activity7);
                measures.Add(activity8);
                measures.Add(activity9);
                measures.Add(activity10);
                measures.Add(activity11);
                measures.Add(activity12);
                measures.Add(activity13);
                measures.Add(activity14);
            }

            #endregion

            #region Unternehmen E

            if (companyName.Equals("Unternehmen E"))
            {
                var activity1 = CreateActivity("HZ01",
                                               "Einbau Zeitgesteuerte Zirkulationseinrichtung",
                                               Priority.Medium,
                                               200,
                                               10,
                                               3500,
                                               electicity,
                                               kWh,
                                               MeasureState.Accomplished);


                var activity2 = CreateActivity("HZ02",
                                               "Zeitsteuerung für Dunkel-Gasstrahler im Gebäude 23: ",
                                               Priority.Medium,
                                               300,
                                               10,
                                               90000,
                                               naturalGas,
                                               kWh,
                                               MeasureState.Accomplished);


                var activity3 = CreateActivity("DA01",
                                               "Optimierung WRG Economizer des Dampfkessels für Speisewassererwärmung ",
                                               Priority.Medium,
                                               60000,
                                               10,
                                               370000,
                                               naturalGas,
                                               kWh,
                                               MeasureState.Accomplished);


                var activity4 = CreateActivity("DA02",
                                               "Prüfung Einsatz Dampfmotor",
                                               Priority.Medium,
                                               360000,
                                               15,
                                               1000000,
                                               electicity,
                                               kWh,
                                               MeasureState.Reject);


                var activity5 = CreateActivity("BL01",
                                               "Überarbeitung der Beleuchtung im Gebäude 21, Elektro-Werkstatt und Einbau neue EVG",
                                               Priority.Medium,
                                               630,
                                               15,
                                               3158,
                                               electicity,
                                               kWh,
                                               MeasureState.Accomplished);


                var activity6 = CreateActivity("BL02",
                                               "Bewegungsmelder im Bereich Herren WC im Bau 21 (Labor und Werkstatt)",
                                               Priority.Medium,
                                               210,
                                               10,
                                               1677,
                                               electicity,
                                               kWh,
                                               MeasureState.Accomplished);


                var activity7 = CreateActivity("BL03",
                                               "Zonenregelung im Gebäude 026 (Kfz-Werkstatt) + Einbau Reflektoren und EVG",
                                               Priority.Medium,
                                               720,
                                               10,
                                               1848,
                                               electicity,
                                               kWh,
                                               MeasureState.Accomplished);


                var activity8 = CreateActivity("BL04",
                                               "Überprüfung HQL Beleuchtung in der Lagerhalle Kryolith",
                                               Priority.Medium,
                                               4000,
                                               10,
                                               2456,
                                               electicity,
                                               kWh,
                                               MeasureState.Reject);

                var activity9 = CreateActivity("E01",
                                               "Einbau von Frequenzregelung an Brunnenwasserförderpumpe",
                                               Priority.Medium,
                                               45000,
                                               10,
                                               110000,
                                               electicity,
                                               kWh,
                                               MeasureState.Accomplished);

                var activity10 = CreateActivity("E02",
                                                "Austausch Rohrgehäusepumpe im Rückkühlwerk",
                                                Priority.Medium,
                                                60000,
                                                10,
                                                230000,
                                                electicity,
                                                kWh,
                                                MeasureState.Accomplished);

                var activity11 = CreateActivity("E02a",
                                                "Einbau von drehzahlgeregelten Kühlturmpumpen",
                                                Priority.Medium,
                                                115000,
                                                10,
                                                245000,
                                                electicity,
                                                kWh,
                                                MeasureState.Reject);


                measures.Add(activity1);
                measures.Add(activity2);
                measures.Add(activity3);
                measures.Add(activity4);
                measures.Add(activity5);
                measures.Add(activity6);
                measures.Add(activity7);
                measures.Add(activity8);
                measures.Add(activity9);
                measures.Add(activity10);
                measures.Add(activity11);
            }

            #endregion

            #region Unternehmen F

            if (companyName.Equals("Unternehmen F"))
            {
                var activity1 = CreateActivity("HZ01",
                                               "Optimierung der Heizungsanlage",
                                               Priority.Medium,
                                               1450,
                                               15,
                                               3985,
                                               electicity,
                                               kWh,
                                               MeasureState.Accomplished);

                var activity2 = CreateActivity("HZ02",
                                               "Anbringung Thermostatventile an Umluftheizregister Lackiererei",
                                               Priority.Medium,
                                               12300,
                                               15,
                                               45000,
                                               heatingOil,
                                               kWh,
                                               MeasureState.Open);

                var activity3 = CreateActivity("HZ03",
                                               "Abschaltung des Kessels in den Sommermonaten",
                                               Priority.Medium,
                                               1500,
                                               15,
                                               25267,
                                               heatingOil,
                                               kWh,
                                               MeasureState.Accomplished);

                var activity4 = CreateActivity("HZ04",
                                               "Heizungsanlage Produktionsgebäude. Vervollständigung der Dämmung an Heizungsrohrleitungen",
                                               Priority.Medium,
                                               3253,
                                               10,
                                               42458,
                                               heatingOil,
                                               kWh,
                                               MeasureState.Accomplished);

                var activity5 = CreateActivity("HZ05",
                                               "Anbringung Thermostatventile an Umluftheizregister Lackiererei",
                                               Priority.Medium,
                                               3500,
                                               10,
                                               5535,
                                               heatingOil,
                                               kWh,
                                               MeasureState.Accomplished);

                var activity6 = CreateActivity("HZ06",
                                               "Optimierung der Heizungspumpe (Einsatz geregelte Pumpen)",
                                               Priority.Medium,
                                               570,
                                               10,
                                               1240,
                                               electicity,
                                               kWh,
                                               MeasureState.Open);

                var activity7 = CreateActivity("HZ08",
                                               "Abschaltung des Kessels (380 kW)  in den Sommermonaten",
                                               Priority.Medium,
                                               500,
                                               10,
                                               21841,
                                               electicity,
                                               kWh,
                                               MeasureState.Open);

                var activity8 = CreateActivity("AW01",
                                               "Nutzung der Abwärme aus dem Lackierprozess und Druckluft zur Gebäu-deheizung oder Prozesserwärmung",
                                               Priority.Medium,
                                               25000,
                                               10,
                                               122384,
                                               naturalGas,
                                               kWh,
                                               MeasureState.Open);


                var activity9 = CreateActivity("BL01",
                                               "Austausch Quecksilberdampflampen in Halle L20 (Lagerbereich)",
                                               Priority.Medium,
                                               5400,
                                               15,
                                               6438,
                                               electicity,
                                               kWh,
                                               MeasureState.Reject);


                var activity10 = CreateActivity("BL02",
                                                "Überarbeitung der Beleuchtung in den Produktionsbereichen in Halle L20 (Bereich Anlieferung)",
                                                Priority.Medium,
                                                500,
                                                10,
                                                3825,
                                                electicity,
                                                kWh,
                                                MeasureState.Reject);


                var activity11 = CreateActivity("AN01",
                                                "Einsatz elektrischer Antriebe mit verbesserter Energieeffizienzklasse",
                                                Priority.Medium,
                                                3000,
                                                10,
                                                60711,
                                                electicity,
                                                kWh,
                                                MeasureState.Reject);


                var activity12 = CreateActivity("DL01",
                                                "Einsatz von Pneumatischen-Ventilen an Steuerluftverbrauchern",
                                                Priority.Medium,
                                                4500,
                                                10,
                                                17388,
                                                electicity,
                                                kWh,
                                                MeasureState.Open);


                var activity13 = CreateActivity("DL03",
                                                "Reduzierung der Leckageverluste im Druckluftnetz (Arbeitsluft)",
                                                Priority.Medium,
                                                2000,
                                                10,
                                                9506,
                                                electicity,
                                                kWh,
                                                MeasureState.Reject);


                var activity14 = CreateActivity("KÄ01",
                                                "Brunnenwasser zur Rückkühlung nutzen",
                                                Priority.Medium,
                                                20000,
                                                15,
                                                8000,
                                                electicity,
                                                kWh,
                                                MeasureState.Reject);


                var activity15 = CreateActivity("REG01",
                                                "PV-Anlage",
                                                Priority.Medium,
                                                200000,
                                                15,
                                                26734,
                                                electicity,
                                                kWh,
                                                MeasureState.Reject);
                measures.Add(activity1);
                measures.Add(activity2);
                measures.Add(activity3);
                measures.Add(activity4);
                measures.Add(activity5);
                measures.Add(activity6);
                measures.Add(activity7);
                measures.Add(activity8);
                measures.Add(activity9);
                measures.Add(activity10);
                measures.Add(activity11);
                measures.Add(activity12);
                measures.Add(activity13);
                measures.Add(activity14);
                measures.Add(activity15);
            }

            #endregion

            #region Unternehmen G

            if (companyName.Equals("Unternehmen G"))
            {
                var activity1 = CreateActivity("HZ01",
                                               "Optimierung der Heizungsanlage",
                                               Priority.Medium,
                                               1450,
                                               15,
                                               3985,
                                               electicity,
                                               kWh,
                                               MeasureState.Accomplished);

                var activity2 = CreateActivity("HZ02",
                                               "Anbringung Thermostatventile an Umluftheizregister Lackiererei",
                                               Priority.Medium,
                                               12300,
                                               15,
                                               45000,
                                               heatingOil,
                                               kWh,
                                               MeasureState.Open);

                var activity3 = CreateActivity("HZ03",
                                               "Abschaltung des Kessels in den Sommermonaten",
                                               Priority.Medium,
                                               1500,
                                               15,
                                               25267,
                                               heatingOil,
                                               kWh,
                                               MeasureState.Accomplished);

                var activity4 = CreateActivity("HZ04",
                                               "Heizungsanlage Produktionsgebäude. Vervollständigung der Dämmung an Heizungsrohrleitungen",
                                               Priority.Medium,
                                               3253,
                                               10,
                                               42458,
                                               heatingOil,
                                               kWh,
                                               MeasureState.Accomplished);

                var activity5 = CreateActivity("HZ05",
                                               "Anbringung Thermostatventile an Umluftheizregister Lackiererei",
                                               Priority.Medium,
                                               3500,
                                               10,
                                               5535,
                                               heatingOil,
                                               kWh,
                                               MeasureState.Accomplished);

                var activity6 = CreateActivity("HZ06",
                                               "Optimierung der Heizungspumpe (Einsatz geregelte Pumpen)",
                                               Priority.Medium,
                                               570,
                                               10,
                                               1240,
                                               electicity,
                                               kWh,
                                               MeasureState.Open);

                var activity7 = CreateActivity("HZ08",
                                               "Abschaltung des Kessels (380 kW)  in den Sommermonaten",
                                               Priority.Medium,
                                               500,
                                               10,
                                               21841,
                                               electicity,
                                               kWh,
                                               MeasureState.Open);

                var activity8 = CreateActivity("AW01",
                                               "Nutzung der Abwärme aus dem Lackierprozess und Druckluft zur Gebäu-deheizung oder Prozesserwärmung",
                                               Priority.Medium,
                                               25000,
                                               10,
                                               122384,
                                               naturalGas,
                                               kWh,
                                               MeasureState.Open);


                var activity9 = CreateActivity("BL01",
                                               "Austausch Quecksilberdampflampen in Halle L20 (Lagerbereich)",
                                               Priority.Medium,
                                               5400,
                                               15,
                                               6438,
                                               electicity,
                                               kWh,
                                               MeasureState.Reject);


                var activity10 = CreateActivity("BL02",
                                                "Überarbeitung der Beleuchtung in den Produktionsbereichen in Halle L20 (Bereich Anlieferung)",
                                                Priority.Medium,
                                                500,
                                                10,
                                                3825,
                                                electicity,
                                                kWh,
                                                MeasureState.Reject);


                var activity11 = CreateActivity("AN01",
                                                "Einsatz elektrischer Antriebe mit verbesserter Energieeffizienzklasse",
                                                Priority.Medium,
                                                3000,
                                                10,
                                                60711,
                                                electicity,
                                                kWh,
                                                MeasureState.Reject);


                var activity12 = CreateActivity("DL01",
                                                "Einsatz von Pneumatischen-Ventilen an Steuerluftverbrauchern",
                                                Priority.Medium,
                                                4500,
                                                10,
                                                17388,
                                                electicity,
                                                kWh,
                                                MeasureState.Open);


                var activity13 = CreateActivity("DL03",
                                                "Reduzierung der Leckageverluste im Druckluftnetz (Arbeitsluft)",
                                                Priority.Medium,
                                                2000,
                                                10,
                                                9506,
                                                electicity,
                                                kWh,
                                                MeasureState.Reject);


                measures.Add(activity1);
                measures.Add(activity2);
                measures.Add(activity3);
                measures.Add(activity4);
                measures.Add(activity5);
                measures.Add(activity6);
                measures.Add(activity7);
                measures.Add(activity8);
                measures.Add(activity9);
                measures.Add(activity10);
                measures.Add(activity11);
                measures.Add(activity12);
                measures.Add(activity13);
            }

            #endregion

            #region Unternehmen H

            if (companyName.Equals("Unternehmen H"))
            {
                var activity1 = CreateActivity("HZ01",
                                               "Optimierung der Heizungsanlage",
                                               Priority.Medium,
                                               1450,
                                               15,
                                               3985,
                                               electicity,
                                               kWh,
                                               MeasureState.Accomplished);

                var activity2 = CreateActivity("HZ02",
                                               "Anbringung Thermostatventile an Umluftheizregister Lackiererei",
                                               Priority.Medium,
                                               12300,
                                               15,
                                               45000,
                                               heatingOil,
                                               kWh,
                                               MeasureState.Open);

                var activity3 = CreateActivity("HZ03",
                                               "Abschaltung des Kessels in den Sommermonaten",
                                               Priority.Medium,
                                               1500,
                                               15,
                                               25267,
                                               heatingOil,
                                               kWh,
                                               MeasureState.Accomplished);

                var activity4 = CreateActivity("HZ04",
                                               "Heizungsanlage Produktionsgebäude. Vervollständigung der Dämmung an Heizungsrohrleitungen",
                                               Priority.Medium,
                                               3253,
                                               10,
                                               42458,
                                               heatingOil,
                                               kWh,
                                               MeasureState.Accomplished);

                var activity5 = CreateActivity("HZ05",
                                               "Anbringung Thermostatventile an Umluftheizregister Lackiererei",
                                               Priority.Medium,
                                               3500,
                                               10,
                                               5535,
                                               heatingOil,
                                               kWh,
                                               MeasureState.Accomplished);

                var activity6 = CreateActivity("HZ06",
                                               "Optimierung der Heizungspumpe (Einsatz geregelte Pumpen)",
                                               Priority.Medium,
                                               570,
                                               10,
                                               1240,
                                               electicity,
                                               kWh,
                                               MeasureState.Open);

                var activity7 = CreateActivity("HZ08",
                                               "Abschaltung des Kessels (380 kW)  in den Sommermonaten",
                                               Priority.Medium,
                                               500,
                                               10,
                                               21841,
                                               electicity,
                                               kWh,
                                               MeasureState.Open);

                var activity8 = CreateActivity("AW01",
                                               "Nutzung der Abwärme aus dem Lackierprozess und Druckluft zur Gebäu-deheizung oder Prozesserwärmung",
                                               Priority.Medium,
                                               25000,
                                               10,
                                               122384,
                                               naturalGas,
                                               kWh,
                                               MeasureState.Open);


                var activity9 = CreateActivity("BL01",
                                               "Austausch Quecksilberdampflampen in Halle L20 (Lagerbereich)",
                                               Priority.Medium,
                                               5400,
                                               15,
                                               6438,
                                               electicity,
                                               kWh,
                                               MeasureState.Reject);


                var activity10 = CreateActivity("BL02",
                                                "Überarbeitung der Beleuchtung in den Produktionsbereichen in Halle L20 (Bereich Anlieferung)",
                                                Priority.Medium,
                                                500,
                                                10,
                                                3825,
                                                electicity,
                                                kWh,
                                                MeasureState.Reject);


                measures.Add(activity1);
                measures.Add(activity2);
                measures.Add(activity3);
                measures.Add(activity4);
                measures.Add(activity5);
                measures.Add(activity6);
                measures.Add(activity7);
                measures.Add(activity8);
                measures.Add(activity9);
                measures.Add(activity10);
            }

            #endregion

            #region Unternehmen J

            if (companyName.Equals("Unternehmen J"))
            {
                var activity1 = CreateActivity("HZ01",
                                               "Optimierung der Heizungsanlage",
                                               Priority.Medium,
                                               1450,
                                               15,
                                               3985,
                                               electicity,
                                               kWh,
                                               MeasureState.Accomplished);

                var activity2 = CreateActivity("HZ02",
                                               "Anbringung Thermostatventile an Umluftheizregister Lackiererei",
                                               Priority.Medium,
                                               12300,
                                               15,
                                               45000,
                                               heatingOil,
                                               kWh,
                                               MeasureState.Open);

                var activity3 = CreateActivity("HZ03",
                                               "Abschaltung des Kessels in den Sommermonaten",
                                               Priority.Medium,
                                               1500,
                                               15,
                                               25267,
                                               heatingOil,
                                               kWh,
                                               MeasureState.Accomplished);

                var activity4 = CreateActivity("HZ04",
                                               "Heizungsanlage Produktionsgebäude. Vervollständigung der Dämmung an Heizungsrohrleitungen",
                                               Priority.Medium,
                                               3253,
                                               10,
                                               42458,
                                               heatingOil,
                                               kWh,
                                               MeasureState.Accomplished);

                var activity5 = CreateActivity("HZ05",
                                               "Anbringung Thermostatventile an Umluftheizregister Lackiererei",
                                               Priority.Medium,
                                               3500,
                                               10,
                                               5535,
                                               heatingOil,
                                               kWh,
                                               MeasureState.Accomplished);


                measures.Add(activity1);
                measures.Add(activity2);
                measures.Add(activity3);
                measures.Add(activity4);
                measures.Add(activity5);
            }

            #endregion

            #region Unternehmen L

            if (companyName.Equals("Unternehmen L"))
            {
                var activity2 = CreateActivity("HZ02",
                                               "Zeitsteuerung für Dunkel-Gasstrahler im Gebäude 23: ",
                                               Priority.Medium,
                                               300,
                                               10,
                                               90000,
                                               naturalGas,
                                               kWh,
                                               MeasureState.Accomplished);


                var activity3 = CreateActivity("DA01",
                                               "Optimierung WRG Economizer des Dampfkessels für Speisewassererwärmung ",
                                               Priority.Medium,
                                               60000,
                                               10,
                                               370000,
                                               naturalGas,
                                               kWh,
                                               MeasureState.Accomplished);


                var activity4 = CreateActivity("DA02",
                                               "Prüfung Einsatz Dampfmotor",
                                               Priority.Medium,
                                               360000,
                                               15,
                                               1000000,
                                               electicity,
                                               kWh,
                                               MeasureState.Reject);


                var activity5 = CreateActivity("BL01",
                                               "Überarbeitung der Beleuchtung im Gebäude 21, Elektro-Werkstatt und Einbau neue EVG",
                                               Priority.Medium,
                                               630,
                                               15,
                                               3158,
                                               electicity,
                                               kWh,
                                               MeasureState.Accomplished);


                var activity6 = CreateActivity("BL02",
                                               "Bewegungsmelder im Bereich Herren WC im Bau 21 (Labor und Werkstatt)",
                                               Priority.Medium,
                                               210,
                                               10,
                                               1677,
                                               electicity,
                                               kWh,
                                               MeasureState.Accomplished);


                var activity7 = CreateActivity("BL03",
                                               "Zonenregelung im Gebäude 026 (Kfz-Werkstatt) + Einbau Reflektoren und EVG",
                                               Priority.Medium,
                                               720,
                                               10,
                                               1848,
                                               electicity,
                                               kWh,
                                               MeasureState.Accomplished);


                var activity8 = CreateActivity("BL04",
                                               "Überprüfung HQL Beleuchtung in der Lagerhalle Kryolith",
                                               Priority.Medium,
                                               4000,
                                               10,
                                               2456,
                                               electicity,
                                               kWh,
                                               MeasureState.Reject);

                var activity9 = CreateActivity("E01",
                                               "Einbau von Frequenzregelung an Brunnenwasserförderpumpe",
                                               Priority.Medium,
                                               45000,
                                               10,
                                               110000,
                                               electicity,
                                               kWh,
                                               MeasureState.Accomplished);

                var activity10 = CreateActivity("E02",
                                                "Austausch Rohrgehäusepumpe im Rückkühlwerk",
                                                Priority.Medium,
                                                60000,
                                                10,
                                                230000,
                                                electicity,
                                                kWh,
                                                MeasureState.Accomplished);

                var activity11 = CreateActivity("E02a",
                                                "Einbau von drehzahlgeregelten Kühlturmpumpen",
                                                Priority.Medium,
                                                115000,
                                                10,
                                                245000,
                                                electicity,
                                                kWh,
                                                MeasureState.Reject);

                var activity12 = CreateActivity("E03",
                                                "Gebäudetechnische Anlagen",
                                                Priority.Medium,
                                                5000,
                                                10,
                                                27000,
                                                electicity,
                                                kWh,
                                                MeasureState.Accomplished);

                var activity13 = CreateActivity("DL01",
                                                "Reduzierung der Leckageverluste",
                                                Priority.Medium,
                                                20000,
                                                10,
                                                100241,
                                                electicity,
                                                kWh,
                                                MeasureState.Accomplished);

                var activity14 = CreateActivity("DL01",
                                                "Absenkung des Druckluftniveaus",
                                                Priority.Medium,
                                                0,
                                                10,
                                                221197,
                                                electicity,
                                                kWh,
                                                MeasureState.Accomplished);

                var activity15 = CreateActivity("DL04",
                                                "Übergeordnete Steuerung Drucklufterzeugung",
                                                Priority.Medium,
                                                90000,
                                                10,
                                                60000,
                                                electicity,
                                                kWh,
                                                MeasureState.Accomplished);

                var activity16 = CreateActivity("DL04",
                                                "Übergeordnete Steuerung Drucklufterzeugung",
                                                Priority.Medium,
                                                140000,
                                                10,
                                                400000,
                                                electicity,
                                                kWh,
                                                MeasureState.Accomplished);

                var activity17 = CreateActivity("DL05",
                                                "Abwärmenutzung aus den Druckkluftanlagen im anorganischen Bereich zur Gebäudeheizung und BWW-Erwärmung",
                                                Priority.Medium,
                                                140000,
                                                10,
                                                400000,
                                                electicity,
                                                kWh,
                                                MeasureState.Reject);

                var activity18 = CreateActivity("DL03",
                                                "Aufbau getrennte Druckluftversorgung für HCL-Ausblasung",
                                                Priority.Medium,
                                                140000,
                                                10,
                                                400000,
                                                electicity,
                                                kWh,
                                                MeasureState.Reject);


                measures.Add(activity2);
                measures.Add(activity3);
                measures.Add(activity4);
                measures.Add(activity5);
                measures.Add(activity6);
                measures.Add(activity7);
                measures.Add(activity8);
                measures.Add(activity9);
                measures.Add(activity10);
                measures.Add(activity11);
                measures.Add(activity12);
                measures.Add(activity13);
                measures.Add(activity14);
                measures.Add(activity15);
                measures.Add(activity16);
                measures.Add(activity17);
                measures.Add(activity18);
            }

            #endregion

            #region Unternehmen K

            if (companyName.Equals("Unternehmen K"))
            {
                var activity2 = CreateActivity("HZ02",
                                               "Zeitsteuerung für Dunkel-Gasstrahler im Gebäude 23: ",
                                               Priority.Medium,
                                               300,
                                               10,
                                               90000,
                                               naturalGas,
                                               kWh,
                                               MeasureState.Accomplished);


                var activity3 = CreateActivity("DA01",
                                               "Optimierung WRG Economizer des Dampfkessels für Speisewassererwärmung ",
                                               Priority.Medium,
                                               60000,
                                               10,
                                               370000,
                                               naturalGas,
                                               kWh,
                                               MeasureState.Accomplished);


                var activity4 = CreateActivity("DA02",
                                               "Prüfung Einsatz Dampfmotor",
                                               Priority.Medium,
                                               360000,
                                               15,
                                               1000000,
                                               electicity,
                                               kWh,
                                               MeasureState.Reject);


                var activity5 = CreateActivity("BL01",
                                               "Überarbeitung der Beleuchtung im Gebäude 21, Elektro-Werkstatt und Einbau neue EVG",
                                               Priority.Medium,
                                               630,
                                               15,
                                               3158,
                                               electicity,
                                               kWh,
                                               MeasureState.Accomplished);


                var activity6 = CreateActivity("BL02",
                                               "Bewegungsmelder im Bereich Herren WC im Bau 21 (Labor und Werkstatt)",
                                               Priority.Medium,
                                               210,
                                               10,
                                               1677,
                                               electicity,
                                               kWh,
                                               MeasureState.Accomplished);


                var activity7 = CreateActivity("BL03",
                                               "Zonenregelung im Gebäude 026 (Kfz-Werkstatt) + Einbau Reflektoren und EVG",
                                               Priority.Medium,
                                               720,
                                               10,
                                               1848,
                                               electicity,
                                               kWh,
                                               MeasureState.Accomplished);


                var activity8 = CreateActivity("BL04",
                                               "Überprüfung HQL Beleuchtung in der Lagerhalle Kryolith",
                                               Priority.Medium,
                                               4000,
                                               10,
                                               2456,
                                               electicity,
                                               kWh,
                                               MeasureState.Reject);

                var activity9 = CreateActivity("E01",
                                               "Einbau von Frequenzregelung an Brunnenwasserförderpumpe",
                                               Priority.Medium,
                                               45000,
                                               10,
                                               110000,
                                               electicity,
                                               kWh,
                                               MeasureState.Accomplished);

                var activity10 = CreateActivity("E02",
                                                "Austausch Rohrgehäusepumpe im Rückkühlwerk",
                                                Priority.Medium,
                                                60000,
                                                10,
                                                230000,
                                                electicity,
                                                kWh,
                                                MeasureState.Accomplished);

                var activity11 = CreateActivity("E02a",
                                                "Einbau von drehzahlgeregelten Kühlturmpumpen",
                                                Priority.Medium,
                                                115000,
                                                10,
                                                245000,
                                                electicity,
                                                kWh,
                                                MeasureState.Reject);

                var activity12 = CreateActivity("E03",
                                                "Gebäudetechnische Anlagen",
                                                Priority.Medium,
                                                5000,
                                                10,
                                                27000,
                                                electicity,
                                                kWh,
                                                MeasureState.Accomplished);

                var activity13 = CreateActivity("DL01",
                                                "Reduzierung der Leckageverluste",
                                                Priority.Medium,
                                                20000,
                                                10,
                                                100241,
                                                electicity,
                                                kWh,
                                                MeasureState.Accomplished);

                var activity14 = CreateActivity("DL01",
                                                "Absenkung des Druckluftniveaus",
                                                Priority.Medium,
                                                0,
                                                10,
                                                221197,
                                                electicity,
                                                kWh,
                                                MeasureState.Accomplished);

                var activity15 = CreateActivity("DL04",
                                                "Übergeordnete Steuerung Drucklufterzeugung",
                                                Priority.Medium,
                                                90000,
                                                10,
                                                60000,
                                                electicity,
                                                kWh,
                                                MeasureState.Accomplished);

                var activity16 = CreateActivity("DL04",
                                                "Übergeordnete Steuerung Drucklufterzeugung",
                                                Priority.Medium,
                                                140000,
                                                10,
                                                400000,
                                                electicity,
                                                kWh,
                                                MeasureState.Accomplished);


                measures.Add(activity2);
                measures.Add(activity3);
                measures.Add(activity4);
                measures.Add(activity5);
                measures.Add(activity6);
                measures.Add(activity7);
                measures.Add(activity8);
                measures.Add(activity9);
                measures.Add(activity10);
                measures.Add(activity11);
                measures.Add(activity12);
                measures.Add(activity13);
                measures.Add(activity14);
                measures.Add(activity15);
                measures.Add(activity16);
            }

            #endregion

            #region Iso Unternehmen

            if (companyName.Equals("Iso Unternehmen"))
            {
                var activity2 = CreateActivity("HZ02",
                                               "Zeitsteuerung für Dunkel-Gasstrahler im Gebäude 23: ",
                                               Priority.Medium,
                                               300,
                                               10,
                                               90000,
                                               naturalGas,
                                               kWh,
                                               MeasureState.Accomplished);


                var activity3 = CreateActivity("DA01",
                                               "Optimierung WRG Economizer des Dampfkessels für Speisewassererwärmung ",
                                               Priority.Medium,
                                               60000,
                                               10,
                                               370000,
                                               naturalGas,
                                               kWh,
                                               MeasureState.Accomplished);


                var activity4 = CreateActivity("DA02",
                                               "Prüfung Einsatz Dampfmotor",
                                               Priority.Medium,
                                               360000,
                                               15,
                                               1000000,
                                               electicity,
                                               kWh,
                                               MeasureState.Reject);


                var activity5 = CreateActivity("BL01",
                                               "Überarbeitung der Beleuchtung im Gebäude 21, Elektro-Werkstatt und Einbau neue EVG",
                                               Priority.Medium,
                                               630,
                                               15,
                                               3158,
                                               electicity,
                                               kWh,
                                               MeasureState.Accomplished);


                var activity6 = CreateActivity("BL02",
                                               "Bewegungsmelder im Bereich Herren WC im Bau 21 (Labor und Werkstatt)",
                                               Priority.Medium,
                                               210,
                                               10,
                                               1677,
                                               electicity,
                                               kWh,
                                               MeasureState.Accomplished);


                var activity7 = CreateActivity("BL03",
                                               "Zonenregelung im Gebäude 026 (Kfz-Werkstatt) + Einbau Reflektoren und EVG",
                                               Priority.Medium,
                                               720,
                                               10,
                                               1848,
                                               electicity,
                                               kWh,
                                               MeasureState.Accomplished);


                var activity8 = CreateActivity("BL04",
                                               "Überprüfung HQL Beleuchtung in der Lagerhalle Kryolith",
                                               Priority.Medium,
                                               4000,
                                               10,
                                               2456,
                                               electicity,
                                               kWh,
                                               MeasureState.Reject);

                var activity9 = CreateActivity("E01",
                                               "Einbau von Frequenzregelung an Brunnenwasserförderpumpe",
                                               Priority.Medium,
                                               45000,
                                               10,
                                               110000,
                                               electicity,
                                               kWh,
                                               MeasureState.Accomplished);

                var activity10 = CreateActivity("E02",
                                                "Austausch Rohrgehäusepumpe im Rückkühlwerk",
                                                Priority.Medium,
                                                60000,
                                                10,
                                                230000,
                                                electicity,
                                                kWh,
                                                MeasureState.Accomplished);

                var activity11 = CreateActivity("E02a",
                                                "Einbau von drehzahlgeregelten Kühlturmpumpen",
                                                Priority.Medium,
                                                115000,
                                                10,
                                                245000,
                                                electicity,
                                                kWh,
                                                MeasureState.Reject);

                var activity12 = CreateActivity("E03",
                                                "Gebäudetechnische Anlagen",
                                                Priority.Medium,
                                                5000,
                                                10,
                                                27000,
                                                electicity,
                                                kWh,
                                                MeasureState.Accomplished);

                var activity13 = CreateActivity("DL01",
                                                "Reduzierung der Leckageverluste",
                                                Priority.Medium,
                                                20000,
                                                10,
                                                100241,
                                                electicity,
                                                kWh,
                                                MeasureState.Accomplished);

                var activity14 = CreateActivity("DL01",
                                                "Absenkung des Druckluftniveaus",
                                                Priority.Medium,
                                                0,
                                                10,
                                                221197,
                                                electicity,
                                                kWh,
                                                MeasureState.Accomplished);

                var activity15 = CreateActivity("DL04",
                                                "Übergeordnete Steuerung Drucklufterzeugung",
                                                Priority.Medium,
                                                90000,
                                                10,
                                                60000,
                                                electicity,
                                                kWh,
                                                MeasureState.Accomplished);

                var activity16 = CreateActivity("DL04",
                                                "Übergeordnete Steuerung Drucklufterzeugung",
                                                Priority.Medium,
                                                140000,
                                                10,
                                                400000,
                                                electicity,
                                                kWh,
                                                MeasureState.Accomplished);


                measures.Add(activity2);
                measures.Add(activity3);
                measures.Add(activity4);
                measures.Add(activity5);
                measures.Add(activity6);
                measures.Add(activity7);
                measures.Add(activity8);
                measures.Add(activity9);
                measures.Add(activity10);
                measures.Add(activity11);
                measures.Add(activity12);
                measures.Add(activity13);
                measures.Add(activity14);
                measures.Add(activity15);
                measures.Add(activity16);
            }

            #endregion

            foreach (var activity in measures)
            {
                m_Context.Measures.Add(activity);
            }

            m_Context.SaveChanges();

            return measures;
        }

        private static ICollection<Idea> CreateIdeas(string companyName)
        {
            var ideas = new Collection<Idea>();

            #region Iso Unternehmen

            if (companyName.Equals("Iso Unternehmen"))
            {
                var idea1 = CreateIdea("Idee1",
                                       "Unnötiger Energieverbrauch durch Glühbirnen in Gebäude F",
                                       "Energiesparlampen in Gebäude F",
                                       m_Context.UserProfiles.Find(5) // = IsoAdmin
                    );

                var idea2 = CreateIdea("Idee2",
                                       "Unnötiger Energieverbrauch durch Glühbirnen in Gebäude F",
                                       "Energiesparlampen in Gebäude F",
                                       m_Context.UserProfiles.Find(19) // = IsoUser
                    );

                ideas.Add(idea1);
                ideas.Add(idea2);
            }

            #endregion

            foreach (var idea in ideas)
            {
                m_Context.Ideas.Add(idea);
            }

            m_Context.SaveChanges();

            return ideas;
        }

        private static Valuation CreateEnergeticValuation(decimal investment, double lifetime, double energeticSavings, Source source, Unit unit)
        {
            var energeticValuation = new Valuation{
                                                      Investment = investment,
                                                      Lifetime = lifetime,
                                                      EnergeticSavings = energeticSavings,
                                                      Source = source,
                                                      Unit = unit
                                                  };

            m_Context.Valuations.Add(energeticValuation);
            m_Context.SaveChanges();

            return energeticValuation;
        }

        private static ICollection<EnergyConsumption> CreateEnergyConsumptions(OpEnMsDbContext context, string companyName)
        {
            var energyConsumptions = new Collection<EnergyConsumption>();

            #region LTI-Metalltechnik GmbH

            if (companyName.Equals("UnternehmenA"))
            {
                var electicity = CreateEnergyConsumption(context,
                                                         Resources.Resources.Electicity,
                                                         "MWh",
                                                         CreateReadings(2008,
                                                                        5919,
                                                                        2009,
                                                                        5227,
                                                                        2010,
                                                                        6593,
                                                                        2011,
                                                                        6535));


                var natrualGas = CreateEnergyConsumption(context,
                                                         Resources.Resources.NatrualGas,
                                                         "MWh",
                                                         CreateReadings(2008,
                                                                        2128,
                                                                        2009,
                                                                        1804,
                                                                        2010,
                                                                        1556,
                                                                        2011,
                                                                        2321));

                var heatingOil = CreateEnergyConsumption(context,
                                                         Resources.Resources.HeatingOil,
                                                         "MWh",
                                                         CreateReadings(2008,
                                                                        802,
                                                                        2009,
                                                                        751,
                                                                        2010,
                                                                        874,
                                                                        2011,
                                                                        662));


                energyConsumptions.Add(electicity);
                energyConsumptions.Add(natrualGas);
                energyConsumptions.Add(heatingOil);
            }

            #endregion

            #region Solvay Fluor GmbH

            if (companyName.Equals("Unternehmen B"))
            {
                var electicity = CreateEnergyConsumption(context,
                                                         Resources.Resources.Electicity,
                                                         "MWh",
                                                         CreateReadings(2008,
                                                                        80421,
                                                                        2009,
                                                                        66885,
                                                                        2010,
                                                                        75813,
                                                                        2011,
                                                                        75506));


                var natrualGas = CreateEnergyConsumption(context,
                                                         Resources.Resources.NatrualGas,
                                                         "MWh",
                                                         CreateReadings(2008,
                                                                        113804,
                                                                        2009,
                                                                        99739,
                                                                        2010,
                                                                        110747,
                                                                        2011,
                                                                        94308));

                var heatingOil = CreateEnergyConsumption(context,
                                                         Resources.Resources.HeatingOil,
                                                         "MWh",
                                                         CreateReadings(2008,
                                                                        243,
                                                                        2009,
                                                                        141,
                                                                        2010,
                                                                        0,
                                                                        2011,
                                                                        39));


                energyConsumptions.Add(electicity);
                energyConsumptions.Add(natrualGas);
                energyConsumptions.Add(heatingOil);
            }

            #endregion

            #region Bauer GmbH Druckerei

            if (companyName.Equals("Unternehmen C"))
            {
                var electicity = CreateEnergyConsumption(context,
                                                         Resources.Resources.Electicity,
                                                         "MWh",
                                                         CreateReadings(2008,
                                                                        1187,
                                                                        2009,
                                                                        1143,
                                                                        2010,
                                                                        1256,
                                                                        2011,
                                                                        1278));


                var natrualGas = CreateEnergyConsumption(context,
                                                         Resources.Resources.NatrualGas,
                                                         "MWh",
                                                         CreateReadings(2008,
                                                                        618,
                                                                        2009,
                                                                        603,
                                                                        2010,
                                                                        669,
                                                                        2011,
                                                                        673));


                energyConsumptions.Add(electicity);
                energyConsumptions.Add(natrualGas);
            }

            #endregion

            #region Münzing Chemie

            if (companyName.Equals("Unternehmen D"))
            {
                var electicity = CreateEnergyConsumption(context,
                                                         Resources.Resources.Electicity,
                                                         "MWh",
                                                         CreateReadings(2008,
                                                                        1583,
                                                                        2009,
                                                                        1498,
                                                                        2010,
                                                                        1760,
                                                                        2011,
                                                                        1802));


                var natrualGas = CreateEnergyConsumption(context,
                                                         Resources.Resources.NatrualGas,
                                                         "MWh",
                                                         CreateReadings(2008,
                                                                        2449,
                                                                        2009,
                                                                        2219,
                                                                        2010,
                                                                        2079,
                                                                        2011,
                                                                        1721));

                var heatingOil = CreateEnergyConsumption(context,
                                                         Resources.Resources.HeatingOil,
                                                         "MWh",
                                                         CreateReadings(2008,
                                                                        169,
                                                                        2009,
                                                                        143,
                                                                        2010,
                                                                        183,
                                                                        2011,
                                                                        171));


                energyConsumptions.Add(electicity);
                energyConsumptions.Add(natrualGas);
                energyConsumptions.Add(heatingOil);
            }

            #endregion

            #region KACO GmbH & Co. KG

            if (companyName.Equals("Unternehmen E"))
            {
                var electicity = CreateEnergyConsumption(context,
                                                         Resources.Resources.Electicity,
                                                         "MWh",
                                                         CreateReadings(2008,
                                                                        7939,
                                                                        2009,
                                                                        6409,
                                                                        2010,
                                                                        8524,
                                                                        2011,
                                                                        8605));

                var natrualGas = CreateEnergyConsumption(context,
                                                         Resources.Resources.NatrualGas,
                                                         "MWh",
                                                         CreateReadings(2008,
                                                                        212.46,
                                                                        2009,
                                                                        245,
                                                                        2010,
                                                                        302,
                                                                        2011,
                                                                        228));

                var heatingOil = CreateEnergyConsumption(context,
                                                         Resources.Resources.HeatingOil,
                                                         "MWh",
                                                         CreateReadings(2008,
                                                                        1924.07,
                                                                        2009,
                                                                        2129,
                                                                        2010,
                                                                        2336,
                                                                        2011,
                                                                        2089));


                energyConsumptions.Add(electicity);
                energyConsumptions.Add(natrualGas);
                energyConsumptions.Add(heatingOil);
            }

            #endregion

            #region Albert Berner Deutschland GmbH

            if (companyName.Equals("Unternehmen F"))
            {
                var electicity = CreateEnergyConsumption(context,
                                                         Resources.Resources.Electicity,
                                                         "MWh",
                                                         CreateReadings(2008,
                                                                        1006.6,
                                                                        2009,
                                                                        962.7,
                                                                        2010,
                                                                        842.4,
                                                                        2011,
                                                                        796.5));

                var natrualGas = CreateEnergyConsumption(context,
                                                         Resources.Resources.NatrualGas,
                                                         "MWh",
                                                         CreateReadings(2008,
                                                                        1513.3,
                                                                        2009,
                                                                        1237.5,
                                                                        2010,
                                                                        996,
                                                                        2011,
                                                                        1653.5));

                var heatingOil = CreateEnergyConsumption(context,
                                                         Resources.Resources.HeatingOil,
                                                         "MWh",
                                                         CreateReadings(2008,
                                                                        163.8,
                                                                        2009,
                                                                        515.9,
                                                                        2010,
                                                                        915.2,
                                                                        2011,
                                                                        0));


                energyConsumptions.Add(electicity);
                energyConsumptions.Add(natrualGas);
                energyConsumptions.Add(heatingOil);
            }

            #endregion

            #region VION Crailsheim GmbH

            if (companyName.Equals("Unternehmen G"))
            {
                var electicity = CreateEnergyConsumption(context,
                                                         Resources.Resources.Electicity,
                                                         "MWh",
                                                         CreateReadings(2008,
                                                                        13400,
                                                                        2009,
                                                                        15184,
                                                                        2010,
                                                                        15324,
                                                                        2011,
                                                                        15360));

                var natrualGas = CreateEnergyConsumption(context,
                                                         Resources.Resources.NatrualGas,
                                                         "MWh",
                                                         CreateReadings(2008,
                                                                        18628,
                                                                        2009,
                                                                        18926,
                                                                        2010,
                                                                        20840,
                                                                        2011,
                                                                        19091));


                energyConsumptions.Add(electicity);
                energyConsumptions.Add(natrualGas);
            }

            #endregion

            #region Volksbank Heilbronn eG

            if (companyName.Equals("Unternehmen H"))
            {
                var electicity = CreateEnergyConsumption(context,
                                                         Resources.Resources.Electicity,
                                                         "MWh",
                                                         CreateReadings(2008,
                                                                        13400,
                                                                        2009,
                                                                        15184,
                                                                        2010,
                                                                        15324,
                                                                        2011,
                                                                        15360));

                var natrualGas = CreateEnergyConsumption(context,
                                                         Resources.Resources.NatrualGas,
                                                         "MWh",
                                                         CreateReadings(2008,
                                                                        18628,
                                                                        2009,
                                                                        18926,
                                                                        2010,
                                                                        20840,
                                                                        2011,
                                                                        19091));


                energyConsumptions.Add(electicity);
                energyConsumptions.Add(natrualGas);
            }

            #endregion

            #region ThyssenKrupp System Engineering GmbH

            if (companyName.Equals("Unternehmen I"))
            {
                var electicity = CreateEnergyConsumption(context,
                                                         Resources.Resources.Electicity,
                                                         "MWh",
                                                         CreateReadings(2008,
                                                                        9056,
                                                                        2009,
                                                                        6572,
                                                                        2010,
                                                                        7627,
                                                                        2011,
                                                                        8893));

                var natrualGas = CreateEnergyConsumption(context,
                                                         Resources.Resources.NatrualGas,
                                                         "MWh",
                                                         CreateReadings(2008,
                                                                        230,
                                                                        2009,
                                                                        242,
                                                                        2010,
                                                                        347,
                                                                        2011,
                                                                        269));

                var heatingOil = CreateEnergyConsumption(context,
                                                         Resources.Resources.HeatingOil,
                                                         "MWh",
                                                         CreateReadings(2008,
                                                                        1050,
                                                                        2009,
                                                                        1338.86,
                                                                        2010,
                                                                        1557.42,
                                                                        2011,
                                                                        1376.1));


                energyConsumptions.Add(electicity);
                energyConsumptions.Add(natrualGas);
                energyConsumptions.Add(heatingOil);
            }

            #endregion

            #region Wilhelm Layher GmbH & Co. KG

            if (companyName.Equals("Unternehmen J"))
            {
                var electicity = CreateEnergyConsumption(context,
                                                         Resources.Resources.Electicity,
                                                         "MWh",
                                                         CreateReadings(2008,
                                                                        9931,
                                                                        2009,
                                                                        8539,
                                                                        2010,
                                                                        10634,
                                                                        2011,
                                                                        11071.05));

                var natrualGas = CreateEnergyConsumption(context,
                                                         Resources.Resources.NatrualGas,
                                                         "MWh",
                                                         CreateReadings(2008,
                                                                        20022,
                                                                        2009,
                                                                        14568,
                                                                        2010,
                                                                        21385,
                                                                        2011,
                                                                        18965.58));


                energyConsumptions.Add(electicity);
                energyConsumptions.Add(natrualGas);
            }

            #endregion

            #region RECARO Aircraft Seating GmbH & Co. KG

            if (companyName.Equals("Unternehmen K"))
            {
                var electicity = CreateEnergyConsumption(context,
                                                         Resources.Resources.Electicity,
                                                         "MWh",
                                                         CreateReadings(2008,
                                                                        4265,
                                                                        2009,
                                                                        3990,
                                                                        2010,
                                                                        3783,
                                                                        2011,
                                                                        3607));

                var natrualGas = CreateEnergyConsumption(context,
                                                         Resources.Resources.NatrualGas,
                                                         "MWh",
                                                         CreateReadings(2008,
                                                                        1226,
                                                                        2009,
                                                                        1227,
                                                                        2010,
                                                                        1228,
                                                                        2011,
                                                                        1220));


                energyConsumptions.Add(electicity);
                energyConsumptions.Add(natrualGas);
            }

            #endregion

            foreach (var energyConsumption in energyConsumptions)
            {
                m_Context.EnergyConsumptions.Add(energyConsumption);
            }

            m_Context.SaveChanges();

            return energyConsumptions;
        }

        private static ICollection<Reading> CreateReadings(int year1, double reading1, int year2, double reading2, int year3, double reading3, int year4, double reading4)
        {
            var readings = new Collection<Reading>();

            var readingOne = new Reading{
                                            Timestamp = new DateTime(year1,
                                                                     12,
                                                                     31),
                                            Value = reading1
                                        };
            var readingTwo = new Reading{
                                            Timestamp = new DateTime(year2,
                                                                     12,
                                                                     31),
                                            Value = reading2
                                        };
            var readingThree = new Reading{
                                              Timestamp = new DateTime(year3,
                                                                       12,
                                                                       31),
                                              Value = reading3
                                          };

            var readingFour = new Reading{
                                             Timestamp = new DateTime(year4,
                                                                      12,
                                                                      31),
                                             Value = reading3
                                         };
            readings.Add(readingOne);
            readings.Add(readingTwo);
            readings.Add(readingThree);
            readings.Add(readingFour);

            foreach (var reading in readings)
            {
                m_Context.Readings.Add(reading);
            }

            m_Context.SaveChanges();

            return readings;
        }

        private static EnergyConsumption CreateEnergyConsumption(OpEnMsDbContext context, string energyType, string unit, ICollection<Reading> readings)
        {
            var energyConsumption = new EnergyConsumption{
                                                             Source = context.Sources.FirstOrDefault(s => s.Name == energyType),
                                                             Unit = context.Units.FirstOrDefault(u => u.Symbol == unit),
                                                             Readings = readings
                                                         };
            return energyConsumption;
        }

        private static Measure CreateActivity(string name, string solution, Priority priority, decimal investment, double lifetime, double energeticSavings, Source source, Unit unit, MeasureState state)
        {
            var activity = new Measure{
                                          Solution = solution,
                                          Name = name,
                                          Priority = priority,
                                          Valuation = CreateEnergeticValuation(investment,
                                                                               lifetime,
                                                                               energeticSavings,
                                                                               source,
                                                                               unit),
                                          DueDate = DateTime.Now,
                                          EntryDate = DateTime.Now,
                                          State = state
                                      };


            return activity;
        }

        private static Idea CreateIdea(string name, string solution, string description, UserProfile creator)
        {
            var idea = new Idea{
                                   Name = name,
                                   Solution = solution,
                                   Description = description,
                                   Creator = creator
                               };

            return idea;
        }

        private static MeasuringPoint CreateMeasuringPoint(string name)
        {
            var measuringPoint = new MeasuringPoint{
                                                       Name = name,
                                                       EnergyConsumptions = new Collection<EnergyConsumption>()
                                                   };

            var electicity = CreateEnergyConsumption(m_Context,
                                                     Resources.Resources.Electicity,
                                                     "MWh",
                                                     CreateReadings(2008,
                                                                    1345,
                                                                    2009,
                                                                    1500,
                                                                    2010,
                                                                    1764,
                                                                    2011,
                                                                    2005));

            measuringPoint.EnergyConsumptions.Add(electicity);

            return measuringPoint;
        }

        private static void CreateSources(OpEnMsDbContext context)
        {
            var electicity = new Source{
                                           Name = Resources.Resources.Electicity
                                       };
            var natrualGas = new Source{
                                           Name = Resources.Resources.NatrualGas
                                       };
            var liquidGas = new Source{
                                          Name = Resources.Resources.LiquidGas
                                      };
            var heatingOil = new Source{
                                           Name = Resources.Resources.HeatingOil
                                       };
            var pellet = new Source{
                                       Name = Resources.Resources.Pellet
                                   };
            var woodchips = new Source{
                                          Name = Resources.Resources.Woodchips
                                      };
            context.Sources.Add(electicity);
            context.Sources.Add(natrualGas);
            context.Sources.Add(liquidGas);
            context.Sources.Add(heatingOil);
            context.Sources.Add(pellet);
            context.Sources.Add(woodchips);
            context.SaveChanges();
        }

        private static void CreateUnits(OpEnMsDbContext context)
        {
            // Seed initial Units
            var unitFactory = new UnitFactory();
            foreach (Unit unit in unitFactory.CreateEnergySiUnits())
            {
                context.Units.Add(unit);
            }
            foreach (Unit unit in unitFactory.CreateVolumeSiUnits())
            {
                context.Units.Add(unit);
            }
            foreach (Unit unit in unitFactory.CreateMassSiUnits())
            {
                context.Units.Add(unit);
            }
        }


        private static void CreateUserData()
        {
            WebSecurity.InitializeDatabaseConnection("OpEnMsDbContext",
                                                     "UserProfile",
                                                     "UserId",
                                                     "UserName",
                                                     autoCreateTables : true);
            var roles = (SimpleRoleProvider) Roles.Provider;

            CreateRoles(roles);
            CreateUsers(roles);
            CreateCompanyUsers(roles);
        }

        private static void CreateCompanyUsers(SimpleRoleProvider roles)
        {
            if (!WebSecurity.UserExists("CHamel"))
            {
                WebSecurity.CreateUserAndAccount("CHamel",
                                                 "password",
                                                 new{
                                                        GivenName = "Christian",
                                                        SurName = "Hamel",
                                                        Phone = "0706351115",
                                                        Email = "s0530312@htw-berlin.de"
                                                    },
                                                 false);
            }
            if (!roles.GetRolesForUser("CHamel").
                       Equals("Benutzer"))
            {
                roles.AddUsersToRoles(new[]{
                                               "CHamel"
                                           },
                                      new[]{
                                               "Benutzer"
                                           });
            }
            if (!WebSecurity.UserExists("KDSchwab"))
            {
                WebSecurity.CreateUserAndAccount("KDSchwab",
                                                 "password",
                                                 new{
                                                        GivenName = "Klaus-Dieter",
                                                        SurName = "Schwab",
                                                        Phone = "0706351115",
                                                        Email = "s0530312@htw-berlin.de"
                                                    },
                                                 false);
            }
            if (!roles.GetRolesForUser("KDSchwab").
                       Equals("Benutzer"))
            {
                roles.AddUsersToRoles(new[]{
                                               "KDSchwab"
                                           },
                                      new[]{
                                               "Benutzer"
                                           });
            }
            if (!WebSecurity.UserExists("HUllrich"))
            {
                WebSecurity.CreateUserAndAccount("HUllrich",
                                                 "password",
                                                 new{
                                                        GivenName = "Herwig",
                                                        SurName = "Ullrich",
                                                        Phone = "079309945212",
                                                        Email = "s0530312@htw-berlin.de"
                                                    },
                                                 false);
            }
            if (!roles.GetRolesForUser("HUllrich").
                       Equals("Benutzer"))
            {
                roles.AddUsersToRoles(new[]{
                                               "HUllrich"
                                           },
                                      new[]{
                                               "Benutzer"
                                           });
            }
            if (!WebSecurity.UserExists("CLob"))
            {
                WebSecurity.CreateUserAndAccount("CLob",
                                                 "password",
                                                 new{
                                                        GivenName = "Christian",
                                                        SurName = "Lob",
                                                        Phone = "07131987149",
                                                        Email = "s0530312@htw-berlin.de"
                                                    },
                                                 false);
            }
            if (!roles.GetRolesForUser("CLob").
                       Equals("Benutzer"))
            {
                roles.AddUsersToRoles(new[]{
                                               "CLob"
                                           },
                                      new[]{
                                               "Benutzer"
                                           });
            }

            if (!WebSecurity.UserExists("ENehl"))
            {
                WebSecurity.CreateUserAndAccount("ENehl",
                                                 "password",
                                                 new{
                                                        GivenName = "Eberhard",
                                                        SurName = "Nehl",
                                                        Phone = "07135104155",
                                                        Email = "s0530312@htw-berlin.de"
                                                    },
                                                 false);
            }
            if (!roles.GetRolesForUser("ENehl").
                       Equals("Benutzer"))
            {
                roles.AddUsersToRoles(new[]{
                                               "ENehl"
                                           },
                                      new[]{
                                               "Benutzer"
                                           });
            }

            if (!WebSecurity.UserExists("ABach"))
            {
                WebSecurity.CreateUserAndAccount("ABach",
                                                 "password",
                                                 new{
                                                        GivenName = "Alexander",
                                                        SurName = "Bach",
                                                        Phone = "07131636366",
                                                        Email = "s0530312@htw-berlin.de"
                                                    },
                                                 false);
            }
            if (!roles.GetRolesForUser("ABach").
                       Equals("Benutzer"))
            {
                roles.AddUsersToRoles(new[]{
                                               "ABach"
                                           },
                                      new[]{
                                               "Benutzer"
                                           });
            }
            if (!WebSecurity.UserExists("HJHeßlinger"))
            {
                WebSecurity.CreateUserAndAccount("HJHeßlinger",
                                                 "password",
                                                 new{
                                                        GivenName = "Hans-Joachim",
                                                        SurName = "Heßlinger",
                                                        Phone = "07940121159",
                                                        Email = "s0530312@htw-berlin.de"
                                                    },
                                                 false);
            }
            if (!roles.GetRolesForUser("HJHeßlinger").
                       Equals("Benutzer"))
            {
                roles.AddUsersToRoles(new[]{
                                               "HJHeßlinger"
                                           },
                                      new[]{
                                               "Benutzer"
                                           });
            }
            if (!WebSecurity.UserExists("KRemmlinger"))
            {
                WebSecurity.CreateUserAndAccount("KRemmlinger",
                                                 "password",
                                                 new{
                                                        GivenName = "Klaus",
                                                        SurName = "Remmlinger",
                                                        Phone = "079513031047",
                                                        Email = "s0530312@htw-berlin.de"
                                                    },
                                                 false);
            }
            if (!roles.GetRolesForUser("KRemmlinger").
                       Equals("Benutzer"))
            {
                roles.AddUsersToRoles(new[]{
                                               "KRemmlinger"
                                           },
                                      new[]{
                                               "Benutzer"
                                           });
            }
            if (!WebSecurity.UserExists("MMustermann"))
            {
                WebSecurity.CreateUserAndAccount("MMustermann",
                                                 "password",
                                                 new{
                                                        GivenName = "Max",
                                                        SurName = "Mustermann",
                                                        Phone = "074128741",
                                                        Email = "s0530312@htw-berlin.de"
                                                    },
                                                 false);
            }
            if (!roles.GetRolesForUser("MMustermann").
                       Equals("Benutzer"))
            {
                roles.AddUsersToRoles(new[]{
                                               "MMustermann"
                                           },
                                      new[]{
                                               "Benutzer"
                                           });
            }
            if (!WebSecurity.UserExists("EBauer"))
            {
                WebSecurity.CreateUserAndAccount("EBauer",
                                                 "password",
                                                 new{
                                                        GivenName = "Eberhard",
                                                        SurName = "Bauer",
                                                        Phone = "07941917410",
                                                        Email = "s0530312@htw-berlin.de"
                                                    },
                                                 false);
            }
            if (!roles.GetRolesForUser("EBauer").
                       Equals("Benutzer"))
            {
                roles.AddUsersToRoles(new[]{
                                               "EBauer"
                                           },
                                      new[]{
                                               "Benutzer"
                                           });
            }
            if (!WebSecurity.UserExists("SSchneider"))
            {
                WebSecurity.CreateUserAndAccount("SSchneider",
                                                 "password",
                                                 new{
                                                        GivenName = "Sven",
                                                        SurName = "Schneider",
                                                        Phone = "071311569401",
                                                        Email = "s0530312@htw-berlin.de"
                                                    },
                                                 false);
            }
            if (!roles.GetRolesForUser("SSchneider").
                       Equals("Benutzer"))
            {
                roles.AddUsersToRoles(new[]{
                                               "SSchneider"
                                           },
                                      new[]{
                                               "Benutzer"
                                           });
            }
            if (!WebSecurity.UserExists("KMüller"))
            {
                WebSecurity.CreateUserAndAccount("KMüller",
                                                 "password",
                                                 new{
                                                        GivenName = "Klaus",
                                                        SurName = "Müller",
                                                        Phone = "0713570354",
                                                        Email = "s0530312@htw-berlin.de"
                                                    },
                                                 false);
            }
            if (!roles.GetRolesForUser("KMüller").
                       Equals("Benutzer"))
            {
                roles.AddUsersToRoles(new[]{
                                               "KMüller"
                                           },
                                      new[]{
                                               "Benutzer"
                                           });
            }
            if (!WebSecurity.UserExists("BWalter"))
            {
                WebSecurity.CreateUserAndAccount("BWalter",
                                                 "password",
                                                 new{
                                                        GivenName = "Bernd",
                                                        SurName = "walter",
                                                        Phone = "07915037141",
                                                        Email = "s0530312@htw-berlin.de"
                                                    },
                                                 false);
            }
            if (!roles.GetRolesForUser("BWalter").
                       Equals("Benutzer"))
            {
                roles.AddUsersToRoles(new[]{
                                               "BWalter"
                                           },
                                      new[]{
                                               "Benutzer"
                                           });
            }
            if (!WebSecurity.UserExists("IsoUser"))
            {
                WebSecurity.CreateUserAndAccount("IsoUser",
                                                 "IsoUser",
                                                 new{
                                                        GivenName = "Iso",
                                                        SurName = "User",
                                                        Phone = "030777",
                                                        Email = "iso.user@htw-berlin.de"
                                                    },
                                                 false);
            }
            if (!roles.GetRolesForUser("IsoUser").
                       Equals("IsoBenutzer"))
            {
                roles.AddUsersToRoles(new[]{
                                               "IsoUser"
                                           },
                                      new[]{
                                               "IsoBenutzer"
                                           });
            }
        }


        private static void CreateUsers(SimpleRoleProvider roles)
        {
            if (!WebSecurity.UserExists("Admin"))
            {
                WebSecurity.CreateUserAndAccount("Admin",
                                                 "Admin",
                                                 new{
                                                        GivenName = "Maximilian",
                                                        SurName = "Schneider",
                                                        Email = "s0530312@htw-berlin.de"
                                                    },
                                                 false);
            }
            if (!roles.GetRolesForUser("Admin").
                       Equals("Administrator"))
            {
                roles.AddUsersToRoles(new[]{
                                               "Admin"
                                           },
                                      new[]{
                                               "Administrator"
                                           });
            }

            if (!WebSecurity.UserExists("Nicole Meier"))
            {
                WebSecurity.CreateUserAndAccount("Nicole Meier",
                                                 "password",
                                                 new{
                                                        GivenName = "Nicole",
                                                        SurName = "Meier",
                                                        Email = "nmeier@noreply.de"
                                                    },
                                                 false);
            }

            if (!roles.GetRolesForUser("Nicole Meier").
                       Equals("Administrator"))
            {
                roles.AddUsersToRoles(new[]{
                                               "Nicole Meier"
                                           },
                                      new[]{
                                               "Administrator"
                                           });
            }

            if (!WebSecurity.UserExists("Jürgen Szilinski"))
            {
                WebSecurity.CreateUserAndAccount("Jürgen Szilinski",
                                                 "password",
                                                 new{
                                                        GivenName = "Jürgen",
                                                        SurName = "Szilinski",
                                                        Email = "jszilinski@norepley.de"
                                                    },
                                                 false);
            }
            if (!roles.GetRolesForUser("Jürgen Szilinski").
                       Equals("Administrator"))
            {
                roles.AddUsersToRoles(new[]{
                                               "Jürgen Szilinski"
                                           },
                                      new[]{
                                               "Administrator"
                                           });
            }
            if (!WebSecurity.UserExists("Monika Hack"))
            {
                WebSecurity.CreateUserAndAccount("Monika Hack",
                                                 "password",
                                                 new{
                                                        GivenName = "Monika",
                                                        SurName = "Hack",
                                                        Email = "mhack@norepley.de"
                                                    },
                                                 false);
            }
            if (!roles.GetRolesForUser("Monika Hack").
                       Equals("Administrator"))
            {
                roles.AddUsersToRoles(new[]{
                                               "Monika Hack"
                                           },
                                      new[]{
                                               "Administrator"
                                           });
            }
            if (!WebSecurity.UserExists("IsoAdmin"))
            {
                WebSecurity.CreateUserAndAccount("IsoAdmin",
                                                 "IsoAdmin",
                                                 new{
                                                        GivenName = "Iso",
                                                        SurName = "Admin",
                                                        Email = "iadmin@norepley.de"
                                                    },
                                                 false);
            }
            if (!roles.GetRolesForUser("IsoAdmin").
                       Equals("IsoAdministrator"))
            {
                roles.AddUsersToRoles(new[]{
                                               "IsoAdmin"
                                           },
                                      new[]{
                                               "IsoAdministrator"
                                           });
            }
        }

        private static void CreateRoles(SimpleRoleProvider roles)
        {
            if (!roles.RoleExists("Administrator"))
            {
                roles.CreateRole("Administrator");
            }
            if (!roles.RoleExists("Benutzer"))
            {
                roles.CreateRole("Benutzer");
            }
            if (!roles.RoleExists("IsoAdministrator"))
            {
                roles.CreateRole("IsoAdministrator");
            }
            if (!roles.RoleExists("IsoBenutzer"))
            {
                roles.CreateRole("IsoBenutzer");
            }
        }

        private static void CreateIndicators(OpEnMsDbContext context)
        {
            var pieces = new Unit
            {
                Dimension = "Quantity",
                Name = "pieces",
                Symbol = "pcs.",
            };
            context.Units.Add(pieces);

            var kwhperpiece = new Unit
            {
                Dimension = "Consumtion",
                Name = "kWh per piece",
                Symbol = "kWh/piece",
            };
            context.Units.Add(kwhperpiece);

            var person = new Unit
            {
                Dimension = "Quantity",
                Name = "person",
                Symbol = "pers.",
            };
            context.Units.Add(person);

            var allgemeineUnternehmensdaten = new Topic{
                                                           Name = "Allgemeine Unternehmenskennzahlen"
                                                       };
            context.Topics.Add(allgemeineUnternehmensdaten);

            var anzahlbeschaeftigte = new Indicator();
            anzahlbeschaeftigte.Created = DateTime.Now;
            anzahlbeschaeftigte.CreatedBy = context.UserProfiles.FirstOrDefault();
            anzahlbeschaeftigte.LastEdit = DateTime.Now;
            anzahlbeschaeftigte.LastEditBy = anzahlbeschaeftigte.CreatedBy;
            anzahlbeschaeftigte.Name = "Anzahl der Beschäftigten";
            anzahlbeschaeftigte.Number = "AU1";
            anzahlbeschaeftigte.Explanation = "Anzahl aller im Unternehmen angestellten Mitarbeiter (exklusive Leiharbeiter)";
            anzahlbeschaeftigte.Unit = person;
            anzahlbeschaeftigte.Topic = allgemeineUnternehmensdaten;

            var energieverbrauch = new Topic
            {
                Name = "Verbrauch"
            };
            context.Topics.Add(energieverbrauch);

            var verbrauchterStrom = new Indicator();
            verbrauchterStrom.Created = DateTime.Now;
            verbrauchterStrom.CreatedBy = context.UserProfiles.FirstOrDefault();
            verbrauchterStrom.LastEdit = DateTime.Now;
            verbrauchterStrom.LastEditBy = anzahlbeschaeftigte.CreatedBy;
            verbrauchterStrom.Name = "In der Produktion eingesetzter Strom";
            verbrauchterStrom.Number = "E1";
            verbrauchterStrom.Explanation = "Gesamter zur Produktion von Gütern eingesetzter Strom";
            verbrauchterStrom.Unit = context.Units.First(u => u.Symbol.Equals("kWh"));
            verbrauchterStrom.Topic = energieverbrauch;

            var produktiontopic = new Topic
            {
                Name = "Fertigung"
            };
            context.Topics.Add(produktiontopic);

            var produzierteGueter = new Indicator();
            produzierteGueter.Created = DateTime.Now;
            produzierteGueter.CreatedBy = context.UserProfiles.FirstOrDefault();
            produzierteGueter.LastEdit = DateTime.Now;
            produzierteGueter.LastEditBy = anzahlbeschaeftigte.CreatedBy;
            produzierteGueter.Name = "Hergestellte Gürter";
            produzierteGueter.Number = "F1";
            produzierteGueter.Explanation = "Alle Hergestellten Güter inkl. Ausschuss";
            produzierteGueter.Unit = pieces;
            produzierteGueter.Topic = produktiontopic;

            var betriebsAusstattung = new Topic{
                                                   Name = "Betriebsausstattung"
                                               };
            context.Topics.Add(betriebsAusstattung);

            var anzahlkaffemaschinen = new Indicator();
            anzahlkaffemaschinen.Created = DateTime.Now;
            anzahlkaffemaschinen.CreatedBy = context.UserProfiles.FirstOrDefault();
            anzahlkaffemaschinen.LastEdit = DateTime.Now;
            anzahlkaffemaschinen.LastEditBy = anzahlbeschaeftigte.CreatedBy;
            anzahlkaffemaschinen.Name = "Anzahl der Kaffemaschinen";
            anzahlkaffemaschinen.Number = "BA1";
            anzahlkaffemaschinen.Explanation = "Anzahl der vom Betrieb gestellten Keffemaschinen";
            anzahlkaffemaschinen.Unit = pieces;
            anzahlkaffemaschinen.Topic = betriebsAusstattung;

            var anzahlGemeinschaftsraeume = new Indicator();
            anzahlGemeinschaftsraeume.Created = DateTime.Now;
            anzahlGemeinschaftsraeume.CreatedBy = context.UserProfiles.FirstOrDefault();
            anzahlGemeinschaftsraeume.LastEdit = DateTime.Now;
            anzahlGemeinschaftsraeume.LastEditBy = anzahlbeschaeftigte.CreatedBy;
            anzahlGemeinschaftsraeume.Name = "Fläche der Gemeinschaftsräume";
            anzahlGemeinschaftsraeume.Number = "BA2";
            anzahlGemeinschaftsraeume.Explanation = "Ohne Sanitäre Einrichtungen";
            anzahlGemeinschaftsraeume.Unit = pieces;
            anzahlGemeinschaftsraeume.Topic = betriebsAusstattung;

            var produktivitaet = new Topic{
                                              Name = "Produktivität"
                                          };
            context.Topics.Add(produktivitaet);

            var mitarbeiterproduktivität = new Indicator();
            mitarbeiterproduktivität.Created = DateTime.Now;
            mitarbeiterproduktivität.CreatedBy = context.UserProfiles.FirstOrDefault();
            mitarbeiterproduktivität.LastEdit = DateTime.Now;
            mitarbeiterproduktivität.LastEditBy = anzahlbeschaeftigte.CreatedBy;
            mitarbeiterproduktivität.Name = "Anzahl der Kaffemaschinen je Mitarbeiter";
            mitarbeiterproduktivität.Number = "P1";
            mitarbeiterproduktivität.Explanation = "Je mehr Kaffemaschinen desto besser";
            mitarbeiterproduktivität.Unit = pieces;
            mitarbeiterproduktivität.Topic = produktivitaet;
            mitarbeiterproduktivität.IndicatorExpression = @"[BA1]/[AU1]";


            var effizienz = new Topic
            {
                Name = "Effizienz"
            };
            context.Topics.Add(effizienz);

            var stromverbrauchproduktionseinheit = new Indicator();
            stromverbrauchproduktionseinheit.Created = DateTime.Now;
            stromverbrauchproduktionseinheit.CreatedBy = context.UserProfiles.FirstOrDefault();
            stromverbrauchproduktionseinheit.LastEdit = DateTime.Now;
            stromverbrauchproduktionseinheit.LastEditBy = anzahlbeschaeftigte.CreatedBy;
            stromverbrauchproduktionseinheit.Name = "Eingesetzer Strom pro produziertem Gut";
            stromverbrauchproduktionseinheit.Number = "P1";
            stromverbrauchproduktionseinheit.Explanation = "Ein kleinerer Wert bedeutet eine höhere Energieeffizienz";
            stromverbrauchproduktionseinheit.Unit = kwhperpiece;
            stromverbrauchproduktionseinheit.Topic = effizienz;
            stromverbrauchproduktionseinheit.IndicatorExpression = @"[E1]/[F1]";

            context.Indicators.Add(anzahlbeschaeftigte);
            context.Indicators.Add(anzahlGemeinschaftsraeume);
            context.Indicators.Add(anzahlkaffemaschinen);
            context.Indicators.Add(anzahlkaffemaschinen);
            context.Indicators.Add(produzierteGueter);
            context.Indicators.Add(verbrauchterStrom);
            context.Indicators.Add(mitarbeiterproduktivität);
            context.Indicators.Add(stromverbrauchproduktionseinheit);

            context.SaveChanges();
        }
    }
}