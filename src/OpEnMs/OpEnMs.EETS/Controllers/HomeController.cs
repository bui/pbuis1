﻿#region Licence

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0.html
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// 
// Copyright (c) 2013, HTW Berlin

#endregion

using System.Web.Mvc;
using OpEnMs.Application.Services.IdeaManagement;
using OpEnMs.Application.Services.IsoMeasureManagement;
using OpEnMs.Application.Services.UserProfileManagement;
using OpEnMs.EETS.Factories;

namespace OpEnMs.EETS.Controllers
{
    public class HomeController: Controller
    {
        private readonly IIsoMeasureManagementService m_IsoMeasureManagementService;
        private readonly IIdeaManagementService m_IdeaManagementService;
        private readonly IUserProfileManagementService m_UserProfileManagementService;

        public HomeController(IIsoMeasureManagementService isoMeasureManagementService, IIdeaManagementService ideaManagementService, IUserProfileManagementService userProfileManagementService)
        {
            m_IsoMeasureManagementService = isoMeasureManagementService;
            m_IdeaManagementService = ideaManagementService;
            m_UserProfileManagementService = userProfileManagementService;
        }

        public ActionResult Index()
        {
            ViewBag.Message = Resources.Resources.Welcome;
            var viewModel = HomeViewModelFactory.CreateHomeViewModel();
            if (User.Identity.IsAuthenticated)
            {
                var user = m_UserProfileManagementService.Get(u => u.UserName.Equals(User.Identity.Name));
                if (user == null)
                {
                    return HttpNotFound();
                }

                viewModel = HomeViewModelFactory.CreateHomeViewModel(IdeaViewModelFactory.CreateIdeaListViewModels(m_IdeaManagementService.GetIdeasByUserIdentity(User.Identity.Name)),
                                                                         IsoMeasureViewModelFactory.CreateIsoMeasureListViewModels(m_IsoMeasureManagementService.GetIsoMeasuresByUserIdentity(User.Identity.Name)));
            }
            return View(viewModel);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your app description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}