﻿#region Licence

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0.html
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// 
// Copyright (c) 2013, HTW Berlin

#endregion

using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web.Mvc;
using Emporer.Unit;
using OpEnMs.Application.Services.IndicatorManagement;
using OpEnMs.Domain.Entities;
using OpEnMs.EETS.Factories;
using OpEnMs.EETS.ViewModels.Indicator;
using OpEnMs.EnergyChart.Factories;

namespace OpEnMs.EETS.Controllers
{
    [Authorize(Roles = "IsoAdministrator, Benutzer, Administrator")]
    public class IndicatorController: Controller
    {
        private readonly IIndicatorManagementService m_IndicatorManagementService;
        private readonly IHighchartFactory m_HighchartFactory;

        public IndicatorController(IIndicatorManagementService indicatorManagementService, IHighchartFactory highchartFactory)
        {
            m_IndicatorManagementService = indicatorManagementService;
            m_HighchartFactory = highchartFactory;
        }

        public ActionResult Index()
        {
            List<Indicator> indicators = m_IndicatorManagementService.Get(includeProperties : "Unit,Topic").
                                                                      OrderBy(i => i.Topic.Name).
                                                                      ThenBy(i => i.Number).
                                                                      ToList();

            var viewModel = IndicatorViewModelFactory.CreateIndicatorListViewModel(indicators,
                                                                                   m_IndicatorManagementService.GetUnits().
                                                                                                                ToList(),
                                                                                   m_IndicatorManagementService.GetTopics().
                                                                                                                ToList());
            return View(viewModel);
        }

        public ActionResult IndexForFormulaEditor()
        {
            List<Indicator> indicators = m_IndicatorManagementService.Get(includeProperties : "Unit, Topic").
                                                                      OrderBy(i => i.Topic.Name).
                                                                      ThenBy(i => i.Number).
                                                                      ToList();

            var viewModel = IndicatorViewModelFactory.CreateIndicatorListViewModel(indicators,
                                                                                   m_IndicatorManagementService.GetUnits().
                                                                                                                ToList(),
                                                                                   m_IndicatorManagementService.GetTopics().
                                                                                                                ToList());
            return PartialView("_ListFormulaEditor",
                               viewModel);
        }

        public ActionResult AddValue(int id = 0)
        {
            var indicator = m_IndicatorManagementService.Get(i => i.Id == id).
                                                         First();
            if (indicator == null)
            {
                return HttpNotFound();
            }

            var viewModel = IndicatorViewModelFactory.CreateIndicatorAddValueViewModel(indicator);

            return View(viewModel);
        }

        [HttpPost, Authorize]
        public ActionResult AddValue(IndicatorAddValueViewModel viewModel)
        {
            var indicator = m_IndicatorManagementService.Get(i => i.Id == viewModel.IndicatorId,
                                                             includeProperties : "IndicatorValues").
                                                         FirstOrDefault();

            if (indicator == null)
            {
                return HttpNotFound("Indicator is null");
            }

            if (indicator.IndicatorValues != null && indicator.IndicatorValues.Any(iv => iv.Timestamp.Year == viewModel.Year))
            {
                ModelState.AddModelError("Year",
                                         "Für diesen Bezugszeitraum ist bereits ein Wert erfasst");
            }

            if (ModelState.IsValid)
            {
                if (!User.Identity.IsAuthenticated)
                {
                    return new HttpStatusCodeResult(401);
                }
                var user = m_IndicatorManagementService.GetUserProfiles(u => u.UserName.Equals(User.Identity.Name)).
                                                        First();

                var indicatorValue = new IndicatorValue();
                indicatorValue.Value = viewModel.IndicatorValue.Value;
                indicatorValue.Created = DateTime.Now;
                indicatorValue.CreatedBy = user;
                indicatorValue.LastEdited = DateTime.Now;
                indicatorValue.LastEditedBy = user;
                indicatorValue.Timestamp = new DateTime(viewModel.Year,
                                                        01,
                                                        01);
                indicatorValue.Indicator = indicator;
                if (indicator.IndicatorValues == null)
                {
                    indicator.IndicatorValues = new List<IndicatorValue>();
                }
                indicator.IndicatorValues.Add(indicatorValue);

                m_IndicatorManagementService.UpdateIndicator(indicator);

                List<Indicator> evaluate = m_IndicatorManagementService.GetIndicatorstorsHavingIndicatorValueIndicatorAsParameter(indicatorValue).
                                                                        ToList();

                foreach (var indicatorse in evaluate)
                {
                    try
                    {
                        var value = m_IndicatorManagementService.EvaluateExpression(indicatorse,
                                                                                    viewModel.Year);
                        var evaluatedValue = m_IndicatorManagementService.GetIndicatorValues(iv => iv.Indicator.Id == indicatorse.Id && iv.Timestamp.Year == viewModel.Year).
                                                                          FirstOrDefault();
                        if (evaluatedValue != null)
                        {
                            evaluatedValue.Value = (double) value;
                            evaluatedValue.LastEdited = DateTime.Now;
                            evaluatedValue.LastEditedBy = m_IndicatorManagementService.GetUserProfiles(u => u.UserName.Equals("IsoAdmin")).
                                                                                       First();
                        }
                        else
                        {
                            evaluatedValue = new IndicatorValue();
                            evaluatedValue.Value = (double) value;
                            evaluatedValue.Created = DateTime.Now;
                            evaluatedValue.CreatedBy = m_IndicatorManagementService.GetUserProfiles(u => u.UserName.Equals("IsoAdmin")).
                                                                                    First();
                            evaluatedValue.LastEdited = DateTime.Now;
                            evaluatedValue.LastEditedBy = evaluatedValue.CreatedBy;
                            evaluatedValue.Timestamp = new DateTime(viewModel.Year,
                                                                    1,
                                                                    1);

                            if (indicatorse.IndicatorValues == null)
                            {
                                indicatorse.IndicatorValues = new List<IndicatorValue>();
                            }
                            m_IndicatorManagementService.AddIndicatorValue(evaluatedValue);
                        }
                        evaluatedValue.Indicator = indicatorse;
                        indicatorse.IndicatorValues.Add(evaluatedValue);
                        m_IndicatorManagementService.UpdateIndicator(indicatorse);
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }
                return RedirectToAction("Index");
            }
            return View(viewModel);
        }

        public ActionResult DeleteValue(int id = 0)
        {
            IndicatorValue indicatorValue = m_IndicatorManagementService.GetIndicatorValues(i => i.Id == id,
                                                                                            includeProperties : "Indicator").
                                                                         FirstOrDefault();
            if (indicatorValue == null)
            {
                return HttpNotFound();
            }
            return View(indicatorValue);
        }

        [HttpPost, ActionName("DeleteValue")]
        public ActionResult DeleteValueConfirmed(int id = 0)
        {
            var indicatorId = m_IndicatorManagementService.GetIndicatorValues(iv => iv.Id == id,
                                                                              includeProperties : "Indicator").
                                                           First().
                                                           Indicator.Id;
            m_IndicatorManagementService.DeleteIndicatorValue(id);

            var indicator = m_IndicatorManagementService.Get(i => i.Id == indicatorId,
                                                             includeProperties : "IndicatorValues").
                                                         First();
            var model = IndicatorViewModelFactory.CreateIndicatorListValuesViewModel();
            model.Indicator = indicator;
            model.LineChart = m_HighchartFactory.CreateLineChartForDateTime(indicator.IndicatorValues.OrderBy(i => i.Timestamp.Year),
                                                                            indicator.Name,
                                                                            indicator.Unit.Name);

            return View("ListValues",
                        model);
        }

        public ActionResult ListValues(int id = 0)
        {
            var indicator = m_IndicatorManagementService.Get(i => i.Id == id,
                                                             includeProperties : "IndicatorValues").
                                                         FirstOrDefault();

            if (indicator == null)
            {
                return HttpNotFound();
            }

            var model = IndicatorViewModelFactory.CreateIndicatorListValuesViewModel();
            model.Indicator = indicator;
            model.LineChart = m_HighchartFactory.CreateLineChartForDateTime(indicator.IndicatorValues.OrderBy(i => i.Timestamp.Year),
                                                                            indicator.Name,
                                                                            indicator.Unit.Name);
            return View(model);
        }

        public ActionResult Details(int id = 0)
        {
            var indicator = m_IndicatorManagementService.FindIndicatorById(id);
            if (indicator == null)
            {
                return HttpNotFound();
            }
            return View(indicator);
        }

        public ActionResult Create()
        {
            return View(IndicatorViewModelFactory.CreateIndicatorViewModel(m_IndicatorManagementService.GetUnits().
                                                                                                        ToList(),
                                                                           m_IndicatorManagementService.GetTopics().
                                                                                                        ToList()));
        }

        [HttpPost]
        public ActionResult Create(IndicatorViewModel viewModel)
        {
            DbEntityValidationResult validationResultFromDb = m_IndicatorManagementService.GetValidationResult(viewModel.Indicator);
            if (!validationResultFromDb.IsValid)
            {
                foreach (var error in validationResultFromDb.ValidationErrors)
                {
                    ModelState.AddModelError("Indicator." + error.PropertyName,
                                             error.ErrorMessage);
                }
            }

            if (ModelState.IsValid)
            {
                if (!User.Identity.IsAuthenticated)
                {
                    return new HttpStatusCodeResult(401);
                }
                var user = m_IndicatorManagementService.GetUserProfiles(u => u.UserName.Equals(User.Identity.Name)).
                                                        First();

                viewModel.Indicator.Created = DateTime.Now;
                viewModel.Indicator.CreatedBy = user;
                viewModel.Indicator.LastEdit = DateTime.Now;
                viewModel.Indicator.LastEditBy = user;
                m_IndicatorManagementService.AddIndicator(viewModel.Indicator);
                return RedirectToAction("Index");
            }

            viewModel.Topics = new SelectList(m_IndicatorManagementService.GetTopics().
                                                                           ToList(),
                                              "Id",
                                              "Name",
                                              viewModel.Indicator.TopicId);


            viewModel.Units = new SelectList(m_IndicatorManagementService.GetUnits().
                                                                          ToList(),
                                             "Id",
                                             "Name",
                                             viewModel.Indicator.UnitId);

            return View(viewModel);
        }

        public ActionResult Edit(int id = 0)
        {
            Indicator indicator = m_IndicatorManagementService.Get(i => i.Id == id,
                                                                   includeProperties : "Unit,Topic").
                                                               FirstOrDefault();
            if (indicator == null)
            {
                return HttpNotFound();
            }
            return View(IndicatorViewModelFactory.CreateIndicatorViewModel(indicator,
                                                                           m_IndicatorManagementService.GetUnits().
                                                                                                        ToList(),
                                                                           m_IndicatorManagementService.GetTopics().
                                                                                                        ToList()));
        }

        [HttpPost]
        public ActionResult Edit(IndicatorViewModel viewModel)
        {
            var indicatorDb = m_IndicatorManagementService.Get(i => i.Id == viewModel.Indicator.Id,
                                                               includeProperties : "Unit,Topic").
                                                           FirstOrDefault();


            if (viewModel.Indicator.IndicatorExpression != null)
            {
                List<string> formulaErrors = m_IndicatorManagementService.GetFormulaErrors(viewModel.Indicator.IndicatorExpression);
                if (formulaErrors.Count > 0)
                {
                    string formulaErrorsString = string.Join("<br />",
                                                             formulaErrors);
                    ModelState.AddModelError("Indicator.IndicatorExpression",
                                             formulaErrorsString);
                }
            }

            DbEntityValidationResult validationResultFromDb = m_IndicatorManagementService.GetValidationResult(viewModel.Indicator);
            if (!validationResultFromDb.IsValid)
            {
                foreach (var error in validationResultFromDb.ValidationErrors)
                {
                    ModelState.AddModelError("Indicator." + error.PropertyName,
                                             error.ErrorMessage);
                }
            }

            if (ModelState.IsValid)
            {
                if (!User.Identity.IsAuthenticated)
                {
                    return new HttpStatusCodeResult(401);
                }

                var user = m_IndicatorManagementService.GetUserProfiles(u => u.UserName.Equals(User.Identity.Name)).
                                                        FirstOrDefault();
                indicatorDb.LastEdit = DateTime.Now;
                indicatorDb.LastEditBy = user;
                indicatorDb.Unit = m_IndicatorManagementService.GetUnits(u => u.Id == viewModel.Indicator.Unit.Id).
                                                                First();
                indicatorDb.Name = viewModel.Indicator.Name;
                indicatorDb.Explanation = viewModel.Indicator.Explanation;
                indicatorDb.Topic = m_IndicatorManagementService.GetTopics(t => t.Id == viewModel.Indicator.TopicId).
                                                                 First();
                indicatorDb.IndicatorExpression = viewModel.Indicator.IndicatorExpression;

                //TODO: Wäre schön aber bisher keine Lösung mit EF gefunden 
                /*
                if (viewModel.Indicator.IndicatorExpression != null)
                {
                    indicatorDb.IndicatorExpression = viewModel.Indicator.IndicatorExpression;
                    var numbers = getDistinctIndicatorNumbersOfExpression(viewModel.Indicator.IndicatorExpression);
                    foreach (var number in numbers)
                    {
                        //indicatorDb.IndicatorExpression.Indicators.Add(m_IndicatorRepository.getByNumber(number));
                    }
                    
                }*/

                m_IndicatorManagementService.UpdateIndicator(indicatorDb);
                return RedirectToAction("Index");
            }

            viewModel.Topics = new SelectList(m_IndicatorManagementService.GetTopics().
                                                                           ToList(),
                                              "Id",
                                              "Name",
                                              viewModel.Indicator.TopicId);


            viewModel.Units = new SelectList(m_IndicatorManagementService.GetUnits().
                                                                          ToList(),
                                             "Id",
                                             "Name",
                                             viewModel.Indicator.UnitId);
            return View(viewModel);
        }

        [HttpPost]
        public PartialViewResult FormulaBuilder(string formula)
        {
            return PartialView("_FormulaBuilder",
                               IndicatorViewModelFactory.CreateIndicatorViewModel(m_IndicatorManagementService.GetUnits().
                                                                                                               ToList(),
                                                                                  m_IndicatorManagementService.GetTopics().
                                                                                                               ToList()));
        }

        public ActionResult Delete(int id = 0)
        {
            Indicator indicator = m_IndicatorManagementService.FindIndicatorById(id);
            if (indicator == null)
            {
                return HttpNotFound();
            }
            return View(indicator);
        }

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            m_IndicatorManagementService.DeleteIndicator(id);
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult IsFormulaValid(string formula)
        {
            var errorMessages = new List<string>();

            if (formula == null || formula.Length < 1)
            {
                errorMessages.Add("Keine Formel zum Übernehmen angegeben");
                return Json(errorMessages,
                            JsonRequestBehavior.AllowGet);
            }

            errorMessages.AddRange(m_IndicatorManagementService.GetFormulaErrors(formula));

            if (errorMessages.Count == 0)
            {
                return Json(true,
                            JsonRequestBehavior.AllowGet);
            }

            return Json(errorMessages,
                        JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public virtual JsonResult GetElementsOfFormula(string formula)
        {
            var exp = new Regex(@"\[.*?\]|\*|\+|\-|\\|[-+]?[0-9]*\.?[0-9]+",
                                RegexOptions.IgnoreCase);
            MatchCollection matchlist = exp.Matches(formula);
            var formulaElements = new List<string>();

            foreach (Match match in matchlist)
            {
                formulaElements.Add(match.Value);
            }
            return Json(formulaElements,
                        JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetTopics(int? id)
        {
            var topics = new SelectList(m_IndicatorManagementService.GetTopics(),
                                        "Id",
                                        "Name",
                                        id ?? 0);

            return Json(topics,
                        JsonRequestBehavior.AllowGet);
        }

        public ActionResult CreateTopic()
        {
            return PartialView("_CreateTopic");
        }

        [HttpPost]
        public ActionResult CreateTopic(Topic topic)
        {
            DbEntityValidationResult validationResultFromDb = m_IndicatorManagementService.GetValidationResult(topic);
            if (!validationResultFromDb.IsValid)
            {
                foreach (var error in validationResultFromDb.ValidationErrors)
                {
                    ModelState.AddModelError(error.PropertyName,
                                             error.ErrorMessage);
                }
            }
            if (ModelState.IsValid)
            {
                try
                {
                    var newTopic = m_IndicatorManagementService.AddTopic(topic);
                    return Json(new{
                                       success = true,
                                       newTopicId = newTopic.Id
                                   });
                }
                catch (Exception e)
                {
                    ModelState.AddModelError("",
                                             e.Message);
                }
            }
            //Something bad happened
            return PartialView("_CreateTopic",
                               topic);
        }

        public ActionResult CreateUnit()
        {
            return PartialView("_CreateUnit");
        }

        [HttpPost]
        public ActionResult CreateUnit(UnitCreateViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var newUnit = m_IndicatorManagementService.AddUnit(new Unit{
                                                                                   Name = viewModel.Name,
                                                                                   Symbol = viewModel.Symbol
                                                                               });
                    return Json(new{
                                       success = true,
                                       newId = newUnit.Id
                                   });
                }
                catch (Exception e)
                {
                    ModelState.AddModelError("",
                                             e.Message);
                }
            }
            return PartialView("_CreateUnit",
                               viewModel);
        }

        public ActionResult GetUnits(int? id)
        {
            var topics = new SelectList(m_IndicatorManagementService.GetUnits(),
                                        "Id",
                                        "Name",
                                        id ?? 0);

            return Json(topics,
                        JsonRequestBehavior.AllowGet);
        }
    }
}