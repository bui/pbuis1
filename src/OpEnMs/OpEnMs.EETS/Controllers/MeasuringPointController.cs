﻿using System.Linq;
using System.Web.Mvc;
using OpEnMs.Application.Services.IsoMeasureManagement;
using OpEnMs.Application.Services.MeasuringPointManagement;
using OpEnMs.Application.Services.UserProfileManagement;
using OpEnMs.EETS.Factories;
using OpEnMs.EETS.ViewModels.MeasuringPoint;
using OpEnMs.EnergyChart.Factories;

namespace OpEnMs.EETS.Controllers
{
  public class MeasuringPointController: Controller
  {
		private readonly IIsoMeasureManagementService m_IsoMeasureManagementService;
		private readonly IUserProfileManagementService m_UserProfileManagementService;
    private readonly IMeasuringPointManagementService m_MeasuringPointManagementService;
    private IHighchartFactory m_Highchartfactory;

		public MeasuringPointController(IMeasuringPointManagementService measuringPointManagementService, IIsoMeasureManagementService isoMeasureManagementService,IUserProfileManagementService userProfileManagementService, IHighchartFactory highchartFactory)
    {
			m_IsoMeasureManagementService = isoMeasureManagementService;
      m_MeasuringPointManagementService = measuringPointManagementService;
			m_UserProfileManagementService = userProfileManagementService;
      m_Highchartfactory = highchartFactory;
    }

    //
    // GET: /MeasuringPoint/

    public ActionResult Index()
    {
      var viewModel = MeasuringPointViewModelFactory.CreateMeasuringPoinListViewModel(m_MeasuringPointManagementService.GetMeasuringPointsByUserIdentity(User.Identity.Name));

      return View(viewModel);
    }

		[Authorize(Roles = "Administrator, Benutzer, IsoAdministrator")]
    public ActionResult Create(int companyId = 0)
    {
      var sources = m_MeasuringPointManagementService.GetSources();
      var units = m_MeasuringPointManagementService.GetUnits();
      var model = m_MeasuringPointManagementService.CreateMeasuringPoint();

      //TODO: Check if null
      Session["CompanyId"] = companyId;

      var viewModel = MeasuringPointViewModelFactory.CreateMeasuringPointCreateViewModel(model,
        units,
        sources);

      return View(viewModel);
    }

    //
    // POST: /Company/Create

    [HttpPost]
    [ValidateAntiForgeryToken]
		[Authorize(Roles = "Administrator, Benutzer, IsoAdministrator")]
    public ActionResult Create(MeasuringPointCreateViewModel viewModel)
		{
      //TODO: Check if null
			var companyId = (int) Session["CompanyId"];

      if (ModelState.IsValid)
      {
        var model = viewModel.Model;
				if (User.IsInRole("IsoAdministrator"))
				{
					
					var user = m_IsoMeasureManagementService.GetUserProfiles(u => u.UserName.Contains(User.Identity.Name)).FirstOrDefault();
					companyId = m_UserProfileManagementService.FindCompanyByUser(user).Id;
				}
        model.EnergyConsumptions = m_MeasuringPointManagementService.CreateEnergyConsumption(viewModel.UnitId,
          viewModel.SourceId);

        m_MeasuringPointManagementService.AddMeasuringPoint(viewModel.Model,
          companyId);

				if (User.IsInRole("IsoAdministrator"))
				{
					return RedirectToAction("Index",
						"MeasuringPoint",
						new
						{
							id = companyId
						});
				}
				else
				{
					return RedirectToAction("Edit",
						"Company",
						new
						{
							id = companyId
						});
				}
      }

      return View(viewModel);
    }

    public ActionResult Edit(int id = 0, int companyId = 0)
    {
      var measuringPoint = m_MeasuringPointManagementService.FindById(id);
      var sources = m_MeasuringPointManagementService.GetSources();
      var units = m_MeasuringPointManagementService.GetUnits();

      Session["CompanyId"] = companyId;

      if (measuringPoint == null)
      {
        return HttpNotFound();
      }

      var energyConsumptionDetailsViewModel = EnergyConsumptionViewModelFactory.CreateEnergyConsumptionDetailsViewModel(measuringPoint.EnergyConsumptions.FirstOrDefault(),
        m_Highchartfactory);

      var viewModel = MeasuringPointViewModelFactory.CreateMeasuringPointEditViewModel(measuringPoint,
        units,
        sources);

      viewModel.EnergyConsumptionDetailsViewModel = energyConsumptionDetailsViewModel;

      return View(viewModel);
    }

    [HttpPost]
    [ValidateAntiForgeryToken]
    public ActionResult Edit(MeasuringPointEditViewModel viewModel)
    {
      if (ModelState.IsValid)
      {
        m_MeasuringPointManagementService.UpdateMeasuringPoint(viewModel.Model);

        var companyId = (int) Session["CompanyId"];

        
				if (User.IsInRole("IsoAdministrator"))
				{
					return RedirectToAction("Index",
						"MeasuringPoint",
						new
						{
							id = companyId
						});
				}
				else
				{
					return RedirectToAction("Edit",
						"Company",
						new
						{
							id = companyId
						});
				}
      }
      return View(viewModel);
    }


    [Authorize]
    public ActionResult Delete(int id = 0, int companyId = 0)
    {
      var measuringPoint = m_MeasuringPointManagementService.FindById(id);

      Session["CompanyId"] = companyId;

      if (measuringPoint == null)
      {
        return HttpNotFound();
      }
      return View(measuringPoint);
    }

    //
    // POST: /Measure/Delete/5

    [HttpPost, ActionName("Delete")]
    [ValidateAntiForgeryToken]
    [Authorize]
    public ActionResult DeleteConfirmed(int id)
    {
      m_MeasuringPointManagementService.DeleteMeasuringPoint(id);

			var companyId = (int) Session["CompanyId"];

			if (User.IsInRole("IsoAdministrator"))
			{
				return RedirectToAction("Index",
					"MeasuringPoint",
					new
					{
						id = companyId
					});
			}
			else
			{
				return RedirectToAction("Edit",
					"Company",
					new
					{
						id = companyId
					});
			}
    }

    public ActionResult Readings(int id = 0)
    {
      var measuringPoint = m_MeasuringPointManagementService.FindById(id);

      var viewModel = EnergyConsumptionViewModelFactory.CreateEnergyConsumptionDetailsViewModel(measuringPoint.EnergyConsumptions.FirstOrDefault(),
        m_Highchartfactory);

      return View(viewModel);
    }
  }
}