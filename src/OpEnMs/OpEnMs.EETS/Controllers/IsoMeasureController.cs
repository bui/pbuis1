﻿using System.Linq;
using System.Web.Mvc;
using OpEnMs.Application.Services.IsoMeasureManagement;
using OpEnMs.Domain.Entities;
using OpEnMs.EETS.Factories;
using OpEnMs.EETS.ViewModels.IsoMeasure;
using OpEnMs.EETS.ViewModels.Valuation;
using OpEnMs.Resources;

namespace OpEnMs.EETS.Controllers
{
	public class IsoMeasureController: Controller
	{
		private readonly IIsoMeasureManagementService m_IsoMeasureManagementService;

		public IsoMeasureController(IIsoMeasureManagementService isoMeasureManagementService)
		{
			m_IsoMeasureManagementService = isoMeasureManagementService;
		}

		//
		// GET: /Measure/

		public ActionResult Index()
		{
			var company = m_IsoMeasureManagementService.GetCompanyByUserIdentity(User.Identity.Name);

			if (company == null)
			{
				return HttpNotFound("Company is null");
			}

			var viewModel = IsoMeasureViewModelFactory.CreateIsoMeasureListViewModels(company.IsoMeasures);

			return View(viewModel);
		}

		public ActionResult Create(int id = 0)
		{
			if (User.IsInRole("IsoUser") || User.IsInRole("IsoAdministrator"))
			{
				var company = m_IsoMeasureManagementService.GetCompanyByUserIdentity(User.Identity.Name);

				//TODO: Check if null
				Session["CompanyId"] = company.Id;
			}
			else
			{
				//TODO: Check if null
				Session["CompanyId"] = id;
			}


			var viewModel = IsoMeasureViewModelFactory.CreateIsoMeasureCreateViewModel(m_IsoMeasureManagementService.CreateIsoMeasure());
			viewModel.Model.Valuation  = new Valuation();

			return View(viewModel);
		}

		//
		// POST: /Measure/Create

		[HttpPost]
		[ValidateAntiForgeryToken]
		[Authorize(Roles = "Administrator, Benutzer, IsoAdministrator")]
		public ActionResult Create(IsoMeasureCreateViewModel viewModel)
		{
			//TODO: Check if null
			//TODO: Bug, after invalud model state, tempdata is gone
			var companyId = (int) Session["CompanyId"];
			var user = m_IsoMeasureManagementService.GetUserProfiles(u => u.UserName.Contains(User.Identity.Name)).
				FirstOrDefault();
			var users = m_IsoMeasureManagementService.GetUserProfiles();
			var employeeInCharge = Helper.SplitUserProfileFromView(viewModel.EmployeeInChargeString, users);
			

			if (employeeInCharge == null)
			{
				ModelState.AddModelError("EmplyoyeeInChargeString",
					ErrorMessageResources.UserNameDoesNotMatch);
			}

			viewModel.Model.EmployeeInCharge = employeeInCharge;
			viewModel.Model.Creator = user;
			viewModel.Model.Valuation = viewModel.Valuation;

			if (ModelState.IsValid)
			{
				m_IsoMeasureManagementService.AddIsoMeasure(viewModel.Model,
					companyId);

				if (User.IsInRole("Benutzer") || User.IsInRole("Administrator"))
				{
					return RedirectToAction("Edit",
						"Company",
						new{
							   id = companyId
						   });
				}
				return RedirectToAction("Index",
					"IsoMeasure");
			}

			return View(viewModel);
		}

		//
		// GET: /IsoMeasure/Edit

		[Authorize(Roles = "Administrator, Benutzer, IsoAdministrator")]
		public ActionResult Edit(int id = 0, int companyId = 0)
		{
			var measure = m_IsoMeasureManagementService.FindById(id);
			Session["CompanyId"] = companyId;

			if (measure == null)
			{
				return HttpNotFound("Measure is null");
			}

			var viewModel = IsoMeasureViewModelFactory.CreateMeasureEditViewModel(measure);

			return View(viewModel);
		}

		//
		// POST: /IsoMeasure/Edit

		[HttpPost]
		[ValidateAntiForgeryToken]
		[Authorize(Roles = "Administrator, Benutzer, IsoAdministrator")]
		public ActionResult Edit(IsoMeasureEditViewModel viewModel)
		{
			if (ModelState.IsValid)
			{
				m_IsoMeasureManagementService.UpdateIsoMeasure(viewModel.Model);

				var companyId = (int) Session["CompanyId"];

				if (User.IsInRole("Benutzer") || User.IsInRole("Administrator"))
				{
					return RedirectToAction("Edit",
						"Company",
						new{
							   id = companyId
						   });
				}
				return RedirectToAction("Index",
					"IsoMeasure");
			}
			return View(viewModel);
		}

		//
		// GET: /Measure/Delete/5

		[Authorize(Roles = "Administrator, Benutzer, IsoAdministrator")]
		public ActionResult Delete(int id = 0, int companyId = 0)
		{
			var measure = m_IsoMeasureManagementService.FindById(id);

			Session["CompanyId"] = companyId;

			if (measure == null)
			{
				return HttpNotFound("Measure is null");
			}
			return View(measure);
		}

		//
		// POST: /Measure/Delete/5

		[HttpPost, ActionName("Delete")]
		[ValidateAntiForgeryToken]
		[Authorize(Roles = "Administrator, Benutzer, IsoAdministrator")]
		public ActionResult DeleteConfirmed(int id)
		{
			m_IsoMeasureManagementService.DeleteIsoMeasure(id);

			var companyId = (int) Session["CompanyId"];

			if (User.IsInRole("Benutzer") || User.IsInRole("Administrator"))
			{
				return RedirectToAction("Edit",
					"Company",
					new{
						   id = companyId
					   });
			}
			return RedirectToAction("Index",
				"IsoMeasure");
		}

		//
		// GET: /IsoMeasure/Details/

		public ActionResult Details(int id = 0)
		{
			var isoMeasure = m_IsoMeasureManagementService.FindById(id);
			var viewModel = IsoMeasureViewModelFactory.CreateIsoMeasureDetailsViewModel(isoMeasure);

			return View(viewModel);

		}

		//[HttpPost, ActionName("ChangeState")]
		//[ValidateAntiForgeryToken]
		//[Authorize(Roles = "Administrator, Benutzer, IsoAdministrator")]
		public ActionResult ChangeState(int id, OpEnMs.Domain.Entities.MeasureState stateEnum)
		{
			var isoMeasure = m_IsoMeasureManagementService.FindById(id);
			isoMeasure.State = stateEnum;
			m_IsoMeasureManagementService.UpdateIsoMeasure(isoMeasure);

			return RedirectToAction("Details",
				"IsoMeasure",
				new
				{
					id = id
				}
				);

		}
		
	}
}