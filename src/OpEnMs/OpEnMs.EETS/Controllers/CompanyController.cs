﻿#region Licence

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0.html
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// 
// Copyright (c) 2013, HTW Berlin

#endregion

using System;
using System.IO;
using System.Web;
using System.Web.Mvc;
using DoddleReport;
using DoddleReport.Web;
using OpEnMs.Application.Services.CompanyManagement;
using OpEnMs.Domain.Entities;
using OpEnMs.EETS.Factories;
using OpEnMs.EETS.ViewModels.Company;
using OpEnMs.EnergyChart.Factories;

namespace OpEnMs.EETS.Controllers
{
	public class CompanyController: Controller
	{
		private readonly ICompanyManagementService m_CompanyManagementService;
		private IHighchartFactory m_HighchartFactory;

		public CompanyController(ICompanyManagementService companyManagementService, IHighchartFactory highchartFactory)
		{
			m_CompanyManagementService = companyManagementService;
			m_HighchartFactory = highchartFactory;
		}

		//
		// GET: /Company/

		[Authorize(Roles = "Administrator")]
		public ActionResult Index()
		{
			var viewModel = CompanyViewModelFactory.CreateCompanyListViewModel(m_CompanyManagementService.Get());
			return View(viewModel);
		}

		[Authorize (Roles ="Benutzer")]
		public ActionResult MyCompany()
		{
			var company = m_CompanyManagementService.GetCompanyByUserIdentity(User.Identity.Name);

			if (company == null)
			{
				return HttpNotFound();
			}
			return View(company);
		}

		//
		// GET: /Company/Details/5

		public ActionResult Details(int id = 0)
		{
			var company = m_CompanyManagementService.FindById(id);
			if (company == null)
			{
				return HttpNotFound();
			}
			var viewModel = CompanyViewModelFactory.CreateCompanyDetailsViewModel(company, m_HighchartFactory);

			return View(viewModel);
		}

		//
		// GET: /Company/Create

		[Authorize(Roles = "Administrator")]
		public ActionResult Create()
		{
			return View();
		}

		//
		// POST: /Company/Create

		[HttpPost]
		[ValidateAntiForgeryToken]
		[Authorize(Roles = "Administrator")]
		public ActionResult Create(Company company)
		{
			if (ModelState.IsValid)
			{
				m_CompanyManagementService.AddCompany(company);
				return RedirectToAction("Index");
			}

			return View(company);
		}

		//
		// GET: /Company/Edit/5

		[Authorize(Roles = "Administrator")]
		public ActionResult Edit(int id = 0)
		{
			var company = m_CompanyManagementService.FindById(id);
			Session["CompanyId"] = id;
			
			if (company == null)
			{
				return HttpNotFound();
			}
			var viewModel = CompanyViewModelFactory.CreateCompanyEditViewModel(company);
			ViewBag.CompanyId = id;
			return View(viewModel);
		}

		//
		// POST: /Company/Edit/5

		[HttpPost]
		[ValidateAntiForgeryToken]
		[Authorize(Roles = "Administrator")]
		public ActionResult Edit(CompanyEditViewModel viewModel)
		{
			if (ModelState.IsValid)
			{
				m_CompanyManagementService.UpdateCompany(viewModel.Model);

				return RedirectToAction("Index");
			}
			return View(viewModel);
		}

		//
		// GET: /Company/Delete/5

		[Authorize(Roles = "Administrator")]
		public ActionResult Delete(int id = 0)
		{
			var company = m_CompanyManagementService.FindById(id);
			if (company == null)
			{
				return HttpNotFound();
			}
			return View(company);
		}

		//
		// POST: /Company/Delete/5

		[HttpPost, ActionName("Delete")]
		[ValidateAntiForgeryToken]
		[Authorize(Roles = "Administrator")]
		public ActionResult DeleteConfirmed(int id)
		{
			var company = m_CompanyManagementService.FindById(id);
			m_CompanyManagementService.DeleteCompany(id);
			return RedirectToAction("Index");
		}

		public ReportResult PivotReport()
		{
			var company = m_CompanyManagementService.FindById(1);

			var results = company.Measures;
			var report = new Report(results.ToReportSource());
      report.TextFields.Title = company.Name + " - " + Resources.Resources.Measures;
      report.TextFields.Header = string.Format("Erstellt am: {0}", DateTime.Now.ToShortDateString());
      report.RenderHints.Orientation = ReportOrientation.Landscape;
      report.DataFields["Id"].Hidden = true;
      report.DataFields["Ideas"].Hidden = true;
			return new ReportResult(report);
		}

		protected override void Dispose(bool disposing)
		{
			m_CompanyManagementService.Dispose();
			base.Dispose(disposing);
		}
	}
}