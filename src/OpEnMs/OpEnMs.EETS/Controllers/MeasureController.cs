﻿using System.Web.Mvc;
using OpEnMs.Application.Services.MeasureManagement;
using OpEnMs.EETS.Factories;
using OpEnMs.EETS.ViewModels.Measure;

namespace OpEnMs.EETS.Controllers
{
  public class MeasureController: Controller
  {
    private IMeasureManagementService m_MeasureManagementService;

    public MeasureController(IMeasureManagementService measureManagementService)
    {
      m_MeasureManagementService = measureManagementService;
    }

    //
    // GET: /Measure/

    public ActionResult Index()
    {
      var company = m_MeasureManagementService.GetCompanyByUserIdentity(User.Identity.Name);

      if (company == null)
      {
        return HttpNotFound();
      }

      var viewModel = MeasureViewModelFactory.CreateMeasureListViewModels(company.Measures);

      return View(viewModel);
    }

    public ActionResult Create(int companyId = 0)
    {
      if (User.IsInRole("IsoUser") || User.IsInRole("IsoAdministrator"))
      {
        var company = m_MeasureManagementService.GetCompanyByUserIdentity(User.Identity.Name);

        //TODO: Check if null
        Session["CompanyId"] = company.Id;
      }
      else
      {
        //TODO: Check if null
        Session["CompanyId"] = companyId;
      }


      var viewModel = MeasureViewModelFactory.CreateMeasureCreateViewModel(m_MeasureManagementService.CreateMeasure());

      return View(viewModel);
    }

    //
    // POST: /Measure/Create

    [HttpPost]
    [ValidateAntiForgeryToken]
    [Authorize(Roles = "Administrator, Benutzer, IsoAdministrator")]
    public ActionResult Create(MeasureCreateViewModel viewModel)
    {
      //TODO: Check if null
      //TODO: Bug, after invalud model state, tempdata is gone
      var companyId = (int) Session["CompanyId"];

      if (ModelState.IsValid)
      {
        m_MeasureManagementService.AddMeasure(viewModel.Model,
          companyId);

        return RedirectToAction("Edit",
          "Company",
          new{
               id = companyId
             });
      }

      return View(viewModel);
    }

    public ActionResult Edit(int id = 0, int companyId = 0)
    {
      var measure = m_MeasureManagementService.FindById(id);
      Session["CompanyId"] = companyId;

      if (measure == null)
      {
        return HttpNotFound();
      }

      var viewModel = MeasureViewModelFactory.CreateMeasureEditViewModel(measure);

      return View(viewModel);
    }

    [HttpPost]
    [ValidateAntiForgeryToken]
    public ActionResult Edit(MeasureEditViewModel viewModel)
    {
      if (ModelState.IsValid)
      {
        m_MeasureManagementService.UpdateMeasure(viewModel.Model);

        var companyId = (int) Session["CompanyId"];

        return RedirectToAction("Edit",
          "Company",
          new{
               id = Session["CompanyId"]
             });
      }
      return View(viewModel);
    }

    //
    // GET: /Measure/Delete/5

    [Authorize]
    public ActionResult Delete(int id = 0, int companyId = 0)
    {
      var measure = m_MeasureManagementService.FindById(id);

      Session["CompanyId"] = companyId;

      if (measure == null)
      {
        return HttpNotFound();
      }
      return View(measure);
    }

    //
    // POST: /Measure/Delete/5

    [HttpPost, ActionName("Delete")]
    [ValidateAntiForgeryToken]
    [Authorize]
    public ActionResult DeleteConfirmed(int id)
    {
      m_MeasureManagementService.DeleteMeasure(id);

      return RedirectToAction("Edit",
        "Company",
        new{
             id = Session["CompanyId"]
           });
    }
  }
}