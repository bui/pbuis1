﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web.Mvc;
using OpEnMs.Application.Services.IdeaManagement;
using OpEnMs.Application.Services.IsoMeasureManagement;
using OpEnMs.Application.Services.MeasureManagement;
using OpEnMs.Domain.Entities;
using OpEnMs.EETS.Factories;
using OpEnMs.EETS.ViewModels.Idea;
using OpEnMs.Resources;

namespace OpEnMs.EETS.Controllers
{
	public class IdeaController: Controller
	{
		private IIdeaManagementService m_IdeaManagementService;
		private IMeasureManagementService m_MeasureManagementService;
		private IIsoMeasureManagementService m_IsoMeasureManagementService;

		public IdeaController(IIdeaManagementService ideaManagementService, IMeasureManagementService measureManagementService, IIsoMeasureManagementService isoMeasureManagementService)
		{
			m_IdeaManagementService = ideaManagementService;
			m_MeasureManagementService = measureManagementService;
			m_IsoMeasureManagementService = isoMeasureManagementService;
		}

		//
		// GET: /Idea/

		[Authorize]
		public ActionResult Index()
		{
			var company = m_IdeaManagementService.GetCompanyByUserIdentity(User.Identity.Name);

			if (company == null)
			{
				return HttpNotFound("User has no company");
			}

			IdeaListViewModel viewModel;

			if (User.IsInRole("Benutzer") || User.IsInRole("IsoBenutzer"))
			{
				viewModel = IdeaViewModelFactory.CreateIdeaListViewModels(m_IdeaManagementService.GetIdeasByUserIdentity(User.Identity.Name));
			}
			else
			{
				viewModel = IdeaViewModelFactory.CreateIdeaListViewModels(company.Ideas);
			}

			return View(viewModel);
		}

		//
		//  GET: /Idea/Create

		[Authorize]
		public ActionResult Create()
		{
			return View();
		}

		//
		// POST: /Idea/Create/

		[HttpPost]
		[Authorize]
		[ValidateAntiForgeryToken]
		public ActionResult Create(Idea idea)
		{
			if (ModelState.IsValid)
			{
				var company = m_IdeaManagementService.GetCompanyByUserIdentity(User.Identity.Name);
				var user = m_IdeaManagementService.GetUserProfiles(u => u.UserName.Contains(User.Identity.Name)).
					FirstOrDefault();
				idea.Creator = user;

				m_IdeaManagementService.AddIdea(idea,
					company.Id);

				return RedirectToAction("Index",
					"Idea");
			}
			return View();
		}

		//
		//  GET: /Idea/Details

		[Authorize]
		public ActionResult Details(int id = 0)
		{
			var idea = m_IdeaManagementService.FindById(id);
			if (idea == null)
			{
				return new HttpNotFoundResult("Idea not found");
			}
			var viewModel = IdeaViewModelFactory.CreateIdeaDetailsViewModel(idea);
			return View(viewModel);
		}

		//
		//  GET: /Idea/Edit

		[Authorize]
		public ActionResult Edit(int id = 0)
		{
			var idea = m_IdeaManagementService.FindById(id);
			if (idea == null)
			{
				return new HttpNotFoundResult("Idea not found");
			}
			return View(idea);
		}

		//
		//  POST: /Idea/Edit/5

		[HttpPost]
		[Authorize]
		[ValidateAntiForgeryToken]
		public ActionResult Edit(Idea idea)
		{
			if (idea == null)
			{
				return new HttpNotFoundResult("Idea is null");
			}

			if (ModelState.IsValid)
			{
				m_IdeaManagementService.UpdateIdea(idea);
				return RedirectToAction("Index",
					"Idea");
			}

			return View();
		}

		//
		// GET: /Idea/ListMyIdeas
		[Authorize(Roles = "Administrator, IsoAdministrator")]
		public ActionResult ListMyIdeas()
		{
			var company = m_IdeaManagementService.GetCompanyByUserIdentity(User.Identity.Name);

			if (company == null)
			{
				return HttpNotFound("User has no company");
			}
			var viewModel = IdeaViewModelFactory.CreateIdeaListViewModels(m_IdeaManagementService.GetIdeasByUserIdentity(User.Identity.Name));

			return View(viewModel);
		}

		//
		// GET: /Idea/Search?query=abcdef
		[Authorize(Roles = "Administrator, IsoAdministrator")]
		public ActionResult Search(string query = null)
		{
			var ideas = m_IdeaManagementService.GetSearchQuery(query);
			var viewModel = IdeaViewModelFactory.CreateIdeaListViewModels(ideas);

			ViewBag.Query = query;

			return View(viewModel);
		}

		[Authorize(Roles = "Administrator, IsoAdministrator"), ActionName("ToMeasure")]
		public ActionResult ConvertToMeasure(int id = 0)
		{
			Idea idea = m_IdeaManagementService.FindById(id);
			if (idea == null)
			{
				return HttpNotFound();
			}

			if (m_MeasureManagementService.IdeaIsInMeasure(idea))
			{
				return new HttpStatusCodeResult(400,
					ErrorMessageResources.IdeaAlreadyConvertedToActivity);
			}

			var viewModel = IdeaViewModelFactory.CreateIdeaToMeasureViewModel(idea);

			viewModel.DueDate = DateTime.Now;

			//Setting values of idea to viewmodel to modify them
			viewModel.Name = idea.Name;
			viewModel.Solution = idea.Solution;
			viewModel.Description = idea.Description;
			viewModel.Valuation = idea.Valuation;


			return View(viewModel);
		}

		[Authorize(Roles = "IsoAdministrator"), ActionName("ToIsoMeasure")]
		public ActionResult ConvertToIsoMeasure(int id = 0)
		{
			Idea idea = m_IdeaManagementService.FindById(id);
			if (idea == null)
			{
				return HttpNotFound();
			}

			if (m_MeasureManagementService.IdeaIsInMeasure(idea))
			{
				return new HttpStatusCodeResult(400,
					ErrorMessageResources.IdeaAlreadyConvertedToActivity);
			}

			var viewModel = IdeaViewModelFactory.CreateIdeaToIsoMeasureViewModel(idea);

			viewModel.DueDate = DateTime.Now;

			//Setting values of idea to viewmodel to modify them
			viewModel.Name = idea.Name;
			viewModel.Solution = idea.Solution;
			viewModel.Description = idea.Description;
			viewModel.Valuation = idea.Valuation;


			return View(viewModel);
		}

		[HttpPost, ActionName("ToMeasure"), Authorize(Roles = "Administrator")]
		public ActionResult ConvertToMeasureConfirmed(int id, IdeaToMeasureViewModel viewModel)
		{
			Idea idea = m_IdeaManagementService.FindById(id);
			if (idea == null)
			{
				return HttpNotFound();
			}

			if (m_MeasureManagementService.IdeaIsInMeasure(idea))
			{
				return new HttpStatusCodeResult(400,
					ErrorMessageResources.IdeaAlreadyConvertedToActivity);
			}

			//TODO: Must be set manually, because its returned null atm
			viewModel.Model = idea;

			//var uncheckedEmployee = Helper.SplitUserProfileFromView(viewModel.EmployeeInChargeString, m_UserProfileRepository);

			//if (uncheckedEmployee == null)
			//{
			//    ModelState.AddModelError("EmplyoyeeInChargeString",
			//                             ErrorMessageResources.UserNameDoesNotMatch);
			//}

			if (ModelState.IsValid)
			{
				var user = m_IdeaManagementService.GetUserProfiles(u => u.UserName.Equals(User.Identity.Name)).
					FirstOrDefault();

				//Creating new IdeaState to set the state of the idea to "duringImplementation"
				//idea.States.Add(new IdeaState
				//{
				//    IdeaId = idea.Id,
				//    IdeaStateNameId = 5,
				//    TimeStamp = DateTime.Now,
				//    StateEnabler = user
				//});


				var measure = new Measure();
				if (measure.Ideas == null)
				{
					measure.Ideas = new Collection<Idea>();
				}


				// Setting values to new generated activity
				measure.Name = viewModel.Name;
				measure.Solution = viewModel.Solution;
				measure.Description = viewModel.Description;
				measure.DueDate = viewModel.DueDate;
				measure.Valuation = viewModel.Valuation;
				//measure.EmployeeInCharge = uncheckedEmployee;
				//measure.Creator = user;
				measure.Priority = viewModel.Priority;
				measure.Ideas.Add(idea);

				//if (activity.ActivityStates == null)
				//{
				//    activity.ActivityStates = new Collection<ActivityState>();
				//}

				var company = m_IdeaManagementService.GetCompanyByUserIdentity(User.Identity.Name);
				m_MeasureManagementService.AddMeasure(measure,
					company.Id);

				// We need to save the IdeaState later in order to have a valid measure-Id from DB!
				//activity.ActivityStates.Add(new ActivityState
				//{
				//    ActivityId = activity.Id,
				//    ActivityStateNameId = (int)IdeaStateName.New,
				//    TimeStamp = DateTime.Now,
				//    StateEnabler = user
				//});


				////creates qualified URL to send with Email
				//var qualifiedURL = createQualifiedUrlWithId("Details", idea.Id);
				////Notify user that idea has been transformed into an activity
				//Mailer.TransformedIntoActivity(user,
				//                               idea,
				//                               activity,
				//               qualifiedURL).
				//       Send();

				////Notifiy EmployeeInCharge about activity that he has been assigned to
				//Mailer.AssignedToActivity(activity.EmployeeInCharge,
				//                          activity,
				//          qualifiedURL).
				//       Send();

				return RedirectToAction("Index",
					"Measure");
			}
			return View(viewModel);
		}

		[HttpPost, ActionName("ToIsoMeasure"), Authorize(Roles = "IsoAdministrator")]
		public ActionResult ConvertToIsoMeasureConfirmed(int id, IdeaToIsoMeasureViewModel viewModel)
		{
			Idea idea = m_IsoMeasureManagementService.FindIdeaById(id);
			if (idea == null)
			{
				return HttpNotFound();
			}

			if (m_IsoMeasureManagementService.IdeaIsInMeasure(idea))
			{
				return new HttpStatusCodeResult(400,
					ErrorMessageResources.IdeaAlreadyConvertedToActivity);
			}

			//TODO: Must be set manually, because its returned null atm
			viewModel.Model = idea;

			var users = m_IsoMeasureManagementService.GetUserProfiles();
			var employeeInCharge = Helper.SplitUserProfileFromView(viewModel.EmployeeInChargeString, users);

			if (employeeInCharge == null)
			{
				ModelState.AddModelError("EmplyoyeeInChargeString",
																 ErrorMessageResources.UserNameDoesNotMatch);
			}

			if (ModelState.IsValid)
			{
				var user = m_IsoMeasureManagementService.GetUserProfiles(u => u.UserName.Equals(User.Identity.Name)).
					FirstOrDefault();

				idea.IdeaState = IdeaState.DuringImplementation;
				//m_IsoMeasureManagementService.ChangeIdeaState(id, IdeaState.DuringImplementation);
				

				//Creating new IdeaState to set the state of the idea to "duringImplementation"
				//idea.States.Add(new IdeaState
				//{
				//    IdeaId = idea.Id,
				//    IdeaStateNameId = 5,
				//    TimeStamp = DateTime.Now,
				//    StateEnabler = user
				//});


				var isoMeasure = m_IsoMeasureManagementService.CreateIsoMeasure();
				
				// Setting values to new generated activity
				isoMeasure.Name = viewModel.Name;
				isoMeasure.Solution = viewModel.Solution;
				isoMeasure.Description = viewModel.Description;
				isoMeasure.DueDate = viewModel.DueDate;
				isoMeasure.Valuation = viewModel.Valuation;
				isoMeasure.EmployeeInCharge = employeeInCharge;
				isoMeasure.Creator = user;
				isoMeasure.Priority = viewModel.Priority;
				//isoMeasure.Ideas.Add(idea);
				m_IsoMeasureManagementService.AddIdea(isoMeasure,idea);

				//if (activity.ActivityStates == null)
				//{
				//    activity.ActivityStates = new Collection<ActivityState>();
				//}
				var company = m_IsoMeasureManagementService.GetCompanyByUserIdentity(User.Identity.Name);
				m_IsoMeasureManagementService.AddIsoMeasure(isoMeasure,
					company.Id);

				// We need to save the IdeaState later in order to have a valid isoMeasure-Id from DB!
				//activity.ActivityStates.Add(new ActivityState
				//{
				//    ActivityId = activity.Id,
				//    ActivityStateNameId = (int)IdeaStateName.New,
				//    TimeStamp = DateTime.Now,
				//    StateEnabler = user
				//});


				////creates qualified URL to send with Email
				//var qualifiedURL = createQualifiedUrlWithId("Details", idea.Id);
				////Notify user that idea has been transformed into an activity
				//Mailer.TransformedIntoActivity(user,
				//                               idea,
				//                               activity,
				//               qualifiedURL).
				//       Send();

				////Notifiy EmployeeInCharge about activity that he has been assigned to
				//Mailer.AssignedToActivity(activity.EmployeeInCharge,
				//                          activity,
				//          qualifiedURL).
				//       Send();

				return RedirectToAction("Index",
					"IsoMeasure");
			}
			return View(viewModel);
		}
	}
}