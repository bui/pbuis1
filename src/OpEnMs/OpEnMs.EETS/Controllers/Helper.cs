﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using OpEnMs.Domain.Entities;
using OpEnMs.EETS.ViewModels;

namespace OpEnMs.EETS.Controllers
{
  public static class Helper
  {
    public static UserProfile SplitUserProfileFromView(string userProfileString, IEnumerable<UserProfile> users)
    {
      string surName = ViewModelHelper.SplitStringByArgument(userProfileString,
        0);
      string givenName = ViewModelHelper.SplitStringByArgument(userProfileString,
        1);
      UserProfile employee;

      if (String.IsNullOrEmpty(userProfileString) || String.IsNullOrEmpty(surName) || String.IsNullOrEmpty(givenName))
      {
        employee = null;
      }
      else
      {
        // Deletes the " " within givenName
        givenName = givenName.Substring(1,
          givenName.Length - 1);

        employee = users.FirstOrDefault(u => u.SurName.Contains(surName) && u.GivenName.Contains(givenName));
      }
      return employee;
    }

    public static MvcHtmlString IconActionLink(this HtmlHelper helper, string button, string icon, string text, string actionName, string controllerName, object routeValues, object htmlAttributes)
    {
      var buttonTag = new TagBuilder("button");
      var iconTag = new TagBuilder("i");
      var anchorTag = new TagBuilder("a");

      buttonTag.MergeAttribute("class",
        button);
      iconTag.MergeAttribute("class",
        icon);

      var urlHelper = new UrlHelper(helper.ViewContext.RequestContext);

      anchorTag.MergeAttribute("href",
        urlHelper.Action(actionName,
          controllerName,
          routeValues));


      var iconHtml = iconTag.ToString(TagRenderMode.Normal) + text;


      buttonTag.InnerHtml = iconHtml;
      var buttonHtml = buttonTag.ToString(TagRenderMode.Normal);
      anchorTag.InnerHtml = buttonHtml;

      var html = anchorTag.ToString(TagRenderMode.Normal);

      return MvcHtmlString.Create(html);
    }
  }
}