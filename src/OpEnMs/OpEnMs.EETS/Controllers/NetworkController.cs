﻿#region Licence

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0.html
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// 
// Copyright (c) 2013, HTW Berlin

#endregion

using System.Web.Mvc;
using OpEnMs.Application.Services.NetworkManagement;
using OpEnMs.EETS.Factories;
using OpEnMs.EETS.ViewModels;
using OpEnMs.EETS.ViewModels.Network;

namespace OpEnMs.EETS.Controllers
{
	public class NetworkController: Controller
	{
		private readonly INetworkManagementService m_NetworkManagementService;

		public NetworkController(INetworkManagementService networkManagementService)
		{
			m_NetworkManagementService = networkManagementService;
		}

		//
		// GET: /Company/

		public ActionResult Index()
		{
			var viewModel = NetworkViewModelFactory.CreateNetworkListViewModel(m_NetworkManagementService.Get());
			return View(viewModel);
		}

		public ActionResult Overview()
		{
			var viewModel = NetworkViewModelFactory.CreateNetworkOverviewViewModel(m_NetworkManagementService.Get());

			return View(viewModel);
		}

		
		//
		// GET: /Company/Create
		[Authorize(Roles = "Administrator")]
		public ActionResult Create()
		{
			var viewModel = NetworkViewModelFactory.CreateNetworkCreateViewModel(m_NetworkManagementService.CreateNetwork(),
			                                                                     m_NetworkManagementService.GetCompanies());
			return View(viewModel);
		}

		//
		// POST: /Company/Create

		[HttpPost]
		[ValidateAntiForgeryToken]
		[Authorize(Roles = "Administrator")]
		public ActionResult Create(NetworkCreateViewModel viewModel)
		{
			if (ModelState.IsValid)
			{
				var network = viewModel.Model;
				network.Companies = viewModel.CompanyCheckBoxViewModels.ToCompany(m_NetworkManagementService.GetCompanies());
				
				m_NetworkManagementService.AddNetwork(network);
				return RedirectToAction("Index");
			}

			return View(viewModel);
		}

		//
		// GET: /Company/Edit/5

		public ActionResult Edit(int id = 0)
		{
			var network = m_NetworkManagementService.FindById(id);
			if (network == null)
			{
				return HttpNotFound();
			}

			var viewModel = NetworkViewModelFactory.CreateNetworkEditViewModel(network,
			                                                                   m_NetworkManagementService.GetCompanies());

			return View(viewModel);
		}

		//
		// POST: /Company/Edit/5

		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult Edit(NetworkEditViewModel viewModel)
		{
			if (ModelState.IsValid)
			{
				m_NetworkManagementService.UpdateNetwork(viewModel.Model);

				return RedirectToAction("Index");
			}
			return View(viewModel);
		}

		//
		// GET: /Company/Delete/5

		public ActionResult Delete(int id = 0)
		{
			var network = m_NetworkManagementService.FindById(id);
			if (network == null)
			{
				return HttpNotFound();
			}
			return View(network);
		}

		//
		// POST: /Company/Delete/5

		[HttpPost, ActionName("Delete")]
		[ValidateAntiForgeryToken]
		public ActionResult DeleteConfirmed(int id)
		{
			var network = m_NetworkManagementService.FindById(id);
			m_NetworkManagementService.DeleteNetwork(id);
			return RedirectToAction("Index");
		}

		protected override void Dispose(bool disposing)
		{
			m_NetworkManagementService.Dispose();
			base.Dispose(disposing);
		}
	}
}