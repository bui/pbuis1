﻿using System.Web.Mvc;
using OpEnMs.Application.Services.EnergyConsumptionManagement;
using OpEnMs.EETS.Factories;
using OpEnMs.EETS.ViewModels.EnergyConsumption;
using OpEnMs.EnergyChart.Factories;

namespace OpEnMs.EETS.Controllers
{
  public class EnergyConsumptionController: Controller
  {
    private readonly IEnergyConsumptionManagementService m_EnergyConsumptionManagementService;
    private IHighchartFactory m_Highchartfactory;

    public EnergyConsumptionController(IEnergyConsumptionManagementService energyConsumptionManagementService, IHighchartFactory highchartFactory)
    {
      m_EnergyConsumptionManagementService = energyConsumptionManagementService;
      m_Highchartfactory = highchartFactory;
    }


    //
    // GET: /EnergyConsumption/

    public ActionResult Index()
    {
      var company = m_EnergyConsumptionManagementService.GetCompanyByUserIdentity(User.Identity.Name);

      if (company == null)
      {
        return HttpNotFound();
      }

      var viewModel = EnergyConsumptionViewModelFactory.CreateEnergyConsumptionListViewModel(company.EnergyConsumptions);

      return View(viewModel);
    }

    public ActionResult Create(int companyId = 0)
    {
      Session["CompanyId"] = companyId;

      var sources = m_EnergyConsumptionManagementService.GetSources();
      var units = m_EnergyConsumptionManagementService.GetUnits();
      var model = m_EnergyConsumptionManagementService.CreateEnergyConsumption();

      var viewModel = EnergyConsumptionViewModelFactory.CreateEnergyConsumptionCreateViewModel(model,
        units,
        sources);

      return View(viewModel);
    }

    [HttpPost]
    [ValidateAntiForgeryToken]
    [Authorize(Roles = "Administrator, Benutzer")]
    public ActionResult Create(EnergyConsumptionCreateViewModel viewModel)
    {
      //TODO: Check if null
      var companyId = (int) Session["CompanyId"];

      if (ModelState.IsValid)
      {
        m_EnergyConsumptionManagementService.AddEnergyConsumption(viewModel.Model,
          companyId,
          viewModel.UnitId,
          viewModel.SourceId);


        return RedirectToAction("Edit",
          "Company",
          new{
               id = companyId
             });
      }

      return View(viewModel);
    }

    public ActionResult Edit(int id = 0, int companyId = 0)
    {
      var energyConsumption = m_EnergyConsumptionManagementService.FindById(id);
      Session["CompanyId"] = companyId;

      if (energyConsumption == null)
      {
        return HttpNotFound();
      }

      var viewModel = EnergyConsumptionViewModelFactory.CreateEnergyConsumptionEditViewModel(energyConsumption);
      var energyConsumptionDetailsViewModel = EnergyConsumptionViewModelFactory.CreateEnergyConsumptionDetailsViewModel(energyConsumption,
        m_Highchartfactory);

      viewModel.EnergyConsumptionDetailsViewModel = energyConsumptionDetailsViewModel;

      return View(viewModel);
    }

    [HttpPost]
    [ValidateAntiForgeryToken]
    public ActionResult Edit(EnergyConsumptionEditViewModel viewModel)
    {
      if (ModelState.IsValid)
      {
        m_EnergyConsumptionManagementService.UpdateEnergyConsumption(viewModel.Model);

        var companyId = (int) Session["CompanyId"];

        return RedirectToAction("Edit",
          "Company",
          new{
               id = Session["CompanyId"]
             });
      }
      return View(viewModel);
    }

    public ActionResult Details(int id = 0)
    {
      var energyConsumption = m_EnergyConsumptionManagementService.FindById(id);

      if (energyConsumption == null)
      {
        return HttpNotFound();
      }

      var viewModel = EnergyConsumptionViewModelFactory.CreateEnergyConsumptionDetailsViewModel(energyConsumption,
        m_Highchartfactory);

      return View(viewModel);
    }

    [Authorize]
    public ActionResult Delete(int id = 0, int companyId = 0)
    {
      var energyConsumption = m_EnergyConsumptionManagementService.FindById(id);

      Session["CompanyId"] = companyId;

      if (energyConsumption == null)
      {
        return HttpNotFound();
      }
      return View(energyConsumption);
    }

    //
    // POST: /Measure/Delete/5

    [HttpPost, ActionName("Delete")]
    [ValidateAntiForgeryToken]
    [Authorize]
    public ActionResult DeleteConfirmed(int id)
    {
      m_EnergyConsumptionManagementService.DeleteEnergyConsumption(id);

      return RedirectToAction("Edit",
        "Company",
        new{
             id = Session["CompanyId"]
           });
    }
  }
}