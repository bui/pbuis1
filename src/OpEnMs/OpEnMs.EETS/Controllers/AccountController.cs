﻿#region Licence

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0.html
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// 
// Copyright (c) 2013, HTW Berlin

#endregion

using System;
using System.Linq;
using System.Transactions;
using System.Web.Mvc;
using System.Web.Security;
using Microsoft.Web.WebPages.OAuth;
using OpEnMs.Application.Services.UserProfileManagement;
using OpEnMs.Domain.Entities;
using OpEnMs.EETS.Factories;
using OpEnMs.EETS.ViewModels.UserProfile;
using WebMatrix.WebData;

namespace OpEnMs.EETS.Controllers
{
  [Authorize]
  //[InitializeSimpleMembership]
  public class AccountController: Controller
  {
    private readonly IUserProfileManagementService m_UserProfileManagementService;

    public AccountController(IUserProfileManagementService userProfileManagementService)
    {
      m_UserProfileManagementService = userProfileManagementService;
    }

    [Authorize(Roles = "Administrator")]
    public ActionResult Index()
    {
      var viewModel = UserProfileViewModelFactory.CreateUserProfileListViewModel(m_UserProfileManagementService.Get(),
        m_UserProfileManagementService.GetCompanies());

      return View(viewModel);
    }

    [Authorize(Roles = "Administrator")]
    public ActionResult Edit(int id = 0)
    {
      var userProfile = m_UserProfileManagementService.FindById(id);
      if (userProfile == null)
      {
        return HttpNotFound();
      }

      var company = m_UserProfileManagementService.FindCompanyByUser(userProfile);

      UserProfileEditViewModel viewModel;

      if (company != null)
      {
        viewModel = UserProfileViewModelFactory.CreateUserProfileEditViewModel(userProfile,
          m_UserProfileManagementService.GetCompanies(),
          company.Id);
      }
      else
      {
        viewModel = UserProfileViewModelFactory.CreateUserProfileEditViewModel(userProfile,
          m_UserProfileManagementService.GetCompanies(),
          0);
      }


      return View(viewModel);
    }

    //
    // POST: /Company/Edit/5

    [HttpPost]
    [ValidateAntiForgeryToken]
    [Authorize(Roles = "Administrator")]
    public ActionResult Edit(UserProfileEditViewModel viewModel)
    {
      if (ModelState.IsValid)
      {
        var userProfile = viewModel.Model;

        m_UserProfileManagementService.UpdateUserProfile(userProfile);

        RemoveUserProfileFromCompany(userProfile);

        var company = m_UserProfileManagementService.FindCompanyById(viewModel.CompanyId);

        AddUserProfileToCompany(userProfile,
          company);


        if (!WebSecurity.IsCurrentUser(userProfile.UserName))
        {
          if (Roles.GetRolesForUser(userProfile.UserName).
            Any())
          {
            Roles.RemoveUserFromRole(userProfile.UserName,
              Roles.GetRolesForUser(userProfile.UserName).
                FirstOrDefault());
          }

          AddUserToRole(userProfile.UserName,
            viewModel.Role);

          return RedirectToAction("Index");
        }

        WebSecurity.Logout();

        Roles.RemoveUserFromRole(userProfile.UserName,
          Roles.GetRolesForUser(userProfile.UserName).
            FirstOrDefault());

        AddUserToRole(userProfile.UserName,
          viewModel.Role);

        return RedirectToAction("Login",
          "Account");
      }
      return View(viewModel);
    }

    private void AddUserProfileToCompany(UserProfile userProfile, Company company)
    {
      if (company != null)
      {
        company.Employees.Add(userProfile);

        m_UserProfileManagementService.UpdateCompany(company);
      }
    }

    private void RemoveUserProfileFromCompany(UserProfile userProfile)
    {
      var company = m_UserProfileManagementService.GetCompanies(c => c.Employees.FirstOrDefault(u => u.UserId.Equals(userProfile.UserId)).
        UserId == userProfile.UserId).
        FirstOrDefault();

      if (company != null)
      {
        company.Employees.Remove(userProfile);
        m_UserProfileManagementService.UpdateCompany(company);
      }
    }


    private void AddUserToRole(string userName, string role)
    {
      if (!Roles.IsUserInRole(userName,
        role))
      {
        Roles.AddUserToRole(userName,
          role);
      }
    }

    //
    // GET: /Account/Login

    [AllowAnonymous]
    public ActionResult Login(string returnUrl)
    {
      ViewBag.ReturnUrl = returnUrl;
      return View();
    }

    //
    // POST: /Account/Login

    [HttpPost]
    [AllowAnonymous]
    [ValidateAntiForgeryToken]
    public ActionResult Login(Login model, string returnUrl)
    {
      if (ModelState.IsValid && WebSecurity.Login(model.UserName,
        model.Password,
        persistCookie : model.RememberMe))
      {
        return RedirectToLocal(returnUrl);
      }

      // Wurde dieser Punkt erreicht, ist ein Fehler aufgetreten; Formular erneut anzeigen.
      ModelState.AddModelError("",
        "Der angegebene Benutzername oder das angegebene Kennwort ist ungültig.");
      return View(model);
    }

    //
    // POST: /Account/LogOff

    [HttpPost]
    [ValidateAntiForgeryToken]
    public ActionResult LogOff()
    {
      WebSecurity.Logout();

      return RedirectToAction("Index",
        "Home");
    }

    //
    // GET: /Account/Register

    [AllowAnonymous]
    public ActionResult Register()
    {
      return View();
    }

    //
    // POST: /Account/Register

    [HttpPost]
    [AllowAnonymous]
    [ValidateAntiForgeryToken]
    public ActionResult Register(Register model)
    {
      if (ModelState.IsValid)
      {
        // Versuch, den Benutzer zu registrieren
        try
        {
          WebSecurity.CreateUserAndAccount(model.UserName,
            model.Password);
          WebSecurity.Login(model.UserName,
            model.Password);
          return RedirectToAction("Index",
            "Home");
        }
        catch (MembershipCreateUserException e)
        {
          ModelState.AddModelError("",
            ErrorCodeToString(e.StatusCode));
        }
      }

      // Wurde dieser Punkt erreicht, ist ein Fehler aufgetreten; Formular erneut anzeigen.
      return View(model);
    }

    //
    // POST: /Account/Disassociate

    [HttpPost]
    [ValidateAntiForgeryToken]
    public ActionResult Disassociate(string provider, string providerUserId)
    {
      string ownerAccount = OAuthWebSecurity.GetUserName(provider,
        providerUserId);
      ManageMessageId? message = null;

      // Die Zuordnung des Kontos nur aufheben, wenn der aktuell angemeldete Benutzer der Besitzer ist
      if (ownerAccount == User.Identity.Name)
      {
        // Eine Transaktion verwenden, um zu verhindern, dass der Benutzer seine letzten Anmeldeinformationen löscht
        using (var scope = new TransactionScope(TransactionScopeOption.Required,
          new TransactionOptions{
                                  IsolationLevel = IsolationLevel.Serializable
                                }))
        {
          bool hasLocalAccount = OAuthWebSecurity.HasLocalAccount(WebSecurity.GetUserId(User.Identity.Name));
          if (hasLocalAccount || OAuthWebSecurity.GetAccountsFromUserName(User.Identity.Name).
            Count > 1)
          {
            OAuthWebSecurity.DeleteAccount(provider,
              providerUserId);
            scope.Complete();
            message = ManageMessageId.RemoveLoginSuccess;
          }
        }
      }

      return RedirectToAction("Manage",
        new{
             Message = message
           });
    }

    //
    // GET: /Account/Manage

    public ActionResult Manage(ManageMessageId? message)
    {
      ViewBag.StatusMessage = message == ManageMessageId.ChangePasswordSuccess ? "Ihr Kennwort wurde geändert." : message == ManageMessageId.SetPasswordSuccess ? "Ihr Kennwort wurde festgelegt." : message == ManageMessageId.RemoveLoginSuccess ? "Die externe Anmeldung wurde entfernt." : "";
      ViewBag.HasLocalPassword = OAuthWebSecurity.HasLocalAccount(WebSecurity.GetUserId(User.Identity.Name));
      ViewBag.ReturnUrl = Url.Action("Manage");
      return View();
    }

    //
    // POST: /Account/Manage

    [HttpPost]
    [ValidateAntiForgeryToken]
    public ActionResult Manage(LocalPassword model)
    {
      bool hasLocalAccount = OAuthWebSecurity.HasLocalAccount(WebSecurity.GetUserId(User.Identity.Name));
      ViewBag.HasLocalPassword = hasLocalAccount;
      ViewBag.ReturnUrl = Url.Action("Manage");
      if (hasLocalAccount)
      {
        if (ModelState.IsValid)
        {
          // "ChangePassword" löst in bestimmten Fehlerszenarien eine Ausnahme aus, anstatt "false" zurückzugeben.
          bool changePasswordSucceeded;
          try
          {
            changePasswordSucceeded = WebSecurity.ChangePassword(User.Identity.Name,
              model.OldPassword,
              model.NewPassword);
          }
          catch (Exception)
          {
            changePasswordSucceeded = false;
          }

          if (changePasswordSucceeded)
          {
            return RedirectToAction("Manage",
              new{
                   Message = ManageMessageId.ChangePasswordSuccess
                 });
          }
          else
          {
            ModelState.AddModelError("",
              "Das aktuelle Kennwort ist nicht korrekt, oder das Kennwort ist ungültig.");
          }
        }
      }
      else
      {
        // Der Benutzer besitzt kein lokales Kennwort. Entfernen Sie daher alle Überprüfungsfehler, deren Ursache ein fehlendes
        // Feld "OldPassword" ist.
        ModelState state = ModelState["OldPassword"];
        if (state != null)
        {
          state.Errors.Clear();
        }

        if (ModelState.IsValid)
        {
          try
          {
            WebSecurity.CreateAccount(User.Identity.Name,
              model.NewPassword);
            return RedirectToAction("Manage",
              new{
                   Message = ManageMessageId.SetPasswordSuccess
                 });
          }
          catch (Exception)
          {
            ModelState.AddModelError("",
              String.Format("Das lokale Konto kann nicht erstellt werden. Möglicherweise ist ein Konto mit dem Namen \"{0}\" bereits vorhanden.",
                User.Identity.Name));
          }
        }
      }

      // Wurde dieser Punkt erreicht, ist ein Fehler aufgetreten; Formular erneut anzeigen.
      return View(model);
    }

    //
    // GET: /Account/Quicksearch/abc

    [Authorize]
    public ActionResult Quicksearch(string term)
    {
      return Json(m_UserProfileManagementService.FindByString(term),
        JsonRequestBehavior.AllowGet);
    }

    //
    // POST: /Account/ExternalLogin

    //[HttpPost]
    //[AllowAnonymous]
    //[ValidateAntiForgeryToken]
    //public ActionResult ExternalLogin(string provider, string returnUrl)
    //{
    //	return new ExternalLoginResult(provider, Url.Action("ExternalLoginCallback", new
    //	{
    //		ReturnUrl = returnUrl
    //	}));
    //}

    ////
    //// GET: /Account/ExternalLoginCallback

    //[AllowAnonymous]
    //public ActionResult ExternalLoginCallback(string returnUrl)
    //{
    //	AuthenticationResult result = OAuthWebSecurity.VerifyAuthentication(Url.Action("ExternalLoginCallback", new
    //	{
    //		ReturnUrl = returnUrl
    //	}));
    //	if (!result.IsSuccessful)
    //	{
    //		return RedirectToAction("ExternalLoginFailure");
    //	}

    //	if (OAuthWebSecurity.Login(result.Provider, result.ProviderUserId, createPersistentCookie : false))
    //	{
    //		return RedirectToLocal(returnUrl);
    //	}

    //	if (User.Identity.IsAuthenticated)
    //	{
    //		// Wenn der aktuelle Benutzer angemeldet ist, das neue Konto hinzufügen
    //		OAuthWebSecurity.CreateOrUpdateAccount(result.Provider, result.ProviderUserId, User.Identity.Name);
    //		return RedirectToLocal(returnUrl);
    //	}
    //	else
    //	{
    //		// Der Benutzer ist neu. Nach dem gewünschten Mitgliedschaftsnamen fragen.
    //		string loginData = OAuthWebSecurity.SerializeProviderUserId(result.Provider, result.ProviderUserId);
    //		ViewBag.ProviderDisplayName = OAuthWebSecurity.GetOAuthClientData(result.Provider).DisplayName;
    //		ViewBag.ReturnUrl = returnUrl;
    //		return View("ExternalLoginConfirmation", new RegisterExternalLoginModel
    //		{
    //			UserName = result.UserName,
    //			ExternalLoginData = loginData
    //		});
    //	}
    //}

    ////
    //// POST: /Account/ExternalLoginConfirmation

    //[HttpPost]
    //[AllowAnonymous]
    //[ValidateAntiForgeryToken]
    //public ActionResult ExternalLoginConfirmation(RegisterExternalLoginModel model, string returnUrl)
    //{
    //	string provider = null;
    //	string providerUserId = null;

    //	if (User.Identity.IsAuthenticated || !OAuthWebSecurity.TryDeserializeProviderUserId(model.ExternalLoginData, out provider, out providerUserId))
    //	{
    //		return RedirectToAction("Manage");
    //	}

    //	if (ModelState.IsValid)
    //	{
    //		// Neuen Benutzer in die Datenbank einfügen
    //		using (UsersContext db = new UsersContext())
    //		{
    //			UserProfile user = db.UserProfiles.FirstOrDefault(u => u.UserName.ToLower() == model.UserName.ToLower());
    //			// Überprüfen, ob der Benutzer bereits vorhanden ist
    //			if (user == null)
    //			{
    //				// Name in die Profiltabelle einfügen
    //				db.UserProfiles.Add(new UserProfile
    //				{
    //					UserName = model.UserName
    //				});
    //				db.SaveChanges();

    //				OAuthWebSecurity.CreateOrUpdateAccount(provider, providerUserId, model.UserName);
    //				OAuthWebSecurity.Login(provider, providerUserId, createPersistentCookie : false);

    //				return RedirectToLocal(returnUrl);
    //			}
    //			else
    //			{
    //				ModelState.AddModelError("UserName", "Der Benutzername ist bereits vorhanden. Geben Sie einen anderen Benutzernamen an.");
    //			}
    //		}
    //	}

    //	ViewBag.ProviderDisplayName = OAuthWebSecurity.GetOAuthClientData(provider).DisplayName;
    //	ViewBag.ReturnUrl = returnUrl;
    //	return View(model);
    //}

    ////
    //// GET: /Account/ExternalLoginFailure

    //[AllowAnonymous]
    //public ActionResult ExternalLoginFailure()
    //{
    //	return View();
    //}

    //[AllowAnonymous]
    //[ChildActionOnly]
    //public ActionResult ExternalLoginsList(string returnUrl)
    //{
    //	ViewBag.ReturnUrl = returnUrl;
    //	return PartialView("_ExternalLoginsListPartial", OAuthWebSecurity.RegisteredClientData);
    //}

    //[ChildActionOnly]
    //public ActionResult RemoveExternalLogins()
    //{
    //	ICollection<OAuthAccount> accounts = OAuthWebSecurity.GetAccountsFromUserName(User.Identity.Name);
    //	List<ExternalLogin> externalLogins = new List<ExternalLogin>();
    //	foreach (OAuthAccount account in accounts)
    //	{
    //		AuthenticationClientData clientData = OAuthWebSecurity.GetOAuthClientData(account.Provider);

    //		externalLogins.Add(new ExternalLogin
    //		{
    //			Provider = account.Provider,
    //			ProviderDisplayName = clientData.DisplayName,
    //			ProviderUserId = account.ProviderUserId,
    //		});
    //	}

    //	ViewBag.ShowRemoveButton = externalLogins.Count > 1 || OAuthWebSecurity.HasLocalAccount(WebSecurity.GetUserId(User.Identity.Name));
    //	return PartialView("_RemoveExternalLoginsPartial", externalLogins);
    //}

    #region Hilfsprogramme

    private ActionResult RedirectToLocal(string returnUrl)
    {
      if (Url.IsLocalUrl(returnUrl))
      {
        return Redirect(returnUrl);
      }
      else
      {
        return RedirectToAction("Index",
          "Home");
      }
    }

    public enum ManageMessageId
    {
      ChangePasswordSuccess,
      SetPasswordSuccess,
      RemoveLoginSuccess,
    }

    internal class ExternalLoginResult: ActionResult
    {
      public ExternalLoginResult(string provider, string returnUrl)
      {
        Provider = provider;
        ReturnUrl = returnUrl;
      }

      public string Provider { get; private set; }
      public string ReturnUrl { get; private set; }

      public override void ExecuteResult(ControllerContext context)
      {
        OAuthWebSecurity.RequestAuthentication(Provider,
          ReturnUrl);
      }
    }

    private static string ErrorCodeToString(MembershipCreateStatus createStatus)
    {
      // Unter "http://go.microsoft.com/fwlink/?LinkID=177550" finden Sie
      // vollständige Liste mit Statuscodes.
      switch (createStatus)
      {
        case MembershipCreateStatus.DuplicateUserName:
          return "Der Benutzername ist bereits vorhanden. Geben Sie einen anderen Benutzernamen an.";

        case MembershipCreateStatus.DuplicateEmail:
          return "Für diese E-Mail-Adresse ist bereits ein Benutzername vorhanden. Geben Sie eine andere E-Mail-Adresse ein.";

        case MembershipCreateStatus.InvalidPassword:
          return "Das angegebene Kennwort ist ungültig. Geben Sie einen gültigen Kennwortwert ein.";

        case MembershipCreateStatus.InvalidEmail:
          return "Die angegebene E-Mail-Adresse ist ungültig. Überprüfen Sie den Wert, und wiederholen Sie den Vorgang.";

        case MembershipCreateStatus.InvalidAnswer:
          return "Die angegebene Kennwortabrufantwort ist ungültig. Überprüfen Sie den Wert, und wiederholen Sie den Vorgang.";

        case MembershipCreateStatus.InvalidQuestion:
          return "Die angegebene Kennwortabruffrage ist ungültig. Überprüfen Sie den Wert, und wiederholen Sie den Vorgang.";

        case MembershipCreateStatus.InvalidUserName:
          return "Der angegebene Benutzername ist ungültig. Überprüfen Sie den Wert, und wiederholen Sie den Vorgang.";

        case MembershipCreateStatus.ProviderError:
          return "Vom Authentifizierungsanbieter wurde ein Fehler zurückgegeben. Überprüfen Sie die Eingabe, und wiederholen Sie den Vorgang. Sollte das Problem weiterhin bestehen, wenden Sie sich an den zuständigen Systemadministrator.";

        case MembershipCreateStatus.UserRejected:
          return "Die Benutzererstellungsanforderung wurde abgebrochen. Überprüfen Sie die Eingabe, und wiederholen Sie den Vorgang. Sollte das Problem weiterhin bestehen, wenden Sie sich an den zuständigen Systemadministrator.";

        default:
          return "Unbekannter Fehler. Überprüfen Sie die Eingabe, und wiederholen Sie den Vorgang. Sollte das Problem weiterhin bestehen, wenden Sie sich an den zuständigen Systemadministrator.";
      }
    }

    #endregion
  }
}