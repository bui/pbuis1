﻿/// <reference path="jquery-ui-1.10.2.js" />
/// <reference path="jquery-1.9.1.js" />


function deactivateTooltip(e) {
    //Get the corresponding editor-label and retrieve name from it to adress the element the popover is activated for
    var elementID = $(e).parent().prev().children().children().attr('for');
    var helpTextDiv = $(e).parent().parent().parent().next();
    if ($('#' + elementID).attr('title')==='') {
        $('#' + elementID).tooltip("enable");
        helpTextDiv.css('display', 'none')
    }
    else { 
        $('#' + elementID).tooltip("disable");
        helpTextDiv.css('display', 'block');
    }   
}

