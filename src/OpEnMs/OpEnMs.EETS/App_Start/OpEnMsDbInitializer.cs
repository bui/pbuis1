﻿using System.Data.Entity;
using OpEnMs.Data;
using OpEnMs.EETS.Migrations;

namespace OpEnMs.EETS.App_Start
{
	public class OpEnMsDbInitializer: DropCreateDatabaseIfModelChanges<OpEnMsDbContext>
	{
		protected override void Seed(OpEnMsDbContext context)
		{
			DbHelper.CreateTestData(context);
		}
	}
}