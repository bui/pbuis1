﻿#region Licence

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0.html
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// 
// Copyright (c) 2013, HTW Berlin

#endregion

namespace OpEnMs.EETS
{
	public static class AuthConfig
	{
		public static void RegisterAuth()
		{
			// Damit sich Benutzer dieser Website mithilfe ihrer Konten von anderen Websites (z. B. Microsoft, Facebook und Twitter) anmelden können,
			// müssen Sie diese Website aktualisieren. Weitere Informationen finden Sie unter "http://go.microsoft.com/fwlink/?LinkID=252166".

			//OAuthWebSecurity.RegisterMicrosoftClient(
			//    clientId: "",
			//    clientSecret: "");

			//OAuthWebSecurity.RegisterTwitterClient(
			//    consumerKey: "",
			//    consumerSecret: "");

			//OAuthWebSecurity.RegisterFacebookClient(
			//    appId: "",
			//    appSecret: "");

			//OAuthWebSecurity.RegisterGoogleClient();
		}
	}
}