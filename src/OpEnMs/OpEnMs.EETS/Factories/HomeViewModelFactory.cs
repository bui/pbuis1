﻿using OpEnMs.EETS.ViewModels.Idea;
using OpEnMs.EETS.ViewModels.IsoMeasure;
using OpEnMs.ViewModels.Home;

namespace OpEnMs.EETS.Factories
{
    public static class HomeViewModelFactory
    {

        public static HomeViewModel CreateHomeViewModel()
        {
            var viewModel = new HomeViewModel();
            return viewModel;
        }

        public static HomeViewModel CreateHomeViewModel(IdeaListViewModel ideaListViewModel, IsoMeasureListViewModel isoMeasureListViewModel)
        {
            var viewModel = new HomeViewModel(ideaListViewModel,
                                              isoMeasureListViewModel);
            return viewModel;
        }
    }
}