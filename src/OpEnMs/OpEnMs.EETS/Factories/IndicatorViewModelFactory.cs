﻿using System.Collections.Generic;
using Emporer.Unit;
using OpEnMs.Domain.Entities;
using OpEnMs.EETS.ViewModels.Indicator;

namespace OpEnMs.EETS.Factories
{
    public static class IndicatorViewModelFactory
    {
        public static IndicatorAddValueViewModel CreateIndicatorAddValueViewModel(Indicator indicator)
        {
            var viewModel = new IndicatorAddValueViewModel(indicator);
            return viewModel;
        }

        public static IndicatorListValuesViewModel CreateIndicatorListValuesViewModel()
        {
            var viewModel = new IndicatorListValuesViewModel();
            ;
            return viewModel;
        }

        public static IndicatorListViewModel CreateIndicatorListViewModel(List<Indicator> indicators, List<Unit> units, List<Topic> topics)
        {
            var viewModels = new List<IndicatorViewModel>();


            foreach (var indicator in indicators)
            {
                viewModels.Add(new IndicatorViewModel(indicator,
                                                      units,
                                                      topics));
            }

            return new IndicatorListViewModel(viewModels);
        }

        public static IndicatorViewModel CreateIndicatorViewModel(List<Unit> units, List<Topic> topics)
        {
            return new IndicatorViewModel(units,
                                          topics);
        }

        public static IndicatorViewModel CreateIndicatorViewModel(Indicator indicator, List<Unit> units, List<Topic> topics)
        {
            return new IndicatorViewModel(indicator,
                                          units,
                                          topics);
        }

        public static TopicDropDownViewModel CreateTopicDropDownViewModel()
        {
            return new TopicDropDownViewModel();
        }
    }
}