﻿using System.Collections.Generic;
using OpEnMs.Domain.Entities;
using OpEnMs.EETS.ViewModels.Company;
using OpEnMs.EETS.ViewModels.Network;

namespace OpEnMs.EETS.Factories
{
	public static class NetworkViewModelFactory
	{
		public static NetworkCreateViewModel CreateNetworkCreateViewModel(Network network, IEnumerable<Company> companies)
		{
			var viewModel = new NetworkCreateViewModel{
				                                          Model = network,
				                                          CompanyCheckBoxViewModels = CreateCompanyCheckBoxViewModels(network,
				                                                                                                      companies)
			                                          };
			return viewModel;
		}

		public static NetworkEditViewModel CreateNetworkEditViewModel(Network network, IEnumerable<Company> companies)
		{
			var viewModel = new NetworkEditViewModel{
				                                        Model = network,
				                                        CompanyCheckBoxViewModels = CreateCompanyCheckBoxViewModels(network,
				                                                                                                    companies)
			                                        };
			return viewModel;
		}


		public static NetworkListViewModel CreateNetworkListViewModel(IEnumerable<Network> networks)
		{
			var viewModel = new NetworkListViewModel{
				                                        Networks = CreateNetworkListElementViewModels(networks)
			                                        };
			return viewModel;
		}


		private static IList<NetworkListElementViewModel> CreateNetworkListElementViewModels(IEnumerable<Network> networks)
		{
			var viewModels = new List<NetworkListElementViewModel>();


			foreach (var network in networks)
			{
				viewModels.Add(new NetworkListElementViewModel{
					                                              Model = network
				                                              });
			}

			return viewModels;
		}


		private static IList<CompanyCheckBoxViewModel> CreateCompanyCheckBoxViewModels(Network network, IEnumerable<Company> companies)
		{
			var viewModels = new List<CompanyCheckBoxViewModel>();

			foreach (var company in companies)
			{
				var viewModel = new CompanyCheckBoxViewModel{
					                                            Name = company.Name,
					                                            CompanyId = company.Id
				                                            };
				if (network.Companies.Contains(company))
				{
					viewModel.IsChecked = true;
				}
				viewModels.Add(viewModel);
			}
			return viewModels;
		}

		public static NetworkOverviewViewModel CreateNetworkOverviewViewModel(IEnumerable<Network> networks)
		{
			var viewModel = new NetworkOverviewViewModel{
				                                            Networks = CreateNetworkOverviewElementViewModels(networks)
			                                            };

			return viewModel;
		}

		private static IList<NetworkOverviewElementViewModel> CreateNetworkOverviewElementViewModels(IEnumerable<Network> networks)
		{
			var viewModels = new List<NetworkOverviewElementViewModel>();

			foreach (var network in networks)
			{
				var viewModel = new NetworkOverviewElementViewModel{
					                                                   Model = network,
					                                                   Companies = CompanyViewModelFactory.CreateCompanyListElementViewModels(network.Companies)
				                                                   };

				viewModels.Add(viewModel);
			}
			return viewModels;
		}
	}
}