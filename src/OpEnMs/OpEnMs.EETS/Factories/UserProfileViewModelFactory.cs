﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Web.Security;
using OpEnMs.Domain.Entities;
using OpEnMs.EETS.ViewModels;
using OpEnMs.EETS.ViewModels.UserProfile;

namespace OpEnMs.EETS.Factories
{
  public static class UserProfileViewModelFactory
  {
    public static UserProfileListViewModel CreateUserProfileListViewModel(IEnumerable<UserProfile> userProfiles, IEnumerable<Company> companies)
    {
      var viewModel = new UserProfileListViewModel{
        UserProfiles = CreateUserProfileListElementViewModels(userProfiles, companies)
                                                  };
      return viewModel;
    }

    private static IList<UserProfileListElementViewModel> CreateUserProfileListElementViewModels(IEnumerable<UserProfile> userProfiles, IEnumerable<Company> companies)
    {
      var viewModels = new List<UserProfileListElementViewModel>();


      foreach (var userProfile in userProfiles)
      {
        var viewModel = new UserProfileListElementViewModel{
                                                            Model = userProfile
                                                          };
       if (companies.Any(c => c.Employees.Contains(userProfile)))
        {
          var company = companies.FirstOrDefault(c =>
          {
            var firstOrDefault = c.Employees.FirstOrDefault(e => e.UserId == userProfile.UserId);
            return firstOrDefault != null && firstOrDefault.UserId == userProfile.UserId;
          });

          viewModel.Company = company.Name;
        }

       viewModels.Add(viewModel);
      }

      return viewModels;
    }

    public static UserProfileEditViewModel CreateUserProfileEditViewModel(UserProfile userProfile, IEnumerable<Company> companies, int companyId)
    {
      var viewModel = new UserProfileEditViewModel{
                                                          Model = userProfile,
                                                          Companies = companies.ToSelectListItems(companyId),
                                                          CompanyId = companyId,
                                                          Role = Roles.GetRolesForUser(userProfile.UserName).FirstOrDefault(),
                                                          Roles = Roles.GetAllRoles().ToSelectListItems(Roles.GetRolesForUser(userProfile.UserName).FirstOrDefault())
                                                        };
      return viewModel;
    }
  }
}