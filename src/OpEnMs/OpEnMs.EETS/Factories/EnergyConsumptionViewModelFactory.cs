﻿using System.Collections.Generic;
using Emporer.Unit;
using OpEnMs.Domain.Entities;
using OpEnMs.EETS.ViewModels.EnergyConsumption;
using OpEnMs.EETS.ViewModels.Reading;
using OpEnMs.EnergyChart.Factories;

namespace OpEnMs.EETS.Factories
{
  public static class EnergyConsumptionViewModelFactory
  {
    public static EnergyConsumptionCreateViewModel CreateEnergyConsumptionCreateViewModel(EnergyConsumption energyConsumption, IEnumerable<Unit> units, IEnumerable<Source> sources)
    {
      var viewModel = new EnergyConsumptionCreateViewModel{
                                                            Model = energyConsumption,
                                                            Units = units,
                                                            Sources = sources
                                                          };
      return viewModel;
    }


    public static EnergyConsumptionDetailsViewModel CreateEnergyConsumptionDetailsViewModel(EnergyConsumption energyConsumption, IHighchartFactory highchartFactory)
    {
      var viewModel = new EnergyConsumptionDetailsViewModel{
                                                             Linechart = highchartFactory.CreateLineChart(energyConsumption.Readings,
                                                               energyConsumption.Source.Name,
                                                               energyConsumption.Unit.Symbol),
                                                             Model = energyConsumption,
                                                             ReadingListViewModel = CreateReadingListViewModel(energyConsumption.Readings)
                                                           };
      return viewModel;
    }

    public static ReadingListViewModel CreateReadingListViewModel(ICollection<Reading> readings)
    {
      var viewModel = new ReadingListViewModel{
                                                Readings = CreateReadingListElementViewModels(readings)
                                              };
      return viewModel;
    }

    private static IList<ReadingListElementViewModel> CreateReadingListElementViewModels(ICollection<Reading> readings)
    {
      var viewModels = new List<ReadingListElementViewModel>();


      foreach (var reading in readings)
      {
        viewModels.Add(new ReadingListElementViewModel{
                                                        Model = reading
                                                      });
      }

      return viewModels;
    }

    public static EnergyConsumptionListViewModel CreateEnergyConsumptionListViewModel(ICollection<EnergyConsumption> energyConsumptions)
    {
      var viewModel = new EnergyConsumptionListViewModel{
                                                          EnergyConsumptions = CreateEnergyConsumptionListElementViewModels(energyConsumptions)
                                                        };
      return viewModel;
    }

    private static IList<EnergyConsumptionListElementViewModel> CreateEnergyConsumptionListElementViewModels(ICollection<EnergyConsumption> energyConsumptions)
    {
      var viewModels = new List<EnergyConsumptionListElementViewModel>();


      foreach (var energyConsumption in energyConsumptions)
      {
        viewModels.Add(new EnergyConsumptionListElementViewModel{
                                                                  Model = energyConsumption
                                                                });
      }

      return viewModels;
    }


    public static EnergyConsumptionEditViewModel CreateEnergyConsumptionEditViewModel(EnergyConsumption energyConsumption)
    {
      var energyConsumptionEditViewModel = new EnergyConsumptionEditViewModel{
                                                                               Model = energyConsumption
                                                                             };

      return energyConsumptionEditViewModel;
    }
  }
}