﻿
using OpEnMs.Domain.Entities;
using OpEnMs.EETS.ViewModels.Idea;

using System.Collections.Generic;


namespace OpEnMs.EETS.Factories
{
    public static class IdeaViewModelFactory
    {
        public static IdeaListViewModel CreateIdeaListViewModels(IEnumerable<Idea> ideas)
        {
            var viewModel = new IdeaListViewModel
            {
                Ideas = CreateIdeaListElementViewModels(ideas)
            };
            return viewModel;
        }

        private static IList<IdeaListElementViewModel> CreateIdeaListElementViewModels(IEnumerable<Idea> ideas)
        {
            var viewModels = new List<IdeaListElementViewModel>();


            foreach (var idea in ideas) 
            {
                viewModels.Add(new IdeaListElementViewModel
                {
                    Model = idea
                });
            }

            return viewModels;
        }

        public static IdeaCreateViewModel CreateIdeaCreateViewModel(Idea idea)
        {
            var viewModel = new IdeaCreateViewModel
            {
                Model = idea
            };
            return viewModel;
        }

        public static IdeaDetailsViewModel CreateIdeaDetailsViewModel(Idea idea)
        {
            var viewModel = new IdeaDetailsViewModel
            {
                Model = idea

            };
            return viewModel;
        }

        public static IdeaToMeasureViewModel CreateIdeaToMeasureViewModel(Idea idea)
        {
            var viewModel = new IdeaToMeasureViewModel
            {
                Model = idea
            };

            return viewModel;
        }

				public static IdeaToIsoMeasureViewModel CreateIdeaToIsoMeasureViewModel(Idea idea)
				{
					var viewModel = new IdeaToIsoMeasureViewModel
					{
						Model = idea
					};

					return viewModel;
				}
    }
}