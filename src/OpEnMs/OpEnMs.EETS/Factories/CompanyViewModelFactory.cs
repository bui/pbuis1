﻿using System.Collections.Generic;
using OpEnMs.Domain.Entities;
using OpEnMs.EETS.ViewModels.Company;
using OpEnMs.EETS.ViewModels.EnergyConsumption;
using OpEnMs.EETS.ViewModels.Measure;
using OpEnMs.EETS.ViewModels.MeasuringPoint;
using OpEnMs.EnergyChart.Factories;

namespace OpEnMs.EETS.Factories
{
  public static class CompanyViewModelFactory
  {
    public static CompanyEditViewModel CreateCompanyEditViewModel(Company company)
    {
      var viewModel = new CompanyEditViewModel{
                                                Model = company,
                                                MeasureListViewModel = CreateMeasureListViewModels(company.Measures),
                                                MeasuringPointListViewModel = CreateMeasuringPointListViewModel(company.MeasuringPoints),
                                                EnergyConsumptionListViewModel = CreateEnergyConsumptionListViewModel(company.EnergyConsumptions)
                                              };
      return viewModel;
    }


    private static EnergyConsumptionListViewModel CreateEnergyConsumptionListViewModel(ICollection<EnergyConsumption> energyConsumptions)
    {
      var viewModel = new EnergyConsumptionListViewModel{
                                                          EnergyConsumptions = CreateEnergyConsumptionListElementViewModels(energyConsumptions)
                                                        };
      return viewModel;
    }

    private static IList<EnergyConsumptionListElementViewModel> CreateEnergyConsumptionListElementViewModels(ICollection<EnergyConsumption> energyConsumptions)
    {
      var viewModels = new List<EnergyConsumptionListElementViewModel>();


      foreach (var energyConsumption in energyConsumptions)
      {
        viewModels.Add(new EnergyConsumptionListElementViewModel{
                                                                  Model = energyConsumption
                                                                });
      }

      return viewModels;
    }

    private static MeasuringPointListViewModel CreateMeasuringPointListViewModel(ICollection<MeasuringPoint> measuringPoints)
    {
      var viewModel = new MeasuringPointListViewModel{
                                                       MeasuringPoints = CreateMeasuringPointListElementViewModels(measuringPoints)
                                                     };
      return viewModel;
    }

    private static IList<MeasuringPointListElementViewModel> CreateMeasuringPointListElementViewModels(ICollection<MeasuringPoint> measuringPoints)
    {
      var viewModels = new List<MeasuringPointListElementViewModel>();


      foreach (var measuringPoint in measuringPoints)
      {
        viewModels.Add(new MeasuringPointListElementViewModel{
                                                               Model = measuringPoint
                                                             });
      }

      return viewModels;
    }

    private static MeasureListViewModel CreateMeasureListViewModels(IEnumerable<Measure> measures)
    {
      var viewModel = new MeasureListViewModel{
                                                Measures = CreateMeasureListElementViewModels(measures)
                                              };
      return viewModel;
    }

    private static IList<MeasureListElementViewModel> CreateMeasureListElementViewModels(IEnumerable<Measure> measures)
    {
      var viewModels = new List<MeasureListElementViewModel>();


      foreach (var measure in measures)
      {
        viewModels.Add(new MeasureListElementViewModel{
                                                        Model = measure
                                                      });
      }

      return viewModels;
    }

    //private static IList<UserProfileCheckBoxViewModel> CreateUserprofileCheckBoxViewModels(Company company, IEnumerable<UserProfile> userProfiles)
    //{
    //	var userCheckBoxViewModels = new List<UserProfileCheckBoxViewModel>();

    //	foreach (var userProfil in userProfiles)
    //	{
    //		var userProfileCheckBoxViewModel = new UserProfileCheckBoxViewModel{
    //																																				 UserProfile = userProfil
    //																																			 };
    //		if (company.Employees.Contains(userProfil))
    //		{
    //			userProfileCheckBoxViewModel.IsChecked = true;
    //		}
    //		userCheckBoxViewModels.Add(userProfileCheckBoxViewModel);
    //	}
    //	return userCheckBoxViewModels;
    //}

    public static CompanyListViewModel CreateCompanyListViewModel(IEnumerable<Company> companies)
    {
      var viewModel = new CompanyListViewModel{
                                                Companies = CreateCompanyListElementViewModels(companies)
                                              };
      return viewModel;
    }


    public static IList<CompanyListElementViewModel> CreateCompanyListElementViewModels(IEnumerable<Company> companies)
    {
      var viewModels = new List<CompanyListElementViewModel>();


      foreach (var company in companies)
      {
        viewModels.Add(new CompanyListElementViewModel{
                                                        Model = company
                                                      });
      }

      return viewModels;
    }

    public static CompanyDetailsViewModel CreateCompanyDetailsViewModel(Company company, IHighchartFactory highchartFactory)
    {
      var companyAdapterLine = new CompanyAdapter(company,
        "_Liniendiagramm");
      var companyAdapterColumn = new CompanyAdapter(company,
        "_Spaltendiagramm");
      var companyAdapterPie = new CompanyAdapter(company,
        "_Kuchendiagramm");

      var viewModel = new CompanyDetailsViewModel{
                                                   Linechart = highchartFactory.CreateLineChart(companyAdapterLine),
                                                   ColumnChart = highchartFactory.CreateColumnChart(companyAdapterColumn),
                                                   PieChart = highchartFactory.CreatePieChart(companyAdapterPie)
                                                 };
      return viewModel;
    }
  }
}