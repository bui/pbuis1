﻿using System.Collections.Generic;
using OpEnMs.Domain.Entities;
using OpEnMs.EETS.ViewModels.Measure;

namespace OpEnMs.EETS.Factories
{
	public static class MeasureViewModelFactory
	{
		public static MeasureListViewModel CreateMeasureListViewModels(IEnumerable<Measure> measures)
		{
			var viewModel = new MeasureListViewModel{
				                                        Measures = CreateMeasureListElementViewModels(measures)
			                                        };
			return viewModel;
		}

		private static IList<MeasureListElementViewModel> CreateMeasureListElementViewModels(IEnumerable<Measure> measures)
		{
			var viewModels = new List<MeasureListElementViewModel>();


			foreach (var measure in measures)
			{
				viewModels.Add(new MeasureListElementViewModel{
					                                              Model = measure
				                                              });
			}

			return viewModels;
		}

		public static MeasureCreateViewModel CreateMeasureCreateViewModel(Measure measure)
		{
			var viewModel = new MeasureCreateViewModel{
				                                          Model = measure
			                                          };
			return viewModel;
		}

		public static MeasureEditViewModel CreateMeasureEditViewModel(Measure measure)
		{
			var viewModel = new MeasureEditViewModel
			{
				Model = measure
			};
			return viewModel;
		}
	}
}