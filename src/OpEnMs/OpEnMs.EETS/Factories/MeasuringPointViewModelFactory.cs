﻿using System.Collections.Generic;
using System.Linq;
using Emporer.Unit;
using OpEnMs.Domain.Entities;
using OpEnMs.EETS.ViewModels.MeasuringPoint;

namespace OpEnMs.EETS.Factories
{
  public static class MeasuringPointViewModelFactory
  {
    public static MeasuringPointCreateViewModel CreateMeasuringPointCreateViewModel(MeasuringPoint measuringPoint, IEnumerable<Unit> units, IEnumerable<Source> sources)
    {
      var viewModel = new MeasuringPointCreateViewModel{
                                                         Model = measuringPoint,
                                                         Sources = sources,
                                                         Units = units
                                                       };

      return viewModel;
    }

    public static MeasuringPointListViewModel CreateMeasuringPoinListViewModel(IEnumerable<MeasuringPoint> measuringPoints)
    {
      var viewModel = new MeasuringPointListViewModel{
                                                       MeasuringPoints = MeasuringPointListElementViewModels(measuringPoints)
                                                     };
      return viewModel;
    }

    private static IList<MeasuringPointListElementViewModel> MeasuringPointListElementViewModels(IEnumerable<MeasuringPoint> measuringPoints)
    {
      var viewModels = new List<MeasuringPointListElementViewModel>();

      if (measuringPoints != null)
      {
        foreach (var measuringPoint in measuringPoints)
        {
          viewModels.Add(new MeasuringPointListElementViewModel{
                                                                 Model = measuringPoint
                                                               });
        }
      }


      return viewModels;
    }

    public static MeasuringPointEditViewModel CreateMeasuringPointEditViewModel(MeasuringPoint measuringPoint, IEnumerable<Unit> units, IEnumerable<Source> sources)
    {
      var viewModel = new MeasuringPointEditViewModel{
                                                       Model = measuringPoint,
                                                       Sources = sources,
                                                       Units = units,
                                                       SourceId = measuringPoint.EnergyConsumptions.FirstOrDefault().
                                                         Source.Id,
                                                       UnitId = measuringPoint.EnergyConsumptions.FirstOrDefault().
                                                         Unit.Id
                                                     };
      return viewModel;
    }
  }
}