﻿using System.Collections.Generic;
using OpEnMs.Domain.Entities;
using OpEnMs.EETS.ViewModels.IsoMeasure;

namespace OpEnMs.EETS.Factories
{
	public class IsoMeasureViewModelFactory
	{
		public static IsoMeasureListViewModel CreateIsoMeasureListViewModels(IEnumerable<IsoMeasure> isoMeasures)
		{
			var viewModel = new IsoMeasureListViewModel{
				                                           IsoMeasures = CreateIsoMeasureListElementViewModels(isoMeasures)
			                                           };
			return viewModel;
		}

		private static IList<IsoMeasureListElementViewModel> CreateIsoMeasureListElementViewModels(IEnumerable<IsoMeasure> isoMeasures)
		{
			var viewModels = new List<IsoMeasureListElementViewModel>();


			foreach (var isoMeasure in isoMeasures)
			{
				viewModels.Add(new IsoMeasureListElementViewModel{
					                                              Model = isoMeasure
				                                              });
			}

			return viewModels;
		}

		public static IsoMeasureCreateViewModel CreateIsoMeasureCreateViewModel(IsoMeasure isoMeasure)
		{
			var viewModel = new IsoMeasureCreateViewModel{
				                                          Model = isoMeasure
			                                          };
			return viewModel;
		}

		public static IsoMeasureEditViewModel CreateMeasureEditViewModel(IsoMeasure isoMeasure)
		{
			var viewModel = new IsoMeasureEditViewModel{
				                                        Model = isoMeasure
			                                        };
			return viewModel;
		}

		public static IsoMeasureDetailsViewModel CreateIsoMeasureDetailsViewModel(IsoMeasure isoMeasure)
		{
			var viewModel = new IsoMeasureDetailsViewModel{
				                                              Model = isoMeasure
			                                              };
			return viewModel;
		}
	}
}