﻿using System.Collections.Generic;
using System.Linq;

namespace OpEnMs.EETS.ViewModels.Reading
{
	public class ReadingListViewModel
	{
		public IList<ReadingListElementViewModel> Readings { get; set; }

		public int Count
		{
			get
			{
				return Readings.Count();
			}
		}
	}
}