﻿using System.ComponentModel.DataAnnotations;
using OpEnMs.Resources;

namespace OpEnMs.EETS.ViewModels.Reading
{
	public class ReadingListElementViewModel
	{
		private Domain.Entities.Reading m_Model;

		public Domain.Entities.Reading Model
		{
			get
			{
				return m_Model;
			}
			set
			{
				m_Model = value;
			}
		}

		[Display(ResourceType = typeof (DisplayNameResources), Name = "Timestamp")]
		public string Timestamp
		{
			get
			{
				if (m_Model.Timestamp != null)
				{
					return m_Model.Timestamp.ToShortDateString();
				}
				return "";
			}
		}

		[Display(ResourceType = typeof (DisplayNameResources), Name = "Value")]
		public string Value
		{
			get
			{
				return m_Model.Value.ToString();
			}
		}
	}
}