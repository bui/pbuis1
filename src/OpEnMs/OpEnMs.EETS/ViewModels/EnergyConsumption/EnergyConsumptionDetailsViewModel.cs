﻿using DotNet.Highcharts;
using OpEnMs.EETS.ViewModels.Reading;

namespace OpEnMs.EETS.ViewModels.EnergyConsumption
{
	public class EnergyConsumptionDetailsViewModel
	{
		public Domain.Entities.EnergyConsumption Model { get; set; }
		public ReadingListViewModel ReadingListViewModel { get; set; }
		public Highcharts Linechart { get; set; }
	}
}