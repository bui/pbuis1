﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Emporer.Unit;
using OpEnMs.Domain.Entities;
using OpEnMs.Resources;

namespace OpEnMs.EETS.ViewModels.EnergyConsumption
{
  public class EnergyConsumptionCreateViewModel
  {
    public Domain.Entities.EnergyConsumption Model { get; set; }

    [Display(ResourceType = typeof (DisplayNameResources), Name = "Source")]
    public IEnumerable<Source> Sources { get; set; }

    public int SourceId { get; set; }

    public int UnitId { get; set; }

    [Display(ResourceType = typeof (DisplayNameResources), Name = "Unit")]
    public IEnumerable<Unit> Units { get; set; }
  }
}