﻿using System.Collections.Generic;
using System.Linq;

namespace OpEnMs.EETS.ViewModels.EnergyConsumption
{
	public class EnergyConsumptionListViewModel
	{
		public IList<EnergyConsumptionListElementViewModel> EnergyConsumptions { get; set; }

		public int Count
		{
			get
			{
				return EnergyConsumptions.Count();
			}
		}
	}
}