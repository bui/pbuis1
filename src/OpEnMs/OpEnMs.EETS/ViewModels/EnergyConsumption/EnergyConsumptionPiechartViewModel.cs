﻿using System.Collections.Generic;
using OpEnMs.EnergyChart;

namespace OpEnMs.EETS.ViewModels.EnergyConsumption
{
	public class EnergyConsumptionPiechartViewModel: IChartable
	{
		public EnergyConsumptionPiechartViewModel(string namen, ICollection<Domain.Entities.EnergyConsumption> energyConsumptions)
		{
			Name = Name;
			EnergyConsumptions = energyConsumptions;
		}

		public string Name { get; private set; }
		public ICollection<Domain.Entities.EnergyConsumption> EnergyConsumptions { get; private set; }
	}
}