﻿using System.ComponentModel.DataAnnotations;
using OpEnMs.Resources;

namespace OpEnMs.EETS.ViewModels.EnergyConsumption
{
	public class EnergyConsumptionListElementViewModel
	{
		private Domain.Entities.EnergyConsumption m_Model;

		public Domain.Entities.EnergyConsumption Model
		{
			get
			{
				return m_Model;
			}
			set
			{
				m_Model = value;
			}
		}


		[Display(ResourceType = typeof (DisplayNameResources), Name = "Source")]
		public string Source
		{
			get
			{
				return ViewModelHelper.ShowFirstChars(m_Model.Source.Name);
			}
		}

		[Display(ResourceType = typeof (DisplayNameResources), Name = "Unit")]
		public string Unit
		{
			get
			{
				return ViewModelHelper.ShowFirstChars(m_Model.Unit.Symbol);
			}
		}

		[Display(ResourceType = typeof(DisplayNameResources), Name = "EnergyPrice")]
		public string EnergyPrice
		{
			get
			{
				return ViewModelHelper.ShowFirstChars(m_Model.EnergyPrice.ToString());
			}
		}
	}
}