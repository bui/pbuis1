﻿namespace OpEnMs.EETS.ViewModels.EnergyConsumption
{
  public class EnergyConsumptionEditViewModel
  {
    public Domain.Entities.EnergyConsumption Model { get; set; }
    public EnergyConsumptionDetailsViewModel EnergyConsumptionDetailsViewModel { get; set; }
  }
}