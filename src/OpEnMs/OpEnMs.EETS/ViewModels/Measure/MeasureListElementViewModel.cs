﻿using System.ComponentModel.DataAnnotations;
using OpEnMs.Resources;

namespace OpEnMs.EETS.ViewModels.Measure
{
	public class MeasureListElementViewModel
	{
		private Domain.Entities.Measure m_Model;

		public Domain.Entities.Measure Model
		{
			get
			{
				return m_Model;
			}
			set
			{
				m_Model = value;
			}
		}

	
		[Display(ResourceType = typeof(DisplayNameResources), Name = "Name")]
		public string Name
		{
			get
			{
				return ViewModelHelper.ShowFirstChars(m_Model.Name);
			}
		}

		 [Display(ResourceType = typeof(DisplayNameResources), Name = "Description")]
		public string Description
		{
			get
			{
				return ViewModelHelper.ShowFirstChars(m_Model.Description);
			}
		}

		 [Display(ResourceType = typeof(DisplayNameResources), Name = "Solution")]
		 public string Solution
		 {
			 get
			 {
				 return ViewModelHelper.ShowFirstChars(m_Model.Solution);
			 }
		 }

		[Display(ResourceType = typeof(DisplayNameResources), Name = "Annotation")]
		 public string Annotation
		 {
			 get
			 {
				 return ViewModelHelper.ShowFirstChars(m_Model.Annotation);
			 }
		 }

		[Display(ResourceType = typeof (DisplayNameResources), Name = "State")]
		public string State
		{
			get
			{
				return m_Model.State.ToString();
			}
		}

		[Display(ResourceType = typeof(DisplayNameResources), Name = "EntryDate")]
		public string EntryDate
		{
			get
			{
				if (m_Model.EntryDate != null)
				{
					//return m_Model.EntryDate.ToShortDateString();
					return m_Model.EntryDate.ToString();
				}
				return "";
			}
		}

		
	}
}