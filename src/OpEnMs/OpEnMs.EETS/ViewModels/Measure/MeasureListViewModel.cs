﻿using System.Collections.Generic;
using System.Linq;

namespace OpEnMs.EETS.ViewModels.Measure
{
	public class MeasureListViewModel
	{
		public IList<MeasureListElementViewModel> Measures { get; set; }

		public int Count
		{
			get
			{
				return Measures.Count();
			}
		}
	}
}