#region Licence

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0.html
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// 
// Copyright (c) 2013, HTW Berlin

#endregion

using OpEnMs.EETS.ViewModels.Idea;
using OpEnMs.EETS.ViewModels.IsoMeasure;

namespace OpEnMs.ViewModels.Home
{
	public class HomeViewModel
	{
	    public HomeViewModel()
	    {    
	    }

		public HomeViewModel(IdeaListViewModel ideaListViewModel, IsoMeasureListViewModel isoMeasureListViewModel)
		{
		    IdeaListViewModel = ideaListViewModel;
            IsoMeasureListViewModel = isoMeasureListViewModel;
		}

		public IdeaListViewModel IdeaListViewModel { get; set; }
		public IsoMeasureListViewModel IsoMeasureListViewModel { get; set; }
	}
}