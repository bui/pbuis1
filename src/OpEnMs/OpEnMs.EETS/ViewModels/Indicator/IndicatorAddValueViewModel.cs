﻿#region Licence

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0.html
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// 
// Copyright (c) 2013, HTW Berlin

#endregion

using System;
using OpEnMs.Domain.Entities;

namespace OpEnMs.EETS.ViewModels.Indicator
{
    public class IndicatorAddValueViewModel
    {
        public int IndicatorId { get; set; }
        public String IndicatorName { get; set; }
        public String IndicatorUnitName { get; set; }
        public IndicatorValue IndicatorValue { get; set; }
        public int Year { get; set; }

        public IndicatorAddValueViewModel(Domain.Entities.Indicator indicator)
        {
            IndicatorId = indicator.Id;
            IndicatorName = indicator.Name;
            IndicatorUnitName = indicator.Unit.Name;
            IndicatorValue = new IndicatorValue();
            IndicatorValue.Indicator = indicator;
        }

        public IndicatorAddValueViewModel()
        {
            
        }
    }
}