﻿using System.ComponentModel.DataAnnotations;
using OpEnMs.Domain.Entities.Attributes;
using OpEnMs.Resources;

namespace OpEnMs.EETS.ViewModels.Indicator
{
    public class UnitCreateViewModel
    {
        [Required]
        [PlaceHolder(typeof (PlaceHolderResources), "UnitName")]
        [Display(ResourceType = typeof (DisplayNameResources), Name = "Name")]
        public string Name { get; set; }

        [Required]
        [PlaceHolder(typeof (PlaceHolderResources), "UnitSymbol")]
        [Display(ResourceType = typeof (DisplayNameResources), Name = "Symbol")]
        public string Symbol { get; set; }
    }
}