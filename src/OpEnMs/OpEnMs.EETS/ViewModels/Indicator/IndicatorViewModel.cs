﻿#region Licence

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0.html
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// 
// Copyright (c) 2013, HTW Berlin

#endregion

using System.Collections.Generic;
using System.Web.Mvc;
using Emporer.Unit;
using OpEnMs.Domain.Entities;

namespace OpEnMs.EETS.ViewModels.Indicator
{
    public class IndicatorViewModel
    {
        public Domain.Entities.Indicator Indicator { get; set; }
        public SelectList Units { get; set; }
        public SelectList Topics { get; set; }



        public IndicatorViewModel()
        {
            
        }

        public IndicatorViewModel(Domain.Entities.Indicator indicator, List<Unit> units, List<Topic> topics)
        {
            Indicator = indicator;

            Units = new SelectList(units,
                                   "Id",
                                   "Name",
                                   indicator.Unit.Id);
            Topics = new SelectList(topics,
                                    "Id",
                                    "Name",
                                    indicator.Topic.Id);
        }

        public IndicatorViewModel(List<Unit> units, List<Topic> topics)
        {
            Units = new SelectList(units,
                                   "Id",
                                   "Name",-1);
            Topics = new SelectList(topics,
                                    "Id",
                                    "Name",-1);
            Indicator = new Domain.Entities.Indicator();
        }
    }
}