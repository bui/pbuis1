﻿using System.ComponentModel.DataAnnotations;
using OpEnMs.Resources;

namespace OpEnMs.EETS.ViewModels.Network
{
	public class NetworkListElementViewModel
	{
	  private Domain.Entities.Network m_Model;

	  public Domain.Entities.Network Model
	  {
	    get
	    {
	      return m_Model;
	    }
	    set
	    {
	      m_Model = value;
	    }
	  }

		[Display(ResourceType = typeof(DisplayNameResources), Name = "NumberOfCompanies")]
		public int NumberOfCompanies
		{
			get
			{
				return m_Model.Companies.Count;
			}
		}

		public string Name
		{
			get
			{
				return ViewModelHelper.ShowFirstChars(m_Model.Name);
			}
		}


		public string Description
		{
			get
			{
				return ViewModelHelper.ShowFirstChars(m_Model.Description);
			}
		}


		public string State
		{
			get
			{
				return m_Model.State.ToString();
			}
		}

		[Display(ResourceType = typeof(DisplayNameResources), Name = "Term")]
		public string Term
		{
			get
			{
				return m_Model.StartDate.ToShortDateString() + " bis " + m_Model.EndDate.ToShortDateString();
			}
		}
	}
}