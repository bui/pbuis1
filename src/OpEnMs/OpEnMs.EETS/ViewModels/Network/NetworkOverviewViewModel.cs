﻿using System.Collections.Generic;
using System.Linq;
using OpEnMs.EETS.ViewModels.Company;

namespace OpEnMs.EETS.ViewModels.Network
{
	public class NetworkOverviewViewModel
	{
		public IList<NetworkOverviewElementViewModel> Networks { get; set; }

		public int Count
		{
			get
			{
				return Networks.Count();
			}
		}
	}

	public class NetworkOverviewElementViewModel
	{
		private Domain.Entities.Network m_Model;

		public Domain.Entities.Network Model
		{
			get
			{
				return m_Model;
			}
			set
			{
				m_Model = value;
			}
		}

		public int Count
		{
			get
			{
				return Companies.Count();
			}
		}

		public IList<CompanyListElementViewModel> Companies { get; set; }
	}
}