﻿using System.Collections.Generic;
using System.Linq;

namespace OpEnMs.EETS.ViewModels.Network
{
	public class NetworkListViewModel
	{
		public IList<NetworkListElementViewModel> Networks { get; set; }

		public int Count
		{
			get
			{
				return Networks.Count();
			}
		}
	}
}