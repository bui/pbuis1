﻿using System.Collections.Generic;
using OpEnMs.EETS.ViewModels.Company;

namespace OpEnMs.EETS.ViewModels.Network
{
	public class NetworkCreateViewModel
	{
		public Domain.Entities.Network Model { get; set; }

		public IList<CompanyCheckBoxViewModel> CompanyCheckBoxViewModels { get; set; }
	}
}