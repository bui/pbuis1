﻿#region Licence

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0.html
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// 
// Copyright (c) 2013, HTW Berlin

#endregion

using System.Collections.Generic;
using System.Linq;

namespace OpEnMs.EETS.ViewModels.Idea
{
	public class IdeaListViewModel
	{
		public IList<IdeaListElementViewModel> Ideas { get; set; }

		public int Count
		{
			get
			{
				if (Ideas == null)
				{
					return 0;
				}
				return Ideas.Count();
			}
		}

		public void AddIdeasToViewModel(IEnumerable<Domain.Entities.Idea> ideas)
		{
			if (Ideas == null)
			{
				Ideas = new List<IdeaListElementViewModel>();
			}
			if (ideas != null)
			{
				foreach (var idea in ideas)
				{
					Ideas.Add(new IdeaListElementViewModel{
						                                      Model = idea
					                                      });
				}
			}
		}
	}
}