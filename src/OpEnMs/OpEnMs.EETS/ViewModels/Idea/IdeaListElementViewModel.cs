﻿#region Licence

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0.html
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// 
// Copyright (c) 2013, HTW Berlin

#endregion

using System.ComponentModel.DataAnnotations;


using OpEnMs.Resources;
using System.Linq;
using OpEnMs.Domain.Entities;

namespace OpEnMs.EETS.ViewModels.Idea
{
	public class IdeaListElementViewModel
	{
		public OpEnMs.Domain.Entities.Idea Model { get; set; }

		[Display(ResourceType = typeof (DisplayNameResources), Name = "Name")]
		public string Name
		{
			get
			{
				return ViewModelHelper.ShowFirstChars(Model.Name);
			}
		}

		[Display(ResourceType = typeof (DisplayNameResources), Name = "Description")]
		public string Description
		{
			get
			{
				return ViewModelHelper.ShowFirstChars(Model.Description);
			}
		}

		[Display(ResourceType = typeof (DisplayNameResources), Name = "Solution")]
		public string Solution
		{
			get
			{
				return ViewModelHelper.ShowFirstChars(Model.Solution);
			}
		}

        //TODO: Rename "Employee" as "Creator"
		[Display(ResourceType = typeof (DisplayNameResources), Name = "Creator")]
		public string Employee
		{
			get
			{
				if (Model.Creator != null)
				{
					return string.Format("{0}, {1}",
					                     Model.Creator.SurName,
					                     Model.Creator.GivenName);
				}
				return string.Empty;
			}
		}

        [Display(ResourceType = typeof(DisplayNameResources), Name = "IdeaState")]
        public IdeaState IdeaState
        {
            get;
            set;
        }

        //public string LastState
        //{
        //    get
        //    {
        //        var lastOrDefault = Idea.States.LastOrDefault();
        //        if (lastOrDefault != null)
        //        {
        //            return lastOrDefault.StateName.GetLocalizedName();
        //        }
        //        return Resources.Resources.NotAvailable;
        //    }
        //}
	}
}