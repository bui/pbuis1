﻿#region Licence

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0.html
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// 
// Copyright (c) 2013, HTW Berlin

#endregion

using System;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using OpEnMs.Domain.Entities;
using OpEnMs.Resources;

namespace OpEnMs.EETS.ViewModels.Idea
{
	public class IdeaToIsoMeasureViewModel
	{
		public virtual Domain.Entities.Idea Model { get; set; }


		//Attributes needed for a new activity
		[DataType(DataType.Date)]
		[Display(ResourceType = typeof (DisplayNameResources), Name = "DueDate")]
		public DateTime DueDate { get; set; }

		[HiddenInput(DisplayValue = false)]
		[Display(ResourceType = typeof (DisplayNameResources), Name = "Priority")]
		public Priority Priority { get; set; }

		[Display(ResourceType = typeof (DisplayNameResources), Name = "EmployeeInCharge")]
		//[PlaceHolder(typeof (DisplayNameResources), "EmployeeInCharge")]
		[Required(ErrorMessageResourceName = "EmployeeInChargeRequired", ErrorMessageResourceType = typeof (ErrorMessageResources))]
		public String EmployeeInChargeString { get; set; }

		//Old idea-attributes that can be edited when transformed into activity-attributes
		[Required(ErrorMessageResourceName = "NameIsRequired", ErrorMessageResourceType = typeof (ErrorMessageResources))]
		[StringLength(40, ErrorMessageResourceName = "NameNotLongerThan40Characters", ErrorMessageResourceType = typeof (ErrorMessageResources))]
		[Display(ResourceType = typeof (DisplayNameResources), Name = "Name")]
		//[PlaceHolder(typeof (DisplayNameResources), "Name")]
		public string Name { get; set; }

		[DataType(DataType.MultilineText)]
		[Display(ResourceType = typeof (DisplayNameResources), Name = "Description")]
		[Required(ErrorMessageResourceName = "DescriptionIsRequired", ErrorMessageResourceType = typeof (ErrorMessageResources))]
		//[PlaceHolder(typeof (DisplayNameResources), "Description")]
		public string Description { get; set; }

		[DataType(DataType.MultilineText)]
		[Display(ResourceType = typeof (DisplayNameResources), Name = "Solution")]
		[Required(ErrorMessageResourceName = "SolutionIsRequired", ErrorMessageResourceType = typeof (ErrorMessageResources))]
		//[PlaceHolder(typeof (DisplayNameResources), "Solution")]
		public string Solution { get; set; }

		[Display(ResourceType = typeof (DisplayNameResources), Name = "Valuation")]
		public virtual Domain.Entities.Valuation Valuation { get; set; }
	}
}