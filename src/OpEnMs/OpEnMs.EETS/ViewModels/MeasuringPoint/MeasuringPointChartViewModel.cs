﻿using System.Collections.Generic;
using OpEnMs.Domain.Entities;
using OpEnMs.EnergyChart;

namespace OpEnMs.EETS.ViewModels.MeasuringPoint
{
	public class MeasuringPointChartViewModel: IChartable
	{
		private Domain.Entities.MeasuringPoint m_Model;

		public Domain.Entities.MeasuringPoint Model
		{
			get
			{
				return m_Model;
			}
			set
			{
				m_Model = value;
			}
		}

		public string Name
		{
			get
			{
				return m_Model.Name;
			}
		}

		public ICollection<Domain.Entities.EnergyConsumption> EnergyConsumptions
		{
			get
			{
				return m_Model.EnergyConsumptions;
			}
		}
	}
}