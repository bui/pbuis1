﻿using System.ComponentModel.DataAnnotations;
using OpEnMs.Resources;

namespace OpEnMs.EETS.ViewModels.MeasuringPoint
{
	public class MeasuringPointListElementViewModel
	{
		private Domain.Entities.MeasuringPoint m_Model;

		public Domain.Entities.MeasuringPoint Model
		{
			get
			{
				return m_Model;
			}
			set
			{
				m_Model = value;
			}
		}


		[Display(ResourceType = typeof (DisplayNameResources), Name = "Name")]
		public string Name
		{
			get
			{
				return ViewModelHelper.ShowFirstChars(m_Model.Name);
			}
		}


		[Display(ResourceType = typeof (DisplayNameResources), Name = "MeasuringPointType")]
		public string Type
		{
			get
			{
				return m_Model.Type.ToString();
			}
		}
	}
}