﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Emporer.Unit;
using OpEnMs.Domain.Entities;
using OpEnMs.Resources;

namespace OpEnMs.EETS.ViewModels.MeasuringPoint
{
	public class MeasuringPointCreateViewModel
	{
		public Domain.Entities.MeasuringPoint Model { get; set; }
		public int SourceId { get; set; }

		[Display(ResourceType = typeof (DisplayNameResources), Name = "Source")]
		public virtual IEnumerable<Source> Sources { get; set; }

		public int UnitId { get; set; }

		[Display(ResourceType = typeof (DisplayNameResources), Name = "Unit")]
		public virtual IEnumerable<Unit> Units { get; set; }
	}
}