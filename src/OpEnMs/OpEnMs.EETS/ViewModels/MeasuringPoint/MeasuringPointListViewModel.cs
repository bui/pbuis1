﻿using System.Collections.Generic;
using System.Linq;

namespace OpEnMs.EETS.ViewModels.MeasuringPoint
{
	public class MeasuringPointListViewModel
	{
		public IList<MeasuringPointListElementViewModel> MeasuringPoints { get; set; }

		public int Count
		{
			get
			{
				return MeasuringPoints.Count();
			}
		}
	}
}