﻿namespace OpEnMs.EETS.ViewModels.UserProfile
{
	public class UserProfileCheckBoxViewModel
	{
		public Domain.Entities.UserProfile UserProfile { get; set; }

		public string Name
		{
			get
			{
				return string.Format("{0}, {1}",
				                     UserProfile.SurName,
				                     UserProfile.GivenName);
			}
		}

		public string Email
		{
			get
			{
				return UserProfile.Email;
			}
		}

		public string Phone
		{
			get
			{
				return UserProfile.Phone;
			}
		}

		public bool IsChecked { get; set; }
	}
}