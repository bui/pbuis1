using System.Collections.Generic;

namespace OpEnMs.EETS.ViewModels.UserProfile
{
  public class UserProfileListViewModel
  {
    public IList<UserProfileListElementViewModel> UserProfiles { get; set; }

    public int Count
    {
      get
      {
        return UserProfiles.Count;
      }
    }
  }
}