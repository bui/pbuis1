using System.Collections.Generic;
using System.Web.Mvc;

namespace OpEnMs.EETS.ViewModels.UserProfile
{
  public class UserProfileEditViewModel
  {
    public Domain.Entities.UserProfile Model
    {
      get;
      set;
    }
    public IEnumerable<SelectListItem> Companies
    {
      get;
      set;
    }

    public int CompanyId { get; set; } 
    public string Role { get; set; } 
    
    public IEnumerable<SelectListItem> Roles
    {
      get;
      set;
    }
  }
}