using System.ComponentModel.DataAnnotations;
using OpEnMs.Resources;

namespace OpEnMs.EETS.ViewModels.UserProfile
{
  public class UserProfileListElementViewModel
  {
    public Domain.Entities.UserProfile Model { get; set; }

    [Display(ResourceType = typeof (DisplayNameResources), Name = "Name")]
    public string UserName
    {
      get
      {
        return ViewModelHelper.ShowFirstChars(Model.UserName);
      }
    }

    [Display(ResourceType = typeof (DisplayNameResources), Name = "Company")]
    public string Company { get; set; }
  }
}