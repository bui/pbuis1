﻿using System.Collections.Generic;
using System.Linq;

namespace OpEnMs.EETS.ViewModels.IsoMeasure
{
	public class IsoMeasureListViewModel
	{
		public IList<IsoMeasureListElementViewModel> IsoMeasures { get; set; }

		public int Count
		{
			get
			{
				return IsoMeasures.Count();
			}
		}
	}
}