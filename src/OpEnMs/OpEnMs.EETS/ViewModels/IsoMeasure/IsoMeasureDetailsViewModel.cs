﻿using OpEnMs.EETS.ViewModels.Idea;
using OpEnMs.EETS.ViewModels.Valuation;

namespace OpEnMs.EETS.ViewModels.IsoMeasure
{
	public class IsoMeasureDetailsViewModel
	{
		public Domain.Entities.IsoMeasure Model { get; set; }


		//public IEnumerable<ActivityStateListViewModel> ActivityStates
		//{
		//	get
		//	{
		//		foreach (ActivityState activityState in Activity.ActivityStates)
		//		{
		//			yield return new ActivityStateListViewModel
		//			{
		//				ActivityState = activityState
		//			};
		//		}
		//	}
		//}

		public IdeaListViewModel Ideas
		{
			get
			{
				var viewModel = new IdeaListViewModel();
				if (Model.Ideas != null && Model.Ideas.Count != 0)
				{
					viewModel.AddIdeasToViewModel(Model.Ideas);
				}
				return viewModel;
			}
		}


		public string CurrentState
		{
			get
			{
				return Model.State.GetLocalizedName();
			}
		}

		public string Prority
		{
			get
			{
				return Model.Priority.GetLocalizedName();
			}
		}

		public string EmployeeInChargeName
		{
			get
			{
				var employee = Model.EmployeeInCharge;
				if (employee != null)
				{
					return string.Format(employee.SurName + ", " + employee.GivenName);
				}
				return null;
			}
		}

		public string CreatorName
		{
			get
			{
				var employee = Model.Creator;
				if (employee != null)
				{
					return string.Format(employee.SurName + ", " + employee.GivenName);
				}
				return null;
			}
		}

		public ValuationDetailsViewModel Valuation
		{
			get
			{
				var viewModel = new ValuationDetailsViewModel{
					                                             Model = Model.Valuation
				                                             };
				return viewModel;
			}
		}
	}
}