﻿using System.ComponentModel.DataAnnotations;
using OpEnMs.Domain.Entities;
using OpEnMs.EETS.ViewModels.IsoMeasure;
using OpEnMs.Resources;

namespace OpEnMs.EETS.ViewModels.IsoMeasure
{
	public class IsoMeasureCreateViewModel
	{
		public Domain.Entities.IsoMeasure Model { get; set; }

		public Domain.Entities.Valuation Valuation { get; set; }

		[Display(ResourceType = typeof(DisplayNameResources), Name = "EmployeeInChargeString")]
		[ToolTip(typeof(ToolTipResources), "Measure_EmployeeInCharge")]
		public string EmployeeInChargeString { get; set; }
	}
}