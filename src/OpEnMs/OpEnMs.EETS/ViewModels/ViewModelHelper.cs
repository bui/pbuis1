﻿#region Licence

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0.html
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// 
// Copyright (c) 2013, HTW Berlin

#endregion

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace OpEnMs.EETS.ViewModels
{
	public static class 
       
        ViewModelHelper
	{
		public static string ShowFirstChars(string fullString)
		{
			if (fullString == null)
			{
				return null;
			}

			string substring;
			const int stringLength = 35;

			if (fullString.Length > stringLength)
			{
				substring = string.Format("{0}...",
																	fullString.Substring(0,
																											 stringLength));
			}
			else
			{
				substring = fullString;
			}
			return substring;
		}

		// Splits strings that are seperated by ","
		// legacyString = String that should be splitted
		// arguement = defines the part of the string thats supposed to be returned
		public static string SplitStringByArgument(string legacyString, int argument)
		{
			string[] split;
			try
			{
				split = legacyString.Split(',');

				return split[argument];
			}
			catch
			{
				return null;
			}
		}

		public static string ConvertStringArrayToString(IEnumerable<string> array)
		{
			var builder = new StringBuilder();
			builder.Append("[");
			foreach (string value in array)
			{
				builder.Append(String.Format("&quot;{0}&quot;,",
																		 value));
			}
			builder.Append("]");

			return HttpUtility.HtmlDecode(builder.ToString());
		}

		public static string ConvertStringArrayToString(string[,] array)
		{
			var builder = new StringBuilder();

			for (int i = 0; i < array.GetLength(0) - 1; i++)
			{
				//Adding String in this format ["Parent", 10, "Child"], for Sankey
				builder.Append("[");
				builder.Append(String.Format("&quot;{0}&quot;, {1}, &quot;{2}&quot;",
																		 array[i,
																					 0],
																		 array[i,
																					 1],
																		 array[i,
																					 2]));
				builder.Append("],\n");
			}
			return HttpUtility.HtmlDecode(builder.ToString());
		}

		public static string ConvertStringArrayToString(string[] array)
		{
			if (array.Length.Equals(0))
			{
				return "";
			}
			var builder = new StringBuilder();
			int i = 0;

			while (i <= array.Length)
			{
				builder.Append(array[i]);
				i++;
				if (i == array.Length)
				{
					break;
				}
				builder.Append(", ");
			}


			return HttpUtility.HtmlDecode(builder.ToString());
		}

		// ToDo:Think about a solution to save a user ajustable color in db 
		public static string GetColorHexBySouceName(string sourceName)
		{
			Color color = Color.Black;
			switch (sourceName)
			{
				case "Strom":
					color = Color.CornflowerBlue;
					break;

				case "Erdgas":
					color = Color.Gold;
					break;

				case "Flüssiggas":
					color = Color.DarkOrange;
					break;

				case "Heizöl":
					color = Color.Gray;
					break;

				case "Pellets":
					color = Color.DarkGreen;
					break;

				case "Hackschnitzel":
					color = Color.Brown;
					break;
			}

			return ColorTranslator.ToHtml(color);
		}

		public static string GetLocalizedName(this Enum @enum)
		{
			//TODO Check = 0 - condition
			if (@enum == null || @enum.ToString() == "0")
			{
				return null;
			}
			string description = @enum.ToString();
			FieldInfo fieldInfo = @enum.GetType().
																	GetField(description);

			var attributes = (DisplayAttribute[]) fieldInfo.GetCustomAttributes(typeof(DisplayAttribute),
																																					false);
			if (attributes.Any())
			{
				description = attributes[0].GetDescription();
			}
			return description;
		}

		public static IEnumerable<SelectListItem> ToSelectList(this Enum enumValue, bool selectMe = true)
		{
			return (from Enum e in Enum.GetValues(enumValue.GetType())
							select new SelectListItem
							{
								Selected = (selectMe && e.Equals(enumValue)),
								Text = e.GetLocalizedName(),
								Value = e.ToString()
							}).ToList();
		}

		public static string GetSurNameGivenName(Domain.Entities.UserProfile user)
		{
			var name = string.Format("{0}, {1}",
															 user.SurName,
															 user.GivenName);
			return name;
		}

		public static string GetGivenNameSurName(Domain.Entities.UserProfile user)
		{
			var name = string.Format("{0} {1}",
															 user.GivenName,
															 user.SurName);
			return name;
		}

        public static string DisplayAttribute<TEnum>(this TEnum enumValue) where TEnum : struct
        {
            //You can't use a type constraints on the special class Enum. So I use this workaround
            if (!typeof(TEnum).IsEnum)
                throw new ArgumentException("TEnum must be of type System.Enum");

            Type type = typeof(TEnum);
            MemberInfo[] memberInfo = type.GetMember(enumValue.ToString());
            if (memberInfo != null && memberInfo.Length > 0)
            {
                object[] attrs = memberInfo[0].GetCustomAttributes(typeof(DisplayAttribute), false);
                if (attrs != null && attrs.Length > 0)
                    return ((DisplayAttribute)attrs[0]).GetName();
            }
            return enumValue.ToString();
        }
	}
}