﻿using System.ComponentModel.DataAnnotations;
using System.Linq;
using OpEnMs.Resources;

namespace OpEnMs.EETS.ViewModels.Valuation
{
	public class ValuationDetailsViewModel
	{
		public Domain.Entities.Valuation Model { get; set; }

		[Display(ResourceType = typeof(DisplayNameResources), Name = "InterestRate")]
		public string InterestRate
		{
			get
			{
				return string.Format("{0} %",
					Model.InterestRate);
			}
		}

		[Display(ResourceType = typeof(DisplayNameResources), Name = "Investment")]
		public string Investment
		{
			get
			{
				return string.Format("{0:C}",
					Model.Investment);
			}
		}

		[Display(ResourceType = typeof(DisplayNameResources), Name = "Lifetime")]
		public string Lifetime
		{
			get
			{
				return string.Format("{0} {1}",
					Model.Lifetime,
					DisplayNameResources.Year);
			}
		}

		[Display(ResourceType = typeof(DisplayNameResources), Name = "MonetarySavings")]
		public string MonetarySavings
		{
			get
			{
				return string.Format("{0}",Model.MonetarySavings);
			}
		}

		[Display(ResourceType = typeof(DisplayNameResources), Name = "EnergeticSavings")]
		public string EnergeticSavings
		{
			get
			{
				//Todo: Unit should never be null!
				if (Model.Unit != null)
				{
					return string.Format("{0:0.###} {1}",
						Model.EnergeticSavings,
						Model.Unit);
				}
				return string.Format("{0:0.###} kWh",
					Model.EnergeticSavings);
			}
		}


		[Display(ResourceType = typeof (DisplayNameResources), Name = "StatisticAmortization")]
		public string StatisticAmortization
		{
			get
			{
				if (GetStatisticAmortization() == 0)
				{
					return Resources.Resources.NotEnoughData;
				}
				return string.Format("{0:0.##} {1}",
					Model.StatisticAmortization,
					DisplayNameResources.Year);
			}
		}

		[Display(ResourceType = typeof(DisplayNameResources), Name = "DynamicAmortization")]
		public string DynamicAmortization
		{
			get
			{
				if (GetStatisticAmortization() == 0)
				{
					return Resources.Resources.NotEnoughData;
				}
				return string.Format("{0:0.##} {1}",
					Model.DynamicAmortization,
					DisplayNameResources.Euro);
			}
		}

		private decimal? GetStatisticAmortization()
		{
			if (Model.MonetarySavings == 0)
			{
				return 0;
			}

			return Model.Investment / Model.MonetarySavings;
		}
	}
}