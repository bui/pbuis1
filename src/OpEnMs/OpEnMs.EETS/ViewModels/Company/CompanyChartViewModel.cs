﻿using System.Collections.Generic;
using OpEnMs.EnergyChart;

namespace OpEnMs.EETS.ViewModels.Company
{
	public class CompanyChartViewModel: IChartable
	{
		private Domain.Entities.Company m_Model;

		public Domain.Entities.Company Model
		{
			get
			{
				return m_Model;
			}
			set
			{
				m_Model = value;
			}
		}

		public string Name
		{
			get
			{
				return m_Model.Name;
			}
		}

		public ICollection<Domain.Entities.EnergyConsumption> EnergyConsumptions
		{
			get
			{
				return m_Model.EnergyConsumptions;
			}
		}
	}
}