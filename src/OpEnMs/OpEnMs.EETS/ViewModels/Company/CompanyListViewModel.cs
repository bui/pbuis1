﻿using System.Collections.Generic;
using System.Linq;

namespace OpEnMs.EETS.ViewModels.Company
{
	public class CompanyListViewModel
	{
		public IList<CompanyListElementViewModel> Companies { get; set; }

		public int Count
		{
			get
			{
				return Companies.Count();
			}
		}
	}
}