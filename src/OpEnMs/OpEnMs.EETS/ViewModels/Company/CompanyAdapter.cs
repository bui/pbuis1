﻿using System.Collections.Generic;
using OpEnMs.EnergyChart;

namespace OpEnMs.EETS.ViewModels.Company
{
	public class CompanyAdapter: IChartable
	{
		private readonly Domain.Entities.Company m_Company;

		public CompanyAdapter(Domain.Entities.Company company, string parameter)
		{
			m_Company = company;
			Name = company.Name + parameter;
		}


		public string Name { get; private set; }

		public ICollection<Domain.Entities.EnergyConsumption> EnergyConsumptions
		{
			get
			{
				return m_Company.EnergyConsumptions;
			}
		}
	}
}