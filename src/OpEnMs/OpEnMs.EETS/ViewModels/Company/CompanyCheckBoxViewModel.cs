﻿using System.ComponentModel.DataAnnotations;
using OpEnMs.Resources;

namespace OpEnMs.EETS.ViewModels.Company
{
	public class CompanyCheckBoxViewModel
	{
		[Display(ResourceType = typeof(DisplayNameResources), Name = "IsChecked")]
		public bool IsChecked { get; set; }
		[Display(ResourceType = typeof(DisplayNameResources), Name = "Name")]
		public string Name { get; set; }
		public int CompanyId { get; set; }
	}
}