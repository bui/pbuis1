﻿using OpEnMs.EETS.ViewModels.EnergyConsumption;
using OpEnMs.EETS.ViewModels.Measure;
using OpEnMs.EETS.ViewModels.MeasuringPoint;

namespace OpEnMs.EETS.ViewModels.Company
{
	public class CompanyEditViewModel
	{
		public Domain.Entities.Company Model { get; set; }
		//public IList<UserProfileCheckBoxViewModel> UserCheckBoxViewModels { get; set; }
		public MeasureListViewModel MeasureListViewModel { get; set; }
		public MeasuringPointListViewModel MeasuringPointListViewModel { get; set; }
		public EnergyConsumptionListViewModel EnergyConsumptionListViewModel { get; set; }
	}
}