﻿using DotNet.Highcharts;

namespace OpEnMs.EETS.ViewModels.Company
{
	public class CompanyDetailsViewModel
	{
		public Highcharts Linechart { get; set; }
		public Highcharts ColumnChart { get; set; }
		public Highcharts PieChart { get; set; }
	}
}