﻿using System.ComponentModel.DataAnnotations;
using OpEnMs.Resources;

namespace OpEnMs.EETS.ViewModels.Company
{
	public class CompanyListElementViewModel
	{
		private Domain.Entities.Company m_Model;

		public Domain.Entities.Company Model
		{
			get
			{
				return m_Model;
			}
			set
			{
				m_Model = value;
			}
		}

		[Display(ResourceType = typeof(DisplayNameResources), Name = "Name")]
		public string Name
		{
			get
			{
				return ViewModelHelper.ShowFirstChars(m_Model.Name);
			}
		}

		[Display(ResourceType = typeof(DisplayNameResources), Name = "Street")]
		public string Street
		{
			get
			{
				return ViewModelHelper.ShowFirstChars(m_Model.Street);
			}
		}

		[Display(ResourceType = typeof(DisplayNameResources), Name = "City")]
		public string PostalCodeAndCity
		{
			get
			{
				return string.Format("{0} - {1}",
				                     m_Model.PostalCode,
				                     m_Model.City);
			}
		}

		[Display(ResourceType = typeof (DisplayNameResources), Name = "Website")]
		public string Website
		{
			get
			{
				return ViewModelHelper.ShowFirstChars(m_Model.Website);
			}
		}
	}
}