﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web.Mvc;
using OpEnMs.EETS.ViewModels.Company;

namespace OpEnMs.EETS.ViewModels
{
  public static class ExtensionMethods
  {
    public static ICollection<Domain.Entities.Company> ToCompany(this IEnumerable<CompanyCheckBoxViewModel> companyCheckBoxViewModels, IEnumerable<Domain.Entities.Company> allCompanies)
    {
      ICollection<Domain.Entities.Company> companies = new Collection<Domain.Entities.Company>();
      if (companyCheckBoxViewModels != null)
      {
        foreach (var companyCheckBoxViewModel in companyCheckBoxViewModels)
        {
          if (companyCheckBoxViewModel.IsChecked)
          {
            var company = allCompanies.FirstOrDefault(c => c.Id == companyCheckBoxViewModel.CompanyId);
            companies.Add(company);
          }
        }
      }
      return companies;
    }

    public static IEnumerable<SelectListItem> ToSelectListItems(this IEnumerable<Domain.Entities.Company> companies, int selectedId)
    {
      return companies.OrderBy(c => c.Name).
        Select(c => new SelectListItem{
                                        Selected = (c.Id == selectedId),
                                        Text = c.Name,
                                        Value = c.Id.ToString()
                                      });
    }

    public static IEnumerable<SelectListItem> ToSelectListItems(this string[] roles, string selectedRole)
    {
      return roles.Select(c => new SelectListItem{
                                                   Selected = (c.ToString() == selectedRole),
                                                   Text = c.ToString(),
                                                   Value = c.ToString()
                                                 });
    }
  }
}